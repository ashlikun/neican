package com.hanbang.cxgz_2.appliction;

/**
 * 全局配置
 * Created by 阳 on 2016/6/5.
 */
public class Global {

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 默认帐号与密码
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static final String DEFAULT_PHONE = "1390154";
    public static final String DEFAULT_PASSWORD = "123456";


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 发布的类型
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static String ISSUE_SHOW_FIELD = "秀场";
    public static String ISSUE_ESOTERIC_K = "开放秘籍";
    public static String ISSUE_ESOTERIC_D = "买断秘籍";
    public static String ISSUE_STUDY_CLASSROOM = "学习课堂";
    public static String ISSUE_DEMAND_REWARD = "悬赏需求";


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 文本长度
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //价格长度
    public static int TEXT_LENGTH_PRICE = 6;
    //名称长度
    public static int TEXT_LENGTH_NAME = 12;
    //地址长度
    public static int TEXT_LENGTH_ADDRESS = 30;
    //发布的内容长度
    public static int TEXT_LENGTH_CONTENT = 500;


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 广播
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //评论—秘籍详情
    public static final String COMMENT_ESOTERIC = "COMMENT_ESOTERIC";
    //评论—热点资讯详情
    public static final String COMMENT_INFORMATION = "COMMENT_INFORMATION";
    //评论—学习课堂
    public static final String COMMENT_CLASSROOM = "COMMENT_CLASSROOM";

    //点赞—秘籍详情
    public static final String DIAN_ZAN_ESOTERIC = "DIAN_ZAN_ESOTERIC";
    //点赞—热点资讯详情
    public static final String DIAN_ZAN_INFORMATION = "DIAN_ZAN_INFORMATION";
    //点赞—学习课堂
    public static final String DIAN_ZAN_CLASSROOM = "DIAN_ZAN_CLASSROOM";

    //用户信息改变的时候发的广播
    public static final String LOGIN_BROADCAST = "LOGIN_BROADCAST";
    //分享的广播
    public static final String SHARED = "SHARED";


}
