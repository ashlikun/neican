package com.hanbang.cxgz_2.appliction;

import android.app.Activity;
import android.app.Application;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;

import com.hanbang.cxgz_2.BuildConfig;
import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.utils.db.LiteOrmUtil;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.http.download.DownloadManager;
import com.hanbang.cxgz_2.view.widget.empty_layout.LoadingAndRetryManager;

import java.io.File;

import butterknife.ButterKnife;
import cn.sharesdk.framework.ShareSDK;
import me.drakeet.library.CrashWoodpecker;

public class MyApplication extends Application {
    public static Application myApp;
    public NetWorkType netWorkType = NetWorkType.NULL;
    public static String appCachePath;
    public static String appFilePath;
    public static String appSDCachePath;
    public static String appSDFilePath;

    public enum NetWorkType {
        WIFI, ETHERNET, MOBILE, NetWorkType, NULL
    }


    @Override
    public void onCreate() {
        super.onCreate();
        DownloadManager.init(this);
        LiteOrmUtil.init(this);
        ButterKnife.setDebug(BuildConfig.DEBUG);
        myApp = this;
        GlideUtils.init(myApp);
        CrashWoodpecker.init(this);
        //分享的初始化
        ShareSDK.initSDK(this);


        new NetWorkBroadcastReceiver(this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        //typeface = Typeface.createFromAsset(getAssets(), "fzlt.ttf");
        appCachePath = getCacheDir().getPath();
        appFilePath = getFilesDir().getPath();
        appSDCachePath = Environment.getExternalStorageDirectory().getPath() + "/neiCan/cache";
        File file = new File(appSDCachePath);
        appSDFilePath = Environment.getExternalStorageDirectory().getPath() + "/neiCan/file";
        File file2 = new File(appSDFilePath);
        if (file2.exists() || file2.mkdirs())
            ;
        File fff = new File(appSDFilePath);
        if (fff.exists() || fff.mkdirs())
            ;

        LoadingAndRetryManager.BASE_EMPTY_LAYOUT_ID = R.layout.base_load_empty;
        LoadingAndRetryManager.BASE_RETRY_LAYOUT_ID = R.layout.base_load_retry;
        LoadingAndRetryManager.BASE_LOADING_LAYOUT_ID = R.layout.base_load_loading;

        registerActivityLifecycleCallbacks(activityLifecycleCallbacks);


    }


    public void onTerminate() {
        super.onTerminate();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    ActivityLifecycleCallbacks activityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            ActivityManager.getInstance().pushActivity(activity);
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            ActivityManager.getInstance().exitActivity(activity);
        }
    };


}
