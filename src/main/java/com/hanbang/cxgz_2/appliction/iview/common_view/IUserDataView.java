package com.hanbang.cxgz_2.appliction.iview.common_view;

import com.hanbang.cxgz_2.mode.javabean.base.UserData;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 17:52
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：获取个人资料
 */

public interface IUserDataView {

    void getUserData(UserData data);
}
