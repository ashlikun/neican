package com.hanbang.cxgz_2.appliction.iview.common_view;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/20 15:44
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：点赞
 */

public interface IDianZanView {

    void ianZan(int id);
}
