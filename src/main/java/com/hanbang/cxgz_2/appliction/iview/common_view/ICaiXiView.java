package com.hanbang.cxgz_2.appliction.iview.common_view;

import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;

import java.util.ArrayList;

/**
 * Created by yang on 2016/9/6.
 */

public interface ICaiXiView {
    /**
     * @param datas 菜系列表
     */
    void getCaiXiData(ArrayList<CaiXiData> datas);
}
