package com.hanbang.cxgz_2.appliction.iview.common_view;

import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;

import java.util.ArrayList;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 19:41
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：职位
 */

public interface IJobView {

    void getJobData(ArrayList<CaiXiData> datas);
}
