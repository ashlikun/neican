package com.hanbang.cxgz_2.appliction.iview.common_view;

import com.hanbang.cxgz_2.mode.httpresponse.other.CityHttpResponse;

/**
 * Created by yang on 2016/8/27.
 */

public interface ICityDataView   {

    void getCityDatas(CityHttpResponse datas);

}
