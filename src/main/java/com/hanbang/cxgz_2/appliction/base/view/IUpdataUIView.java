package com.hanbang.cxgz_2.appliction.base.view;

import com.hanbang.cxgz_2.appliction.iview.common_view.IProgressView;

/**
 * Created by yang on 2016/8/29.
 */

public interface IUpdataUIView<T> extends BaseView, IProgressView {
    void upDataUi(T datas);
}
