package com.hanbang.cxgz_2.appliction.base.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.hanbang.cxgz_2.appliction.ActivityManager;
import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.utils.ui.DrawableUtils;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.activity.AboutMeActivity;
import com.hanbang.cxgz_2.view.chanpin.activity.ChanpingActivity;
import com.hanbang.cxgz_2.view.home.activity.HomeActivity;
import com.hanbang.cxgz_2.view.jianghu.activity.JianghuActivity;
import com.hanbang.cxgz_2.view.jieyou.activity.JieyouActivity;
import com.hanbang.cxgz_2.R;

import butterknife.BindView;

/**
 * Created by Administrator on 2016/8/15.
 */

public abstract class BaseMainActivity<V extends BaseView, T extends BasePresenter<V>> extends BaseMvpActivity<V, T> implements AHBottomNavigation.OnTabSelectedListener {
    @BindView(R.id.bottom_navigation_bar)
    AHBottomNavigation ahBottomNavigation;
    private long exitTime = 0;
    private static String broadFlag = "Buttom_BroadFlag";
    private MyBroadcastReceiver broadcastReceiver = new MyBroadcastReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter intentFilter = new IntentFilter(broadFlag);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void initView() {
        ahBottomNavigation.setAccentColor(getResources().getColor(R.color.main_color));

        ahBottomNavigation.addItem(new AHBottomNavigationItem(getResources().getString(R.string.bottom_home), DrawableUtils.getStateSelectDrawable(R.mipmap.logotab_home_on, R.mipmap.logotab_home)));
        ahBottomNavigation.addItem(new AHBottomNavigationItem(getResources().getString(R.string.bottom_jianghu), DrawableUtils.getStateSelectDrawable(R.mipmap.logotab_jianghu_on, R.mipmap.logotab_jianghu)));
        ahBottomNavigation.addItem(new AHBottomNavigationItem(getResources().getString(R.string.bottom_jieyou), DrawableUtils.getStateSelectDrawable(R.mipmap.logotab_jieyouproduct_on, R.mipmap.logotab_jieyouproduct)));
        ahBottomNavigation.addItem(new AHBottomNavigationItem(getResources().getString(R.string.bottom_chanping), DrawableUtils.getStateSelectDrawable(R.mipmap.logotab_product_on, R.mipmap.logotab_product)));
        ahBottomNavigation.addItem(new AHBottomNavigationItem(getResources().getString(R.string.bottom_my), DrawableUtils.getStateSelectDrawable(R.mipmap.logotab_my_on, R.mipmap.logotab_my)));

        ahBottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.main_black));
        ahBottomNavigation.setCurrentItem(currentItem(), false);
        ahBottomNavigation.setForceTitlesDisplay(true);
        ahBottomNavigation.setOnTabSelectedListener(this);
        ahBottomNavigation.setBehaviorTranslationEnabled(false);

    }

    @Override
    public void parseIntent(Intent intent) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        ahBottomNavigation.setCurrentItem(currentItem(), false);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public abstract int currentItem();

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        if (position != currentItem()) {
            Intent intent = new Intent(broadFlag);
            intent.putExtra("position", position);
            sendBroadcast(intent);
            if (position == 0) {
                HomeActivity.startUI(this);
            } else if (position == 1) {
                JianghuActivity.startUI(this);
            } else if (position == 2) {
                JieyouActivity.startUI(this,0);
            } else if (position == 3) {
                ChanpingActivity.startUI(this);
            } else if (position == 4) {
                AboutMeActivity.startUI(this);
            }
        }
        return true;
    }

    private class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ahBottomNavigation.setCurrentItem(intent.getIntExtra("position", 0), false);
        }
    }

    @Override
    public void onBackPressed() {
        isExitApplication(this);
    }


    public void isExitApplication(Context context) {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            SnackbarUtil.showLong(this, "再按一次退出程序", SnackbarUtil.Info).show();
            exitTime = System.currentTimeMillis();
        } else {
            // 退出
            ActivityManager.getInstance().exitAllActivity();
        }
    }
}
