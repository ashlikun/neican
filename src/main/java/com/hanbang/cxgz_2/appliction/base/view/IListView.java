package com.hanbang.cxgz_2.appliction.base.view;

import com.hanbang.cxgz_2.appliction.base.presenter.ListPresenter;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IListView<T> extends BaseListView {
    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:01
     * <p>
     * 方法功能：更新界面
     */

    void upDataUi(List<T> datas);

    ListPresenter.ApiType getApiType();
}
