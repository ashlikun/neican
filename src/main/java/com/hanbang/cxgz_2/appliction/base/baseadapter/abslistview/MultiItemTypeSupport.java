package com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview;

public interface MultiItemTypeSupport<T>
{
	int getLayoutId(int position, T t);

	int getViewTypeCount();

	int getItemViewType(int position, T t);
}