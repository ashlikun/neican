package com.hanbang.cxgz_2.appliction.base.presenter;

import com.hanbang.cxgz_2.appliction.base.view.IListView;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class ListPresenter<T> extends BasePresenter<IListView<T>> {
    //请求的Http方法
    public enum ApiType {
        GetHomeImg
    }


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setShowLoadding(false).setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<T>>>(buider) {
            @Override
            public void onNext(HttpResult<List<T>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<T>) mvpView.getValidData(result.getList()));
                }
            }
        };

        switch (mvpView.getApiType()) {
            case GetHomeImg:
                HttpRequest.GeRenBackgroundAll(httpCallBack);
                break;

        }
    }


}
