package com.hanbang.cxgz_2.appliction.base.view.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.BaseAdapter;

import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;

import java.util.Collection;

import butterknife.BindView;

import static com.hanbang.cxgz_2.R.id.switchRoot;

/**
 * Created by Administrator on 2016/8/8.
 */

public abstract class BaseListFragment<V extends BaseView, T extends BasePresenter<V>> extends BaseMvpFragment<V, T>
        implements ListSwipeView.ListSwipeViewListener {
    @BindView(switchRoot)
    protected ListSwipeView listSwipeView;

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.getSwipeRefreshLayout();
    }

    public StatusChangListener getStatusChangListener() {
        return listSwipeView.getStatusChangListener();
    }

    @Override
    public void initView() {
        Object adapter = getAdapter();
        if (adapter instanceof CommonAdapter) {

            listSwipeView.getRecyclerView().addItemDecoration(
                    getItemDecoration());
            listSwipeView.getRecyclerView().setLayoutManager(getLayoutManager());
            listSwipeView.setAdapter((CommonAdapter) adapter);
        } else if (adapter instanceof BaseAdapter) {
            listSwipeView.setAdapter((BaseAdapter) adapter);
        }
        listSwipeView.setOnRefreshListener(this);
        listSwipeView.setOnLoaddingListener(this);


    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getContext());
    }

    public abstract Object getAdapter();

    public abstract RecyclerView.ItemDecoration getItemDecoration();

    /**
     * 获取分页的有效数据
     */
    public <T> Collection<T> getValidData(Collection<T> c) {
        return listSwipeView.getPagingHelp().getValidData(c);
    }

    public void clearPagingData() {
        listSwipeView.getPagingHelp().clear();
    }

    public int getPageindex() {
        return listSwipeView.getPagingHelp().getPageindex();
    }

    public int getPageCount() {
        return listSwipeView.getPagingHelp().getPageCount();
    }
}
