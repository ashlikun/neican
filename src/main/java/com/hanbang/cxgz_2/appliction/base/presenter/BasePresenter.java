package com.hanbang.cxgz_2.appliction.base.presenter;

import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;

/**
 * Created by Administrator on 2016/8/8.
 */

public abstract class BasePresenter<T extends BaseView> {
    public T mvpView;

    public void onCreate(T mvpView) {
        this.mvpView = mvpView;
    }

    public void onDestroy() {
        mvpView = null;
    }

    public void onLowMemory() {
        mvpView = null;
    }


    /**
     * 返回值提示信息
     * @param result
     */
    public void showMsg(BaseHttpResponse result) {
        if (result.isSucceed()) {
            mvpView.showSnackbar(result.getMsg(), SnackbarUtil.Info, false);
        } else if (result.isReturnFaiure()) {
            mvpView.showWarningSnackbar(result.getMsg());
        } else {
            mvpView.showErrorSnackbar(result.getMsg());
        }
    }


}
