package com.hanbang.cxgz_2.appliction;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.view.home.activity.HomeActivity;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class WelcomeActivity extends BaseActivity {
    private int time = 3000;
    private boolean isToNext = false;
    private boolean isFirst = false;

    @BindView(R.id.image)
    ImageView imageView;
    @BindView(R.id.shimmer_tv)
    ShimmerTextView text;
    Shimmer shimmer;

    @Override
    public int getStatusBarResource() {
        return R.color.transparency;
    }

    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Observable.timer(time, TimeUnit.MILLISECONDS)
                .map(new Func1<Long, Boolean>() {
                    @Override
                    public Boolean call(Long aLong) {
                        while (!isToNext) {
                        }
                        return true;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aLong) {
                        if (!isFirst) {
//                            if (!isLogin(false)) {
//                                LogInActivity.startUI(WelcomeActivity.this);
//                            } else {
//                                if (!userData.isEnable()) {
//                                    PaymentActivity.startUI(WelcomeActivity.this, false);
//                                } else
//                                    LogInActivity.loginSuccess(WelcomeActivity.this, userData);
//
//                            }
                            HomeActivity.startUI(WelcomeActivity.this);
                            finish();
                        }
                    }
                });


        checkIsFirst();


    }

    @Override
    public int getContentView() {
        return R.layout.welcom;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }


    /**
     * 在华为手机上显示桌面徽标
     *
     * @param context
     * @param num
     */
    private static void setHuaweiBadge(Context context, int num) {
        String launcherClassName = WelcomeActivity.class.getName();
        if (launcherClassName == null) {
            return;
        }
        Bundle localBundle = new Bundle();
        localBundle.putString("package", context.getPackageName());
        localBundle.putString("class", launcherClassName);
        localBundle.putInt("badgenumber", num);
        context.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, localBundle);
    }

    @Override
    public void initView() {
//        presenter.getHttpData();

        final AnimatorSet animSet = new AnimatorSet();
//        ObjectAnimator animX = ObjectAnimator.ofFloat(imageView, "translationX", 0.5f, 1.2f,1f);
//        ObjectAnimator animY = ObjectAnimator.ofFloat(imageView, "translationY", startY, endY);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(imageView, "scaleX", 0.7f, 1.4f, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(imageView, "scaleY", 0.7f, 1.4f, 1f);
        ObjectAnimator alpha = ObjectAnimator.ofFloat(imageView, "alpha", 0.6f, 1f);

        ObjectAnimator scaleX2 = ObjectAnimator.ofFloat(text, "scaleX", 0.5f, 1.3f, 1f);
        ObjectAnimator scaleY2 = ObjectAnimator.ofFloat(text, "scaleY", 0.5f, 1.3f, 1f);
        ObjectAnimator alpha2 = ObjectAnimator.ofFloat(text, "alpha", 0.2f, 1f);

        scaleX.setDuration(time);

        scaleY.setDuration(time);
        alpha.setDuration(time);
        scaleX2.setDuration(time);
        scaleY2.setDuration(time);
        alpha2.setDuration(time);
        animSet.setInterpolator(new DecelerateInterpolator());
        animSet.setDuration(time);
        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }
        });
        animSet.playTogether(scaleX, scaleY, alpha, scaleX2, scaleY2, alpha2);
        animSet.start();
        shimmer = new Shimmer();
        shimmer.start(text);

    }

    @Override
    public void parseIntent(Intent intent) {

    }


    private void checkIsFirst() {
//        if (SharedPreferencesUtils.getSharedPreferencesKeyAndValue(this, "Run",
//                "VersionCode", -1) != ObjectUtils.getVersionCode(this)) {
//            isFirst = true;
//            // 第一次
//            SharedPreferencesUtils.setSharedPreferencesKeyAndValue(this, "Run",
//                    "VersionCode", ObjectUtils.getVersionCode(this));
//            Intent intent = new Intent(this, LeadActivity.class);
//            startActivity(intent);
//
//            finish();
//        } else {
//            next();
//        }
        next();
    }


    private void next() {
        if (isLogin(false)) {
            // 检查密码的正确性
            //getServiceUser();
            isToNext = true;
        } else {
            // 未登录
            isToNext = true;
        }

    }


    public void finish() {
        shimmer.cancel();
        super.finish();
    }




}
