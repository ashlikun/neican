package com.hanbang.cxgz_2.mode;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.httpresponse.chanpin.ChanpinHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.CateringFigureHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.HomeHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.InformationHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HomeHotAnswerResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotAnswerHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.ChefHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangDetailsHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.DemandRewardClassifyHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.EsotericDetails_k_HttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.EsotericHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeEsotericHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeRewardHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeStudyHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.StudyClassroomHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.other.CaiXiHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.other.CityHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeBalanceData;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeOrderData;
import com.hanbang.cxgz_2.mode.javabean.about_me.MyAnswerEavesdropListData;
import com.hanbang.cxgz_2.mode.javabean.about_me.MyAnswerListData;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioBeiJinData;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioData;
import com.hanbang.cxgz_2.mode.javabean.base.OrderDetailData;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.can_da.MealAnswerDetailsData;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ChuShiItemData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.DemandRewardDetailsData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.StudyClassroomDetailsData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ZhangGuiItemData;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationDetailsData;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import rx.Observable;

import static com.hanbang.cxgz_2.utils.http.HttpManager.BASE_API;

/**
 * Created by liukun on 16/3/9.
 */
public interface ServiceApi {


    @FormUrlEncoded
    @POST("server/CarCompany/CarCompFindOrders.ashx")
    Observable<HttpResult<List<OrderDetailData>>> getDetaisData(@Field("ID") int id,
                                                                @Field("ic") String ic,
                                                                @Field("firstday") String firstday,
                                                                @Field("PageIndex") int PageIndex,
                                                                @Field("PageSize") int PageSize
    );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 登录注册
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * 获取登录的验证码
     *
     * @param telphone 手机号
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=LoginCode")
    Observable<BaseHttpResponse> loginCode(
            @Field("telphone") String telphone
    );


    /**
     * 获取登录的验证码
     *
     * @param telphone 手机号
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=RegisterCode")
    Observable<BaseHttpResponse> registerCode(
            @Field("telphone") String telphone
    );


    /**
     * 注册
     *
     * @param telphone 手机号
     * @return telphone, code, group, location_name, yaoqingNumber, requestBody)
     * @Part("file\"; filename=\"image.png\"") RequestBody requestBody
     */

    @Multipart
    @POST(BASE_API + "?action=Register")
    Observable<ResponseBody> register(
            @Part("telphone") RequestBody telphone,
            @Part("code") RequestBody code,
            @Part("group") RequestBody group,
            @Part("location_name") RequestBody location_name,
            @Part("yaoqingNumber") RequestBody yaoqingNumber,
            @PartMap Map<String, RequestBody> params
    );


    /**
     * 登录
     *
     * @param telphone
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=Login")
    Observable<ResponseBody> login(
            @Field("telphone") String telphone,
            @Field("code") String code
    );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 首页
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 首页数据
     *
     * @return
     */
    @POST(BASE_API + "?action=ShouYe")
    Observable<HomeHttpResponse> ShouYe();

    /**
     * 餐饮人物志
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=CaterersRwzMoreThan")
    Observable<CateringFigureHttpResponse> cateringFigure(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 资讯
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 信息列表
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=CaterersInfoHeadMoreThan")
    Observable<InformationHttpResponse> informationList(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 资讯与人物志详情
     * <p>
     * 首页----餐饮人物志(资讯)----详情
     * CaterersRwzDetail
     * CaterersRwzID:信息详情主键
     *
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=CaterersRwzDetail")
    Observable<InformationDetailsData> CaterersRwzDetail(
            @Field("CaterersRwzID") int CaterersRwzID

    );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 餐答
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 餐答首页
     *
     * @return
     */
    @POST(BASE_API + "?action=CanDaHotFigure")
    Observable<HotFigureResponse> hotFigureResponse();


    /**
     * 热门用户
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=hotFigureMoreThan")
    Observable<HotFigureHttpResponse> hotFigure(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 热门问答
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=HotWendaMoreThan")
    Observable<HotAnswerHttpResponse> hotAnswer(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 首页热门问答
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=CanDaHotWenda")
    Observable<HomeHotAnswerResponse> homeHotAnswer(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 发布悬赏
     * <p>
     * type：0:菜品研发,1:其他需求,2:招聘,3:求职,4:门店转让,5:二手设备,6:加盟代理
     * XuQiuContent:悬赏内容
     * XunShangPrice:悬赏价格
     * CityId:城市ID(2,3,4,5)
     * Address:地址(2,4,5)
     * ShopingName:店名(2,4,5)
     * Company:公司名称(6)
     * XuQiuImg:图片
     */

    @Multipart
    @POST(BASE_API + "?action=XuShang_Add")
    Observable<BaseHttpResponse> rewardIssuer(
            @Part("type") RequestBody type,
            @Part("XuQiuContent") RequestBody XuQiuContent,
            @Part("XunShangPrice") RequestBody XunShangPrice,
            @Part("CityId") RequestBody CityId,
            @Part("Address") RequestBody Address,
            @Part("ShopingName") RequestBody ShopingName,
            @Part("Company") RequestBody Company,
            @PartMap Map<String, RequestBody> params
    );


    /**
     * 餐答首页与主页----热门人物----详情
     * CdaHtFigureDetail
     * UserId：用户ID,
     *
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=CdaHtFigureDetail")
    Observable<HttpResult<List<MealAnswerDetailsData>>> CdaHtFigureDetail(
            @Field("UserId") int UserId
    );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 江湖
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 江湖大厨
     *
     * @return
     */
    @POST(BASE_API + "?action=jianghuChef")
    Observable<HttpResult<List<ChuShiItemData>>> jianghuChef();

    //掌柜
    @POST(BASE_API + "?action=jianghuManager")
    Observable<HttpResult<List<ZhangGuiItemData>>> jianghuManager();


    /**
     * 秀场
     *
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=dynamicsList")
    Observable<XiuChangHttpResponse> jianghuXiuchang(
            @Field("type") int order,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 秀场详情
     *
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=dynamicsDetail")
    Observable<XiuChangDetailsHttpResponse> jianghuXiuchangDetailes(
            @Field("keyid") int order,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 大厨列表
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=jianghuChef")
    Observable<ChefHttpResponse> chefList(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 9:47
     * <p>
     * 方法功能：掌柜频道更多
     * ManagerInMoreThan	pageIndex,pageSize,biaoqian(6-12)
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=ManagerInMoreThan")
    Observable<HttpResult<List<ManagerData>>> ManagerInMoreThan(
            @Field("biaoqian") int biaoqian,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 9:45
     * <p>
     * 方法功能：大厨频道 更多
     * ChefInMoreThan	pageIndex,pageSize,biaoqian(1-5)
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=ChefInMoreThan")
    Observable<HttpResult<List<ChefData>>> ChefInMoreThan(
            @Field("biaoqian") int biaoqian,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 解忧商铺
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 秘籍列表
     * <p>
     * 秘籍频道----1.爆款产品更多，2.潮流精选更多，3.高毛利更多，4.大师经典更多，20.买断秘籍更多，21.视频秘籍更多
     * CaiPMijiMoreThan
     * biaoqian:标签就是上面的数字
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=CaiPMijiMoreThan")
    Observable<EsotericHttpResponse> esotericList(
            @Field("biaoqian") int biaoqian,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 悬赏频道----0：菜品研发更多----2：招聘更多----3：求职更多----4：门店转让更多----5：二手设备更多----6：加盟代理更多
     * NeedOfferedMoreThan
     *
     * @param XuQiuType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=NeedOfferedMoreThan")
    Observable<DemandRewardClassifyHttpResponse> NeedOfferedMoreThan(
            @Field("XuQiuType") int XuQiuType,
            @Field("pageIndex") int pageIndex,
            @Field("pageSize") int pageSize
    );

    /**
     * 学习课堂列表
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=LearnClassMoreThan")
    Observable<StudyClassroomHttpResponse> studyClassroomList(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 解忧店铺-菜品秘籍
     *
     * @return
     */
    @POST(BASE_API + "?action=mijiShouYe")
    Observable<HomeEsotericHttpResponse> mijiShouYe();


    /**
     * 解忧店铺-学习课堂
     *
     * @return
     */
    @POST(BASE_API + "?action=LearnClassShouye")
    Observable<HomeStudyHttpResponse> studyClass();

    /**
     * 解忧店铺-学习课堂
     * <p>
     * 解忧店铺----视频直播课堂更多----语音直播课堂（群组课堂） ps:收费的哦 更多----线下课堂更多
     * <p>
     * LearClassMorethan
     * type:(0 视频直播课堂更多 1 语音直播课堂更多 2 线下课堂更多)
     * pageIndex,
     * pageSize
     *
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=LearClassMorethan")
    Observable<HttpResult<List<StudyClassroomData>>> LearClassMorethan(
            @Field("type") int type,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 解忧店铺-悬赏需求
     *
     * @return
     */
    @POST(BASE_API + "?action=XuShangShouye")
    Observable<HomeRewardHttpResponse> jieYouReward();


    /**
     * 开放式-秘籍详情
     *
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=MijiDetail")
    Observable<EsotericDetails_k_HttpResponse> esotericDetails_k(
            @Field("id") int id

    );


    /**
     * 学习课堂-详情
     * Ketang_Details
     * id：学习课堂ID
     *
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=Ketang_Details")
    Observable<HttpResult<List<StudyClassroomDetailsData>>> Ketang_Details(
            @Field("id") int id

    );


    /**
     * 需求悬赏----详情
     * action:XuShang_XiangQing
     *
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=XuShang_XiangQing")
    Observable<HttpResult<List<DemandRewardDetailsData>>> XuShang_XiangQing(
            @Field("XuShangID") String id

    );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 产品商铺
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 产品商铺接口
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=CaterersInfoHeadMoreThan")
    Observable<ChanpinHttpResponse> CaterersInfoHeadMoreThan(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 个人中心
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 上传开放工秘籍
     * <p>
     * type:类型，1为开放式，2为买断式
     * title:秘籍名称
     * tedian:特点
     * maidiantese:卖点特色
     * price:价格，免费的传0
     * cuisines_id:菜系，这个在效果图上没有体现出来，需要加上去的
     * img_url:主图，秘籍效果图，一张">url:主图，秘籍效果图，一张
     * shipin:视频，可选
     * <p>
     * 开放式需要传的参数:
     * zhuliao:主料，格式为:（名称:数量|名称:数量）
     * fuliao:辅料，格式为:（名称:数量|名称:数量）
     * gongyiliucheng:工艺流程
     * zhuyishixiang:注意事项
     * img_guocheng:过程图片，可为多张
     */
    @Multipart
    @POST(BASE_API + "?action=Miji_Add")
    Observable<BaseHttpResponse> issuerEsoteric_k(
            @Part("type") RequestBody type,
            @Part("title") RequestBody title,
            @Part("tedian") RequestBody tedian,
            @Part("maidiantese") RequestBody maidiantese,
            @Part("price") RequestBody price,
            @Part("cuisines_id") RequestBody cuisines_id,
            @Part("zhuliao") RequestBody zhuliao,
            @Part("fuliao") RequestBody fuliao,
            @Part("zhuyishixiang") RequestBody zhuyishixiang,
            @Part("gongyiliucheng") RequestBody gongyiliucheng,
            @PartMap Map<String, RequestBody> files
    );


    /**
     * 上传开放工秘籍
     * <p>
     * type:类型，1为开放式，2为买断式
     * title:秘籍名称
     * tedian:特点
     * maidiantese:卖点特色
     * price:价格，免费的传0
     * cuisines_id:菜系，这个在效果图上没有体现出来，需要加上去的
     * img_url:主图，秘籍效果图，一张">url:主图，秘籍效果图，一张
     * shipin:视频，可选
     * <p>
     * 买断式需要传的参数:
     * begin_date:开始时间，格式为2015-09-06
     * end_date:结束时间，格式为2015-09-06
     * pingjia:评价
     * maiduanliucheng:买断流程
     * maiduanshengming:买断声明
     * huanyingzongchoumaiduan:欢迎众筹买断
     * AudioUrl:音频文件
     */
    @Multipart
    @POST(BASE_API + "?action=Miji_Add")
    Observable<BaseHttpResponse> issuerEsoteric_D(
            @Part("type") RequestBody type,
            @Part("title") RequestBody title,
            @Part("maidiantese") RequestBody maidiantese,
            @Part("price") RequestBody price,
            @Part("cuisines_id") RequestBody cuisines_id,
            @Part("begin_date") RequestBody begin_date,
            @Part("end_date") RequestBody end_date,
            @Part("pingjia") RequestBody pingjia,
            @Part("maiduanliucheng") RequestBody maiduanliucheng,
            @Part("maiduanshengming") RequestBody maiduanshengming,
            @Part("huanyingzongchoumaiduan") RequestBody huanyingzongchoumaiduan,
            @PartMap Map<String, RequestBody> files
    );


    /**
     * @param address:开课地址
     * @param begin_time:开课时间，格式为yyyy-MM-dd HH:mm:ss
     * @param end_time:截止时间，格式为yyyy-MM-dd   HH:mm:ss
     * @param price:价格，免费的为0
     * @param remark:课堂简介
     * @param title:标题
     * @param type:课堂形式，1为线下课堂，2为视频课堂
     * @param Zhaosrenshu:招生人数
     * @param files:图片，视频，音频，多图
     * @return
     */
    @Multipart
    @POST(BASE_API + "?action=ketang_Add")
    Observable<BaseHttpResponse> issuerClassroom(
            @Part("address") RequestBody address,
            @Part("begin_time") RequestBody begin_time,
            @Part("end_time") RequestBody end_time,
            @Part("price") RequestBody price,
            @Part("remark") RequestBody remark,
            @Part("title") RequestBody title,
            @Part("type") RequestBody type,
            @Part("Zhaosrenshu") RequestBody Zhaosrenshu,
            @PartMap Map<String, RequestBody> files

    );


    /**
     * 发布秀场
     *
     * @param phone    手机型号
     * @param position 地理位置
     * @param text     正文内容
     * @param files    图片集合
     * @return
     */
    @Multipart
    @POST(BASE_API + "?action=dynamics_Add")
    Observable<BaseHttpResponse> dynamics_Add(
            @Part("phone") RequestBody phone,
            @Part("position") RequestBody position,
            @Part("text") RequestBody text,
            @PartMap Map<String, RequestBody> files

    );

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页	GeRenZhuye	user_id：用户ID
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=GeRenZhuye")
    Observable<HttpResult<List<StudioData>>> GeRenZhuye(
            @Field("user_id") int userid
    );

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页----我的秀场列表	GetMydynamicsList	userid：用户ID
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=GetMydynamicsList")
    Observable<HttpResult<List<XiuChangItemData>>> GetMydynamicsList(
            @Field("userid") int userid,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页----我的秘籍	GetMyMijiList	userid：用户ID
     * 个人主页----我的秘籍	GetMyMijiList	userid：用户ID
     * type:1为我上传的秘籍，2为我购买的秘籍
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=GetMyMijiList")
    Observable<HttpResult<List<EsotericaData>>> GetMyMijiList(
            @Field("userid") int userid,
            @Field("type") int type,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页----我的课堂	GetMyKetangList	userid：用户ID
     * type:1为我上传的课堂，2为我购买的课堂
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=GetMyKetangList")
    Observable<HttpResult<List<StudyClassroomData>>> GetMyKetangList(
            @Field("userid") int userid,
            @Field("type") int type,
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 我----资讯和餐答 我的提问和我的偷听列表
     * ZiXunforMeTiWenAndTouTing
     * pageIndex：分页索引
     * pageSize:每页的条数
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=ZiXunforMeTiWenAndTouTing")
    Observable<HttpResult<List<MyAnswerEavesdropListData>>> ZiXunforMeTiWenAndTouTing(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 我----资讯和餐答 我回答的问题列表	ZiXunforMeHuiDa
     * <p>
     * ZiXunforMeHuiDa
     * pageIndex：分页索引
     * pageSize:每页的条数
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=ZiXunforMeHuiDa")
    Observable<HttpResult<List<MyAnswerListData>>> ZiXunforMeHuiDa(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );


    /**
     * 我的订单	GetOrderList
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=GetOrderList")
    Observable<HttpResult<List<MeOrderData>>> GetOrderList(
            @Field("PageIndex") int PageIndex,
            @Field("PageSize") int PageSize
    );

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 16:13
     * <p>
     * 方法功能：我的悬赏	MyDtXuShang	userid：用户ID
     * pageIndex,pageSize
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=MyDtXuShang")
    Observable<HttpResult<List<DemandRewardData>>> MyDtXuShang(
            @Field("userid") int userid,
            @Field("pageIndex") int pageIndex,
            @Field("pageSize") int pageSize
    );

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 16:13
     * <p>
     * 方法功能：更新个人中心背景图	UpdateHomeImg	img_url：图片地址
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=UpdateHomeImg")
    Observable<BaseHttpResponse> UpdateHomeImg(@Field("img_url") String img_url);

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 16:13
     * <p>
     * 方法功能：个人中心背景图展示所有	GeRenBackgroundAll
     */

    @POST(BASE_API + "?action=GeRenBackgroundAll")
    Observable<HttpResult<List<StudioBeiJinData>>> GeRenBackgroundAll();


    /**
     * 作者　　: 杨阳
     * 创建时间: 2016/9/22 14:40
     * 邮箱　　：360621904@qq.com
     * <p>
     * 功能介绍：获取余额明细	GetAccountDetail
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=GetAccountDetail")
    Observable<MeBalanceData> GetAccountDetail(
            @Field("pageIndex") int pageIndex,
            @Field("pageSize") int pageSize
    );


    /**
     * 作者　　: 杨阳
     * 创建时间: 2016/9/22 14:40
     * 邮箱　　：360621904@qq.com
     * <p>
     * 功能介绍：个人简介展示
     * <p>
     * GeRenJianJie
     * userid:用户ID
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=GeRenJianJie")
    Observable<HttpResult<UserData>> GeRenJianJie(
            @Field("userid") int userid

    );


    /**
     * 作者　　: 杨阳
     * 创建时间: 2016/9/22 14:40
     * 邮箱　　：360621904@qq.com
     * <p>
     * 功能介绍：个人简介编辑
     * <p>
     * GeRenJianJieEdit
     * userid:用户ID
     * avatar:头像
     * UserName:昵称姓名
     * Age:年龄
     * Sex:性别
     * JobId:职位
     * Company:公司
     * city_id:城市 int
     * CuisinesId:擅长
     * Signfoods:招牌菜(经理人不需要传)
     * Workjingli:工作经历
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=GeRenJianJieEdit")
    Observable<HttpResult<UserData>> GeRenJianJieEdit(
            @Field("userid") int userid,
            @Field("avatar") RequestBody avatar,
            @Field("UserName") String UserName,
            @Field("Age") int Age,
            @Field("Sex") String Sex,
            @Field("JobId") int JobId,
            @Field("Company") String Company,
            @Field("city_id") String city_id,
            @Field("CuisinesId") String CuisinesId,
            @Field("Signfoods") String Signfoods,
            @Field("Workjingli") String Workjingli


    );

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 公共
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 10:29
     * <p>
     * 方法功能：
     */
    @POST(BASE_API + "?action=GetCityList")
    Observable<CityHttpResponse> getCityData(
    );


    /**
     * 作者　　: 杨阳
     * 创建时间: 2016/9/5 17:38
     * 邮箱　　：360621904@qq.com
     * <p>
     * 功能介绍：选择菜系
     */

    @POST(BASE_API + "?action=GetCuisinesList")
    Observable<CaiXiHttpResponse> GetCuisinesList(
    );

    /**
     * 职位	GetJobList	pid:(0 大厨,1 掌柜)
     *
     * @return
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=GetJobList")
    Observable<CaiXiHttpResponse> GetJobList(
            @Field("pid") int pid
    );


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:18
     * <p>
     * 方法功能：关注
     *
     * @param useridToHe：我关注人的ID
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=Guanzhu")
    Observable<BaseHttpResponse> Guanzhu(@Field("useridToHe") int useridToHe);

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:18
     * <p>
     * 方法功能：取消关注
     *
     * @param useridToHe：我关注人的ID
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=QuXiaoGuanzhu")
    Observable<BaseHttpResponse> QuXiaoGuanzhu(@Field("useridToHe") int useridToHe);

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:18
     * <p>
     * 方法功能：点赞
     *
     * @param keyid: 主键
     * @param type:  类别 1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     */

    @FormUrlEncoded
    @POST(BASE_API + "?action=PointaPraise")
    Observable<BaseHttpResponse> PointaPraise(@Field("keyid") int keyid,
                                              @Field("type") int type);


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/14 16:58
     * <p>
     * 方法功能：评论	        PinglunAdd	keyid：主键
     * type:1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     * text:评论内容
     * 如果type=7,还需传参数touserid:对某人的评论
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=PinglunAdd")
    Observable<BaseHttpResponse> PinglunAdd(
            @Field("keyid") int keyid,
            @Field("type") int type,
            @Field("text") String text);

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/14 16:58
     * <p>
     * 方法功能：评论	        PinglunAdd	keyid：主键
     * type:1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     * text:评论内容
     * 如果type=7,还需传参数touserid:对某人的评论
     */
    @FormUrlEncoded
    @POST(BASE_API + "?action=PinglunAdd")
    Observable<BaseHttpResponse> PinglunAddXiuChangDetail(
            @Field("keyid") int keyid,
            @Field("type") int type,
            @Field("touserid") int touserid,
            @Field("text") String text);

}

