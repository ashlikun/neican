package com.hanbang.cxgz_2.mode.enumeration;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 11:55
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：菜品秘籍分类的类型
 * <p>
 * 秘籍频道----1.爆款产品更多，2.潮流精选更多，3.高毛利更多，4.大师经典更多，20.买断秘籍更多，21.视频秘籍更多
 */

public enum EsotericClassifyEnum implements BaseEnume<Integer> {

    BAO_KUAN(1, "爆款产品"),
    CHAO_LIU(2, "潮流精选"),
    GAO_MAO_LI(3, "高毛利"),
    DA_SHI_JING_DIANG(4, "大师经典"),
    MAI_DUAN(20, "买断秘籍"),
    SHI_PIN(21, "视频秘籍");

    private String title;
    private int type;

    EsotericClassifyEnum(int type, String valuse) {
        this.title = valuse;
        this.type = type;
    }

    public String getValuse() {
        return title;
    }

    public Integer getKey() {
        return type;
    }
}
