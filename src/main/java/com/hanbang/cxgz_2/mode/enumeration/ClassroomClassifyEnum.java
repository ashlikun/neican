package com.hanbang.cxgz_2.mode.enumeration;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 13:54
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：学习课堂的分类
 * type:(0 视频直播课堂更多 1 语音直播课堂更多 2 线下课堂更多)
 */

public enum ClassroomClassifyEnum implements BaseEnume<Integer> {
    /**
     * type:(0 视频直播课堂更多 1 语音直播课堂更多 2 线下课堂更多)
     */
    ZHI_BO(0, "视频直播课堂"),
    YU_YIN(1, "语音直播课堂"),
    XIAN_XIA(2, "线下课堂");

    private String title;
    private int type;

    ClassroomClassifyEnum(int type, String valuse) {
        this.title = valuse;
        this.type = type;
    }

    public String getValuse() {
        return title;
    }

    public Integer getKey() {
        return type;
    }
}
