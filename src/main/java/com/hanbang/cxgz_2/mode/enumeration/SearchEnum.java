package com.hanbang.cxgz_2.mode.enumeration;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/19 13:28
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：搜索的枚举
 * <p>
 * type以及keyword,都不能为空
 * type=1:动态
 * type=2:厨师
 * type=3:掌柜
 * type=5:秘籍
 * type=6:课程
 * type=9:资讯
 * type=10:餐答(当单击进入详情页面，请再传参数(liebiao=1,ShopingNum))
 */

public enum SearchEnum implements BaseEnume<Integer> {
    /**
     * <string name="ziXun">资讯</string>
     * <string name="canDa">餐答</string>
     * <string name="xiuCang">秀场</string>
     * <string name="chuShi">厨师</string>
     * <string name="zhangGui">掌柜</string>
     * <string name="miJi">秘籍</string>
     * <string name="keTang">课堂</string>
     * <string name="canPin">产品</string>
     * <string name="xuanShang">悬赏</string>
     * <string name="renWuZhi">人物志</string>
     */
    ziXun(9, "资讯"),
    canDa(10, "餐答"),
    xiuCang(1, "秀场"),
    chuShi(2, "厨师"),
    zhangGui(3, "掌柜"),
    miJi(5, "秘籍"),
    keTang(6, "课堂"),
    canPin(-1, "产品"),
    xuanShang(-1, "悬赏"),
    renWuZhi(-1, "人物志");


    private String title;
    private int type;

    SearchEnum(int type, String valuse) {
        this.title = valuse;
        this.type = type;
    }

    public String getValuse() {
        return title;
    }

    public Integer getKey() {
        return type;
    }
}
