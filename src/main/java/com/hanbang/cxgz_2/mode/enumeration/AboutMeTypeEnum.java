package com.hanbang.cxgz_2.mode.enumeration;


import com.hanbang.cxgz_2.R;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 11:15
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：个人中心的模块类型
 * <p>
 * <p>
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_qa, "我的餐答", MeCanDaActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_order, "我的订单", OrderActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_show, "我的秀场", MeXiuChangActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_teacher, "老师学员", HomeActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_friends, "邀请好友", HomeActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_collection, "我的收藏", HomeActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_studio, "我的工作室", HomeActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_money, "我的余额", HomeActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_cheats, "我的秘籍", HomeActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_class, "我的课堂", HomeActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_logo, "关于内餐", WebViewActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.my_explain, "使用说明", WebViewActivity.class.getName()));
 * <p>
 * datas.add(new ItmeMeGridViewData(R.mipmap.ic_launcher, "登录", LoginActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.ic_launcher, "注册", RegisterActivity.class.getName()));
 * datas.add(new ItmeMeGridViewData(R.mipmap.ic_launcher, "测试", TestActivity.class.getName()));
 */

public enum AboutMeTypeEnum {

    CHAN_DA(0, "我的餐答", R.mipmap.my_qa),
    DING_DAN(0, "我的订单", R.mipmap.my_order),
    XIU_CHANG(0, "我的秀场", R.mipmap.my_show),
    LAO_SHI_XUE_YUAN(4, "老师学员", R.mipmap.my_teacher),
    YAO_QING_HAO_YOU(0, "邀请好友", R.mipmap.my_friends),
    SHOU_CANG(0, "我的收藏", R.mipmap.my_collection),
    GONG_ZUO_SHI(0, "我的工作室", R.mipmap.my_studio),
    YU_E(0, "我的余额", R.mipmap.my_money),
    MI_JI(0, "我的秘籍", R.mipmap.my_cheats),
    KE_TANG(0, "我的课堂", R.mipmap.my_class),
    GUAN_YU(0, "关于内餐", R.mipmap.my_logo),
    SHUO_MING(0, "使用说明", R.mipmap.my_explain),
    DENG_LU(0, "登录", R.mipmap.ic_launcher),
    ZHU_CE(0, "注册", R.mipmap.ic_launcher),
    CE_SHI(0, "测试", R.mipmap.ic_launcher);


    private int informationNum;
    private String title;
    private int pictureId;


    AboutMeTypeEnum(int informationNum, String valuse, int pictureId) {
        this.title = valuse;
        this.informationNum = informationNum;
        this.pictureId = pictureId;
    }

    public int getInformationNum() {
        return informationNum;
    }

    public String getTitle() {
        return title;
    }

    public int getPictureId() {
        return pictureId;
    }


    public void setInformationNum(int informationNum) {
        this.informationNum = informationNum;
    }
}
