package com.hanbang.cxgz_2.mode.enumeration;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/2　11:01
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：http的返回值
 */

public enum GuanZhu implements BaseEnume<Boolean>{


    YES(true, "取消关注"), NO(false, "关注"), WEIZHI(false, "关注");

    private String valuse;
    private boolean key;

    GuanZhu(boolean key, String valuse) {
        this.valuse = valuse;
        this.key = key;
    }

    public String getValuse() {
        return valuse;
    }

    public Boolean getKey() {
        return key;
    }

    public static GuanZhu getState(boolean status) {
        if (status == GuanZhu.YES.getKey()) {
            return YES;
        } else if (status == GuanZhu.NO.getKey()) {
            return NO;
        } else if (status == GuanZhu.WEIZHI.getKey()) {
            return WEIZHI;
        }
        return WEIZHI;
    }
}
