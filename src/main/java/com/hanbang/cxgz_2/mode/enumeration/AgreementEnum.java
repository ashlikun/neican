package com.hanbang.cxgz_2.mode.enumeration;


import com.hanbang.cxgz_2.utils.http.HttpManager;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 11:15
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：协议的类型
 * <p>
 * GetArticleByID
 * id=110 使用说明
 * id=109 关于内餐
 * id=108 普通注册协议
 * id=107 VIP经理人注册协议
 * id=106 VIP厨师注册协议
 * <p>
 * http://api.cxgz8.com/admin/Me/guanyu/cxgz_xieyi.aspx?ID=106
 */

public enum AgreementEnum implements BaseEnume<Integer> {

    SHR_YONG_SHUO_MING(110, "使用说明"),
    GUANG_YU_NEI_CAN(109, "关于内餐"),
    PU_TONG_XIE_YE(108, "普通注册协议"),
    VIP_CHU_SHI(107, "VIP经理人注册协议"),
    VIP_CHU_ZHANG_GUI(106, "VIP厨师注册协议");


    private String title;
    private int type;

    AgreementEnum(int type, String valuse) {
        this.title = valuse;
        this.type = type;
    }

    public String getValuse() {
        return title;
    }

    public Integer getKey() {
        return type;
    }


    public String getUrl() {
        return HttpManager.BASE_URL + "/admin/Me/guanyu/cxgz_xieyi.aspx?ID=" + getKey();
    }


}
