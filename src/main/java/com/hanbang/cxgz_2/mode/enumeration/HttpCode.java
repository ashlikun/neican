package com.hanbang.cxgz_2.mode.enumeration;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/2　11:01
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：http的返回值
 */

public enum HttpCode implements BaseEnume<Integer>{


    SUCCEED(0, "成功"), FAILURE(1, "接口失败"), RETURN_FAILURE(2, "返回失败"), NO_LOGIN(-100, "未登录"), WEIZHI(-1000, "未知");

    private String valuse;
    private int key;

    HttpCode(int key, String valuse) {
        this.valuse = valuse;
        this.key = key;
    }

    public String getValuse() {
        return valuse;
    }

    public Integer getKey() {
        return key;
    }

    public static HttpCode getState(int status) {
        if (status == HttpCode.SUCCEED.getKey()) {
            return SUCCEED;
        } else if (status == HttpCode.FAILURE.getKey()) {
            return FAILURE;
        } else if (status == HttpCode.RETURN_FAILURE.getKey()) {
            return RETURN_FAILURE;
        } else if (status == HttpCode.NO_LOGIN.getKey()) {
            return NO_LOGIN;
        } else if (status == HttpCode.WEIZHI.getKey()) {
            return WEIZHI;
        }
        return WEIZHI;
    }
}
