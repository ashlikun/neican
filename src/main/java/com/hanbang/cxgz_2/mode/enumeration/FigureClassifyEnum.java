package com.hanbang.cxgz_2.mode.enumeration;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 16:45
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：热门人物的分类
 * <p>
 */

public enum FigureClassifyEnum implements BaseEnume<Integer> {

    GOU_JI_ZHONG_CAN(1, "国际中餐"),
    ZHONG_XI_KUAI_CAN(2, "中西快餐"),
    HUO_GU_SHAO_KAO(3, "火锅烧烤"),
    DIAN_PU_ZHUAN_RANG(4, "名点小吃"),
    XI_CNA_GU_JI(5, "西餐国际"),
    CHUAN_TONG_QING_ZHEN(6, "传统清真"),
    YANG_SHENG_SU_SHI(7, "养生素食"),
    ZHONG_CAN_CHU_SHI(8, "中餐厨师"),
    XI_CAN_CHU_SHI(9, "西餐厨师"),
    MIAN_DIAN_CHU_SHI(10, "面点厨师"),
    LENG_CAI_CHU_SHI(11, "冷菜厨师"),
    QI_TA_CHU_SHI(12, "其他厨师"),
    MING_REN_TUI_JIAN(-1, "名人推荐");

    private String title;
    private int type;

    FigureClassifyEnum(int type, String valuse) {
        this.title = valuse;
        this.type = type;
    }

    public String getValuse() {
        return title;
    }

    public Integer getKey() {
        return type;
    }
}
