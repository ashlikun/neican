package com.hanbang.cxgz_2.mode.enumeration;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 8:59
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：悬赏分类的类型
 */

public enum RewardTypeEnum implements BaseEnume<Integer> {
    /**
     * 悬赏频道----0：菜品研发更多----2：招聘更多----3：求职更多----4：门店转让更多----5：二手设备更多----6：加盟代理更多
     */
    CAI_PIN_YAN_FA(0, "菜品研发"),
    ZHAO_PIN(2, "招聘"),
    QIU_ZHI(3, "求职"),
    DIAN_PU_ZHUAN_RANG(4, "店铺转让"),
    ER_SHOU_SHE_BEI(5, "二手设备"),
    JIA_MENG_DAI_LI(6, "加盟代理"),
    WEIZHI(-100, "未知");

    private String title;
    private int type;

    RewardTypeEnum(int type, String valuse) {
        this.title = valuse;
        this.type = type;
    }


    public String getValuse() {
        return title;
    }

    public Integer getKey() {
        return type;
    }

    public static RewardTypeEnum getState(int status) {
        if (status == CAI_PIN_YAN_FA.getKey()) {
            return CAI_PIN_YAN_FA;
        } else if (status == ZHAO_PIN.getKey()) {
            return ZHAO_PIN;
        } else if (status == QIU_ZHI.getKey()) {
            return QIU_ZHI;
        } else if (status == DIAN_PU_ZHUAN_RANG.getKey()) {
            return DIAN_PU_ZHUAN_RANG;
        } else if (status == ER_SHOU_SHE_BEI.getKey()) {
            return ER_SHOU_SHE_BEI;
        } else if (status == JIA_MENG_DAI_LI.getKey()) {
            return JIA_MENG_DAI_LI;
        } else if (status == WEIZHI.getKey()) {
            return WEIZHI;
        }
        return WEIZHI;
    }
}
