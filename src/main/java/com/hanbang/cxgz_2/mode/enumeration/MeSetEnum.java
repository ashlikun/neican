package com.hanbang.cxgz_2.mode.enumeration;


import com.hanbang.cxgz_2.R;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 11:31
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的设置
 */

public enum MeSetEnum {

    GE_REN(0, "修改个人信息", R.mipmap.my_set_up_change),
    ZFB(0, "添加支付宝帐号", R.mipmap.my_set_up_zhifubao),
    CAN_DA(0, "餐答设置", R.mipmap.my_set_up_ask);


    private int informationNum;
    private String title;
    private int pictureId;


    MeSetEnum(int informationNum, String valuse, int pictureId) {
        this.title = valuse;
        this.informationNum = informationNum;
        this.pictureId = pictureId;
    }

    public int getInformationNum() {
        return informationNum;
    }

    public String getTitle() {
        return title;
    }

    public int getPictureId() {
        return pictureId;
    }


    public void setInformationNum(int informationNum) {
        this.informationNum = informationNum;
    }
}
