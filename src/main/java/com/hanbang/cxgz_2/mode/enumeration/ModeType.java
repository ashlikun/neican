package com.hanbang.cxgz_2.mode.enumeration;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/2　11:01
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：点赞的类别
 */

public enum ModeType implements BaseEnume<Integer>{

    /**
     * 1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     */

    MIJI_D(1, "秘籍详情"), XXZL_D(2, "学习资料详情"), BZH_D(3, "餐饮标准化详情"), RDZX_D(4, "热点资讯详情"),
    XXKT(5, "学习课堂"), DONGTAI(6, "动态"), DT_D(7, "动态详情"), WEIZHI(-100, "未知");

    private String valuse;
    private int key;

    ModeType(int key, String valuse) {
        this.valuse = valuse;
        this.key = key;
    }

    public String getValuse() {
        return valuse;
    }

    public Integer getKey() {
        return key;
    }

    public static ModeType getState(int status) {
        if (status == ModeType.MIJI_D.getKey()) {
            return MIJI_D;
        } else if (status == ModeType.XXZL_D.getKey()) {
            return XXZL_D;
        } else if (status == ModeType.BZH_D.getKey()) {
            return BZH_D;
        } else if (status == ModeType.RDZX_D.getKey()) {
            return RDZX_D;
        } else if (status == ModeType.XXKT.getKey()) {
            return XXKT;
        } else if (status == ModeType.DONGTAI.getKey()) {
            return DONGTAI;
        } else if (status == ModeType.DT_D.getKey()) {
            return DT_D;
        } else if (status == ModeType.WEIZHI.getKey()) {
            return WEIZHI;
        }
        return WEIZHI;
    }
}
