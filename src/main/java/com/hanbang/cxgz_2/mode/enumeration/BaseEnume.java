package com.hanbang.cxgz_2.mode.enumeration;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/22　11:41
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public interface BaseEnume<T> {

    String getValuse();

    T getKey();
}
