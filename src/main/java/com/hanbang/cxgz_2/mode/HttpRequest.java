package com.hanbang.cxgz_2.mode;

import com.hanbang.cxgz_2.mode.enumeration.ModeType;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.httpresponse.chanpin.ChanpinHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.CateringFigureHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.HomeHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.InformationHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HomeHotAnswerResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotAnswerHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangDetailsHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.DemandRewardClassifyHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.EsotericDetails_k_HttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.EsotericHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeEsotericHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeRewardHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeStudyHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.StudyClassroomHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.other.CaiXiHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.other.CityHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeBalanceData;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeOrderData;
import com.hanbang.cxgz_2.mode.javabean.about_me.MyAnswerEavesdropListData;
import com.hanbang.cxgz_2.mode.javabean.about_me.MyAnswerListData;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioBeiJinData;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioData;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.can_da.MealAnswerDetailsData;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ChuShiItemData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.DemandRewardDetailsData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.StudyClassroomDetailsData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ZhangGuiItemData;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationDetailsData;
import com.hanbang.cxgz_2.utils.http.FileParam;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.utils.http.HttpManager;
import com.hanbang.cxgz_2.utils.http.HttpProgressCallBack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2016/8/3.
 * http的一般请求
 */

public class HttpRequest {
    /**
     * 订阅
     *
     * @param o 被监听者，相当于网络访问
     * @param s 监听者，  相当于回调监听
     */
    private static <T> Subscription toSubscribe(Observable<T> o, Subscriber<T> s) {
        return o.subscribeOn(Schedulers.io())//订阅过程运行于io线程
                .observeOn(AndroidSchedulers.mainThread())//回调运行于主线程
                .subscribe(s);//订阅
    }

    //信息
    public static Subscription getCaterersInfoHeadMoreThan(HttpCallBack<ChanpinHttpResponse> subscriber,
                                                           int PageIndex,
                                                           int PageSize) {
        Observable<ChanpinHttpResponse> observable = HttpManager.getInstance().serviceApi.CaterersInfoHeadMoreThan(PageIndex, PageSize);
        return toSubscribe(observable, subscriber);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 登录注册
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //获取登录验证码
    public static Subscription getLoginVerificationCode(HttpCallBack<BaseHttpResponse> subscriber, String phone) {
        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.loginCode(phone);
        return toSubscribe(observable, subscriber);
    }

    //获取注册验证码
    public static Subscription getRegisterVerificationCode(HttpCallBack<BaseHttpResponse> subscriber, String phone) {
        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.registerCode(phone);
        return toSubscribe(observable, subscriber);
    }


    /**
     * 进行注册
     *
     * @param subscriber
     * @param telphone
     * @param code
     * @param group
     * @param location_name
     * @param yaoqingNumber
     * @param file
     * @return
     */
    public static Subscription register(HttpCallBack<ResponseBody> subscriber, String telphone, String code, String group, String location_name, String yaoqingNumber, File file) {
        List<File> paths = new ArrayList<>();
        paths.add(file);

        Observable<ResponseBody> observable = HttpManager.getInstance().serviceApi.register(
                getTextRequestBody(telphone),
                getTextRequestBody(code),
                getTextRequestBody(group),
                getTextRequestBody(location_name),
                getTextRequestBody(yaoqingNumber),
                getFilesRequestBody(paths));
        return toSubscribe(observable, subscriber);
    }


    //登录
    public static Subscription login(HttpCallBack<ResponseBody> subscriber, String phone, String code) {
        Observable<ResponseBody> observable = HttpManager.getInstance().serviceApi.login(phone, code);
        return toSubscribe(observable, subscriber);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 首页
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //首页
    public static Subscription ShouYe(HttpCallBack<HomeHttpResponse> subscriber) {
        Observable<HomeHttpResponse> observable = HttpManager.getInstance().serviceApi.ShouYe();
        return toSubscribe(observable, subscriber);
    }

    //餐饮人物志
    public static Subscription cateringFigure(HttpCallBack<CateringFigureHttpResponse> subscriber,
                                              int PageIndex,
                                              int PageSize) {
        Observable<CateringFigureHttpResponse> observable = HttpManager.getInstance().serviceApi.cateringFigure(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 资讯
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //信息列表
    public static Subscription informationList(HttpCallBack<InformationHttpResponse> subscriber, int PageIndex, int PageSize) {

        Observable<InformationHttpResponse> observable = HttpManager.getInstance().serviceApi.informationList(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }

    /**
     * 资讯与人物志详情
     * <p>
     * 首页----餐饮人物志(资讯)----详情
     * CaterersRwzDetail
     * CaterersRwzID:信息详情主键
     *
     * @return
     */
    public static Subscription CaterersRwzDetail(HttpCallBack<InformationDetailsData> subscriber, int CaterersRwzID) {

        Observable<InformationDetailsData> observable = HttpManager.getInstance().serviceApi.CaterersRwzDetail(CaterersRwzID);

        return toSubscribe(observable, subscriber);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 餐答
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //餐答首页热门用户
    public static Subscription hotFigureResponse(HttpCallBack<HotFigureResponse> subscriber) {
        Observable<HotFigureResponse> observable = HttpManager.getInstance().serviceApi.hotFigureResponse();
        return toSubscribe(observable, subscriber);
    }


    //热门用户列表
    public static Subscription hotFigure(HttpCallBack<HotFigureHttpResponse> subscriber,
                                         int PageIndex,
                                         int PageSize) {
        Observable<HotFigureHttpResponse> observable = HttpManager.getInstance().serviceApi.hotFigure(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }

    //热门问答列表
    public static Subscription hotAnswer(HttpCallBack<HotAnswerHttpResponse> subscriber,
                                         int PageIndex,
                                         int PageSize) {
        Observable<HotAnswerHttpResponse> observable = HttpManager.getInstance().serviceApi.hotAnswer(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }


    //首页热门问答
    public static Subscription homeHotAnswer(HttpCallBack<HomeHotAnswerResponse> subscriber,
                                             int PageIndex,
                                             int PageSize) {
        Observable<HomeHotAnswerResponse> observable = HttpManager.getInstance().serviceApi.homeHotAnswer(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }


    /**
     * 餐答首页与主页----热门人物----详情
     * CdaHtFigureDetail
     * UserId：用户ID,
     *
     * @return
     */
    public static Subscription CdaHtFigureDetail(HttpCallBack<HttpResult<List<MealAnswerDetailsData>>> subscriber, int UserId) {

        Observable<HttpResult<List<MealAnswerDetailsData>>> observable = HttpManager.getInstance().serviceApi.CdaHtFigureDetail(UserId);

        return toSubscribe(observable, subscriber);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 江湖
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //大厨
    public static Subscription jianghuChef(HttpCallBack<HttpResult<List<ChuShiItemData>>> subscriber) {
        Observable<HttpResult<List<ChuShiItemData>>> observable = HttpManager.getInstance().serviceApi.jianghuChef();
        return toSubscribe(observable, subscriber);
    }

    //掌柜
    public static Subscription jianghuManager(HttpCallBack<HttpResult<List<ZhangGuiItemData>>> subscriber) {
        Observable<HttpResult<List<ZhangGuiItemData>>> observable = HttpManager.getInstance().serviceApi.jianghuManager();
        return toSubscribe(observable, subscriber);
    }

    //秀场
    public static Subscription jianghuXiuchang(HttpCallBack<XiuChangHttpResponse> subscriber,
                                               int order,
                                               int PageIndex,
                                               int PageSize) {
        Observable<XiuChangHttpResponse> observable = HttpManager.getInstance().serviceApi.jianghuXiuchang(order, PageIndex, PageSize);
        return toSubscribe(observable, subscriber);
    }

    //秀场详情
    public static Subscription jianghuXiuchangDetailes(HttpCallBack<XiuChangDetailsHttpResponse> subscriber,
                                                       int id,
                                                       int PageIndex,
                                                       int PageSize) {
        Observable<XiuChangDetailsHttpResponse> observable = HttpManager.getInstance().serviceApi.jianghuXiuchangDetailes(id, PageIndex, PageSize);
        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 9:45
     * <p>
     * 方法功能：大厨频道 更多
     * ChefInMoreThan	pageIndex,pageSize,biaoqian(1-5)
     */
    public static Subscription ChefInMoreThan(HttpCallBack<HttpResult<List<ChefData>>> subscriber,
                                              int biaoqian,
                                              int PageIndex,
                                              int PageSize) {
        Observable<HttpResult<List<ChefData>>> observable = HttpManager.getInstance().serviceApi.ChefInMoreThan(biaoqian, PageIndex, PageSize);
        return toSubscribe(observable, subscriber);
    }


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 9:47
     * <p>
     * 方法功能：掌柜频道更多
     * ManagerInMoreThan	pageIndex,pageSize,biaoqian(6-12)
     */
    public static Subscription ManagerInMoreThan(HttpCallBack<HttpResult<List<ManagerData>>> subscriber,
                                                 int biaoqian,
                                                 int PageIndex,
                                                 int PageSize) {
        Observable<HttpResult<List<ManagerData>>> observable = HttpManager.getInstance().serviceApi.ManagerInMoreThan(biaoqian, PageIndex, PageSize);
        return toSubscribe(observable, subscriber);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 解忧商铺
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //秘籍列表

    /**
     * 秘籍频道----1.爆款产品更多，2.潮流精选更多，3.高毛利更多，4.大师经典更多，20.买断秘籍更多，21.视频秘籍更多
     * CaiPMijiMoreThan
     * biaoqian:标签就是上面的数字
     *
     * @param subscriber
     * @param PageIndex
     * @param PageIndex
     * @param PageSize
     * @return
     */
    public static Subscription esotericList(HttpCallBack<EsotericHttpResponse> subscriber,
                                            int biaoqian,
                                            int PageIndex,
                                            int PageSize) {
        Observable<EsotericHttpResponse> observable = HttpManager.getInstance().serviceApi.esotericList(biaoqian, PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }

    /**
     * 悬赏频道----0：菜品研发更多----2：招聘更多----3：求职更多----4：门店转让更多----5：二手设备更多----6：加盟代理更多
     * NeedOfferedMoreThan
     *
     * @param XuQiuType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public static Subscription NeedOfferedMoreThan(HttpCallBack<DemandRewardClassifyHttpResponse> subscriber,
                                                   int XuQiuType,
                                                   int pageIndex,
                                                   int pageSize) {
        Observable<DemandRewardClassifyHttpResponse> observable = HttpManager.getInstance().serviceApi.NeedOfferedMoreThan(XuQiuType, pageIndex, pageSize);

        return toSubscribe(observable, subscriber);
    }

    //学习课堂列表
    public static Subscription studyClassroomList(HttpCallBack<StudyClassroomHttpResponse> subscriber,
                                                  int PageIndex,
                                                  int PageSize) {
        Observable<StudyClassroomHttpResponse> observable = HttpManager.getInstance().serviceApi.studyClassroomList(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }


    //解忧店铺-菜品秘籍
    public static Subscription mijiShouYe(HttpCallBack<HomeEsotericHttpResponse> subscriber) {
        Observable<HomeEsotericHttpResponse> observable = HttpManager.getInstance().serviceApi.mijiShouYe();
        return toSubscribe(observable, subscriber);
    }

    //解忧店铺-学习课堂
    public static Subscription studyClass(HttpCallBack<HomeStudyHttpResponse> subscriber) {
        Observable<HomeStudyHttpResponse> observable = HttpManager.getInstance().serviceApi.studyClass();
        return toSubscribe(observable, subscriber);
    }


    /**
     * 解忧店铺-学习课堂
     * <p>
     * 解忧店铺----视频直播课堂更多----语音直播课堂（群组课堂） ps:收费的哦 更多----线下课堂更多
     * <p>
     * LearClassMorethan
     * type:(0 视频直播课堂更多 1 语音直播课堂更多 2 线下课堂更多)
     * pageIndex,
     * pageSize
     *
     * @return
     */
    public static Subscription LearClassMorethan(HttpCallBack<HttpResult<List<StudyClassroomData>>> subscriber
            , int type, int pageIndex, int pageSize) {

        Observable<HttpResult<List<StudyClassroomData>>> observable = HttpManager.getInstance().serviceApi.LearClassMorethan(type, pageIndex, pageSize);
        return toSubscribe(observable, subscriber);
    }


    //解忧店铺-悬赏需求
    public static Subscription jieYouReward(HttpCallBack<HomeRewardHttpResponse> subscriber) {
        Observable<HomeRewardHttpResponse> observable = HttpManager.getInstance().serviceApi.jieYouReward();
        return toSubscribe(observable, subscriber);
    }


    //开放式-秘籍详情
    public static Subscription esotericDetails_k(HttpCallBack<EsotericDetails_k_HttpResponse> subscriber, int id) {
        Observable<EsotericDetails_k_HttpResponse> observable = HttpManager.getInstance().serviceApi.esotericDetails_k(id);
        return toSubscribe(observable, subscriber);
    }

    //线下课堂详情
    public static Subscription Ketang_Details(HttpCallBack<HttpResult<List<StudyClassroomDetailsData>>> subscriber, int id) {

        Observable<HttpResult<List<StudyClassroomDetailsData>>> observable = HttpManager.getInstance().serviceApi.Ketang_Details(id);

        return toSubscribe(observable, subscriber);
    }


    //需求悬赏----详情
    public static Subscription XuShang_XiangQing(HttpCallBack<HttpResult<List<DemandRewardDetailsData>>> subscriber, String id) {

        Observable<HttpResult<List<DemandRewardDetailsData>>> observable = HttpManager.getInstance().serviceApi.XuShang_XiangQing(id);

        return toSubscribe(observable, subscriber);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 产品商铺
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 个人中心
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * //上传开放式秘籍
     * <p>
     * <p>
     * type:类型，1为开放式，2为买断式
     * title:秘籍名称
     * tedian:特点
     * maidiantese:卖点特色
     * price:价格，免费的传0
     * cuisines_id:菜系，这个在效果图上没有体现出来，需要加上去的
     * img_url:主图，秘籍效果图，一张">url:主图，秘籍效果图，一张
     * shipin:视频，可选
     * <p>
     * 开放式需要传的参数:
     * zhuliao:主料，格式为:（名称:数量|名称:数量）
     * fuliao:辅料，格式为:（名称:数量|名称:数量）
     * gongyiliucheng:工艺流程
     * zhuyishixiang:注意事项
     * img_guocheng:过程图片，可为多张
     */
    public static Subscription issuerEsoteric_k(HttpProgressCallBack<BaseHttpResponse> subscriber,
                                                String type,
                                                String title,
                                                String tedian,
                                                String maidiantese,
                                                String price,
                                                String cuisines_id,
                                                String zhuliao,
                                                String fuliao,
                                                String gongyiliucheng,
                                                String zhuyishixiang,
                                                ArrayList<FileParam> img_guocheng

    ) {


//        List<FileParam> list = FileParam.getStringToFileParam("img_guocheng", img_guocheng);
//        FileParam.addFileParam(list, new FileParam("img_url", img_url));
//        FileParam.addFileParam(list, new FileParam("AudioUrl", AudioUrl));
//        FileParam.addFileParam(list, new FileParam("shipin", shipin));


        Observable<BaseHttpResponse> observable = HttpManager.getInstance().getServiceApi(subscriber).issuerEsoteric_k(
                getTextRequestBody(type),
                getTextRequestBody(title),
                getTextRequestBody(tedian),
                getTextRequestBody(maidiantese),
                getTextRequestBody(price),
                getTextRequestBody(cuisines_id),
                getTextRequestBody(zhuliao),
                getTextRequestBody(fuliao),
                getTextRequestBody(zhuyishixiang),
                getTextRequestBody(gongyiliucheng),
                getFileParamsRequestBody(img_guocheng)
        );
        return toSubscribe(observable, subscriber);
    }


    /**
     * 上传买断式秘籍
     * <p>
     * type:类型，1为开放式，2为买断式
     * title:秘籍名称
     * tedian:特点
     * maidiantese:卖点特色
     * price:价格，免费的传0
     * cuisines_id:菜系，这个在效果图上没有体现出来，需要加上去的
     * img_url:主图，秘籍效果图，一张">url:主图，秘籍效果图，一张
     * shipin:视频，可选
     * <p>
     * 买断式需要传的参数:
     * begin_date:开始时间，格式为2015-09-06
     * end_date:结束时间，格式为2015-09-06
     * pingjia:评价
     * maiduanliucheng:买断流程
     * maiduanshengming:买断声明
     * huanyingzongchoumaiduan:欢迎众筹买断
     * AudioUrl:音频文件
     */
    public static Subscription issuerEsoteric_D(HttpProgressCallBack<BaseHttpResponse> subscriber,
                                                String type,
                                                String title,
                                                String maidiantese,
                                                String price,
                                                String cuisines_id,
                                                String begin_date,
                                                String end_date,
                                                String pingjia,
                                                String maiduanliucheng,
                                                String maiduanshengming,
                                                String huanyingzongchoumaiduan,
                                                ArrayList<FileParam> list

    ) {

        Observable<BaseHttpResponse> observable = HttpManager.getInstance().getServiceApi(subscriber).issuerEsoteric_D(
                getTextRequestBody(type),
                getTextRequestBody(title),
                getTextRequestBody(maidiantese),
                getTextRequestBody(price),
                getTextRequestBody(cuisines_id),
                getTextRequestBody(begin_date),
                getTextRequestBody(end_date),
                getTextRequestBody(pingjia),
                getTextRequestBody(maiduanliucheng),
                getTextRequestBody(maiduanshengming),
                getTextRequestBody(huanyingzongchoumaiduan),
                getFileParamsRequestBody(list)
        );
        return toSubscribe(observable, subscriber);
    }

    /**
     * 上传课堂
     * address:开课地址
     * begin_time:开课时间，格式为yyyy-MM-dd HH:mm:ss
     * end_time:截止时间，格式为yyyy-MM-dd HH:mm:ss
     * price:价格，免费的为0
     * remark:课堂简介
     * title:标题
     * type:课堂形式，1为线下课堂，2为视频课堂
     * img_url:组图片，只有一张
     * img_guocheng:视频
     * content_img:图片
     * AudioUrl:音频文件
     * guanjianzi:关键字(多个关键字用,分割) timeRemark:时间备注
     *
     * @return
     */
    public static Subscription issuerClassroom(HttpProgressCallBack<BaseHttpResponse> subscriber,
                                               String address,
                                               String begin_time,
                                               String end_time,
                                               String price,
                                               String remark,
                                               String title,
                                               String type,
                                               String studentNum,
                                               ArrayList<FileParam> files
    ) {

        Observable<BaseHttpResponse> observable = HttpManager.getInstance().getServiceApi(subscriber).issuerClassroom(
                getTextRequestBody(address),
                getTextRequestBody(begin_time),
                getTextRequestBody(end_time),
                getTextRequestBody(price),
                getTextRequestBody(remark),
                getTextRequestBody(title),
                getTextRequestBody(type),
                getTextRequestBody(studentNum),
                getFileParamsRequestBody(files)
        );
        return toSubscribe(observable, subscriber);
    }


    /**
     * 发布秀场
     *
     * @param subscriber
     * @param phone      手机型号
     * @param position   地理位置
     * @param text       正文内容
     * @param files      图片集合
     * @return
     */
    public static Subscription dynamics_Add(HttpProgressCallBack<BaseHttpResponse> subscriber,
                                            String phone,
                                            String position,
                                            String text,
                                            ArrayList<FileParam> files
    ) {

        Observable<BaseHttpResponse> observable = HttpManager.getInstance().getServiceApi(subscriber).dynamics_Add(
                getTextRequestBody(phone),
                getTextRequestBody(position),
                getTextRequestBody(text),
                getFileParamsRequestBody(files)
        );
        return toSubscribe(observable, subscriber);
    }

    /**
     * 悬赏发布
     *
     * @Part("type") RequestBody type,
     * @Part("XuQiuContent") RequestBody XuQiuContent,
     * @Part("XunShangPrice") RequestBody XunShangPrice,
     * @Part("CityId") RequestBody CityId,
     * @Part("Address") RequestBody Address,
     * @Part("ShopingName") RequestBody ShopingName,
     * @Part("Company") RequestBody Company,
     * @PartMap Map<String, RequestBody> params
     */
    public static Subscription rewardIssuer(HttpCallBack<BaseHttpResponse> subscriber,
                                            String type,
                                            String XuQiuContent,
                                            String XunShangPrice,
                                            String CityId,
                                            String Address,
                                            String ShopingName,
                                            String Company,
                                            List<FileParam> files
    ) {

        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.rewardIssuer(
                getTextRequestBody(type),
                getTextRequestBody(XuQiuContent),
                getTextRequestBody(XunShangPrice),
                getTextRequestBody(CityId),
                getTextRequestBody(Address),
                getTextRequestBody(ShopingName),
                getTextRequestBody(Company),
                getFileParamsRequestBody(files));

        return toSubscribe(observable, subscriber);
    }


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页	GeRenZhuye	user_id：用户ID
     */

    public static Subscription GeRenZhuye(
            HttpCallBack<HttpResult<List<StudioData>>> subscriber,
            int userid
    ) {
        Observable<HttpResult<List<StudioData>>> observable = HttpManager.getInstance().serviceApi.GeRenZhuye(
                userid
        );
        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页----我的秀场列表	GetMydynamicsList	userid：用户ID
     */

    public static Subscription GetMydynamicsList(
            HttpCallBack<HttpResult<List<XiuChangItemData>>> subscriber,
            int userid,
            int PageIndex,
            int PageSize
    ) {
        Observable<HttpResult<List<XiuChangItemData>>> observable = HttpManager.getInstance().serviceApi.GetMydynamicsList(
                userid,
                PageIndex,
                PageSize
        );
        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页----我的秘籍	GetMyMijiList	userid：用户ID
     * type:1为我上传的秘籍，2为我购买的秘籍
     */
    public static Subscription GetMyMijiList(
            HttpCallBack<HttpResult<List<EsotericaData>>> subscriber,
            int userid,
            int type,
            int PageIndex,
            int PageSize
    ) {
        Observable<HttpResult<List<EsotericaData>>> observable = HttpManager.getInstance().serviceApi.GetMyMijiList(
                userid,
                type,
                PageIndex,
                PageSize
        );
        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/12 11:47
     * <p>
     * 方法功能：个人主页----我的课堂	GetMyKetangList	userid：用户ID
     */
    public static Subscription GetMyKetangList(
            HttpCallBack<HttpResult<List<StudyClassroomData>>> subscriber,
            int userid,
            int type,
            int PageIndex,
            int PageSize
    ) {
        Observable<HttpResult<List<StudyClassroomData>>> observable = HttpManager.getInstance().serviceApi.GetMyKetangList(
                userid,
                type,
                PageIndex,
                PageSize
        );
        return toSubscribe(observable, subscriber);
    }


    /**
     * 我----资讯和餐答 我的提问和我的偷听列表
     * ZiXunforMeTiWenAndTouTing
     * pageIndex：分页索引
     * pageSize:每页的条数
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    public static Subscription ZiXunforMeTiWenAndTouTing(
            HttpCallBack<HttpResult<List<MyAnswerEavesdropListData>>> subscriber, int PageIndex, int PageSize) {

        Observable<HttpResult<List<MyAnswerEavesdropListData>>> observable = HttpManager.getInstance().
                serviceApi.ZiXunforMeTiWenAndTouTing(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }

    /**
     * 我----资讯和餐答 我回答的问题列表	ZiXunforMeHuiDa
     * <p>
     * ZiXunforMeHuiDa
     * pageIndex：分页索引
     * pageSize:每页的条数
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    public static Subscription ZiXunforMeHuiDa(
            HttpCallBack<HttpResult<List<MyAnswerListData>>> subscriber, int PageIndex, int PageSize) {

        Observable<HttpResult<List<MyAnswerListData>>> observable = HttpManager.getInstance().
                serviceApi.ZiXunforMeHuiDa(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }


    /**
     * 我的订单	GetOrderList
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    public static Subscription GetOrderList(
            HttpCallBack<HttpResult<List<MeOrderData>>> subscriber, int PageIndex, int PageSize) {

        Observable<HttpResult<List<MeOrderData>>> observable = HttpManager.getInstance().
                serviceApi.GetOrderList(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }

    /**
     * 我的订单	GetOrderList
     *
     * @param PageIndex
     * @param PageSize
     * @return
     */
    public static Subscription MyDtXuShang(
            HttpCallBack<HttpResult<List<DemandRewardData>>> subscriber, int PageIndex, int PageSize) {

        Observable<HttpResult<List<DemandRewardData>>> observable = HttpManager.getInstance().
                serviceApi.MyDtXuShang(UserData.getUserData().getId(), PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }


    /**
     * 作者　　: 杨阳
     * 创建时间: 2016/9/22 14:40
     * 邮箱　　：360621904@qq.com
     * <p>
     * 功能介绍：获取余额明细	GetAccountDetail
     */
    public static Subscription GetAccountDetail(
            HttpCallBack<MeBalanceData> subscriber, int PageIndex, int PageSize) {

        Observable<MeBalanceData> observable = HttpManager.getInstance().
                serviceApi.GetAccountDetail(PageIndex, PageSize);

        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 16:13
     * <p>
     * 方法功能：更新个人中心背景图	UpdateHomeImg	img_url：图片地址
     */
    public static Subscription UpdateHomeImg(
            HttpCallBack<BaseHttpResponse> subscriber, String img_url) {

        Observable<BaseHttpResponse> observable = HttpManager.getInstance().
                serviceApi.UpdateHomeImg(img_url);

        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/21 16:13
     * <p>
     * 方法功能：个人中心背景图展示所有	GeRenBackgroundAll
     */
    public static Subscription GeRenBackgroundAll(
            HttpCallBack<HttpResult<List<StudioBeiJinData>>> subscriber) {

        Observable<HttpResult<List<StudioBeiJinData>>> observable = HttpManager.getInstance().
                serviceApi.GeRenBackgroundAll();

        return toSubscribe(observable, subscriber);
    }


    /**
     * 作者　　: 杨阳
     * 创建时间: 2016/9/22 14:40
     * 邮箱　　：360621904@qq.com
     * <p>
     * 功能介绍：个人简介展示
     * <p>
     * GeRenJianJie
     * userid:用户ID
     */
    public static Subscription GeRenJianJie(
            HttpCallBack<HttpResult<UserData>> subscriber, int userid) {

        Observable<HttpResult<UserData>> observable = HttpManager.getInstance().
                serviceApi.GeRenJianJie(userid);

        return toSubscribe(observable, subscriber);
    }


    /**
     * 作者　　: 杨阳
     * 创建时间: 2016/9/22 14:40
     * 邮箱　　：360621904@qq.com
     * <p>
     * 功能介绍：个人简介编辑
     * <p>
     * GeRenJianJieEdit
     * userid:用户ID
     * avatar:头像
     * UserName:昵称姓名
     * Age:年龄
     * Sex:性别
     * JobId:职位
     * Company:公司
     * city_id:城市 int
     * CuisinesId:擅长
     * Signfoods:招牌菜(经理人不需要传)
     * Workjingli:工作经历
     *
     */
    public static Subscription GeRenJianJieEdit(
            HttpCallBack<HttpResult<UserData>> subscriber,
            int userid,
            RequestBody avatar,
            String UserName,
            int Age,
            String Sex,
            int JobId,
            String Company,
            String city_id,
            String CuisinesId,
            String Signfoods,
            String Workjingli

    ) {

        Observable<HttpResult<UserData>> observable = HttpManager.getInstance().
                serviceApi.GeRenJianJieEdit(
                userid,
                avatar,
                UserName,
                Age,
                Sex,
                JobId,
                Company,
                city_id,
                CuisinesId,
                Signfoods,
                Workjingli
        );

        return toSubscribe(observable, subscriber);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                 公共
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //获取城市数据
    public static Subscription getCityData(HttpCallBack<CityHttpResponse> subscriber) {
        Observable<CityHttpResponse> observable = HttpManager.getInstance().serviceApi.getCityData();
        return toSubscribe(observable, subscriber);
    }

    //获取菜系
    public static Subscription GetCuisinesList(HttpCallBack<CaiXiHttpResponse> subscriber) {
        Observable<CaiXiHttpResponse> observable = HttpManager.getInstance().serviceApi.GetCuisinesList();
        return toSubscribe(observable, subscriber);
    }


    /**
     * 职位	GetJobList	pid:(0 大厨,1 掌柜)
     * @param subscriber
     * @return
     */
    public static Subscription GetJobList(HttpCallBack<CaiXiHttpResponse> subscriber,int pid) {
        Observable<CaiXiHttpResponse> observable = HttpManager.getInstance().serviceApi.GetJobList(pid);
        return toSubscribe(observable, subscriber);
    }




    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:18
     * <p>
     * 方法功能：关注
     *
     * @param useridToHe：我关注人的ID
     */
    public static Subscription Guanzhu(HttpCallBack<BaseHttpResponse> subscriber, int useridToHe) {
        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.Guanzhu(useridToHe);
        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:18
     * <p>
     * 方法功能：取消关注
     *
     * @param useridToHe：我关注人的ID
     */
    public static Subscription QuXiaoGuanzhu(HttpCallBack<BaseHttpResponse> subscriber, int useridToHe) {
        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.QuXiaoGuanzhu(useridToHe);
        return toSubscribe(observable, subscriber);
    }


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:18
     * <p>
     * 方法功能：点赞
     *
     * @param keyid: 主键
     * @param type:  类别 1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     */
    public static Subscription PointaPraise(HttpCallBack<BaseHttpResponse> subscriber,
                                            int keyid,
                                            ModeType type) {
        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.PointaPraise(keyid, type.getKey());
        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/14 16:58
     * <p>
     * 方法功能：评论	        PinglunAdd	keyid：主键
     * type:1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     * text:评论内容
     * 如果type=7,还需传参数touserid:对某人的评论
     */
    public static Subscription PinglunAddXiuChangDetail(HttpCallBack<BaseHttpResponse> subscriber,
                                                        int keyid,
                                                        ModeType type,
                                                        int touserid,
                                                        String text
    ) {
        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.PinglunAddXiuChangDetail(keyid, type.getKey(), touserid, text);
        return toSubscribe(observable, subscriber);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/14 16:58
     * <p>
     * 方法功能：评论	        PinglunAdd	keyid：主键
     * type:1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     * text:评论内容
     * 如果type=7,还需传参数touserid:对某人的评论
     */
    public static Subscription PinglunAdd(HttpCallBack<BaseHttpResponse> subscriber,
                                          int keyid,
                                          ModeType type,
                                          String text
    ) {
        Observable<BaseHttpResponse> observable = HttpManager.getInstance().serviceApi.PinglunAdd(keyid, type.getKey(), text);
        return toSubscribe(observable, subscriber);
    }


    //=========================================================================转换方法================================================================


    /**
     * String文本 转成RequestBody
     *
     * @param value
     * @return
     */

    public static RequestBody getTextRequestBody(String value) {
        if (value == null) {
            return null;
        }
        return RequestBody.create(MediaType.parse("text/plain" + "; charset=utf-8"), value);
    }


    /**
     * 文件 转成RequestBody
     *
     * @param file
     * @return
     */
    public static RequestBody getFileRequestBody(File file) {
        if (!file.exists()) {
            return null;
        }
        return RequestBody.create(MediaType.parse("multipart/form-data"), file);
    }

    /**
     * 文件路径 转成RequestBody
     *
     * @param filePath
     * @return
     */
    public static RequestBody getFileRequestBody(String filePath) {
        File file = new File(filePath);
        return getFileRequestBody(file);
    }


    /**
     * FileParam集合 转成Map
     *
     * @param params
     * @return
     */
    public static Map<String, RequestBody> getFileParamsRequestBody(List<FileParam>... params) {
        Map<String, RequestBody> map = new HashMap<>();
        for (List<FileParam> lp : params)
            for (FileParam p : lp) {
                if (p.exists()) {
                    map.put(p.getMapKey(), p.getFileRequestBody());
                }
            }
        return map;
    }

    public static Map<String, RequestBody> getFileParamsRequestBody(FileParam... params) {

        List<FileParam> a = new ArrayList();
        for (FileParam p : params) {
            if (p.exists()) {
                a.add(p);
            }
        }
        return getFileParamsRequestBody(a);
    }


    /**
     * String集合 转成Map
     *
     * @param paths
     * @return
     */
    public static Map<String, RequestBody> getFileStringsRequestBody(List<String> paths) {
        return getFileParamsRequestBody(FileParam.getStringToFileParam(paths));
    }

    /**
     * File集合 转成Map
     *
     * @param paths
     * @return
     */
    public static Map<String, RequestBody> getFilesRequestBody(List<File>... paths) {
        return getFileParamsRequestBody(FileParam.getFileToFileParam(paths));
    }

    /**
     * 单个String 转成Map
     *
     * @param paths
     * @return
     */
    public static Map<String, RequestBody> getFileStringRequestBody(String paths) {
        List<String> p = new ArrayList<>();
        p.add(paths);
        return getFileParamsRequestBody(FileParam.getStringToFileParam(p));
    }


}
