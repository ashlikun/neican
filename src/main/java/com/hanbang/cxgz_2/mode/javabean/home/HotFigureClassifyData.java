package com.hanbang.cxgz_2.mode.javabean.home;

/**
 * Created by yang on 2016/8/12.
 */
public class HotFigureClassifyData {
    private int guid;
    private String avatar;
    private String avatarimgSmallurl;
    private String user_name;
    private String jobCN;//职位名称
    private String company;//单位

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarimgSmallurl() {
        return avatarimgSmallurl;
    }

    public void setAvatarimgSmallurl(String avatarimgSmallurl) {
        this.avatarimgSmallurl = avatarimgSmallurl;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getJobCN() {
        return jobCN;
    }

    public void setJobCN(String jobCN) {
        this.jobCN = jobCN;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "HotFigure{" +
                "guid=" + guid +
                ", avatar='" + avatar + '\'' +
                ", avatarimgSmallurl='" + avatarimgSmallurl + '\'' +
                ", user_name='" + user_name + '\'' +
                ", jobCN='" + jobCN + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
