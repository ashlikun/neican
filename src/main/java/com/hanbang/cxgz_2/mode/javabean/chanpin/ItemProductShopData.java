package com.hanbang.cxgz_2.mode.javabean.chanpin;

/**
 * Information , 的实体类
 *
 * @author yang
 */

public class ItemProductShopData {
    private String title;//标题
    private String img_url;//封面图

    public ItemProductShopData(String img_url, String title) {
        this.title = title;
        this.img_url = img_url;
    }

    public String getTitle() {
        return title;
    }

    public String getImg_url() {
        return img_url;
    }
}
