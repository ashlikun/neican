package com.hanbang.cxgz_2.mode.javabean.about_me;

import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.io.Serializable;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/21 14:56
 * 邮箱　　：360621904@qq.com
 * <p>
 * <p>
 * 功能介绍：我的订单
 * <p>
 * <p>
 * "rownumber": 1,
 * "ShopingNum": "1316525101201608301638227989",
 * "id": 3139,
 * "type": 10,
 * "keyid": "3814",
 * "totalPrice": 2.99,
 * "shifoushoufei": true,
 * "status": 3,
 * "payment": 2,
 * "add_time": "\/Date(1472546302000)\/",
 * "pay_time": null,
 * "complete_time": null,
 * "fapiao_title": null,
 * "user_id": 745,
 * "buyUserPhone": null,
 * "sellUserID": "3814",
 * "sellUserPhone": null,
 * "shouhuoren": null,
 * "shouhuorenTel": null,
 * "shouhuorenAddress": null,
 * "shuliang": 1,
 * "shifoushouhuo": null,
 * "shouhuotime": null,
 * "iszhifu": 1,
 * "imgurl": null,
 * "title": null,
 * "damwei": null,
 * "shifoushouhuoCN": "未收货"
 */

public class MeOrderData implements Serializable {
    private String ShopingNum;//订单号
    private String add_time;//订单时间
    private String title;//标题
    private String totalPrice;//价格
    private String imgurl;//图片

    public String getShopingNum() {

        return StringUtils.isNullToConvert("订单编号:" + ShopingNum);
    }

    public String getAdd_time() {

        return StringUtils.isNullToConvert("订单时间:" + DateUtils.getTime(add_time));
    }

    public String getTitle() {
        return StringUtils.isNullToConvert(title);
    }

    public String getTotalPrice() {
        return StringUtils.isNullToConvert(totalPrice);
    }

    public String getImgurl() {
        return StringUtils.isNullToConvert(imgurl);
    }
}
