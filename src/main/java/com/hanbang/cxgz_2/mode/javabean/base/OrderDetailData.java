package com.hanbang.cxgz_2.mode.javabean.base;

/**
 * Created by Administrator on 2016/5/16.
 */
public class OrderDetailData {

    private String IDD;
    private String routeTitle;
    private String PlatformBillNumber;
    private int WebCompanyID;//
    private int WebCompanyID2;//

    private int ALCust;//全部人数
    private int BanTuoCust;//同意人数

    private String TourCopTrueStatus;//旅行社状态   NO_QUEREN(0, "未确认"), YES(1, "已确认"), QUXIAO(2, "已取消"), WEIZHI(-100, "");
    private String nmam;//司机状态  0:已确认，其他为未确认
    private String carBesure;//


    private String RealName;//旅行社联系人名字
    private String Telphone;//旅行社联系人电话

    private int SijiNum;//司机留言
    private int TravNum;//旅行社留言

    private String CompanyName;//旅行社名字
    private String UseCarStartTime;

    public String getIDD() {
        return IDD;
    }

    public String getRouteTitle() {
        return routeTitle;
    }

    public String getPlatformBillNumber() {
        return PlatformBillNumber;
    }

    public int getWebCompanyID() {
        return WebCompanyID;
    }

    public int getWebCompanyID2() {
        return WebCompanyID2;
    }

    public int getALCust() {
        return ALCust;
    }

    public int getBanTuoCust() {
        return BanTuoCust;
    }

    public String getTourCopTrueStatus() {
        return TourCopTrueStatus;
    }

    public String getNmam() {
        return nmam;
    }

    public String getCarBesure() {
        return carBesure;
    }

    public String getRealName() {
        return RealName;
    }

    public String getTelphone() {
        return Telphone;
    }

    public int getSijiNum() {
        return SijiNum;
    }

    public int getTravNum() {
        return TravNum;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public String getUseCarStartTime() {
        return UseCarStartTime;
    }
}
