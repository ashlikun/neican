package com.hanbang.cxgz_2.mode.javabean.about_me;


import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.ArrayList;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 15:02
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的余额实体类
 * <p>
 * "AccountAll": 0.0081,
 * "YongJin": 0.19,
 * "TiXianRemark": "提现金额为扣除平台佣金之后的余额，预计7个工作日到账。为了方便转账给您，提现金额需大于10元方可提现，如有疑问请在我的好友处给客服留言!",
 * "YongJinRemark": "余额数量为平台收取19%服务费以后的金额。服务费包含税费、腾讯及支付宝官方费率、技术支持等。"
 */

public class MeBalanceData extends BaseHttpResponse{
    private double AccountAll;//余额
    private double YongJin;//佣金
    private String TiXianRemark;//提现提示
    private String YongJinRemark;//佣金说明
    private ArrayList<Item> list;


    public String getAccountAll() {
        return StringUtils.isNullToConvert(AccountAll);
    }

    public String getYongJin() {
        return StringUtils.isNullToConvert(YongJin);
    }

    public String getTiXianRemark() {
        return TiXianRemark;
    }

    public String getYongJinRemark() {
        return YongJinRemark;
    }

    public ArrayList<Item> getList() {
        return list;
    }

    public static class Item {
        private double price;
        private String cjsj;   //时间
        private String bankuaiCN;//来源

        public String getPrice() {
            return StringUtils.isNullToConvert(price);
        }

        public String getCjsj() {
            return StringUtils.isNullToConvert(DateUtils.getTime(cjsj));
        }

        public String getBankuaiCN() {
            return StringUtils.isNullToConvert(bankuaiCN);
        }

    }


}
