package com.hanbang.cxgz_2.mode.javabean.can_da;


import com.hanbang.cxgz_2.mode.enumeration.FigureClassifyEnum;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 17:00
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：餐答分类
 */

public class MealAnswerClassifyData {
    private int pictureId;//图片id
    private FigureClassifyEnum classify;


    public MealAnswerClassifyData(int pictureId, FigureClassifyEnum classify) {
        this.pictureId = pictureId;
        this.classify = classify;
    }

    public int getPictureId() {
        return pictureId;
    }

    public FigureClassifyEnum getClassify() {
        return classify;
    }
}
