package com.hanbang.cxgz_2.mode.javabean.other;

/**
 * 菜系
 */
public class CaiXiData extends Object {
    /**
     * 内容Id
     */
    private int id;
    /**
     * 选择的内容
     */
    private String title;
    /**
     * 是否被选择
     */
    private boolean isSelect = false;

    public CaiXiData(int id, String title) {
        super();
        this.id = id;
        this.title = title;


    }


    public boolean isSelect() {
        return isSelect;
    }

    public void setIsSelect(boolean isSelect) {
        this.isSelect = isSelect;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
