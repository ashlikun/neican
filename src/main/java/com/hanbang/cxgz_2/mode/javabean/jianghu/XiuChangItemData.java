package com.hanbang.cxgz_2.mode.javabean.jianghu;

import com.google.gson.annotations.Expose;
import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.List;

/**
 * Created by Administrator on 2016/8/25.
 */

public class XiuChangItemData {
    /**
     * "rownumber": 1,
     * "id": 2613,
     * "uid": 777,
     * "text": "泳嶠伦敦行",
     * "add_time": "\/Date(1472079530273)\/",
     * "sharecount": 0,
     * "position": "",
     * "phone": "iPhone 6Plus",
     * "OrderIndex": 999999999999999999,
     * "fensiCount": 148,
     * "pinglunCount": 1,
     * "dianzanCount": 2,
     * "user_name": "陈泳峤",
     * "avatar": "/upload/201608/07/big/head/201608071607466997.png",
     * "avatarimgMidurl": "/upload/201608/07/middle/head/201608071607466997_Mid.png",
     * "avatarimgSmallurl": "/upload/201608/07/small/head/201608071607466997_Sma.png",
     * "is_vip": true,
     * "job_CN": "总经理",
     * "company": "德庄集团雨情调餐饮管理有限公司",
     * "isguanzhu": false,
     * "list_img": [{
     * "id": 9093,
     * }, {
     */

    List<ImageItemData> list_img;

    int id;
    int uid;


    int fensiCount;
    int pinglunCount;
    int dianzanCount;

    @Expose(deserialize = false, serialize = false)
    boolean clickDianzhan = false;
    @Expose(deserialize = false, serialize = false)
    boolean clickGuanzhu = false;
    @Expose(deserialize = false, serialize = false)
    boolean clickPinglun = false;

    int sharecount;
    boolean is_vip;

    String avatar;
    String position;//地址
    String user_name;
    String job_CN;//
    String company;//公司
    String add_time;//时间
    String text;//内容

    boolean isguanzhu;

    public List<ImageItemData> getList_img() {
        return list_img;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUser_name() {
        return StringUtils.isNullToConvert(user_name);
    }

    public String getJob_CN() {
        return job_CN;
    }

    public int getId() {
        return id;
    }

    public int getUid() {
        return uid;
    }

    public int getFensiCount() {
        return fensiCount;
    }

    public int getPinglunCount() {
        return pinglunCount;
    }

    public int getDianzanCount() {
        return dianzanCount;
    }

    public int getSharecount() {
        return sharecount;
    }

    public boolean is_vip() {
        return is_vip;
    }

    public String getPosition() {
        return StringUtils.isNullToConvert(position);
    }

    public String getCompany() {
        return company;
    }

    public String getAdd_time() {
        return add_time;
    }

    public String getText() {
        return StringUtils.isNullToConvert(text);
    }

    public String getZihweiInfo() {
        return StringUtils.isNullToConvert(job_CN) + " | " + StringUtils.isNullToConvert(company);
    }


    public String getTime() {
        return DateUtils.getTime(add_time);
    }


    public boolean isClickDianzhan() {
        return clickDianzhan;
    }

    public void setClickDianzhan(boolean clickDianzhan) {
        this.clickDianzhan = clickDianzhan;
    }

    public void setClickPinglun(boolean clickPinglun) {
        this.clickPinglun = clickPinglun;
    }

    public boolean isClickPinglun() {
        return clickPinglun;
    }

    public void addDianZhan() {
        dianzanCount += 1;
        setClickDianzhan(true);
    }

    public void addPingLun() {
        pinglunCount += 1;
        setClickPinglun(true);
    }

    public void setIsguanzhu(boolean isguanzhu) {
        this.isguanzhu = isguanzhu;
        clickGuanzhu = true;
    }

    public boolean isguanzhu() {
        return isguanzhu;
    }

    public boolean isClickGuanzhu() {
        return clickGuanzhu;
    }

    public void setClickGuanzhu(boolean clickGuanzhu) {
        this.clickGuanzhu = clickGuanzhu;
    }


    public String guanzhuText() {
        return GuanZhu.getState(isguanzhu).getValuse();
    }

    public int getGuanzhuRes() {
        return isguanzhu ? R.drawable.guanzhu_yes : R.drawable.guanzhu_no;

    }
}
