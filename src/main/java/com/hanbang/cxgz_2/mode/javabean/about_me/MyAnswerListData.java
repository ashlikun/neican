package com.hanbang.cxgz_2.mode.javabean.about_me;

/**
 * 我回答的实体类
 */
public class MyAnswerListData {
    //公共
    private String ShopingNum;//订单号
    private String TiWenQuestion;//提问的问题
    private String IsHuiDaCN;//回答状态
    private String TiWenCjsj;//提问时间
    private int IsHuiDa;//0是未回答，1已回答，2已过期
    private String HuiDaResultUrl;//音频的路径
    private long HuiDaSoundTime;//录音的时间
    private boolean playStatus = false;//判断是不是正在播放
    private double TiWenZhiFuMoney;//提问的价格
    private String MuKuaiKeyi;//文章id
    private String WenZhangBiaoTi;//文章标题

    //提问人
    private int TiWenUserID;//提问人id
    private String tw_user_name;//提问人名字
    private String tw_avatar;//提问头像大图
    private String tw_avatarimgSmallurl;//提问人小图

    //回答人
    private int HuiDaUserID;//回答人id
    private String hd_avatar;//回答人头像大图
    private String hd_avatarimgSmallurl;//回答人头像小图


    public String getShopingNum() {
        return ShopingNum;
    }

    public String getTiWenQuestion() {
        return TiWenQuestion;
    }


    public String getIsHuiDaCN() {
        return IsHuiDaCN;
    }

    public String getTiWenCjsj() {
        return TiWenCjsj;
    }

    //0是未回答，1已回答，2已过期
    public boolean getIsHuiDa() {
        if (IsHuiDa == 0) {
            return false;

        } else if (IsHuiDa == 2) {
            return false;

        } else {
            return true;
        }
    }

    public String getHuiDaResultUrl() {
        return HuiDaResultUrl;
    }

    public long getHuiDaSoundTime() {
        return HuiDaSoundTime;
    }

    public boolean isPlayStatus() {
        return playStatus;
    }

    public double getTiWenZhiFuMoney() {
        return TiWenZhiFuMoney;
    }

    public String getMuKuaiKeyi() {
        return MuKuaiKeyi;
    }

    public String getWenZhangBiaoTi() {
        return WenZhangBiaoTi;
    }

    public int getTiWenUserID() {
        return TiWenUserID;
    }

    public String getTw_user_name() {
        return tw_user_name;
    }

    public String getTw_avatar() {
        return tw_avatar;
    }

    public String getTw_avatarimgSmallurl() {
        return tw_avatarimgSmallurl;
    }

    public int getHuiDaUserID() {
        return HuiDaUserID;
    }

    public String getHd_avatar() {
        return hd_avatar;
    }

    public String getHd_avatarimgSmallurl() {
        return hd_avatarimgSmallurl;
    }
}
