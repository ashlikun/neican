package com.hanbang.cxgz_2.mode.javabean.home;

import android.text.TextUtils;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.enumeration.RewardTypeEnum;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.ArrayList;

import static com.hanbang.cxgz_2.utils.other.StringUtils.transitionPicture;

/**
 * Created by yang on 2016/8/12.
 */
public class DemandRewardData {
    private String guid;//id
    private String ID;//id
    private int userid;//发布人的id
    private String avatar;//发布人的头像
    private String user_name;//姓名
    private String jobCN;//职位
    private String company;//单位
    private String XuQiuContent;//悬赏内容
    private int JoinCount;//参与人数
    private String cjsj;//发布日期
    private int status;//悬赏状态,2进行中，3结束
    private int XuQiuType;//需求分类
    private double XunShangPrice;//悬赏价格
    private String XuQiuImg;//过程图片

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getJobCN() {
        return jobCN;
    }

    public void setJobCN(String jobCN) {
        this.jobCN = jobCN;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getXuQiuContent() {
        return XuQiuContent;
    }

    public void setXuQiuContent(String xuQiuContent) {
        XuQiuContent = xuQiuContent;
    }

    public int getJoinCount() {
        return JoinCount;
    }

    public void setJoinCount(int joinCount) {
        JoinCount = joinCount;
    }

    public String getCjsj() {
        if (TextUtils.isEmpty(cjsj)) {
            return "";
        } else {
            return DateUtils.getTime(cjsj);
        }

    }

    public void setCjsj(String cjsj) {
        this.cjsj = cjsj;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getXunShangPrice() {
        return XunShangPrice;
    }

    public void setXunShangPrice(double xunShangPrice) {
        XunShangPrice = xunShangPrice;
    }

    public String getID() {
        return ID;
    }

    public String getXuQiuImg() {
        if (!StringUtils.isBlank(XuQiuImg)) {
            ArrayList<String> str = transitionPicture(XuQiuImg);
            return str.get(0);
        } else {
            return avatar;
        }


    }


    public RewardTypeEnum getXuQiuType() {
        return RewardTypeEnum.getState(XuQiuType);
    }


    public int getLayoutId() {
        if (getXuQiuType() == RewardTypeEnum.CAI_PIN_YAN_FA) {
            return R.layout.item_homepage_reward;
        } else if (getXuQiuType() == RewardTypeEnum.DIAN_PU_ZHUAN_RANG ||
                getXuQiuType() == RewardTypeEnum.ER_SHOU_SHE_BEI ||
                getXuQiuType() == RewardTypeEnum.JIA_MENG_DAI_LI
                ) {
            return R.layout.item_demand_reward_h;
        } else if (getXuQiuType() == RewardTypeEnum.ZHAO_PIN ||
                getXuQiuType() == RewardTypeEnum.QIU_ZHI
                ) {
            return R.layout.item_homepage_reward_zao_pin;
        }
        return R.layout.item_demand_reward_h;
    }
}
