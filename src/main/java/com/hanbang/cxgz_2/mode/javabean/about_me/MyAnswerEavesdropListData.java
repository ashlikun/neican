package com.hanbang.cxgz_2.mode.javabean.about_me;

import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * 我提问的与我回答的
 */
public class MyAnswerEavesdropListData {
    //公共
    private String ShopingNum;//订单号
    private String TiWenQuestion_tw;//提问的问题
    private String cjsj;//提问时间
    private int IsHuiDa;//0是未回答，1已回答,2已过期
    private String HuiDaResultUrl;//音频的路径
    private int HuiDaSoundTime;//回答的时间（音频的长短）
    private String twOrTt;//提问或者偷听
    private String IsHuiDaCN;//回答提示  
    private String MuKuaiKeyi;//文章id
    private String WenZhangBiaoTi;//文章标题


    //提问人
    private int TiWenUserID_tw;//提问人id
    private String UserName_tw;//提问人名字
    private String avatar_tw;//提问头像大图
    private String avatarimgSmallurl_tw;//提问人小图
    private double TiWenZhiFuMoney_tw;//提问的价格


    //回答人
    private int HuiDaUserID_hd;//回答人id
    private String avatar_hd;//回答人头像大图
    private String avatarimgSmallurl_hd;//回答人头像小图


    public String getShopingNum() {
        return StringUtils.isNullToConvert(ShopingNum);
    }

    public String getTiWenQuestion_tw() {
        return StringUtils.isNullToConvert(TiWenQuestion_tw);
    }

    public String getCjsj() {

        return StringUtils.isNullToConvert(DateUtils.getTime(cjsj));
    }


    /**
     * 0是未回答，1已回答,2已过期
     *
     * @return
     */
    public boolean getIsHuiDa() {
        if (IsHuiDa == 0) {
            return false;

        } else if (IsHuiDa == 2) {
            return false;

        } else {
            return true;
        }

    }


    /**
     * 状态
     *
     * @return
     */
    public String getStatus() {
        if (IsHuiDa == 0) {
            return "未回答";

        } else if (IsHuiDa == 2) {
            return "已过期";

        } else {
            return twOrTt;
        }

    }


    public String getHuiDaResultUrl() {
        return StringUtils.isNullToConvert(HuiDaResultUrl);
    }

    public String getHuiDaSoundTime() {


        return DateUtils.showEavesdropTime(HuiDaSoundTime);
    }

    public String getTwOrTt() {
        return StringUtils.isNullToConvert(twOrTt);
    }

    public String getIsHuiDaCN() {
        return StringUtils.isNullToConvert(IsHuiDaCN);
    }


    public String getMuKuaiKeyi() {
        return StringUtils.isNullToConvert(MuKuaiKeyi);
    }

    public String getWenZhangBiaoTi() {
        return StringUtils.isNullToConvert(WenZhangBiaoTi);
    }

    public int getTiWenUserID_tw() {
        return TiWenUserID_tw;
    }

    public String getUserName_tw() {
        return StringUtils.isNullToConvert(UserName_tw);
    }

    public String getAvatar_tw() {
        return StringUtils.isNullToConvert(avatar_tw);
    }

    public String getAvatarimgSmallurl_tw() {
        return StringUtils.isNullToConvert(avatarimgSmallurl_tw);
    }

    public String getTiWenZhiFuMoney_tw() {
        return StringUtils.isNullToConvert(TiWenZhiFuMoney_tw);
    }

    public int getHuiDaUserID_hd() {
        return HuiDaUserID_hd;
    }

    public String getAvatar_hd() {
        return StringUtils.isNullToConvert(avatar_hd);
    }

    public String getAvatarimgSmallurl_hd() {
        return StringUtils.isNullToConvert(avatarimgSmallurl_hd);
    }
}
