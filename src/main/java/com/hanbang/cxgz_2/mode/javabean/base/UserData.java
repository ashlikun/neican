package com.hanbang.cxgz_2.mode.javabean.base;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.ActivityManager;
import com.hanbang.cxgz_2.utils.db.LiteOrmUtil;
import com.hanbang.cxgz_2.view.login.activity.LoginActivity;
import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.Ignore;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.assit.QueryBuilder;
import com.litesuits.orm.db.assit.WhereBuilder;
import com.litesuits.orm.db.enums.AssignType;
import com.litesuits.orm.db.model.ColumnsValue;
import com.litesuits.orm.db.model.ConflictAlgorithm;

import static com.hanbang.cxgz_2.appliction.Global.LOGIN_BROADCAST;
import static com.hanbang.cxgz_2.appliction.MyApplication.myApp;


/**
 * 用户个人信息
 */
@Table("UserData")
public class UserData  {
    //用户对象
    @Ignore
    public static UserData userData;

    @PrimaryKey(AssignType.BY_MYSELF)
    @Column("id")
    private int id;
    //是否是当前登录的用户（这样就不用sb保存用户ID）
    @Column("isLogin")
    private boolean isLogin;

    @Column("telphone")
    private String telphone;            //手机事情
    @Column("password")
    private String password;            //密码
    @Column("email")
    private String email;               //邮箱
    @Column("avatar")
    private String avatar;              //头像
    @Column("user_name")
    private String user_name;           //姓名
    @Column("sex")
    private int sex;                    //性别
    @Column("age")
    private int age;
    @Column("job_id")
    private int job_id;
    @Column("job")
    private String job;
    @Column("job_CN")
    private String job_CN;
    @Column("cuisines_id")
    private String cuisines_id;
    @Column("cuisines_name")
    private String cuisines_name;
    @Column("province_id")
    private String province_id;
    @Column("city_id")
    private int city_id;
    @Column("district_id")
    private String district_id;
    @Column("address")
    private String address;             //地址
    @Column("signfoods")
    private String signfoods;
    @Column("grade")
    private int grade;
    @Column("company")
    private String company;             //公司名称
    @Column("group_id")
    private int group_id;               //类别:1厨师，2经理
    @Column("is_vip")
    private boolean is_vip;             //是不是vip
    @Column("add_time")
    private String add_time;
    @Column("vip_time")
    private String vip_time;
    @Column("reg_ip")
    private String reg_ip;
    @Column("point")
    private int point;
    @Column("amount")
    private String amount;              //作者
    @Column("exp")
    private int exp;
    @Column("token")
    private String token;               //融云token
    @Column("SuccessID")
    private String SuccessID;           //登录Id
    @Column("CanDaJianJie")
    private String CanDaJianJie;        //餐答的简介
    @Column("CanDaKaiQi")
    private int CanDaKaiQi;             //是否开启餐答：0为不开启，1主开启
    @Column("CanDaTiWenMoney")
    private double CanDaTiWenMoney;     //餐答的提问价格


    public static UserData getDbUserData() {
        try {
            userData = LiteOrmUtil.getLiteOrm().query(QueryBuilder.create(UserData.class).where("isLogin=?", true).limit(0, 1)).get(0);
            return userData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 直接判断是否登录
     *
     * @return
     */
    public static boolean isSLogin() {
        return getUserData() == null ? false : getUserData().isLogin;
    }


    public static boolean clearLogin() {
        int res = LiteOrmUtil.getLiteOrm().update(WhereBuilder.create(UserData.class).where("isLogin=?", true), new ColumnsValue(new String[]{"isLogin"}, new Boolean[]{false}), ConflictAlgorithm.None);
        userData = null;
        myApp.sendBroadcast(new Intent(LOGIN_BROADCAST));
        if (res <= 0) {
            return false;
        }
        return true;
    }

    /**
     * 退出登录
     * 吧数据库标识改成false 标记全部没有登录状态
     */
    public static boolean exitLogin(Context context) {
        //清除其他登录的用户
        boolean a = clearLogin();
        if (!a) {

        }
        ActivityManager.getInstance().exitAllActivity();
        // 返回登录页面
        LoginActivity.startUI(context, true);
        return true;
    }

    /**
     * 退出登录,对话框
     */
    public static void exit(final Context context) {

        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("提示")
                .content("确定退出当前账户吗？")
                .positiveText("残忍退出")
                .negativeText("继续使用")
                .negativeColorRes(R.color.gray_text)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        exitLogin(context);
                    }
                })
                .build();

        dialog.show();


    }


    /**
     * 保存数据到数据库（）
     *
     * @param userData 被保存的数据，这个数据会被默认成数据库的唯一登录数据
     * @return
     */
    public static boolean setDbUserData(UserData userData) {
        if (userData != null) {
            try {
                //清除其他登录的用户
                LiteOrmUtil.getLiteOrm().update(WhereBuilder.create(UserData.class).where("isLogin=?", true), new ColumnsValue(new String[]{"isLogin"}, new Boolean[]{false}), ConflictAlgorithm.None);
                //设置数据库当前登录的用户
                userData.setLogin(true);
                LiteOrmUtil.getLiteOrm().save(userData);
                UserData.userData = userData;
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isLogin() {
        return isLogin;
    }


    public void setLogin(boolean login) {
        isLogin = login;
    }


    @Override
    public String toString() {
        return "UserData{" +
                "id=" + id +
                ", isLogin=" + isLogin +
                ", telphone='" + telphone + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", avatar='" + avatar + '\'' +
                ", user_name='" + user_name + '\'' +
                ", sex=" + sex +
                ", age=" + age +
                ", job_id=" + job_id +
                ", job='" + job + '\'' +
                ", job_CN='" + job_CN + '\'' +
                ", cuisines_id='" + cuisines_id + '\'' +
                ", cuisines_name='" + cuisines_name + '\'' +
                ", province_id='" + province_id + '\'' +
                ", city_id=" + city_id +
                ", district_id='" + district_id + '\'' +
                ", address='" + address + '\'' +
                ", signfoods='" + signfoods + '\'' +
                ", grade=" + grade +
                ", company='" + company + '\'' +
                ", group_id=" + group_id +
                ", is_vip=" + is_vip +
                ", add_time='" + add_time + '\'' +
                ", vip_time='" + vip_time + '\'' +
                ", reg_ip='" + reg_ip + '\'' +
                ", point=" + point +
                ", amount='" + amount + '\'' +
                ", exp=" + exp +
                ", token='" + token + '\'' +
                ", SuccessID='" + SuccessID + '\'' +
                ", CanDaJianJie='" + CanDaJianJie + '\'' +
                ", CanDaKaiQi=" + CanDaKaiQi +
                ", CanDaTiWenMoney=" + CanDaTiWenMoney +
                '}';
    }

    public static UserData getUserData() {
        return userData == null ? getDbUserData() : userData;
    }

    public String getTelphone() {
        return telphone;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUser_name() {
        return user_name;
    }

    public int getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public int getJob_id() {
        return job_id;
    }

    public String getJob() {
        return job;
    }

    public String getJob_CN() {
        return job_CN;
    }

    public String getCuisines_id() {
        return cuisines_id;
    }

    public String getCuisines_name() {
        return cuisines_name;
    }

    public String getProvince_id() {
        return province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public String getAddress() {
        return address;
    }

    public String getSignfoods() {
        return signfoods;
    }

    public int getGrade() {
        return grade;
    }

    public String getCompany() {
        return company;
    }

    public int getGroup_id() {
        return group_id;
    }

    public boolean is_vip() {
        return is_vip;
    }

    public String getAdd_time() {
        return add_time;
    }

    public String getVip_time() {
        return vip_time;
    }

    public String getReg_ip() {
        return reg_ip;
    }

    public int getPoint() {
        return point;
    }

    public String getAmount() {
        return amount;
    }

    public int getExp() {
        return exp;
    }

    public String getToken() {
        return token;
    }

    public String getSuccessID() {
        return SuccessID;
    }

    public String getCanDaJianJie() {
        return CanDaJianJie;
    }

    public int getCanDaKaiQi() {
        return CanDaKaiQi;
    }

    public double getCanDaTiWenMoney() {
        return CanDaTiWenMoney;
    }
}
