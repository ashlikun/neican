package com.hanbang.cxgz_2.mode.javabean.about_me;


import com.hanbang.cxgz_2.mode.enumeration.AboutMeTypeEnum;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 13:05
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：个人中心的模块类型,实体对象
 */

public class AboutMeTypeData {
    AboutMeTypeEnum type;


    public AboutMeTypeData(AboutMeTypeEnum type) {
        this.type = type;
    }

    public AboutMeTypeEnum getType() {
        return type;
    }

    public void setType(AboutMeTypeEnum type) {
        this.type = type;
    }

    public boolean showHint() {

        if (type.getInformationNum() == 0) {
            return false;
        }else {
            return true;
        }
    }
}
