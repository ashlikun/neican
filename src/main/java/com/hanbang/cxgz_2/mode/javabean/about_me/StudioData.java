package com.hanbang.cxgz_2.mode.javabean.about_me;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * Created by Administrator on 2016/8/25.
 */

public class StudioData {
    /**
     * "id": 757,
     * "telphone": "18611447418",
     * "avatar": "/upload/201608/19/big/head/201608191243328569.jpg",
     * "avatarimgMidurl": "/upload/201608/19/middle/head/201608191243328569_Mid.jpg",
     * "avatarimgSmallurl": "/upload/201608/19/small/head/201608191243328569_Sma.jpg",
     * "user_name": "尔红林",
     * "job_id": 53,
     * "jobCN": "技术研发",
     * "group_id": 1,
     * "cuisines_id": "9",
     * "address": "北京东城区东中街9号东环广场A座",
     * "company": "雀巢（中国）有限公司北京分公司",
     * "is_vip": true,
     * "home_img": "",
     * "membershipcount": 136,
     * "mijicount": 7,
     * "ketangcount": 0,
     * "xuexiziliaocount": 4,
     * "xiuchangcount": 1,
     * "isguanzhu": false
     */

    int id;

    String avatarimgMidurl;//头像
    String user_name;
    String jobCN;//职位名称
    int group_id;
    int membershipcount;//粉丝数量
    String address;//地址
    String company;//公司名称
    String home_img;//背景图
    boolean is_vip;//是否是vip
    boolean isguanzhu;//是否关注


    public int getId() {
        return id;
    }

    public String getAvatarimgMidurl() {
        return avatarimgMidurl;
    }

    public String getUser_name() {
        return StringUtils.isNullToConvert(user_name) + "工作室";
    }

    public String getJobCN() {
        return StringUtils.isNullToConvert(jobCN);
    }

    public int getGroup_id() {
        return group_id;
    }

    public String getMembershipcount() {
        return String.valueOf(membershipcount);
    }

    public String getAddress() {
        return address;
    }

    public String getCompany() {
        return StringUtils.isNullToConvert(company);
    }

    public String getHome_img() {
        return home_img;
    }

    public boolean is_vip() {
        return is_vip;
    }

    public boolean isguanzhu() {
        return isguanzhu;
    }

    public String guanzhuText() {
        return GuanZhu.getState(isguanzhu).getValuse();
    }

    public int getGuanzhuRes() {
        return isguanzhu ? R.drawable.guanzhu_yes : R.drawable.guanzhu_no_white;

    }

    public void setIsguanzhu(boolean isguanzhu) {
        this.isguanzhu = isguanzhu;
    }
}
