package com.hanbang.cxgz_2.mode.javabean.jianghu;

import com.hanbang.cxgz_2.mode.javabean.home.ChefData;

import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/20　14:36
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class ChuShiItemData {


    /**
     * title : 活跃厨星
     * biaoqian : 1
     * PandaCount : 0
     * Age :
     * Content : [{"id":757,"telphone":"18611447418","avatar":"/upload/201608/19/big/head/201608191243328569.jpg","avatarimgMidurl":"/upload/201608/19/middle/head/201608191243328569_Mid.jpg","avatarimgSmallurl":"/upload/201608/19/small/head/201608191243328569_Sma.jpg","user_name":"尔红林","signfoods":"御锅传膳 王府大花参","cuisines_id":"9","company":"雀巢（中国）有限公司北京分公司","job":"技术研发","is_vip":true,"is_shoutu":false,"orderindex":9999,"add_time":"/Date(1462859748000)/","yaoqingNumber":"","membershipcount":141,"mijicount":7,"ketangcount":0,"xuexiziliaocount":4,"isguanzhu":false}]
     */

    private String title;
    private int biaoqian;
    private int PandaCount;
    private String Age;
    /**
     * id : 757
     * telphone : 18611447418
     * avatar : /upload/201608/19/big/head/201608191243328569.jpg
     * avatarimgMidurl : /upload/201608/19/middle/head/201608191243328569_Mid.jpg
     * avatarimgSmallurl : /upload/201608/19/small/head/201608191243328569_Sma.jpg
     * user_name : 尔红林
     * signfoods : 御锅传膳 王府大花参
     * cuisines_id : 9
     * company : 雀巢（中国）有限公司北京分公司
     * job : 技术研发
     * is_vip : true
     * is_shoutu : false
     * orderindex : 9999
     * add_time : /Date(1462859748000)/
     * yaoqingNumber :
     * membershipcount : 141
     * mijicount : 7
     * ketangcount : 0
     * xuexiziliaocount : 4
     * isguanzhu : false
     */

    private List<ChefData> Content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getBiaoqian() {
        return biaoqian;
    }

    public void setBiaoqian(int biaoqian) {
        this.biaoqian = biaoqian;
    }

    public int getPandaCount() {
        return PandaCount;
    }

    public void setPandaCount(int PandaCount) {
        this.PandaCount = PandaCount;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String Age) {
        this.Age = Age;
    }

    public List<ChefData> getContent() {
        return Content;
    }

    public void setContent(List<ChefData> Content) {
        this.Content = Content;
    }


}
