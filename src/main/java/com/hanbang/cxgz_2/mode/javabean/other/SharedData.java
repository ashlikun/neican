package com.hanbang.cxgz_2.mode.javabean.other;

import android.os.Parcel;
import android.os.Parcelable;

import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.http.HttpManager;
import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/20 11:05
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：分享的实体类
 */

public class SharedData implements Parcelable {
    //分享的类型
    private int type;
    //分享内容的ID
    private String id;
    //标题
    private String title;
    //内容
    private String content;
    //点击跳转的地址
    private String clickUrl;
    //图片的地址,默认的是logo地址
    private String imageUrl;




    public String getId() {
        return id;
    }


    public int getType() {
        return type;
    }


    public String getTitle() {
        return StringUtils.isNullToConvert(title, "暂无标题");
    }

    public String getContent() {
        return StringUtils.isNullToConvert(content, "暂无内容");
    }

    public String getClickUrl() {
        return StringUtils.isNullToConvert(HttpLocalUtils.getHttpFileUrl(clickUrl), HttpManager.BASE_URL);
    }


    public String getImageUrl() {
        return HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(imageUrl, HttpManager.UPLOAD + HttpManager.SHARED_IMAGE));
    }


    public static class Buider {
        //分享的类型
        private int type;
        //分享内容的ID
        private String id;
        //标题
        private String title;
        //内容
        private String content;
        //点击跳转的地址
        private String clickUrl;
        //图片的地址,默认的是logo地址
        private String imageUrl;

        public SharedData buider() {
            SharedData sharedData = new SharedData();
            sharedData.type = type;
            sharedData.id = id;
            sharedData.title = title;
            sharedData.content = content;
            sharedData.clickUrl = clickUrl;
            sharedData.imageUrl = imageUrl;

            return sharedData;
        }

        public Buider setType(int type) {
            this.type = type;
            return this;
        }

        public Buider setId(String id) {
            this.id = id;
            return this;
        }

        public Buider setId(int id) {
            this.id = String.valueOf(id);
            return this;
        }

        public Buider setTitle(String title) {
            this.title = title;
            return this;
        }

        public Buider setContent(String content) {
            this.content = content;
            return this;
        }

        public Buider setClickUrl(String clickUrl) {
            this.clickUrl = clickUrl;
            return this;
        }

        public Buider setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.content);
        dest.writeString(this.clickUrl);
        dest.writeString(this.imageUrl);
    }

    public SharedData() {
    }

    protected SharedData(Parcel in) {
        this.type = in.readInt();
        this.id = in.readString();
        this.title = in.readString();
        this.content = in.readString();
        this.clickUrl = in.readString();
        this.imageUrl = in.readString();
    }

    public static final Parcelable.Creator<SharedData> CREATOR = new Parcelable.Creator<SharedData>() {
        @Override
        public SharedData createFromParcel(Parcel source) {
            return new SharedData(source);
        }

        @Override
        public SharedData[] newArray(int size) {
            return new SharedData[size];
        }
    };
}
