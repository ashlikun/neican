package com.hanbang.cxgz_2.mode.javabean.home;

/**
 * Created by yang on 2016/8/12.
 */
public class JieYouEsotericaData {
    private int id;
    private String img_url;//菜品大图
    private String img_urlSmallurl;//菜品小图
    private String title;//菜名
    private String user_name;//人名
    private int pingfen;//评分
    private int click;//阅读
    private int buycount;//销量
    private double price;//价格

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPingfen() {
        return pingfen;
    }

    public void setPingfen(int pingfen) {
        this.pingfen = pingfen;
    }

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImg_urlSmallurl() {
        return img_urlSmallurl;
    }

    public void setImg_urlSmallurl(String img_urlSmallurl) {
        this.img_urlSmallurl = img_urlSmallurl;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getBuycount() {
        return buycount;
    }

    public void setBuycount(int buycount) {
        this.buycount = buycount;
    }
}
