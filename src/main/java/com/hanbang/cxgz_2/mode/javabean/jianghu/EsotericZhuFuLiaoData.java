package com.hanbang.cxgz_2.mode.javabean.jianghu;

/**
 * 秘籍主料与辅料
 * Created by yang on 2016/8/30.
 */

public class EsotericZhuFuLiaoData {
    private String name;
    private String weight;

    public EsotericZhuFuLiaoData(String name, String weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
