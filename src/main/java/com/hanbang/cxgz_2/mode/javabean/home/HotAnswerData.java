package com.hanbang.cxgz_2.mode.javabean.home;

/**
 * 热门问答的实体类
 * Created by yang on 2016/8/12.
 */
public class HotAnswerData {
    private String Guid;//问答id
    private int HuiDaUserID;//回答人的id
    private String TiWenQuestion;//问题
    private String avatar_hd;//回答人头像-大图
    private String avatarimgSmallurl_hd;//回答人头像-小图
    private String job_hd;//回答人职位
    private String company_hd;//回答人单位
    private String HuiDaResultUrl;//回答的音频地址
    private int HuiDaSoundTime;//回答时间
    private int TtingCount;//偷听人数
    private int IsBuy;//是否购买，0表示没有



    public String getGuid() {
        return Guid;
    }

    public int getHuiDaUserID() {
        return HuiDaUserID;
    }

    public String getTiWenQuestion() {
        return TiWenQuestion;
    }

    public String getAvatar_hd() {
        return avatar_hd;
    }

    public String getAvatarimgSmallurl_hd() {
        return avatarimgSmallurl_hd;
    }

    public String getJob_hd() {
        return job_hd;
    }

    public String getCompany_hd() {
        return company_hd;
    }

    public String getHuiDaResultUrl() {
        return HuiDaResultUrl;
    }

    public int getHuiDaSoundTime() {
        return HuiDaSoundTime;
    }

    public int getTtingCount() {
        return TtingCount;
    }

    public int getIsBuy() {
        return IsBuy;
    }


}
