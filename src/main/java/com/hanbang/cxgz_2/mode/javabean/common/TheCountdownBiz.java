package com.hanbang.cxgz_2.mode.javabean.common;

import android.os.CountDownTimer;
import android.widget.TextView;


/**
 * 倒计时的业务类
 * Created by yang on 2016/7/29.
 */
public class TheCountdownBiz {
    /**
     * 验证码按钮倒计时 总时长和间隔时长，默认为60秒
     */
    private static long totalTime = 180000, intervalTime = 1000;


    /**
     * 倒计时
     *
     * @param hint
     */
    public static void recordTime(final TextView hint) {

        /**
         * 记录当下剩余时间
         */
        CountDownTimer countDownTimer = new CountDownTimer(totalTime, intervalTime) {
            @Override
            public void onFinish() {// 计时完毕时触发
                hint.setText("重新发送");
                hint.setEnabled(true);
            }

            @Override
            public void onTick(long currentTime) {// 计时过程显示
                hint.setEnabled(false);
                hint.setText(currentTime / 1000 + "秒");
            }
        };

        countDownTimer.start();

    }


}
