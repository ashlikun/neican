package com.hanbang.cxgz_2.mode.javabean.home;

/**
 * Created by yang on 2016/8/12.
 */
public class FigureData {
    private int id;
    private String img_url;
    private String title;
    private String author;
    private String job_CN;
    private String company;
    private String add_time;
    private boolean is_vip;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getJob_CN() {
        return job_CN;
    }

    public void setJob_CN(String job_CN) {
        this.job_CN = job_CN;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public boolean is_vip() {
        return is_vip;
    }

    public void setIs_vip(boolean is_vip) {
        this.is_vip = is_vip;
    }
}
