package com.hanbang.cxgz_2.mode.javabean.can_da;


import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.ArrayList;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 17:00
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：餐答详情面的实体
 * <p>
 * "id": 4332,
 * "telphone": "18511027598",
 * "avatar": "/upload/201609/05/big/head/201609050917587329.jpg",
 * "avatarimgMidurl": "/upload/201609/05/middle/head/201609050917587329_Mid.jpg",
 * "avatarimgSmallurl": "/upload/201609/05/small/head/201609050917587329_Sma.jpg",
 * "user_name": "屈浩",
 * "jobCN": "会长",
 * "company": "屈浩烹饪培训学校",
 * "CanDaJianJie": "鲁菜的传承、发展与创新\r\n烹饪私塾式教育产业的推广\r\n经典鲁菜的烹饪技巧略讲",
 * "CanDaKaiQi": 1,
 * "CanDaTiWenMoney": 2.99,
 */

public class MealAnswerDetailsData extends BaseHttpResponse {
    private int id;
    private String avatar;
    private String user_name;
    private String jobCN;
    private String company;
    private String CanDaJianJie;
    private String CanDaKaiQi;
    private double CanDaTiWenMoney;
    private int WithQuestionCount;//回答问题的数量
    private ArrayList<HotAnswerData> WithQuestion = new ArrayList();


    public int getId() {
        return id;
    }

    public String getAvatar() {
        return StringUtils.isNullToConvert(avatar);
    }

    public String getUser_name() {
        return StringUtils.isNullToConvert(user_name);
    }

    public String getJobCN() {
        return StringUtils.isNullToConvert(jobCN);
    }

    public String getCompany() {
        return StringUtils.isNullToConvert(company);
    }

    public String getCanDaJianJie() {
        return StringUtils.isNullToConvert(CanDaJianJie);
    }

    public String getCanDaKaiQi() {
        return StringUtils.isNullToConvert(CanDaKaiQi);
    }

    public String getCanDaTiWenMoney() {
        return StringUtils.isNullToConvert("￥"+CanDaTiWenMoney);
    }

    public ArrayList getWithQuestion() {
        return WithQuestion;
    }

    public String getWithQuestionCount() {
        return StringUtils.isNullToConvert("回答了" + WithQuestionCount + "个问题");
    }
}
