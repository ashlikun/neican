package com.hanbang.cxgz_2.mode.javabean.base;

public class BannerAdData {

    /*
     * "activity_id":62, "img_url":"/upload/201510/19/201510190930380866.jpg"
     */
    private int activity_id;
    private String img_url;

    public BannerAdData(int activity_id, String img_url) {
        this.activity_id = activity_id;
        this.img_url = img_url;
    }

    public int getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(int activity_id) {
        this.activity_id = activity_id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

}
