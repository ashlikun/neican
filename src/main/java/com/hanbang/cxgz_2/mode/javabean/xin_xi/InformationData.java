package com.hanbang.cxgz_2.mode.javabean.xin_xi;

import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * Created by yang on 2016/8/12.
 */
public class InformationData {
    private int id;
    private String img_url;
    private String title;
    private String user_name;
    private String CanDaJianJie;//餐答简介
    private String job_hd;//职位
    private String company;//单位
    private double CanDaTiWenMoney;//价格
    private String source;//来自
    private int commentcount;//评论
    private int likecount;//赞的数量
    private int CanDaKaiQi;//餐答是否开启，0没有开启，1开启
    private String author;//作者
    private String add_time;//发布时间


    public int getId() {
        return id;
    }

    public String getImg_url() {
        return StringUtils.isNullToConvert(img_url);
    }

    public String getTitle() {
        return StringUtils.isNullToConvert(title);
    }

    public String getUser_name() {
        return StringUtils.isNullToConvert(user_name);
    }

    public String getCanDaJianJie() {
        return StringUtils.isNullToConvert(CanDaJianJie);
    }

    public String getJob_hd() {
        return StringUtils.isNullToConvert(job_hd);
    }

    public String getCompany() {
        return StringUtils.isNullToConvert(company);
    }

    public String getCanDaTiWenMoney() {
        return StringUtils.isNullToConvert(CanDaTiWenMoney);
    }

    public String getSource() {
        return StringUtils.isNullToConvert(source);
    }

    public String getCommentcount() {
        return StringUtils.isNullToConvert(commentcount);
    }

    public String addCommentcount() {
        ++commentcount;
        return StringUtils.isNullToConvert(commentcount);
    }

    public String getLikecount() {
        return StringUtils.isNullToConvert(likecount);
    }

    public String getAdd_time() {
        return StringUtils.isNullToConvert(add_time);
    }



    //等于1为开启
    public boolean getCanDaKaiQi() {
        if (CanDaKaiQi == 1) {
            return true;
        } else {
            return false;
        }

    }

    //有作者表示有餐答
    public boolean getAuthor() {
        if (!StringUtils.isBlank(author)) {
            StringUtils.isNullToConvert(author);
            return true;
        } else {
            return false;
        }

    }
}
