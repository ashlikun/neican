package com.hanbang.cxgz_2.mode.javabean.jianghu;


import android.text.TextUtils;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.ArrayList;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/13 15:20
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：悬赏需求实体类
 * <p>
 * "guid": "2426b738-0688-4ec0-8e4a-78e4e9f863dd",
 * "userid": 845,
 * "is_vip": false,
 * "avatar": "/upload/201605/11/big/head/201605111029311679.jpg",
 * "avatarimgMidurl": "/upload/201605/11/middle/head/201605111029311679_Mid.jpg",
 * "avatarimgSmallurl": "/upload/201605/11/small/head/201605111029311679_Sma.jpg",
 * "user_name": "周先生",
 * "jobCN": "董事长助理",
 * "telphone": "13787294565",
 * "company": "灯塔餐饮管理公司",
 * "ID": "2426b738-0688-4ec0-8e4a-78e4e9f863dd",
 * "XuQiuType": 0,
 * "XuQiuContent": "巴厘龙虾、虾皇或有间虾铺龙虾配料和制作",
 * "XuQiuImg": "",
 * "XuQiuImgMid": "",
 * "XuQiuImgSma": "",
 * "XuQiuShenHetime": "\/Date(1468716758000)\/",
 * "XunShangPrice": 3000.00,
 * "status": 2,
 * "XuQiuTypeCN": "菜品研发",
 * "statusCN": "进行中",
 * "OrderIndex": 9999,
 * "cjsj": "\/Date(1468716758503)\/",
 * "JoinCount": 7,
 */
public class DemandRewardDetailsData extends BaseHttpResponse {
    private String ID;
    private String avatar;//
    private String user_name;//
    private String jobCN;//
    private String company;//
    private String XuQiuContent;//悬赏内容
    private String XunShangPrice;//
    private String JoinCount;//参与
    private String cjsj;//发布日期
    private String statusCN;//状态
    private ArrayList<Participation> Canyu = new ArrayList<>();
    private String XuQiuImg;//过程图片

    public String getID() {
        return ID;
    }

    public String getAvatar() {
        return StringUtils.isNullToConvert(avatar);
    }

    public String getUser_name() {
        return StringUtils.isNullToConvert(user_name);
    }

    public String getJobCN() {
        return StringUtils.isNullToConvert(jobCN);
    }

    public String getCompany() {
        return StringUtils.isNullToConvert(company);
    }

    public String getXuQiuContent() {
        return StringUtils.isNullToConvert(XuQiuContent);
    }

    public String getXunShangPrice() {
        return StringUtils.isNullToConvert(XunShangPrice);
    }

    public String getJoinCount() {
        return StringUtils.isNullToConvert(JoinCount + "人参与");
    }

    public String getCjsj() {
        if (!TextUtils.isEmpty(cjsj)) {
            return DateUtils.getTime(cjsj);
        } else {
            return StringUtils.isNullToConvert(cjsj);
        }
    }

    public String getStatusCN() {
        return statusCN;
    }

    public ArrayList<Participation> getCanyu() {
        return Canyu;
    }

    public String getXuQiuImg() {
        return XuQiuImg;
    }

    public ArrayList<String> getXuQiuImgList() {
        ArrayList<String> list = new ArrayList<>();
        if (!StringUtils.isBlank(getXuQiuImg())) {
            list.addAll(StringUtils.transitionPicture( getXuQiuImg()));
            return list;
        } else {
            return list;
        }


    }


    /**
     * 参与人员
     * <p>
     * "rownumber": 1,
     * "id": 2870,
     * "avatar": "/upload/201607/02/big/head/201607022250578527.jpg",
     * "avatarimgMidurl": "/upload/201607/02/middle/head/201607022250578527_Mid.jpg",
     * "avatarimgSmallurl": "/upload/201607/02/small/head/201607022250578527_Sma.jpg",
     * "user_name": "李中秋",
     * "jobCN": "行政总厨",
     * "telphone": "13597930308",
     * "company": "武汉千石文化",
     * "CONTENT": "我有能力完成此任务，请点击头像，进入工作室，查看我的更多资料",
     * "cjsj": "\/Date(1469246979200)\/"
     */
    public static class Participation {
        private int id;
        private String avatar;//
        private String avatarimgSmallurl;//
        private String user_name;//
        private String jobCN;//
        private String company;//
        private String CONTENT;//
        private String cjsj;//参悟日期

        public int getId() {
            return id;
        }

        public String getAvatar() {
            return StringUtils.isNullToConvert(avatar);
        }

        public String getAvatarimgSmallurl() {
            return StringUtils.isNullToConvert(avatarimgSmallurl);
        }

        public String getUser_name() {
            return StringUtils.isNullToConvert(user_name);
        }

        public String getJobCN() {
            return StringUtils.isNullToConvert(jobCN);
        }

        public String getCompany() {
            return StringUtils.isNullToConvert(company);
        }

        public String getCONTENT() {
            return StringUtils.isNullToConvert(CONTENT);
        }

        public String getCjsj() {
            if (!TextUtils.isEmpty(cjsj)) {
                return DateUtils.getTime(cjsj);
            } else {
                return StringUtils.isNullToConvert(cjsj);
            }
        }
    }
}
