package com.hanbang.cxgz_2.mode.javabean.jie_you;

import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * Created by yang on 2016/8/30.
 * <p>
 * "id": 607,
 * "zhuyishixiang": null,
 * "shifoushoufei": true,
 * "price": 10000.00,
 * "jianjie": null,
 * "click": 0,
 * "type": 2,
 * "user_id": 574,
 * "begin_date": "\/Date(1468684800000)\/",
 * "title": "好滋味鱼头泡饼",
 * "end_date": "\/Date(1500307200000)\/",
 * "pingjia": "此菜有特色，售卖点高，味道好！老少皆宜！",
 * "gongyiliucheng": null,
 * "maiduanliucheng": "一次性付款",
 * "maiduanshengming": "不可以转卖，",
 * "huanyingzongchoumaiduan": "",
 * "add_time": "\/Date(1468714514657)\/",
 * "sharecount": 0,
 * "cuisines_id": 1,
 * "status": 0,
 * "cuisines_idCN": "鲁菜",
 * "img_url": "/upload/201607/17/big/buymiji/201607170815146329.JPG",
 * "tedian": "咸鲜微酸甜，酱香味浓郁！",
 * "zhuliao": null,
 * "fuliao": null,
 * "gongyiliucheng1": null,
 * "guochengtupian": null,
 * "liuchengshipin": "",
 * "iszhiding": 0,
 * "maidiantese": "",
 * "OrderIndex": 999999,
 * "AudioUrl": "",
 * "img_urlMidurl": "/upload/201607/17/middle/buymiji/201607170815146329_Mid.JPG",
 * "img_urlSmallurl": "/upload/201607/17/small/buymiji/201607170815146329_Sma.JPG",
 * "guochengtupianMidurl": "",
 * "guochengtupianSmallurl": "",
 * "avatar": "/upload/201608/04/big/head/201608041309267457.jpg",
 * "avatarimgMidurl": "/upload/201608/04/middle/head/201608041309267457_Mid.jpg",
 * "avatarimgSmallurl": "/upload/201608/04/small/head/201608041309267457_Sma.jpg",
 * "job_CN": "出品总监",
 * "company": "北京市睿智视觉餐饮管理有限公司出品总监",
 * "user_name": "万学武",
 * "is_vip": true,
 * "pingfen": 0,
 * "buycount": 0,
 * "zhongchouCount": 0,
 * "workjingli": "中国北方十大名厨、中国烹饪大师、中国饭店协会名厨委员会委员、中国酒店十佳行政总厨、中国酒店职业经理人金奖！北京电视台幸福厨房特邀嘉宾！",
 * "IsPingFen": false,
 * "IsShouCang": false
 */

public class EsotericDetailsData {
    private int id;//秘籍id
    private int pingfen;//评分
    private int buycount;//销量
    private int click;//阅读
    private boolean is_vip;
    private boolean IsPingFen;//是否评过分
    private boolean IsShouCang;//是否收藏
    private double price;//价格
    private String user_name;
    private String img_url;//海报
    private String avatar;//头像大图
    private String guochengtupianSmallurl;//头像小图
    private String job_CN;//职位
    private String company;//单位
    private String jianjie;//简介
    private String title;//标题
    private String pingjia;//评价
    private String gongyiliucheng;//工艺流程
    private String zhuliao;//主料
    private String fuliao;//辅料
    private String guochengtupian;//过程图片
    private String tedian;//特点
    private String cuisines_idCN;//菜系
    private String zhuyishixiang;//注意事项
    private String AudioUrl;//音频地址
    private String liuchengshipin;//视频地址

    private String maiduanliucheng;//买断流程
    private String maiduanshengming;//买断声明
    private String workjingli;//工作经历
    private String huanyingzongchoumaiduan;//欢迎众筹买断

    public String getHuanyingzongchoumaiduan() {
        return huanyingzongchoumaiduan;
    }

    public String getWorkjingli() {
        return workjingli;
    }

    public String getMaiduanliucheng() {
        return maiduanliucheng;
    }

    public String getMaiduanshengming() {
        return maiduanshengming;
    }

    public String getAudioUrl() {
        return AudioUrl;
    }

    public String getZhuyishixiang() {
        return zhuyishixiang;
    }

    public String getCuisines_idCN() {
        return cuisines_idCN;
    }

    public int getId() {
        return id;
    }

    public int getPingfen() {
        return pingfen;
    }

    public int getBuycount() {
        return buycount;
    }

    public int getClick() {
        return click;
    }

    public boolean is_vip() {
        return is_vip;
    }

    public boolean isPingFen() {
        return IsPingFen;
    }

    public double getPrice() {
        return price;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getGuochengtupianSmallurl() {
        return guochengtupianSmallurl;
    }

    public String getJob_CN() {
        return job_CN;
    }

    public String getCompany() {
        return company;
    }

    public String getJianjie() {
        return jianjie;
    }

    public String getTitle() {
        return StringUtils.isNullToConvert(title);
    }

    public String getPingjia() {
        return pingjia;
    }

    public String getGongyiliucheng() {
        return gongyiliucheng;
    }

    public String getZhuliao() {
        return zhuliao;
    }

    public String getFuliao() {
        return fuliao;
    }

    public String getGuochengtupian() {
        return guochengtupian;
    }

    public boolean isShouCang() {
        return IsShouCang;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getTedian() {
        return tedian;
    }

    public String getLiuchengshipin() {
        return liuchengshipin;
    }
}
