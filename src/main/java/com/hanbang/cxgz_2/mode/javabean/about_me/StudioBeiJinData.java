package com.hanbang.cxgz_2.mode.javabean.about_me;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/22　11:21
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioBeiJinData implements Parcelable {

    /**
     * id : c0b3fced-931f-4d24-8a45-f4825291b595
     * img_url : /upload/201605/03/big/grzxbjt/201605031607354777.jpg
     * img_urlMiddle : /upload/201605/03/middle/grzxbjt/201605031607354777_Mid.jpg
     * img_urlSmall : /upload/201605/03/small/grzxbjt/201605031607354777_Sma.jpg
     */

    private String id;
    private String img_url;
    private String img_urlMiddle;
    private String img_urlSmall;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getImg_urlMiddle() {
        return img_urlMiddle;
    }

    public void setImg_urlMiddle(String img_urlMiddle) {
        this.img_urlMiddle = img_urlMiddle;
    }

    public String getImg_urlSmall() {
        return img_urlSmall;
    }

    public void setImg_urlSmall(String img_urlSmall) {
        this.img_urlSmall = img_urlSmall;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.img_url);
        dest.writeString(this.img_urlMiddle);
        dest.writeString(this.img_urlSmall);
    }

    public StudioBeiJinData() {
    }

    protected StudioBeiJinData(Parcel in) {
        this.id = in.readString();
        this.img_url = in.readString();
        this.img_urlMiddle = in.readString();
        this.img_urlSmall = in.readString();
    }

    public static final Parcelable.Creator<StudioBeiJinData> CREATOR = new Parcelable.Creator<StudioBeiJinData>() {
        @Override
        public StudioBeiJinData createFromParcel(Parcel source) {
            return new StudioBeiJinData(source);
        }

        @Override
        public StudioBeiJinData[] newArray(int size) {
            return new StudioBeiJinData[size];
        }
    };
}
