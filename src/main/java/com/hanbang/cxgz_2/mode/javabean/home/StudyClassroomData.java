package com.hanbang.cxgz_2.mode.javabean.home;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;

/**
 * Created by yang on 2016/8/12.
 */
public class StudyClassroomData extends BaseHttpResponse {
    private int id;
    private String img_url;
    private String img_urlSmall;
    private String title;
    private String add_time;//开课时间
    private int isbaomingcount;//报名人数
    private int pingfen;//评分
    private int click;//阅读
    private double price;//价格

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getImg_urlSmall() {
        return img_urlSmall;
    }

    public void setImg_urlSmall(String img_urlSmall) {
        this.img_urlSmall = img_urlSmall;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPingfen() {
        return pingfen;
    }

    public void setPingfen(int pingfen) {
        this.pingfen = pingfen;
    }

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public int getIsbaomingcount() {
        return isbaomingcount;
    }

    public void setIsbaomingcount(int isbaomingcount) {
        this.isbaomingcount = isbaomingcount;
    }
}
