package com.hanbang.cxgz_2.mode.javabean.jianghu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/25.
 */

public class ImageItemData {
    /**
     * "id": 9093,
     * "img_url": "/upload/201608/25/big/feed/201608250658515423.jpg",
     * "imgMidurl": "/upload/201608/25/middle/feed/201608250658515423_Mid.jpg",
     * "imgSmallurl": "/upload/201608/25/small/feed/201608250658515423_Sma.jpg"
     */
    int id;
    String img_url;
    String imgMidurl;
    String imgSmallurl;




    public int getId() {
        return id;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getImgMidurl() {
        return imgMidurl;
    }

    public String getImgSmallurl() {
        return imgSmallurl;
    }


    public static ArrayList<String> getStringList(List<ImageItemData> list) {
        ArrayList<String> res = new ArrayList<>();
        for (ImageItemData d : list) {
            res.add(d.getImgMidurl());
        }
        return res;
    }



}
