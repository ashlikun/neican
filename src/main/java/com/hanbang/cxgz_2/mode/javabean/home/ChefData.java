package com.hanbang.cxgz_2.mode.javabean.home;

import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;

/**
 * Created by yang on 2016/8/12.
 */
public class ChefData {
    private int id;
    private boolean isguanzhu;
    private String avatar;
    private String avatarimgSmallurl;
    private String company;
    private String user_name;
    private String job;
    private int membershipcount;
    private boolean is_vip;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GuanZhu getIsguanzhu() {
        return GuanZhu.getState(isguanzhu);
    }

    public void setIsguanzhu(boolean isguanzhu) {
        this.isguanzhu = isguanzhu;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarimgSmallurl() {
        return avatarimgSmallurl;
    }

    public void setAvatarimgSmallurl(String avatarimgSmallurl) {
        this.avatarimgSmallurl = avatarimgSmallurl;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public boolean is_vip() {
        return is_vip;
    }

    public void setIs_vip(boolean is_vip) {
        this.is_vip = is_vip;
    }

    public int getMembershipcount() {
        return membershipcount;
    }
}
