package com.hanbang.cxgz_2.mode.javabean.jianghu;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/12 15:26
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：学习课堂的实体类
 */

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * "id": 149,
 * "pingfen": 0,
 * "price": 3650.00,
 * "shifoushoufei": true,
 * "click": 974,
 * "sharecount": 0,
 * "status": 0,
 * "img_url": "/upload/201605/21/big/ketang/201605211821321990.jpg",
 * "content_img_shipin": "",
 * "add_time": "\/Date(1463823357000)\/",
 * "user_id": 1472,
 * "sort_id": 999,
 * "type": 1,
 * "title": "食品雕刻零基础培训",
 * "remark": "四角花，西蕃莲，令箭花，月季花，牡丹花，菊花，荷花，仙鹤，燕鱼，鲤鱼，虾，虾篓，盘饰，西瓜花，鬼脸南瓜雕，孔雀，凤，龙。\r\n\r\n主讲老师：赵惠源大师",
 * "begin_time": null,
 * "end_time": null,
 * "address": "北京市海淀区万寿桥",
 * "guanjianzi": "",
 * "iszhiding": 0,
 * "AudioUrl": "",
 * "OrderIndex": 999,
 * "content_img": "/upload/201605/21/big/ketang/201605211735571493.png,/upload/201605/21/big/ketang/201605211740505789.jpg,/upload/201605/21/big/ketang/201605211740510623.jpg,/upload/201605/21/big/ketang/201605211743474060.jpg,/upload/201605/21/big/ketang/201605211743514617.jpg,/upload/201605/21/big/ketang/201605211743540359.jpg,/upload/201605/21/big/ketang/201605211743556649.jpg,/upload/201605/21/big/ketang/201605211743582967.jpg,/upload/201605/21/big/ketang/201605211744001355.jpg,/upload/201605/21/big/ketang/201605211744021912.jpg",
 * "content_imgMiddle": "/upload/201605/21/middle/ketang/201605211735571493_Mid.png,/upload/201605/21/middle/ketang/201605211740505789_Mid.jpg,/upload/201605/21/middle/ketang/201605211740510623_Mid.jpg,/upload/201605/21/middle/ketang/201605211743474060_Mid.jpg,/upload/201605/21/middle/ketang/201605211743514617_Mid.jpg,/upload/201605/21/middle/ketang/201605211743540359_Mid.jpg,/upload/201605/21/middle/ketang/201605211743556649_Mid.jpg,/upload/201605/21/middle/ketang/201605211743582967_Mid.jpg,/upload/201605/21/middle/ketang/201605211744001355_Mid.jpg,/upload/201605/21/middle/ketang/201605211744021912_Mid.jpg",
 * "content_imgSmall": "/upload/201605/21/small/ketang/201605211735571493_Sma.png,/upload/201605/21/small/ketang/201605211740505789_Sma.jpg,/upload/201605/21/small/ketang/201605211740510623_Sma.jpg,/upload/201605/21/small/ketang/201605211743474060_Sma.jpg,/upload/201605/21/small/ketang/201605211743514617_Sma.jpg,/upload/201605/21/small/ketang/201605211743540359_Sma.jpg,/upload/201605/21/small/ketang/201605211743556649_Sma.jpg,/upload/201605/21/small/ketang/201605211743582967_Sma.jpg,/upload/201605/21/small/ketang/201605211744001355_Sma.jpg,/upload/201605/21/small/ketang/201605211744021912_Sma.jpg",
 * "img_urlMiddle": "/upload/201605/21/middle/ketang/201605211821321990_Mid.jpg",
 * "img_urlSmall": "/upload/201605/21/small/ketang/201605211821321990_Sma.jpg",
 * "timeRemark": "循环课，随报随学；课前预约授课地点，可上门亲授。\r\n全球均可游学教学，如需要赵惠源老师亲授，需要提前15天预约教学时间，教学地点可另行协商。",
 * "buycountJia": 5,
 * "avatar": "/upload/201608/19/big/head/201608191310483476.jpg",
 * "avatarimgMidurl": "/upload/201608/19/middle/head/201608191310483476_Mid.jpg",
 * "avatarimgSmallurl": "/upload/201608/19/small/head/201608191310483476_Sma.jpg",
 * "job_CN": "出品总监",
 * "company": "北京东方艺培训中心校长",
 * "user_name": "赵惠源",
 * "workjingli": "2015年 注册成立 北京东方食艺国际餐饮管理有限公司 任董事长\t\r\n\r\n2014年 成立北京慧源技术培训中心 任校长\r\n\r\n2013年 北京电视台一卡酷动画春晚吃遍节日-瓜雕达人\r\n\r\n成立北京东方食艺职业技能培训学校 任校长\r\n\r\n2012年 北京电视台体育一罗筐节目\r\n\r\n卫视—养生堂栏目—养生菜\r\n北京电视台生活频道栏目—西瓜果冻\r\n北京电视台快乐一点通栏目—南瓜小包子\r\n北京电视台—美食魔法师 \r\n北京电视台—神奇的厨艺魔术\r\n北京卫视—养生堂栏目—养生菜\r\n\r\n2010年 赵惠源大师应邀北京电视台超级大挑战栏目作西瓜水果雕表演\r\n应邀北京电视台超级大挑战栏目作西瓜水果雕表演\r\n\r\n2009年 出版《时尚果盘》《实用冷拼》《食雕精选》《创意盘饰》\r\n应韩国LBS电视台邀请赴韩国表演中国食品雕刻艺术\r\n应马来西亚烹饯饪学院院长RAZAK邀请讲学传中国食品雕刻艺术\r\n被世界华人健康饮食协会聘为副主席\t\r\n\r\n2008年 应韩国青州大学烹饪系邀请赵惠源大师讲学传中国食品雕刻艺术\r\n第二届中加美国际烹饪大赛赛嘉宾\r\n主办第二届赴韩国、日本“中、韩、日”国际厨皇争霸接力赛中方领导\r\n\r\n2007年 主办第一届中韩日国际厨皇争霸接力赛中方领导\r\n\r\n2006年 北京电视台《八方食圣》栏目举办红五月青年厨艺大比拼作大师表演“冰雕”\r\n主办中华餐饮“龙王杯”烹饪食艺大奖赛\r\n\r\n2005年 任“中国青岛第十五、十六届国际萝卜节”总裁判长\r\n\r\n2004年 出版《中华食雕第一龙》\r\n任“中国青岛第十五、十六届国际萝卜节”总裁判长\t\r\n\t\r\n2003年 被东方中国食品杂志刊登为封面人物\r\n在贵州“烹饪王国游”活动中作大师表演“鸳鸯、冰雕”\r\n首届《伊尹杯》烹饪大赛评委，组委会副秘书长\r\n被国际中餐协会评为国际厨艺魔术师称号\r\n餐饮食艺行里号称中华食雕第一龙，称他为“龙王“\t\r\n\r\n2002年 被东方美食烹饪杂志刊登为封面人物\r\n出版《新编龙雕的技法与应用》\r\n应“中国烹协”成立十五周年作特技表演“冰雕”\t\t\t\r\n2001年 应北京电视台春节晚会上作特技表演“冰雕”\r\n\r\n2000年 北京“八方食圣”春节晚会上作特级表演，油雕《中华龙》\r\n\r\n1998年 出版《果蔬雕刻与拼摆》\r\n赵大师名字被编入《中国名师名人录》\r\n\r\n1996年 北京“食雕、冷拼、面点大奖赛”副总指挥\t\r\n\r\n1994年 出版《水果拼摆与雕刻》\r\n北京首届“食品雕刻大赛”组织部副部长\r\n\r\n1990年 出版《食品雕刻与冷拼艺术》\r\n\r\n1988年 首届全国食雕大赛“龙凤三部曲”获奖\t\r\n\r\n1987年 毕业于北京美术学院\t\t\t",
 * "buycount": 5,
 * "IsKaiKe": 4
 * "zhaosrenshu": null,
 * "baomingCount": 15
 */
public class StudyClassroomDetailsData extends BaseHttpResponse {
    private int id;
    private String img_url;//海报
    private String title;//标题
    private String begin_time;//开始时间
    private String end_time;//结束时间
    private String address;
    private String price;
    private String avatar;//头像
    private String user_name;
    private String job_CN;
    private String company;
    private String timeRemark;//备注
    private String workjingli;//老师简介
    private String remark;//课堂简介
    private String content_img;//过程图片
    private String AudioUrl;//音频
    private String content_img_shipin;//视频
    private int zhaosrenshu;//招生人灵敏
    private int baomingCount;//报名人数

    public int getId() {
        return id;
    }

    public String getImg_url() {
        return StringUtils.isNullToConvert(img_url);
    }

    public String getTitle() {
        return StringUtils.isNullToConvert(title);
    }

    public String getBegin_time() {
        if (StringUtils.isBlank(begin_time)) {
            return "随到随学";
        }

        return StringUtils.isNullToConvert(DateUtils.getTime(begin_time));
    }

    public String getEnd_time() {
        if (StringUtils.isBlank(end_time)) {
            return "";
        }
        return StringUtils.isNullToConvert(DateUtils.getTime(end_time));
    }

    public String getAddress() {
        return StringUtils.isNullToConvert(address);
    }

    public String getPrice() {
        return StringUtils.isNullToConvert(price);
    }

    public String getAvatar() {
        return StringUtils.isNullToConvert(avatar);
    }

    public String getUser_name() {
        return StringUtils.isNullToConvert(user_name);
    }

    public String getJob_CN() {
        return StringUtils.isNullToConvert(job_CN);
    }

    public String getCompany() {
        return StringUtils.isNullToConvert(company);
    }

    public String getTimeRemark() {
        return StringUtils.isNullToConvert(timeRemark);
    }

    public String getWorkjingli() {
        return StringUtils.isNullToConvert(workjingli);
    }

    public String getRemark() {
        return StringUtils.isNullToConvert(remark);
    }

    public String getContent_img() {

        return StringUtils.isNullToConvert(content_img);
    }

    public String getAudioUrl() {
        return StringUtils.isNullToConvert(AudioUrl);
    }

    public String getContent_img_shipin() {
        return StringUtils.isNullToConvert(content_img_shipin);
    }

    public int getZhaosrenshu() {
        return zhaosrenshu;
    }

    public int getBaomingCount() {
        return baomingCount;
    }
}
