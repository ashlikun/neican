package com.hanbang.cxgz_2.mode.javabean.jianghu;

import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * Created by Administrator on 2016/8/26.
 */

public class XiuChangCommentData {
    /**
     * "user_name": "阳",
     * "avatar": "/upload/201608/19/big/head/201608191647341635.jpg",
     * "avatarimgMidurl": "/upload/201608/19/middle/head/201608191647341635_Mid.jpg",
     * "avatarimgSmallurl": "/upload/201608/19/small/head/201608191647341635_Sma.jpg",
     * "text": "很好的",
     * "add_time": "\/Date(1472272687367)\/",
     * "likecount": 0
     */

    private String avatarimgSmallurl;
    private String text;
    private String user_name;
    private String add_time;

    private int likecount;
    private int id;

    public String getAvatarimgSmallurl() {
        return avatarimgSmallurl;
    }

    public String getText() {
        return StringUtils.isNullToConvert(text);
    }

    public String getUser_name() {
        return StringUtils.isNullToConvert(user_name);
    }

    public String getAdd_time() {
        return add_time;
    }

    public String getLikecount() {
        return String.valueOf(likecount);
    }

    public int getId() {
        return id;
    }
}
