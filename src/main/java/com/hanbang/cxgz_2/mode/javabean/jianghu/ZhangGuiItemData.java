package com.hanbang.cxgz_2.mode.javabean.jianghu;

import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;

import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/20　14:36
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class ZhangGuiItemData {

    /**
     * title : 国际中餐
     * biaoqian : 6
     * Content : [{"id":121,"telphone":"13910147735","avatar":"/upload/201606/06/big/head/201606061510536781.jpg","avatarimgMidurl":"/upload/201606/06/middle/head/201606061510536781_Mid.jpg","avatarimgSmallurl":"/upload/201606/06/small/head/201606061510536781_Sma.jpg","user_name":"张晓晓","signfoods":"白米饭","cuisines_id":"30","company":"其他","job":"培训师","is_vip":true,"is_shoutu":false,"orderindex":99999,"add_time":"/Date(1460848526000)/","yaoqingNumber":"","membershipcount":194,"mijicount":1,"ketangcount":0,"xuexiziliaocount":5,"isguanzhu":true},{"id":245,"telphone":"18996111441","avatar":"/upload/201605/24/big/head/201605241448512021.png","avatarimgMidurl":"/upload/201605/24/middle/head/201605241448512021_Mid.png","avatarimgSmallurl":"/upload/201605/24/small/head/201605241448512021_Sma.png","user_name":"匠门汇","signfoods":"麻婆豆腐","cuisines_id":"2,21","company":"玖创餐饮管理有限公司","job":"品牌总监","is_vip":true,"is_shoutu":false,"orderindex":999999,"add_time":"/Date(1461052506000)/","yaoqingNumber":"","membershipcount":25,"mijicount":0,"ketangcount":0,"xuexiziliaocount":0,"isguanzhu":false}]
     */

    private String title;
    private int biaoqian;
    /**
     * id : 121
     * telphone : 13910147735
     * avatar : /upload/201606/06/big/head/201606061510536781.jpg
     * avatarimgMidurl : /upload/201606/06/middle/head/201606061510536781_Mid.jpg
     * avatarimgSmallurl : /upload/201606/06/small/head/201606061510536781_Sma.jpg
     * user_name : 张晓晓
     * signfoods : 白米饭
     * cuisines_id : 30
     * company : 其他
     * job : 培训师
     * is_vip : true
     * is_shoutu : false
     * orderindex : 99999
     * add_time : /Date(1460848526000)/
     * yaoqingNumber :
     * membershipcount : 194
     * mijicount : 1
     * ketangcount : 0
     * xuexiziliaocount : 5
     * isguanzhu : true
     */

    private List<ManagerData> Content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getBiaoqian() {
        return biaoqian;
    }

    public void setBiaoqian(int biaoqian) {
        this.biaoqian = biaoqian;
    }

    public List<ManagerData> getContent() {
        return Content;
    }

    public void setContent(List<ManagerData> Content) {
        this.Content = Content;
    }


}
