package com.hanbang.cxgz_2.mode.javabean.jianghu;


import com.hanbang.cxgz_2.mode.enumeration.RewardTypeEnum;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 9:17
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：悬赏首页分类的实体类
 */

public class ItmeRewardClassifyData {
    private int pictureId;//图片id
    private RewardTypeEnum title;//标题



    public ItmeRewardClassifyData(int pictureId, RewardTypeEnum title ) {
        this.pictureId = pictureId;
        this.title = title;

    }

    public RewardTypeEnum getTitle() {
        return title;
    }

    public void setTitle(RewardTypeEnum title) {
        this.title = title;
    }


    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }
}
