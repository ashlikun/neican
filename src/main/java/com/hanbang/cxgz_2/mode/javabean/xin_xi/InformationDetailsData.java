package com.hanbang.cxgz_2.mode.javabean.xin_xi;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.ArrayList;

/**
 * Created by yang on 2016/8/12.
 */
public class InformationDetailsData extends BaseHttpResponse {
    private InformationData RwzDetail;
    private ArrayList<HotAnswerData> WithQuestion;
    private int WithQuestionCount;//回答问题的数量

    public InformationData getRwzDetail() {
        return RwzDetail;
    }

    public ArrayList<HotAnswerData> getWithQuestion() {
        return WithQuestion;
    }

    public String getWithQuestionCount() {
        return StringUtils.isNullToConvert(WithQuestionCount);
    }
}
