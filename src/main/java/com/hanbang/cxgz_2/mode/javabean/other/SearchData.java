package com.hanbang.cxgz_2.mode.javabean.other;

import com.hanbang.cxgz_2.mode.enumeration.SearchEnum;

/**
 * Created by yang on 2016/8/31.
 */

public class SearchData {
    private SearchEnum name;
    private boolean isSelect;

    public SearchData(SearchEnum name, boolean isSelect) {
        this.name = name;
        this.isSelect = isSelect;
    }

    public String getName() {
        return name.getValuse();
    }

    public void setName(SearchEnum name) {
        this.name = name;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
