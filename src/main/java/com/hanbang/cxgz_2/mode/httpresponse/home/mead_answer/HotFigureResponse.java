package com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.enumeration.FigureClassifyEnum;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.can_da.MealAnswerClassifyData;
import com.hanbang.cxgz_2.mode.javabean.home.HotFigureData;

import java.util.ArrayList;

/**
 * Created by yang on 2016/8/17.
 */
public class HotFigureResponse extends BaseHttpResponse {
    public ArrayList<HotFigureData> list;//推荐人物


    public static HotFigureResponse init() {
        HotFigureResponse request = new HotFigureResponse();
        request.list = new ArrayList<>();

        return request;
    }

    public void clearAll() {
        list.clear();

    }

    public void addData(HotFigureResponse response) {
        list.addAll(response.getList());

    }

    public ArrayList<HotFigureData> getList() {
        return list;
    }

    public ArrayList<MealAnswerClassifyData> getClassifyListData() {
        final ArrayList<MealAnswerClassifyData> datas = new ArrayList<>();
        datas.add(new MealAnswerClassifyData(R.mipmap.channel_answer_chinese_food, FigureClassifyEnum.GOU_JI_ZHONG_CAN));
        datas.add(new MealAnswerClassifyData(R.mipmap.channel_answer_fast_food, FigureClassifyEnum.ZHONG_XI_KUAI_CAN));
        datas.add(new MealAnswerClassifyData(R.mipmap.channel_answer_hot_pot, FigureClassifyEnum.HUO_GU_SHAO_KAO));
        datas.add(new MealAnswerClassifyData(R.mipmap.channel_answer_snack, FigureClassifyEnum.DIAN_PU_ZHUAN_RANG));
        datas.add(new MealAnswerClassifyData(R.mipmap.channel_answer_westernstyle_food, FigureClassifyEnum.XI_CNA_GU_JI));
        datas.add(new MealAnswerClassifyData(R.mipmap.channel_answer_muslim, FigureClassifyEnum.CHUAN_TONG_QING_ZHEN));
        datas.add(new MealAnswerClassifyData(R.mipmap.my_qa, FigureClassifyEnum.YANG_SHENG_SU_SHI));
        datas.add(new MealAnswerClassifyData(R.mipmap.my_qa, FigureClassifyEnum.ZHONG_CAN_CHU_SHI));
        datas.add(new MealAnswerClassifyData(R.mipmap.my_qa, FigureClassifyEnum.XI_CAN_CHU_SHI));
        datas.add(new MealAnswerClassifyData(R.mipmap.my_qa, FigureClassifyEnum.MIAN_DIAN_CHU_SHI));
        datas.add(new MealAnswerClassifyData(R.mipmap.my_qa, FigureClassifyEnum.LENG_CAI_CHU_SHI));
        datas.add(new MealAnswerClassifyData(R.mipmap.my_qa, FigureClassifyEnum.QI_TA_CHU_SHI));

        return datas;
    }


}
