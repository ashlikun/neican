package com.hanbang.cxgz_2.mode.httpresponse.jieyou;

import com.hanbang.cxgz_2.mode.enumeration.RewardTypeEnum;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/17.
 * Dishesdevelop:菜品研发
 * Advertises:招聘
 * Candidate:求职
 * Shopassignment:店铺转让
 * Usedplant:二手设备
 * Agentproxy:加盟代理
 */

public class HomeRewardHttpResponse extends BaseHttpResponse {
    private List<DemandRewardData> Dishesdevelop;//菜品研发
    private List<DemandRewardData> Advertises;//招聘
    private List<DemandRewardData> Shopassignment;//店铺转让
    private List<DemandRewardData> Agentproxy;//加盟代理
    private List<DemandRewardData> Candidate;//求职
    private List<DemandRewardData> Usedplant;//二手设备


    public static HomeRewardHttpResponse init() {
        HomeRewardHttpResponse request = new HomeRewardHttpResponse();
        request.Dishesdevelop = new ArrayList<>();
        request.Advertises = new ArrayList<>();
        request.Shopassignment = new ArrayList<>();
        request.Agentproxy = new ArrayList<>();
        request.Candidate = new ArrayList<>();
        request.Usedplant = new ArrayList<>();
        return request;
    }


    public void addData(HomeRewardHttpResponse response) {
        Dishesdevelop.addAll(response.getDishesdevelop());
        Advertises.addAll(response.getAdvertises());
        Shopassignment.addAll(response.getShopassignment());
        Agentproxy.addAll(response.getAgentproxy());
        Candidate.addAll(response.getCandidate());
        Usedplant.addAll(response.getUsedplant());
    }

    public void clear() {
        Dishesdevelop.clear();
        Advertises.clear();
        Shopassignment.clear();
        Agentproxy.clear();
        Candidate.clear();
        Usedplant.clear();
    }


    public List<DemandRewardData> getDishesdevelop() {
        return Dishesdevelop;
    }

    public List<DemandRewardData> getAdvertises() {
        return Advertises;
    }

    public List<DemandRewardData> getShopassignment() {
        return Shopassignment;
    }

    public List<DemandRewardData> getAgentproxy() {
        return Agentproxy;
    }

    public List<DemandRewardData> getCandidate() {
        return Candidate;
    }

    public List<DemandRewardData> getUsedplant() {
        return Usedplant;
    }


    /**
     * 按分类设置数据源
     *
     * @param datas
     * @param title
     */
    public void setDatas(List<DemandRewardData> datas, RewardTypeEnum title) {
        if (title.getValuse().equals(RewardTypeEnum.CAI_PIN_YAN_FA.getValuse())) {
            Dishesdevelop.addAll(datas);

        } else if (title.getValuse().equals(RewardTypeEnum.ZHAO_PIN.getValuse())) {
            Advertises.addAll(datas);

        } else if (title.getValuse().equals(RewardTypeEnum.QIU_ZHI.getValuse())) {
            Candidate.addAll(datas);

        } else if (title.getValuse().equals(RewardTypeEnum.DIAN_PU_ZHUAN_RANG.getValuse())) {
            Shopassignment.addAll(datas);

        } else if (title.getValuse().equals(RewardTypeEnum.ER_SHOU_SHE_BEI.getValuse())) {
            Usedplant.addAll(datas);

        } else if (title.getValuse().equals(RewardTypeEnum.JIA_MENG_DAI_LI.getValuse())) {
            Agentproxy.addAll(datas);

        }

    }


}
