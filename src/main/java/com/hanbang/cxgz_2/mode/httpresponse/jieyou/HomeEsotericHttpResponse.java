package com.hanbang.cxgz_2.mode.httpresponse.jieyou;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.JieYouEsotericaData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/17.
 * baokuan：爆款产品
 * fashionChoice:潮流精选
 * HighMaoli:高毛利
 * buyduan:买断秘籍
 * vidoMiji:视频秘籍
 */

public class HomeEsotericHttpResponse extends BaseHttpResponse {

    private List<JieYouEsotericaData> baokuan;//爆款
    private List<EsotericaData> fashionChoice;//潮流精选
    private List<EsotericaData> HighMaoli;//高毛利
    private List<EsotericaData> buyduan;//买断秘籍
    private List<EsotericaData> vidoMiji;//视频秘籍
    private List<EsotericaData> dashiclassic;//大师经典
    private List<EsotericaData> MiJiSourceNewToTime;//新推荐

    public List<JieYouEsotericaData> getBaokuan() {
        return baokuan;
    }

    public List<EsotericaData> getFashionChoice() {
        return fashionChoice;
    }

    public List<EsotericaData> getHighMaoli() {
        return HighMaoli;
    }

    public List<EsotericaData> getBuyduan() {
        return buyduan;
    }

    public List<EsotericaData> getVidoMiji() {
        return vidoMiji;
    }

    public List<EsotericaData> getDashiclassic() {
        return dashiclassic;
    }

    public List<EsotericaData> getMiJiSourceNewToTime() {
        return MiJiSourceNewToTime;
    }

    public HomeEsotericHttpResponse() {
        baokuan = new ArrayList<>();
        fashionChoice = new ArrayList<>();
        HighMaoli = new ArrayList<>();
        buyduan = new ArrayList<>();
        vidoMiji = new ArrayList<>();
        dashiclassic = new ArrayList<>();
        MiJiSourceNewToTime = new ArrayList<>();
    }

    public void addData(HomeEsotericHttpResponse response) {
        baokuan.addAll(response.getBaokuan());
        fashionChoice.addAll(response.getFashionChoice());
        HighMaoli.addAll(response.getHighMaoli());
        buyduan.addAll(response.getBuyduan());
        vidoMiji.addAll(response.getVidoMiji());
        dashiclassic.addAll(response.getDashiclassic());
        MiJiSourceNewToTime.addAll(response.getMiJiSourceNewToTime());

    }

    public void clare() {
        baokuan.clear();
        fashionChoice.clear();
        HighMaoli.clear();
        buyduan.clear();
        vidoMiji.clear();
        dashiclassic.clear();
        MiJiSourceNewToTime.clear();
    }

}
