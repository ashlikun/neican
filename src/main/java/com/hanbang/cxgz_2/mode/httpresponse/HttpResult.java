package com.hanbang.cxgz_2.mode.httpresponse;

/**
 * Created by likun
 * http返回的基本数据， 用泛型解耦，可以适用于大部分接口
 */
public class HttpResult<T> extends BaseHttpResponse {


    //用来模仿Data
    public T list;

    public T getList() {
        return list;
    }

    public void setList(T data) {
        this.list = data;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("code=" + Result + "msg=" + Msg);
        if (null != list) {
            sb.append(" subjects:" + list.toString());
        }
        return sb.toString();
    }
}
