package com.hanbang.cxgz_2.mode.httpresponse.jieyou;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/17.
 */

public class DemandRewardClassifyHttpResponse extends BaseHttpResponse {

    private List<DemandRewardData> list;

    @Override
    public List getData() {
        return list;
    }
}
