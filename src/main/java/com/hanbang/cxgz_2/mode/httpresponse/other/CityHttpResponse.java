package com.hanbang.cxgz_2.mode.httpresponse.other;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.base.CityData;

import java.util.ArrayList;

/**
 * Created by yang on 2016/8/27.
 */

public class CityHttpResponse extends BaseHttpResponse {
    private ArrayList<CityData> list;

    public ArrayList<CityData> getList() {
        return list;
    }

    public ArrayList<CityData> addPinYin() {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setAllInitial();
        }

        return list;

    }

}
