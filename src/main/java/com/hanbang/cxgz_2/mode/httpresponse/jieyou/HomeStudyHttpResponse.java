package com.hanbang.cxgz_2.mode.httpresponse.jieyou;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/17.
 * GroupKeTang:语音直播课堂
 * LearnClass:线下课堂
 */

public class HomeStudyHttpResponse extends BaseHttpResponse {
    private List<StudyClassroomData> GroupKeTang;//语音直播课堂
    private List<StudyClassroomData> LearnClass;//线下课堂

    public HomeStudyHttpResponse() {
        GroupKeTang = new ArrayList<>();
        LearnClass = new ArrayList<>();
    }

    public void addData(HomeStudyHttpResponse response) {
        GroupKeTang.addAll(response.getGroupKeTang());
        LearnClass.addAll(response.getLearnClass());
    }

    public void clear(){
        GroupKeTang.clear();
        LearnClass.clear();
    }
    public List<StudyClassroomData> getGroupKeTang() {
        return GroupKeTang;
    }

    public List<StudyClassroomData> getLearnClass() {
        return LearnClass;
    }


}
