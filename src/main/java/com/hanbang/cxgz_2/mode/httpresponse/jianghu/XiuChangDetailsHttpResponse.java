package com.hanbang.cxgz_2.mode.httpresponse.jianghu;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangCommentData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/18.
 */

public class XiuChangDetailsHttpResponse extends BaseHttpResponse {


    public List<XiuChangItemData> dynamicsDetail = null;
    public List<XiuChangCommentData> pinglunList = null;

    public static XiuChangDetailsHttpResponse init() {
        XiuChangDetailsHttpResponse response = new XiuChangDetailsHttpResponse();
        response.dynamicsDetail = new ArrayList<>();
        response.pinglunList = new ArrayList<>();
        return response;
    }

    public void clearAll() {
        dynamicsDetail.clear();
        pinglunList.clear();
    }

    public void addData(XiuChangDetailsHttpResponse response) {
        pinglunList.addAll(response.getPinglunList());
        if (response.getDynamicsDetail() != null && response.getDynamicsDetail().size() > 0) {
            dynamicsDetail.clear();
            dynamicsDetail.add(response.getDynamicsDetail().get(0));
        }
    }


    public List<XiuChangItemData> getDynamicsDetail() {
        return dynamicsDetail;
    }

    public List<XiuChangCommentData> getPinglunList() {
        return pinglunList;
    }
}
