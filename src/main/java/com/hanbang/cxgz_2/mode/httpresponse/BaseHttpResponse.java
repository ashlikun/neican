package com.hanbang.cxgz_2.mode.httpresponse;

import com.hanbang.cxgz_2.mode.enumeration.HttpCode;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public class BaseHttpResponse {


    public int Result;
    public String Msg;

    public List getData() {
        return null;
    }

    public int getResult() {
        return Result;
    }

    public String getMsg() {
        return Msg;
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:05
     * <p>
     * 方法功能：返回成功  0
     */

    public boolean isSucceed() {
        return HttpCode.getState(Result) == HttpCode.SUCCEED;
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:06
     * <p>
     * 方法功能：接口失败  1
     */


    public boolean isFaiure() {
        return HttpCode.getState(Result) == HttpCode.FAILURE;
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:07
     * <p>
     * 方法功能：返回失败 2
     */


    public boolean isReturnFaiure() {
        return HttpCode.getState(Result) == HttpCode.RETURN_FAILURE;
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 11:07
     * <p>
     * 方法功能：未登录 -100
     */


    public boolean isNoLogin() {
        return HttpCode.getState(Result) == HttpCode.NO_LOGIN;
    }
}
