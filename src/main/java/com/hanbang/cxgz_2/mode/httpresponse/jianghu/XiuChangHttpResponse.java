package com.hanbang.cxgz_2.mode.httpresponse.jianghu;

import com.hanbang.cxgz_2.mode.javabean.base.BannerAdData;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/18.
 */

public class XiuChangHttpResponse extends BaseHttpResponse {

    public List<BannerAdData> bannerAdDatas = null;

    public List<XiuChangItemData> dongtaiList = null;

    public static XiuChangHttpResponse init() {
        XiuChangHttpResponse response = new XiuChangHttpResponse();
        response.bannerAdDatas = new ArrayList<>();
        response.dongtaiList = new ArrayList<>();
        return response;
    }

    public void clearAll() {
        dongtaiList.clear();
        bannerAdDatas.clear();
    }

    public void addData(XiuChangHttpResponse response) {
        dongtaiList.addAll(response.getDongtaiList());
        bannerAdDatas.addAll(response.getBannerAdDatas());
    }

    public List<BannerAdData> getBannerAdDatas() {
        return bannerAdDatas;
    }

    public List<XiuChangItemData> getDongtaiList() {
        return dongtaiList;
    }
}
