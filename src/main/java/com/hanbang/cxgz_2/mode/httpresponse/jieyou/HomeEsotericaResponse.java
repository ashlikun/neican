package com.hanbang.cxgz_2.mode.httpresponse.jieyou;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/8/15.
 */

public class HomeEsotericaResponse extends BaseHttpResponse {

    private ArrayList<EsotericaData> CaiPMiji;//秘籍

    public static HomeEsotericaResponse init() {
        HomeEsotericaResponse request = new HomeEsotericaResponse();
        request.CaiPMiji = new ArrayList<>();
        request.CaiPMiji.add(new EsotericaData());
        request.CaiPMiji.add(new EsotericaData());
        return request;
    }

    public void clearAll() {
        CaiPMiji.clear();
    }

    public void addData(HomeEsotericaResponse response) {
        CaiPMiji.addAll(response.getCaiPMiji());

    }

    public ArrayList<EsotericaData> getCaiPMiji() {
        return CaiPMiji;
    }




}
