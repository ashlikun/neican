package com.hanbang.cxgz_2.mode.httpresponse.home;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.FigureData;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.mode.javabean.home.HotFigureData;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationData;
import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/8/15.
 */

public class HomeHttpResponse extends BaseHttpResponse {

    private ArrayList<HotFigureData> HotFigure;//热门人物
    private ArrayList<HotAnswerData> HotWenda;//热门问答
    private ArrayList<FigureData> CaterersRwz;//人物志
    private ArrayList<InformationData> CaterersInfoHead;//资讯
    private ArrayList<EsotericaData> CaiPMiji;//秘籍
    private ArrayList<DemandRewardData> NeedOffered;//悬赏需求
    private ArrayList<StudyClassroomData> LearnClass;//学习课堂
    private ArrayList<ChefData> Chef;//厨师
    private ArrayList<ManagerData> Manager;//掌柜

    public static HomeHttpResponse init() {
        HomeHttpResponse request = new HomeHttpResponse();
        request.HotFigure = new ArrayList<>();
        request.HotWenda = new ArrayList<>();
        request.CaterersRwz = new ArrayList<>();
        request.CaterersInfoHead = new ArrayList<>();
        request.CaiPMiji = new ArrayList<>();
        request.NeedOffered = new ArrayList<>();
        request.LearnClass = new ArrayList<>();
        request.Chef = new ArrayList<>();
        request.Manager = new ArrayList<>();
        return request;
    }

    public void clearAll() {
        HotFigure.clear();
        HotWenda.clear();
        CaterersRwz.clear();
        CaterersInfoHead.clear();
        CaiPMiji.clear();
        NeedOffered.clear();
        LearnClass.clear();
        Chef.clear();
        Manager.clear();
    }

    public void addData(HomeHttpResponse response) {
        HotFigure.addAll(response.getHotFigure());
        HotWenda.addAll(response.getHotWenda());
        CaterersRwz.addAll(response.getCaterersRwz());
        CaterersInfoHead.addAll(response.getCaterersInfoHead());
        CaiPMiji.addAll(response.getCaiPMiji());
        NeedOffered.addAll(response.getNeedOffered());
        LearnClass.addAll(response.getLearnClass());
        Chef.addAll(response.getChef());
        Manager.addAll(response.getManager());

    }

    public ArrayList<HotFigureData> getHotFigure() {
        return HotFigure;
    }

    public void setHotFigure(ArrayList<HotFigureData> hotFigure) {
        HotFigure = hotFigure;
    }

    public ArrayList<HotAnswerData> getHotWenda() {
        return HotWenda;
    }

    public void setHotWenda(ArrayList<HotAnswerData> hotWenda) {
        HotWenda = hotWenda;
    }

    public ArrayList<FigureData> getCaterersRwz() {
        return CaterersRwz;
    }

    public void setCaterersRwz(ArrayList<FigureData> caterersRwz) {
        CaterersRwz = caterersRwz;
    }

    public ArrayList<InformationData> getCaterersInfoHead() {
        return CaterersInfoHead;
    }

    public void setCaterersInfoHead(ArrayList<InformationData> caterersInfoHead) {
        CaterersInfoHead = caterersInfoHead;
    }

    public ArrayList<EsotericaData> getCaiPMiji() {
        return CaiPMiji;
    }

    public void setCaiPMiji(ArrayList<EsotericaData> caiPMiji) {
        CaiPMiji = caiPMiji;
    }

    public ArrayList<DemandRewardData> getNeedOffered() {
        return NeedOffered;
    }

    public void setNeedOffered(ArrayList<DemandRewardData> needOffered) {
        NeedOffered = needOffered;
    }

    public ArrayList<StudyClassroomData> getLearnClass() {
        return LearnClass;
    }

    public void setLearnClass(ArrayList<StudyClassroomData> learnClass) {
        LearnClass = learnClass;
    }

    public ArrayList<ChefData> getChef() {
        return Chef;
    }

    public void setChef(ArrayList<ChefData> chef) {
        Chef = chef;
    }

    public ArrayList<ManagerData> getManager() {
        return Manager;
    }

    public void setManager(ArrayList<ManagerData> manager) {
        Manager = manager;
    }


}
