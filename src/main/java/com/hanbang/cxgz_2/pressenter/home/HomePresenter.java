package com.hanbang.cxgz_2.pressenter.home;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.httpresponse.home.HomeHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.home.iview.IHomeView;

/**
 * Created by Administrator on 2016/8/8.
 */

public class HomePresenter extends BasePresenter<IHomeView> {


    public void getHttpData() {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setShowLoadding(false).setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());
        final HttpCallBack httpCallBack = new HttpCallBack<HomeHttpResponse>(buider) {
            @Override
            public void onNext(HomeHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.clearData();
                    mvpView.upDataUi(result);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.ShouYe(httpCallBack));
    }


}
