package com.hanbang.cxgz_2.pressenter.can_da;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HomeHotAnswerResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.can_da.iview.IHomeHotAnswerView;

import java.util.List;

/**
 * Created by yang on 2016/8/17.
 */
public class HomeHotAnswerPresenter extends BasePresenter<IHomeHotAnswerView> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }

        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HomeHotAnswerResponse>(buider) {
            @Override
            public void onNext(HomeHotAnswerResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.clearData();
                    mvpView.upDataUi((List<HotAnswerData>) mvpView.getValidData(result.getData()));
                }
            }
        };


        HttpRequest.homeHotAnswer(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());


    }
}
