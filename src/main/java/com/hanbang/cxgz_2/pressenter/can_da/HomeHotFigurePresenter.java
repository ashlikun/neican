package com.hanbang.cxgz_2.pressenter.can_da;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.can_da.iview.IHomeHotFigureView;

/**
 * Created by yang on 2016/8/17.
 */
public class HomeHotFigurePresenter extends BasePresenter<IHomeHotFigureView> {


    public void getHttpData(final boolean first) {


        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HotFigureResponse>(buider) {
            @Override
            public void onNext(HotFigureResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (first) {
                        mvpView.clearData();
                    }


                    mvpView.upDataUi(result);
                }
            }
        };
        HttpRequest.hotFigureResponse(httpCallBack);
    }
}
