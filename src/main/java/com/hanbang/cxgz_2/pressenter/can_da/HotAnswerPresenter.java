package com.hanbang.cxgz_2.pressenter.can_da;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotAnswerHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.can_da.iview.IHotAnswer;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class HotAnswerPresenter extends BasePresenter<IHotAnswer> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());


        final HttpCallBack httpCallBack = new HttpCallBack<HotAnswerHttpResponse>(buider) {
            @Override
            public void onNext(HotAnswerHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<HotAnswerData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        HttpRequest.hotAnswer(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }






}
