package com.hanbang.cxgz_2.pressenter.can_da;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.can_da.MealAnswerDetailsData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.can_da.iview.IMealAnswerDetailsView;

import java.util.List;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 17:58
 * 邮箱　　：360621904@qq.com
 * UserId：用户ID,
 * 功能介绍：餐答详情
 */

public class MealAnserDetailsPresenter extends BasePresenter<IMealAnswerDetailsView> {


    public void getHttpData(int UserId) {


        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setShowLoadding(false).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<MealAnswerDetailsData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<MealAnswerDetailsData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {

                    mvpView.upDataUi(result.getList().get(0));

                } else {
                    mvpView.showWarningSnackbar(result.getMsg(), true);
                }
            }
        };

        HttpRequest.CdaHtFigureDetail(httpCallBack, UserId);
    }


}
