package com.hanbang.cxgz_2.pressenter.can_da;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.home.FigureData;
import com.hanbang.cxgz_2.mode.httpresponse.home.CateringFigureHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.can_da.iview.ICateringFigure;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class CateringFigurePresenter extends BasePresenter<ICateringFigure> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<CateringFigureHttpResponse>(buider) {
            @Override
            public void onNext(CateringFigureHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<FigureData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        HttpRequest.cateringFigure(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }






}
