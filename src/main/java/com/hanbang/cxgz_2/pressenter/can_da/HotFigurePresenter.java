package com.hanbang.cxgz_2.pressenter.can_da;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.home.HotFigureData;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.can_da.iview.IHotFigur;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class HotFigurePresenter extends BasePresenter<IHotFigur> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HotFigureHttpResponse>(buider) {
            @Override
            public void onNext(HotFigureHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<HotFigureData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        HttpRequest.hotFigure(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }






}
