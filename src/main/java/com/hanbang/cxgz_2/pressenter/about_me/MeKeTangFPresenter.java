package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.about_me.iview.IMeKeTangFView;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/3　8:59
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：我的秘籍
 */

public class MeKeTangFPresenter extends BasePresenter<IMeKeTangFView> {


    public void getMiJiHttpData(final boolean isStart, int id, int type) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setStatusChangListener(mvpView.getStatusChangListener())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout())
                .setShowLoadding(false);
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<StudyClassroomData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<StudyClassroomData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.UpDataMiJi((ArrayList<StudyClassroomData>) mvpView.getValidData(result.getList()));
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GetMyKetangList(httpCallBack, id, type, mvpView.getPageindex(), mvpView.getPageCount()));
    }


}
