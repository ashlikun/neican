package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.pressenter.common.CommonPresenter;
import com.hanbang.cxgz_2.utils.http.FileParam;
import com.hanbang.cxgz_2.utils.http.HttpProgressCallBack;
import com.hanbang.cxgz_2.utils.http.compress.Luban;
import com.hanbang.cxgz_2.utils.http.compress.OnCompressListener;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.iview.IssuerEsoteric_K_View;

import java.util.ArrayList;
import java.util.List;

/**
 * 悬赏需求，的公共Presenter
 * Created by Administrator on 2016/8/8.
 */

public class IssuerEsoteric_D_Presenter extends CommonPresenter<IssuerEsoteric_K_View> {

    /**
     * 上传买断式秘籍
     * <p>
     * type:类型，1为开放式，2为买断式
     * title:秘籍名称
     * tedian:特点
     * maidiantese:卖点特色
     * price:价格，免费的传0
     * cuisines_id:菜系，这个在效果图上没有体现出来，需要加上去的
     * img_url:主图，秘籍效果图，一张">url:主图，秘籍效果图，一张
     * shipin:视频，可选
     * <p>
     * 买断式需要传的参数:
     * begin_date:开始时间，格式为2015-09-06
     * end_date:结束时间，格式为2015-09-06
     * pingjia:评价
     * maiduanliucheng:买断流程
     * maiduanshengming:买断声明
     * huanyingzongchoumaiduan:欢迎众筹买断
     * AudioUrl:音频文件
     */
    public void getHttpData(
            final String type,
            final String title,
            final String maidiantese,
            final String price,
            final String cuisines_id,
            String img_url,
            final String shipin,
            final String begin_date,
            final String end_date,
            final String pingjia,
            final String maiduanliucheng,
            final String maiduanshengming,
            final String huanyingzongchoumaiduan,
            final String AudioUrl) {


        final List<FileParam> list = new ArrayList<>();
        FileParam.addFileParam(list, new FileParam("img_url", img_url));
        HttpProgressCallBack.Buider buider = new HttpProgressCallBack.Buider(IssuerEsoteric_D_Presenter.this);
        buider.setShowLoadding(false);
        final HttpProgressCallBack httpCallBack = new HttpProgressCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upLoading(true);
                    mvpView.showSnackbar(result.getMsg(), SnackbarUtil.Info, true);
                } else {
                    mvpView.upLoading(false);
                    mvpView.showWarningSnackbar(result.getMsg());
                }
            }

            @Override
            public void onCompleted() {
                super.onCompleted();
                Luban.deleteDir();
            }
        };


        new Luban().load(list).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {
                httpCallBack.onStart(true);
            }

            @Override
            public void onSuccess(ArrayList<FileParam> files) {
                FileParam.addFileParam(files, new FileParam("AudioUrl", AudioUrl));
                FileParam.addFileParam(files, new FileParam("shipin", shipin));
                mvpView.addSubscription(
                        HttpRequest.issuerEsoteric_D(httpCallBack,
                                type,
                                title,
                                maidiantese,
                                price,
                                cuisines_id,
                                begin_date.toString(),
                                end_date.toString(),
                                pingjia,
                                maiduanliucheng,
                                maiduanshengming,
                                huanyingzongchoumaiduan,
                                files
                        ));
            }

            @Override
            public void onError(Throwable e) {
                httpCallBack.onError(e);
            }

            @Override
            public void onLoading(int progress, long total) {
                httpCallBack.onLoading(progress, total, false, true);
            }

        }).launch();


    }


}
