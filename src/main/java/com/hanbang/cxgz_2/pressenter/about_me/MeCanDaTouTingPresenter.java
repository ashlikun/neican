package com.hanbang.cxgz_2.pressenter.about_me;


import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.about_me.MyAnswerEavesdropListData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.about_me.iview.IMeCanDaTouTingView;

import java.util.List;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/21 9:01
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的餐答
 */
public class MeCanDaTouTingPresenter extends BasePresenter<IMeCanDaTouTingView> {
    /**
     * 我----资讯和餐答 我的提问和我的偷听列表
     * ZiXunforMeTiWenAndTouTing
     *
     * @return
     */
    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<MyAnswerEavesdropListData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<MyAnswerEavesdropListData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<MyAnswerEavesdropListData>) mvpView.getValidData(result.getList()));
                }
            }
        };

        HttpRequest.ZiXunforMeTiWenAndTouTing(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }


}
