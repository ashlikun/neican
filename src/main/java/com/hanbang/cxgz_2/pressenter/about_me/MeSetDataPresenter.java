package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.pressenter.common.CommonPresenter;
import com.hanbang.cxgz_2.utils.http.FileParam;
import com.hanbang.cxgz_2.utils.http.HttpProgressCallBack;
import com.hanbang.cxgz_2.utils.http.compress.Luban;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.iview.IMeSetDAtaView;

import java.util.ArrayList;
import java.util.List;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 14:10
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：个人资料的设置
 * <p>
 * <p>
 * /**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 14:40
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：个人简介编辑
 * <p>
 * GeRenJianJieEdit
 * userid:用户ID
 * avatar:头像
 * UserName:昵称姓名
 * Age:年龄
 * Sex:性别
 * JobId:职位
 * Company:公司
 * city_id:城市 int
 * CuisinesId:擅长
 * Signfoods:招牌菜(经理人不需要传)
 * Workjingli:工作经历
 */


public class MeSetDataPresenter extends CommonPresenter<IMeSetDAtaView> {


    public void setUserData(
            final int userid,
            final String avatar,
            final String UserName,
            final String Age,
            final String Sex,
            final int JobId,
            final String Company,
            final String city_id,
            final String CuisinesId,
            final String Signfoods,
            final String Workjingli) {


        final List<FileParam> list = new ArrayList<>();
        FileParam.addFileParam(list, new FileParam("img_url", avatar));

        HttpProgressCallBack.Buider buider = new HttpProgressCallBack.Buider(MeSetDataPresenter.this);
        buider.setShowLoadding(false);

        final HttpProgressCallBack httpCallBack = new HttpProgressCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upLoading(true);
                    mvpView.showSnackbar(result.getMsg(), SnackbarUtil.Info, true);
                } else {
                    mvpView.upLoading(false);
                    mvpView.showWarningSnackbar(result.getMsg());
                }
            }

            @Override
            public void onCompleted() {
                super.onCompleted();
                Luban.deleteDir();
            }
        };


    }


}
