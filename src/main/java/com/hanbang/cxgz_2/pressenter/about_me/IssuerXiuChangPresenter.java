package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.pressenter.common.CommonPresenter;
import com.hanbang.cxgz_2.utils.http.FileParam;
import com.hanbang.cxgz_2.utils.http.HttpProgressCallBack;
import com.hanbang.cxgz_2.utils.http.compress.Luban;
import com.hanbang.cxgz_2.utils.http.compress.OnCompressListener;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.iview.IssuerClassroomView;

import java.util.ArrayList;
import java.util.List;

/**
 * 悬赏需求，的公共Presenter
 * Created by Administrator on 2016/8/8.
 */

public class IssuerXiuChangPresenter extends CommonPresenter<IssuerClassroomView> {

    /**
     * 发布秀场
     *
     * @param phone    手机型号
     * @param position 地理位置
     * @param text     正文内容
     * @param datas    图片集合
     * @return
     */
    public void issuer(
            final String phone,
            final String position,
            final String text,
            ArrayList<String> datas
    ) {

        HttpProgressCallBack.Buider buider = new HttpProgressCallBack.Buider(this);
        buider.setShowLoadding(false);

        final HttpProgressCallBack httpCallBack = new HttpProgressCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upLoading(true);
                    mvpView.showSnackbar(result.getMsg(), SnackbarUtil.Info, true);
                } else {
                    mvpView.upLoading(false);
                    mvpView.showWarningSnackbar(result.getMsg());

                }
            }

            @Override
            public void onCompleted() {
                super.onCompleted();
                Luban.deleteDir();
            }
        };


        //把文件放入集合中
        final List<FileParam> list = new ArrayList<>();
        list.addAll(FileParam.getStringToFileParam("feed", datas));

        new Luban().load(list).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {

            }

            //压缩成功进行上传
            @Override
            public void onSuccess(ArrayList<FileParam> files) {

                mvpView.addSubscription(
                        HttpRequest.dynamics_Add(httpCallBack,
                                phone,
                                position,
                                text,
                                files
                        ));
            }

            @Override
            public void onError(Throwable e) {

            }

            //进度的回调
            @Override
            public void onLoading(int progress, long total) {
                httpCallBack.onLoading(progress, total, false, true);
            }
        }).launch();


    }


}
