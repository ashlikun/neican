package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.about_me.iview.IMeXuanShangView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class MeXuanShangPresenter extends BasePresenter<IMeXuanShangView> {

    /**
     *
     */
    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<DemandRewardData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<DemandRewardData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<DemandRewardData>) mvpView.getValidData(result.getList()));
                }
            }
        };
        HttpRequest.MyDtXuShang(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }


}
