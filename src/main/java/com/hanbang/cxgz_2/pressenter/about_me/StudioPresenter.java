package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioBeiJinData;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.about_me.iview.IStudioView;

import java.util.List;

import static com.hanbang.cxgz_2.utils.other.VersionUpdata.isStart;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/3　8:59
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioPresenter extends BasePresenter<IStudioView> {


    public void getHttpData(int id) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setShowLoadding(false);
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<StudioData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<StudioData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    if (result.getList().size() > 0) {
                        mvpView.upDataUI(result.getList().get(0));
                    } else {
                        mvpView.upDataUI(null);
                    }
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GeRenZhuye(httpCallBack, id));

    }


    public void guanzhu(final StudioData data) {
        final GuanZhu guanzhu = GuanZhu.getState(data.isguanzhu());
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在" + guanzhu.getValuse());
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.guanzhuChanged();
                } else if (result.isReturnFaiure()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.guanzhuChanged();
                }
            }
        };
        mvpView.addSubscription(!guanzhu.getKey() ?
                HttpRequest.Guanzhu(httpCallBack, data.getId()) :
                HttpRequest.QuXiaoGuanzhu(httpCallBack, data.getId())
        );
    }

    public void guanzhu(final StudioBeiJinData data) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在更新");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    mvpView.beijingCHang(data);
                }
            }
        };
        mvpView.addSubscription(
                HttpRequest.UpdateHomeImg(httpCallBack, data.getImg_urlMiddle())
        );
    }
}
