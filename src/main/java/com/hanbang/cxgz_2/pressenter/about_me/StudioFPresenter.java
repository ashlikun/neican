package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.enumeration.ModeType;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.about_me.iview.IStudioFView;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/3　8:59
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioFPresenter extends BasePresenter<IStudioFView> {

    public void getXiuChangHttpData(final boolean isStart, int id) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setStatusChangListener(mvpView.getStatusChangListener())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout())
                .setShowLoadding(false);
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<XiuChangItemData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<XiuChangItemData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.UpDataXiuChang((ArrayList<XiuChangItemData>) mvpView.getValidData(result.getList()));
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GetMydynamicsList(httpCallBack, id, mvpView.getPageindex(), mvpView.getPageCount()));

    }

    public void getKeTangHttpData(final boolean isStart, int id) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setStatusChangListener(mvpView.getStatusChangListener())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout())
                .setShowLoadding(false);
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<StudyClassroomData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<StudyClassroomData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.UpDataKeTang((ArrayList<StudyClassroomData>) mvpView.getValidData(result.getList()));
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GetMyKetangList(httpCallBack, id, 1, mvpView.getPageindex(), mvpView.getPageCount()));

    }

    public void getMiJiHttpData(final boolean isStart, int id) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setStatusChangListener(mvpView.getStatusChangListener())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout())
                .setShowLoadding(false);
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<EsotericaData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<EsotericaData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.UpDataMiJi((ArrayList<EsotericaData>) mvpView.getValidData(result.getList()));
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GetMyMijiList(httpCallBack, id, 1, mvpView.getPageindex(), mvpView.getPageCount()));
    }

    public void dianzhan(final XiuChangItemData data, final int position) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在点赞");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.addDianZhan();
                } else if (result.isReturnFaiure()) {
                    data.setClickDianzhan(true);
                    mvpView.itemChanged(position);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PointaPraise(httpCallBack, data.getId(), ModeType.DONGTAI));
    }

    public void guanzhu(final XiuChangItemData data, final int position, final GuanZhu guanzhu) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在" + guanzhu.getValuse());
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.itemChanged(position);
                } else if (result.isReturnFaiure()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.itemChanged(position);
                }
            }
        };
        mvpView.addSubscription(!guanzhu.getKey() ?
                HttpRequest.Guanzhu(httpCallBack, data.getUid()) :
                HttpRequest.QuXiaoGuanzhu(httpCallBack, data.getUid())
        );
    }

    public void pinglun(final XiuChangItemData data, final int position, String pinglun) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在评论");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.addPingLun();
                    mvpView.itemChanged(position);
                } else if (result.isReturnFaiure()) {
                    data.setClickDianzhan(true);
                    mvpView.itemChanged(position);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PinglunAdd(httpCallBack, data.getId(), ModeType.DONGTAI, pinglun));
    }


}
