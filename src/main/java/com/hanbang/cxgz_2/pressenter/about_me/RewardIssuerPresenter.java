package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.appliction.base.view.IUpdataUIView;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.utils.http.FileParam;
import com.hanbang.cxgz_2.utils.http.HttpProgressCallBack;
import com.hanbang.cxgz_2.utils.http.compress.Luban;
import com.hanbang.cxgz_2.utils.http.compress.OnCompressListener;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 悬赏需求，的公共Presenter
 * Created by Administrator on 2016/8/8.
 */

public class RewardIssuerPresenter extends BasePresenter<IUpdataUIView<Boolean>> {

    /**
     * type：0:菜品研发,1:其他需求,2:招聘,3:求职,4:门店转让,5:二手设备,6:加盟代理
     * XuQiuContent:悬赏内容
     * XunShangPrice:悬赏价格
     * CityId:城市ID(2,3,4,5)
     * Address:地址(2,4,5)
     * ShopingName:店名(2,4,5)
     * Company:公司名称(6)
     * XuQiuImg:图片
     *
     * @param type
     * @param XuQiuContent
     * @param XunShangPrice
     * @param CityId
     * @param Address
     * @param ShopingName
     * @param Company
     * @param files
     */
    public void getHttpData(
            final String type,
            final String XuQiuContent,
            final String XunShangPrice,
            final String CityId,
            final String Address,
            final String ShopingName,
            final String Company,
            final List<String> files) {


        HttpProgressCallBack.Buider buider = new HttpProgressCallBack.Buider(this);
        buider.setShowLoadding(false);

        final HttpProgressCallBack httpCallBack = new HttpProgressCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upDataUi(true);
                    mvpView.showSnackbar(result.getMsg(), SnackbarUtil.Info, true);
                } else {
                    mvpView.upDataUi(false);
                    mvpView.showWarningSnackbar(result.getMsg());

                }
            }

            @Override
            public void onCompleted() {
                super.onCompleted();
                Luban.deleteDir();
            }
        };





        //把文件放入集合中
        final List<FileParam> list = new ArrayList<>();
        if (files != null) {
            list.addAll(FileParam.getStringToFileParam("XuQiuImg", files));
        }


        new Luban().load(list).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {

            }

            //压缩成功进行上传
            @Override
            public void onSuccess(ArrayList<FileParam> files) {


                mvpView.addSubscription(
                        HttpRequest.rewardIssuer(httpCallBack,
                                type,
                                XuQiuContent,
                                XunShangPrice,
                                CityId,
                                Address,
                                ShopingName,
                                Company,
                                files
                        ));
            }

            @Override
            public void onError(Throwable e) {

            }

            //进度的回调
            @Override
            public void onLoading(int progress, long total) {
                httpCallBack.onLoading(progress, total, false, true);
            }
        }).launch();


    }


}
