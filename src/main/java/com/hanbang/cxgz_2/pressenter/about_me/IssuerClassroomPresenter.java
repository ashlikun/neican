package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.pressenter.common.CommonPresenter;
import com.hanbang.cxgz_2.utils.http.FileParam;
import com.hanbang.cxgz_2.utils.http.HttpProgressCallBack;
import com.hanbang.cxgz_2.utils.http.compress.Luban;
import com.hanbang.cxgz_2.utils.http.compress.OnCompressListener;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.iview.IssuerClassroomView;

import java.util.ArrayList;
import java.util.List;

/**
 * 悬赏需求，的公共Presenter
 * Created by Administrator on 2016/8/8.
 */

public class IssuerClassroomPresenter extends CommonPresenter<IssuerClassroomView> {

    /**
     * 上传课堂
     * address:开课地址
     * begin_time:开课时间，格式为yyyy-MM-dd HH:mm:ss
     * end_time:截止时间，格式为yyyy-MM-dd HH:mm:ss
     * price:价格，免费的为0
     * remark:课堂简介
     * title:标题
     * type:课堂形式，1为线下课堂，2为视频课堂
     * studentNum:招生人数
     * img_url:组图片，只有一张
     * img_guocheng:视频
     * content_img:图片
     * AudioUrl:音频文件
     *
     * @return
     */
    public void getHttpData(
            final String address,
            final String begin_time,
            final String end_time,
            final String price,
            final String remark,
            final String title,
            final String type,
            final String studentNum,
            final String img_url,//封面
            final String img_guocheng,//视频
            ArrayList<String> content_img,//多图
            final String AudioUrl//音频
    ) {


        HttpProgressCallBack.Buider buider = new HttpProgressCallBack.Buider(this);
        buider.setShowLoadding(false);

        final HttpProgressCallBack httpCallBack = new HttpProgressCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {

                    mvpView.upLoading(true);
                    mvpView.showSnackbar(result.getMsg(), SnackbarUtil.Info, true);
                } else {
                    mvpView.upLoading(false);
                    mvpView.showWarningSnackbar(result.getMsg());
                }


            }

            @Override
            public void onCompleted() {
                super.onCompleted();
                Luban.deleteDir();
            }
        };


        //把文件放入集合中
        final List<FileParam> list = new ArrayList<>();
        list.addAll(FileParam.getStringToFileParam("feed", content_img));
        FileParam.addFileParam(list, new FileParam("img_url", img_url));

        new Luban().load(list).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {

            }

            //压缩成功进行上传
            @Override
            public void onSuccess(ArrayList<FileParam> files) {

                FileParam.addFileParam(files, new FileParam("img_guocheng", img_guocheng));
                FileParam.addFileParam(files, new FileParam("AudioUrl", AudioUrl));


                /**
                 * @param address:开课地址
                 * @param begin_time:开课时间，格式为yyyy-MM-dd HH:mm:ss
                 * @param end_time:截止时间，格式为yyyy-MM-dd   HH:mm:ss
                 * @param price:价格，免费的为0
                 * @param remark:课堂简介
                 * @param title:标题
                 * @param type:课堂形式，1为线下课堂，2为视频课堂
                 * @param Zhaosrenshu:招生人数
                 * @param files:图片，视频，音频，多图
                 * @return
                 */
                mvpView.addSubscription(
                        HttpRequest.issuerClassroom(httpCallBack,
                                address,
                                begin_time,
                                end_time,
                                price,
                                remark,
                                title,
                                type,
                                studentNum,
                                files
                        ));
            }

            @Override
            public void onError(Throwable e) {

            }

            //进度的回调
            @Override
            public void onLoading(int progress, long total) {
                httpCallBack.onLoading(progress, total, false, true);
            }
        }).launch();

    }


}
