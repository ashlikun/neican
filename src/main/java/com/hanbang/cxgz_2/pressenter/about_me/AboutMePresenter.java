package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.mode.enumeration.AboutMeTypeEnum;
import com.hanbang.cxgz_2.mode.javabean.about_me.AboutMeTypeData;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/8/8.
 */

public class AboutMePresenter extends BasePresenter<BaseView> {

    public ArrayList<AboutMeTypeData> getGridData() {
        final ArrayList<AboutMeTypeData> datas = new ArrayList<>();
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.CHAN_DA));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.DING_DAN));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.XIU_CHANG));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.LAO_SHI_XUE_YUAN));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.YAO_QING_HAO_YOU));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.SHOU_CANG));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.GONG_ZUO_SHI));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.YU_E));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.MI_JI));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.KE_TANG));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.GUAN_YU));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.SHUO_MING));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.DENG_LU));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.ZHU_CE));
        datas.add(new AboutMeTypeData(AboutMeTypeEnum.CE_SHI));
        return datas;
    }
}
