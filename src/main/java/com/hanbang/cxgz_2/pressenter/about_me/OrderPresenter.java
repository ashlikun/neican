package com.hanbang.cxgz_2.pressenter.about_me;


import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeOrderData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.about_me.iview.IOrderView;

import java.util.List;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/21 9:01
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的订单
 */
public class OrderPresenter extends BasePresenter<IOrderView> {
    /**
     * 我----资讯和餐答 我的提问和我的偷听列表
     * ZiXunforMeTiWenAndTouTing
     *
     * @return
     */
    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<MeOrderData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<MeOrderData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<MeOrderData>) mvpView.getValidData(result.getList()));
                }
            }
        };

        HttpRequest.GetOrderList(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }


}
