package com.hanbang.cxgz_2.pressenter.about_me;


import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeBalanceData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.about_me.iview.IYuEView;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 14:38
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的余额
 */

public class YuEPresenter extends BasePresenter<IYuEView> {

    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<MeBalanceData>(buider) {
            @Override
            public void onNext(MeBalanceData result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi(result);
                }
            }
        };

        HttpRequest.GetAccountDetail(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }


}
