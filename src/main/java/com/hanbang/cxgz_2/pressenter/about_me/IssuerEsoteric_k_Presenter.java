package com.hanbang.cxgz_2.pressenter.about_me;

import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.EsotericZhuFuLiaoData;
import com.hanbang.cxgz_2.pressenter.common.CommonPresenter;
import com.hanbang.cxgz_2.utils.http.FileParam;
import com.hanbang.cxgz_2.utils.http.HttpProgressCallBack;
import com.hanbang.cxgz_2.utils.http.compress.Luban;
import com.hanbang.cxgz_2.utils.http.compress.OnCompressListener;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.iview.IssuerEsoteric_K_View;

import java.util.ArrayList;
import java.util.List;

/**
 * 悬赏需求，的公共Presenter
 * Created by Administrator on 2016/8/8.
 */

public class IssuerEsoteric_k_Presenter extends CommonPresenter<IssuerEsoteric_K_View> {

    /**
     * //上传开放式秘籍
     * type:类型，1为开放式，2为买断式
     * title:秘籍名称
     * tedian:特点
     * maidiantese:卖点特色
     * price:价格，免费的传0
     * cuisines_id:菜系，这个在效果图上没有体现出来，需要加上去的
     * img_url:主图，秘籍效果图，一张">url:主图，秘籍效果图，一张
     * shipin:视频，可选
     * <p>
     * 开放式需要传的参数:
     * zhuliao:主料，格式为:（名称:数量|名称:数量）
     * fuliao:辅料，格式为:（名称:数量|名称:数量）
     * gongyiliucheng:工艺流程
     * zhuyishixiang:注意事项
     * img_guocheng:过程图片，可为多张
     * AudioUrl:音频文件
     */
    public void getHttpData(
            final String type,
            final String title,
            final String tedian,
            final String maidiantese,
            final String price,
            final String cuisines_id,
            final String img_url,//海报
            final String shipin,//视频
            ArrayList<EsotericZhuFuLiaoData> zhuLiaoDatas,
            ArrayList<EsotericZhuFuLiaoData> fuLiaoDatas,
            final String gongyiliucheng,
            final String zhuyishixiang,
            final ArrayList<String> img_guocheng,//过程图片
            final String AudioUrl//音频
    ) {


        HttpProgressCallBack.Buider buider = new HttpProgressCallBack.Buider(this);
        buider.setShowLoadding(false);

        final HttpProgressCallBack httpCallBack = new HttpProgressCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upLoading(true);
                    mvpView.showSnackbar(result.getMsg(), SnackbarUtil.Info, true);
                } else {
                    mvpView.upLoading(false);
                    mvpView.showWarningSnackbar(result.getMsg());

                }
            }

            @Override
            public void onCompleted() {
                super.onCompleted();
                Luban.deleteDir();
            }
        };


        //主料拼接字符串
        final StringBuffer zhuliao = new StringBuffer();
        if (zhuLiaoDatas.size() > 0) {
            for (int i = 0; i < zhuLiaoDatas.size(); i++) {
                if (i != 0) {
                    zhuliao.append("|" + zhuLiaoDatas.get(i).getName() + ":" + zhuLiaoDatas.get(i).getWeight());
                } else {
                    zhuliao.append(zhuLiaoDatas.get(i).getName() + ":" + zhuLiaoDatas.get(i).getWeight());
                }
            }
        }

        //主料拼接字符串
        final StringBuffer fuliao = new StringBuffer();
        if (fuLiaoDatas.size() > 0) {
            for (int i = 0; i < fuLiaoDatas.size(); i++) {
                if (i != 0) {
                    fuliao.append("|" + fuLiaoDatas.get(i).getName() + ":" + fuLiaoDatas.get(i).getWeight());
                } else {
                    fuliao.append(fuLiaoDatas.get(i).getName() + ":" + fuLiaoDatas.get(i).getWeight());
                }
            }
        }


        //把文件放入集合中
        final List<FileParam> list = new ArrayList<>();
        list.addAll(FileParam.getStringToFileParam("img_guocheng", img_guocheng));
        FileParam.addFileParam(list, new FileParam("img_url", img_url));

        new Luban().load(list).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {

            }

            //压缩成功进行上传
            @Override
            public void onSuccess(ArrayList<FileParam> files) {

                if (!StringUtils.isBlank(shipin)) {
                    FileParam.addFileParam(files, new FileParam("shipin", shipin));
                }
                if (!StringUtils.isBlank(AudioUrl)) {
                    FileParam.addFileParam(files, new FileParam("AudioUrl", AudioUrl));
                }


                mvpView.addSubscription(
                        HttpRequest.issuerEsoteric_k(httpCallBack,
                                type,
                                title,
                                tedian,
                                maidiantese,
                                price,
                                cuisines_id,
                                zhuliao.toString(),
                                fuliao.toString(),
                                gongyiliucheng,
                                zhuyishixiang,
                                files

                        ));
            }

            @Override
            public void onError(Throwable e) {

            }

            //进度的回调
            @Override
            public void onLoading(int progress, long total) {
                httpCallBack.onLoading(progress, total, false, true);
            }
        }).launch();


    }


}
