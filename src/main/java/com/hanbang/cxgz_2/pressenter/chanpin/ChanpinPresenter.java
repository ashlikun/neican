package com.hanbang.cxgz_2.pressenter.chanpin;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.chanpin.ItemProductShopData;
import com.hanbang.cxgz_2.mode.httpresponse.chanpin.ChanpinHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.chanpin.iview.IChanpinView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class ChanpinPresenter extends BasePresenter<IChanpinView> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setShowLoadding(false).setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());
        final HttpCallBack httpCallBack = new HttpCallBack<ChanpinHttpResponse>(buider) {
            @Override
            public void onNext(ChanpinHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUI((List<ItemProductShopData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        mvpView.addSubscription(HttpRequest.getCaterersInfoHeadMoreThan(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount()));
    }


}
