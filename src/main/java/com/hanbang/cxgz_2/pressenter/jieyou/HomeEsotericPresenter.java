package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeEsotericHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.jieyou.iview.IHomeEsotericView;

/**
 * Created by yang on 2016/8/17.
 */
public class HomeEsotericPresenter extends BasePresenter<IHomeEsotericView> {


    public void getHttpData() {
        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HomeEsotericHttpResponse>(buider) {
            @Override
            public void onNext(HomeEsotericHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.clearData();
                    mvpView.upDataUi(result);
                }
            }
        };
        HttpRequest.mijiShouYe(httpCallBack);
    }
}
