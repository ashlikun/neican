package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.StudyClassroomHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.jieyou.iview.IStudyClassroomView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class StudyClassroomPresenter extends BasePresenter<IStudyClassroomView> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<StudyClassroomHttpResponse>(buider) {
            @Override
            public void onNext(StudyClassroomHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<StudyClassroomData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        HttpRequest.studyClassroomList(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }






}
