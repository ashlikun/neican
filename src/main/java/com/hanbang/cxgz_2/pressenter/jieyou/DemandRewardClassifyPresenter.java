package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.DemandRewardClassifyHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jieyou.iview.IDemanRewardClassifyView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class DemandRewardClassifyPresenter extends BasePresenter<IDemanRewardClassifyView> {

    /**
     * 悬赏频道----0：菜品研发更多----2：招聘更多----3：求职更多----4：门店转让更多----5：二手设备更多----6：加盟代理更多
     * NeedOfferedMoreThan
     *
     * @param XuQiuType
     */
    public void getHttpData(final boolean isStart, int XuQiuType) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<DemandRewardClassifyHttpResponse>(buider) {
            @Override
            public void onNext(DemandRewardClassifyHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<DemandRewardData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        HttpRequest.NeedOfferedMoreThan(httpCallBack, XuQiuType, mvpView.getPageindex(), mvpView.getPageCount());
    }


}
