package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.EsotericHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.jieyou.iview.IEsotericClassifyView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class EsotericClassifyPresenter extends BasePresenter<IEsotericClassifyView> {


    public void getHttpData(final boolean isStart, int biaoqian) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<EsotericHttpResponse>(buider) {
            @Override
            public void onNext(EsotericHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<EsotericaData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        HttpRequest.esotericList(httpCallBack, biaoqian, mvpView.getPageindex(), mvpView.getPageCount());
    }


}
