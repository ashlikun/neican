package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeRewardHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jieyou.iview.IHomeDemandRewardView;

/**
 * Created by yang on 2016/8/17.
 */
public class HomeDemandRewardPresenter extends BasePresenter<IHomeDemandRewardView> {


    public void getHttpData() {
        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HomeRewardHttpResponse>(buider) {
            @Override
            public void onNext(HomeRewardHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.clearData();
                    mvpView.upDataUi(result);
                }
            }
        };
        HttpRequest.jieYouReward(httpCallBack);
    }
}
