package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.EsotericDetails_k_HttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.EsotericZhuFuLiaoData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.jieyou.iview.IEsotericDetails_k_View;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/8/8.
 */

public class EsotericDetails_K_Presenter extends BasePresenter<IEsotericDetails_k_View> {


    public void getHttpData(int id) {


        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setShowLoadding(false).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<EsotericDetails_k_HttpResponse>(buider) {
            @Override
            public void onNext(EsotericDetails_k_HttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upDataUi(result.getMijiDetail());
                } else {
                    mvpView.showWarningSnackbar(result.getMsg(), true);
                }
            }
        };
        HttpRequest.esotericDetails_k(httpCallBack, id);
    }


    /**
     * 把主料与辅料放入集合中
     *
     * @param str
     * @return
     */
    public ArrayList<EsotericZhuFuLiaoData> transitionMaterial(String str) {
        ArrayList<EsotericZhuFuLiaoData> list = new ArrayList();

        try {

            if (str != null) {
                //如果字条串是以“|”结尾就把它截取
                if (str.substring(str.length() - 1).equals("|")) {
                    str = str.substring(0, str.lastIndexOf("|"));
                }
                //中间部分以“|”分开
                String[] dataArray = str.split("\\|");
                for (int i = 0; i < dataArray.length; i++) {
                    //在以“:”号分开
                    if (dataArray[i].toString().contains(":")) {

                        String[] type = dataArray[i].toString().split(":");

                        if (type.length >= 1) {
                            list.add(new EsotericZhuFuLiaoData(type[0], type.length > 1 ? type[1] : ""));
                        }

                    } else {
                        list.add(new EsotericZhuFuLiaoData(dataArray[i], ""));
                    }
                }

                return list;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    /**
     * 把过程图片，转换成集合
     *
     * @param str
     * @return
     */
    public ArrayList<EsotericZhuFuLiaoData> transitionPicture(String str) {
        ArrayList<EsotericZhuFuLiaoData> list = new ArrayList();

        try {
            //把过程图片截取成数组
            if (!StringUtils.isBlank(str) && str.contains(",")) {
                String[] array = str.split(",");

                for (int i = 0; i < array.length; i++) {
                    list.add(new EsotericZhuFuLiaoData("步骤" + i+1, array[i]));
                }
            }

            return list;


        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


}
