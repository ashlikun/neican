package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.jianghu.StudyClassroomDetailsData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.jieyou.iview.IStudyClassroomDetailsView;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/12 14:45
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：学习课堂详情
 */
public class StudyClassroomDetailsPresenter extends BasePresenter<IStudyClassroomDetailsView> {


    public void getHttpData(int id) {


        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setShowLoadding(false).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<StudyClassroomDetailsData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<StudyClassroomDetailsData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upDataUi(result.getList().get(0));
                } else {
                    mvpView.showWarningSnackbar(result.getMsg(), true);
                }
            }
        };

        HttpRequest.Ketang_Details(httpCallBack, id);
    }


    /**
     * 把过程图片，转换成集合
     *
     * @param str
     * @return
     */
    public ArrayList<String> transitionPicture(String str) {
        ArrayList<String> list = new ArrayList();


        try {
            //把过程图片截取成数组
            if (!StringUtils.isBlank(str) && str.contains(",")) {
                String[] array = str.split(",");
                for (int i = 0; i < array.length; i++) {
                    list.add(array[i]);
                }
            }
            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


}
