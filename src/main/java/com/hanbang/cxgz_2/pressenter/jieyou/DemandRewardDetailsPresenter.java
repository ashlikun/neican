package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.jianghu.DemandRewardDetailsData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.jieyou.iview.IRemandRewardDetailsView;

import java.util.ArrayList;
import java.util.List;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/13 14:54
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：悬赏需求
 */

public class DemandRewardDetailsPresenter extends BasePresenter<IRemandRewardDetailsView> {


    public void getHttpData(String id) {


        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setShowLoadding(false).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<DemandRewardDetailsData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<DemandRewardDetailsData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upDataUi(result.getList().get(0));
                } else {
                    mvpView.showWarningSnackbar(result.getMsg(), true);
                }
            }
        };

        HttpRequest.XuShang_XiangQing(httpCallBack, id);
    }


    /**
     * 把过程图片，转换成集合
     *
     * @param str
     * @return
     */
    public ArrayList<String> transitionPicture(String str) {
        ArrayList<String> list = new ArrayList();


        try {
            //把过程图片截取成数组
            if (!StringUtils.isBlank(str) && str.contains(",")) {
                String[] array = str.split(",");
                for (int i = 0; i < array.length; i++) {
                    list.add(array[i]);
                }
            }
            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


}
