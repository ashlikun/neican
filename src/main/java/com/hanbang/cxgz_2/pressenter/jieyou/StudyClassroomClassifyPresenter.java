package com.hanbang.cxgz_2.pressenter.jieyou;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jieyou.iview.IStudyClassroomClassifyView;

import java.util.List;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 13:51
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：
 */

public class StudyClassroomClassifyPresenter extends BasePresenter<IStudyClassroomClassifyView> {


    public void getHttpData(final boolean isStart,int type) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<StudyClassroomData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<StudyClassroomData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi(result.getList());
                }
            }
        };
        HttpRequest.LearClassMorethan(httpCallBack,type, mvpView.getPageindex(), mvpView.getPageCount());
    }






}
