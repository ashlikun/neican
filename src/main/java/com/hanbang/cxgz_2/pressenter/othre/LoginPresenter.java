package com.hanbang.cxgz_2.pressenter.othre;

import android.app.Activity;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.utils.http.GsonHelper;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.utils.other.LogUtils;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.login.iview.IloginView;

import org.json.JSONObject;

import okhttp3.ResponseBody;

/**
 * Created by yang on 2016/8/17.
 */
public class LoginPresenter extends BasePresenter<IloginView> {

    public void getHttpData(final Activity activity, String phone) {

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setShowLoadding(true).setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager());

        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {

            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);

                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.clearData();
                    mvpView.isVerification(true);
                    SnackbarUtil.showLong(activity, result.getMsg(), SnackbarUtil.Info).show();
                } else {
                    mvpView.isVerification(false);
                    SnackbarUtil.showLong(activity, result.getMsg(), SnackbarUtil.Info).show();
                }

            }
        };

        mvpView.addSubscription(HttpRequest.getLoginVerificationCode(httpCallBack, phone));
    }


    public void login(String telphone, String code) {

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this);

        HttpCallBack httpCallBack = new HttpCallBack<ResponseBody>(buider) {

            @Override
            public void onNext(ResponseBody result) {
                super.onNext(result);

                try {
                    JSONObject jsonObject = new JSONObject(result.string().toString());
                    int status = jsonObject.getInt("Result");

                    if (status == 0) {
                        UserData userData = GsonHelper.getGson().fromJson(jsonObject.getJSONObject("model").toString(), UserData.class);
                        mvpView.login(userData);
                    } else {
                        mvpView.showWarningSnackbar(jsonObject.getString("Msg"));
                    }


                    LogUtils.e(result.string());

                } catch (Exception e) {
                    e.printStackTrace();
                }


                mvpView.clearData();

            }
        };


        mvpView.addSubscription(HttpRequest.login(httpCallBack, telphone, code));
    }


}
