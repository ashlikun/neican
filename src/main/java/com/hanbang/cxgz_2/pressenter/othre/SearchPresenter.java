package com.hanbang.cxgz_2.pressenter.othre;

import android.app.Activity;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.enumeration.SearchEnum;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.other.SearchData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.other.iview.ISearchView;

import java.util.ArrayList;

/**
 * Created by yang on 2016/8/17.
 */
public class SearchPresenter extends BasePresenter<ISearchView> {

    public void getHttpData(final Activity activity, String phone) {

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setShowLoadding(true).setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager());

        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {

            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);

                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.clearData();

                    SnackbarUtil.showLong(activity, result.getMsg(), SnackbarUtil.Info).show();
                } else {

                    SnackbarUtil.showLong(activity, result.getMsg(), SnackbarUtil.Info).show();
                }

            }
        };

        mvpView.addSubscription(HttpRequest.getLoginVerificationCode(httpCallBack, phone));
    }

    /**
     * 返回搜索分类内容
     *
     * @param activity
     * @return /**
     * <string name="ziXun">资讯</string>
     * <string name="canDa">餐答</string>
     * <string name="xiuCang">秀场</string>
     * <string name="chuShi">厨师</string>
     * <string name="zhangGui">掌柜</string>
     * <string name="miJi">秘籍</string>
     * <string name="keTang">课堂</string>
     * <string name="canPin">产品</string>
     * <string name="xuanShang">悬赏</string>
     * <string name="renWuZhi">人物志</string>
     */


    public ArrayList<SearchData> getDatas(Activity activity) {
        ArrayList<SearchData> getDatas = new ArrayList<>();
        getDatas.add(new SearchData(SearchEnum.ziXun, true));
        getDatas.add(new SearchData(SearchEnum.canDa, false));
        getDatas.add(new SearchData(SearchEnum.xiuCang, false));
        getDatas.add(new SearchData(SearchEnum.chuShi, false));
        getDatas.add(new SearchData(SearchEnum.zhangGui, false));
        getDatas.add(new SearchData(SearchEnum.miJi, false));
        getDatas.add(new SearchData(SearchEnum.keTang, false));
        getDatas.add(new SearchData(SearchEnum.canPin, false));
        getDatas.add(new SearchData(SearchEnum.xuanShang, false));
        getDatas.add(new SearchData(SearchEnum.renWuZhi, false));
        return getDatas;
    }


}
