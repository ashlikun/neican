package com.hanbang.cxgz_2.pressenter.jianghu;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jianghu.iview.IManagerView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class ManagerPresenter extends BasePresenter<IManagerView> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<ManagerData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<ManagerData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<ManagerData>) mvpView.getValidData(result.getList()));
                }
            }
        };
        HttpRequest.ManagerInMoreThan(httpCallBack, mvpView.getBiaoqian(), mvpView.getPageindex(), mvpView.getPageCount());
    }


}
