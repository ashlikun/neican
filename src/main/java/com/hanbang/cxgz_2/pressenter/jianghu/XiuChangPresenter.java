package com.hanbang.cxgz_2.pressenter.jianghu;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.enumeration.ModeType;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.base.BannerAdData;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jianghu.iview.IXiuChangView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class XiuChangPresenter extends BasePresenter<IXiuChangView> {

    /**
     * 当前加载的状态
     */
    private int type = 4;


    public void getHttpData(boolean isStart) {
        getHttpData(isStart, -1);
    }

    /**
     * @param isStart
     * @param order   赛选状态type：
     *                4:全部,
     *                1:我关注的全部,
     *                2:我关注的厨师,
     *                3:我关注的经理人,
     *                4:全部,
     *                5:厨师,
     *                6:经理人
     *                -1：没有赛选
     */
    public void getHttpData(final boolean isStart, int order) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setShowLoadding(order == -1 ? false : true)
                .setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setStatusChangListener(mvpView.getStatusChangListener())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());
        final HttpCallBack httpCallBack = new HttpCallBack<XiuChangHttpResponse>(buider) {
            @Override
            public void onNext(XiuChangHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    result.bannerAdDatas = new ArrayList<>();
                    result.getBannerAdDatas().add(new BannerAdData(1, "http://imgsrc.baidu.com/forum/pic/item/b1c343d9f2d3572ccf18c7308a13632763d0c373.jpg"));
                    result.getBannerAdDatas().add(new BannerAdData(2, "http://img0.imgtn.bdimg.com/it/u=891814847,3397298689&fm=21&gp=0.jpg"));
                    result.getBannerAdDatas().add(new BannerAdData(3, "http://img3.imgtn.bdimg.com/it/u=2286035086,4168118241&fm=21&gp=0.jpg"));
                    result.getBannerAdDatas().add(new BannerAdData(4, "http://img4.imgtn.bdimg.com/it/u=3806360125,3446946955&fm=21&gp=0.jpg"));
                    if (isStart) {
                        mvpView.clearData();
                    }
                    result.dongtaiList = (List<XiuChangItemData>) mvpView.getValidData(result.getDongtaiList());
                    mvpView.upDataUI(result);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.jianghuXiuchang(httpCallBack, order == -1 ? type : order, mvpView.getPageindex(), mvpView.getPageCount()));
    }

    public void dianzhan(final XiuChangItemData data, final int position) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在点赞");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.setClickDianzhan(true);
                    data.addDianZhan();
                    mvpView.itemChanged(position);
                } else if (result.isReturnFaiure()) {
                    data.setClickDianzhan(true);
                    mvpView.itemChanged(position);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PointaPraise(httpCallBack, data.getId(), ModeType.DONGTAI));
    }

    public void guanzhu(final XiuChangItemData data, final int position, final GuanZhu guanzhu) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在" + guanzhu.getValuse());
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.itemChanged(position);
                } else if (result.isReturnFaiure()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.itemChanged(position);
                }
            }
        };
        mvpView.addSubscription(!guanzhu.getKey() ?
                HttpRequest.Guanzhu(httpCallBack, data.getUid()) :
                HttpRequest.QuXiaoGuanzhu(httpCallBack, data.getUid())
        );
    }

    public void pinglun(final XiuChangItemData data, final int position, String pinglun) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在评论");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.addPingLun();
                    mvpView.itemChanged(position);
                } else if (result.isReturnFaiure()) {
                    data.setClickDianzhan(true);
                    mvpView.itemChanged(position);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PinglunAdd(httpCallBack, data.getId(), ModeType.DONGTAI, pinglun));
    }
}
