package com.hanbang.cxgz_2.pressenter.jianghu;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.enumeration.ModeType;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangDetailsHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangCommentData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jianghu.iview.IXiuChangDetailsView;

import java.util.List;

import static android.R.attr.order;

/**
 * Created by Administrator on 2016/8/8.
 */

public class XiuChangDetailsPresenter extends BasePresenter<IXiuChangDetailsView> {


    public void getHttpData(final boolean isStart, int id) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setShowLoadding(order == -1 ? true : false)
                .setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setStatusChangListener(mvpView.getStatusChangListener())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());
        final HttpCallBack httpCallBack = new HttpCallBack<XiuChangDetailsHttpResponse>(buider) {
            @Override
            public void onNext(XiuChangDetailsHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    result.pinglunList = (List<XiuChangCommentData>) mvpView.getValidData(result.getPinglunList());
                    mvpView.upDataUi(result);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.jianghuXiuchangDetailes(httpCallBack, id, mvpView.getPageindex(), mvpView.getPageCount()));
    }

    public void dianzhan(final XiuChangItemData data) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在点赞");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.addDianZhan();
                    data.setClickDianzhan(true);
                    mvpView.headChanged();
                } else if (result.isReturnFaiure()) {
                    data.setClickDianzhan(true);
                    mvpView.headChanged();
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PointaPraise(httpCallBack, data.getId(), ModeType.DT_D));
    }

    public void guanzhu(final XiuChangItemData data, final GuanZhu guanzhu) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在" + guanzhu.getValuse());
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.headChanged();
                } else if (result.isReturnFaiure()) {
                    data.setIsguanzhu(!guanzhu.getKey());
                    mvpView.headChanged();
                }
            }
        };
        mvpView.addSubscription(!guanzhu.getKey() ?
                HttpRequest.Guanzhu(httpCallBack, data.getUid()) :
                HttpRequest.QuXiaoGuanzhu(httpCallBack, data.getUid())
        );
    }

    public void pinglun(final XiuChangItemData data, String pinglun) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在评论");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.addPingLun();
                    mvpView.pingLinSuccess();
                } else if (result.isReturnFaiure()) {
                    data.setClickPinglun(true);
                    mvpView.pingLinSuccess();
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PinglunAdd(httpCallBack, data.getId(), ModeType.DONGTAI, pinglun));
    }

    public void pinglunPerson(final XiuChangItemData data, final XiuChangCommentData dataItem, String pinglun) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在评论");
        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    data.addPingLun();
                    mvpView.pingLinSuccess();
                } else if (result.isReturnFaiure()) {
                    data.setClickPinglun(true);
                    mvpView.pingLinSuccess();
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PinglunAddXiuChangDetail(httpCallBack, data.getId(), ModeType.DT_D, dataItem.getId(), pinglun));
    }
}
