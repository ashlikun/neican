package com.hanbang.cxgz_2.pressenter.jianghu;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ChuShiItemData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jianghu.iview.IDaChuView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class DaChuPresenter extends BasePresenter<IDaChuView> {


    public void getHttpData() {
        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setShowLoadding(false).setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<ChuShiItemData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<ChuShiItemData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.clearData();
                    mvpView.upDataUI(result.getList());
                }
            }
        };
        HttpRequest.jianghuChef(httpCallBack);
    }
//


}
