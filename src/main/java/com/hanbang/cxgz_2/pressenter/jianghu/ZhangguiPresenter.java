package com.hanbang.cxgz_2.pressenter.jianghu;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ZhangGuiItemData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.jianghu.iview.IZhangguiView;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class ZhangGuiPresenter extends BasePresenter<IZhangguiView> {


    public void getHttpData() {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this)
                .setShowLoadding(false).setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager())
                .setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());
        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<List<ZhangGuiItemData>>>(buider) {
            @Override
            public void onNext(HttpResult<List<ZhangGuiItemData>> result) {
                super.onNext(result);
                //进行data处理
                if (result.isSucceed()) {
                    mvpView.clearData();
                    mvpView.upDataUI(result.getList());
                }
            }
        };
        HttpRequest.jianghuManager(httpCallBack);
    }


}
