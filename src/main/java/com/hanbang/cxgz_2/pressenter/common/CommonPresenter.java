package com.hanbang.cxgz_2.pressenter.common;

import android.content.Context;
import android.content.Intent;

import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.appliction.iview.common_view.ICaiXiView;
import com.hanbang.cxgz_2.appliction.iview.common_view.ICityDataView;
import com.hanbang.cxgz_2.appliction.iview.common_view.IDianZanView;
import com.hanbang.cxgz_2.appliction.iview.common_view.IJobView;
import com.hanbang.cxgz_2.appliction.iview.common_view.IPingLunView;
import com.hanbang.cxgz_2.appliction.iview.common_view.IUserDataView;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.enumeration.ModeType;
import com.hanbang.cxgz_2.mode.httpresponse.BaseHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.HttpResult;
import com.hanbang.cxgz_2.mode.httpresponse.other.CaiXiHttpResponse;
import com.hanbang.cxgz_2.mode.httpresponse.other.CityHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.base.CityData;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.utils.other.LogUtils;
import com.hanbang.cxgz_2.utils.ui.ToastUtils;

import java.util.ArrayList;

/**
 * Created by yang on 2016/9/6.
 */

public class CommonPresenter<T extends BaseView> extends BasePresenter<T> {


    /**
     * 获取菜系
     *
     * @param whetherShowAll 是否显示全部,true为显示
     */
    public void getCaiXiData(final boolean whetherShowAll) {

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(true);

        final HttpCallBack httpCallBack = new HttpCallBack<CaiXiHttpResponse>(buider) {
            @Override
            public void onNext(CaiXiHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    ArrayList<CaiXiData> list = new ArrayList<>();

                    list.addAll(result.getList());
                    //把全部取消
                    if (!whetherShowAll) {
                        list.remove(0);
                    }
                    if (mvpView != null && mvpView instanceof ICaiXiView) {
                        ((ICaiXiView) mvpView).getCaiXiData(list);
                    } else {
                        LogUtils.e("没有继承ICaiXiView");
                        ToastUtils.getToastShort("没有继承ICaiXiView");
                    }
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GetCuisinesList(httpCallBack));
    }



    /**
     * 职位	GetJobList	pid:(0 大厨,1 掌柜)
     * @return
     */
    public void GetJobList(final boolean whetherShowAll,int pid ) {

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(true);

        final HttpCallBack httpCallBack = new HttpCallBack<CaiXiHttpResponse>(buider) {
            @Override
            public void onNext(CaiXiHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    ArrayList<CaiXiData> list = new ArrayList<>();

                    list.addAll(result.getList());
                    //把全部取消
                    if (!whetherShowAll) {
                        list.remove(0);
                    }
                    if (mvpView != null && mvpView instanceof IJobView) {
                        ((IJobView) mvpView).getJobData(list);

                    } else {
                        ToastUtils.getToastShort("没有继承IJobView");
                    }
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GetCuisinesList(httpCallBack));
    }




















    /**
     * 获取用户数据
     */
    public void getUserData(int userid) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(true);

        final HttpCallBack httpCallBack = new HttpCallBack<HttpResult<UserData>>(buider) {
            @Override
            public void onNext(HttpResult<UserData> result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (mvpView != null && mvpView instanceof IUserDataView) {
                        ((IUserDataView) mvpView).getUserData(result.getList());


                    } else {

                        ToastUtils.getToastShort("没有继承IUserDataView");
                    }
                }
            }
        };
        mvpView.addSubscription(HttpRequest.GeRenJianJie(httpCallBack,userid));
    }


    /**
     * 获取城市列表
     */
    public void getCityData() {

        mvpView.getLoadingAndRetryManager();
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false);
        final HttpCallBack httpCallBack = new HttpCallBack<CityHttpResponse>(buider) {
            @Override
            public void onNext(CityHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    ArrayList<CityData> list = new ArrayList<>();
                    list.addAll(result.addPinYin());
                    CityData.saveDb(list);

                    ((ICityDataView) mvpView).getCityDatas(result);
                }
            }
        };
        mvpView.addSubscription(HttpRequest.getCityData(httpCallBack));
    }


    /**
     * 类型
     *  MIJI_D(1, "秘籍详情"),
     *  XXZL_D(2, "学习资料详情"),
     *  BZH_D(3, "餐饮标准化详情"),
     *  RDZX_D(4, "热点资讯详情"),
     *  XXKT(5, "学习课堂"),
     *  DONGTAI(6, "动态"),
     *  DT_D(7, "动态详情"),
     *  WEIZHI(-100, "未知");
     */


    /**
     * 评论
     * <p>
     * type:1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     * text:评论内容
     * 如果type=7,还需传参数touserid:对某人的评论
     *
     * @param id
     * @param type    评论的类型
     * @param pinglun 评论的内容
     */
    public void pinglun(final Context context, final int id, final ModeType type, String pinglun) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在评论");

        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    Intent intent = new Intent();

                    //秘籍详情
                    if (type.getKey() == ModeType.MIJI_D.getKey()) {
                        intent.setAction(Global.COMMENT_ESOTERIC);

                        //热点资讯详情
                    } else if (type.getKey() == ModeType.RDZX_D.getKey()) {
                        intent.setAction(Global.COMMENT_INFORMATION);

                        //学习课堂
                    } else if (type.getKey() == ModeType.XXKT.getKey()) {
                        intent.setAction(Global.COMMENT_CLASSROOM);

                    }

                    //回调的设置
                    if (mvpView != null && mvpView instanceof IPingLunView) {
                        ((IPingLunView) mvpView).pingLun(id);
                    }

                    intent.putExtra("id", id);
                    context.sendBroadcast(intent);

                } else {
                    ToastUtils.getToastShort("评论失败");
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PinglunAdd(httpCallBack, id, type, pinglun));
    }


    /**
     * 点赞
     *
     * @param id   主键
     * @param type 类别 1、秘籍 详情 ,2、学习资料 详情, 3、餐饮标准化详情 ,4、热点资讯详情 ,5、学习课堂 ,6、动态, 7、动态评论
     */
    public void dianZan(final Context context, final int id, final ModeType type) {
        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).setHint("正在点选");

        HttpCallBack httpCallBack = new HttpCallBack<BaseHttpResponse>(buider) {
            @Override
            public void onNext(BaseHttpResponse result) {
                super.onNext(result);
                showMsg(result);
                if (result.isSucceed()) {
                    Intent intent = new Intent();

                    //秘籍详情
                    if (type.getKey() == ModeType.MIJI_D.getKey()) {
                        intent.setAction(Global.DIAN_ZAN_ESOTERIC);

                        //热点资讯详情
                    } else if (type.getKey() == ModeType.RDZX_D.getKey()) {
                        intent.setAction(Global.DIAN_ZAN_INFORMATION);

                        //学习课堂
                    } else if (type.getKey() == ModeType.XXKT.getKey()) {
                        intent.setAction(Global.DIAN_ZAN_CLASSROOM);

                    }

                    //回调的设置
                    if (mvpView != null && mvpView instanceof IDianZanView) {
                        ((IPingLunView) mvpView).pingLun(id);
                    }

                    intent.putExtra("id", id);
                    context.sendBroadcast(intent);

                } else {
                    ToastUtils.getToastShort("点赞失败");
                }
            }
        };
        mvpView.addSubscription(HttpRequest.PointaPraise(httpCallBack, id, type));
    }


}
