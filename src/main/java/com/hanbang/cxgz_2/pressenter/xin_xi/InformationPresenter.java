package com.hanbang.cxgz_2.pressenter.xin_xi;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationData;
import com.hanbang.cxgz_2.mode.httpresponse.home.InformationHttpResponse;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.view.xin_xi.iview.IInformation;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */

public class InformationPresenter extends BasePresenter<IInformation> {


    public void getHttpData(final boolean isStart) {
        if (isStart) {
            mvpView.clearPagingData();
        }
        mvpView.getLoadingAndRetryManager();

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setShowLoadding(false).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setStatusChangListener(mvpView.getStatusChangListener()).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());


        final HttpCallBack httpCallBack = new HttpCallBack<InformationHttpResponse>(buider) {
            @Override
            public void onNext(InformationHttpResponse result) {
                super.onNext(result);
                //进行data处理
                if (result.getResult() == 0) {
                    if (isStart) {
                        mvpView.clearData();
                    }
                    mvpView.upDataUi((List<InformationData>) mvpView.getValidData(result.getData()));
                }
            }
        };
        HttpRequest.informationList(httpCallBack, mvpView.getPageindex(), mvpView.getPageCount());
    }






}
