package com.hanbang.cxgz_2.pressenter.xin_xi;

import com.hanbang.cxgz_2.mode.HttpRequest;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationDetailsData;
import com.hanbang.cxgz_2.pressenter.common.CommonPresenter;
import com.hanbang.cxgz_2.utils.http.HttpCallBack;
import com.hanbang.cxgz_2.view.xin_xi.iview.IInformationDetailsView;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/18 10:50
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：资讯与人物志的详情页
 */

public class InformationDetailsPresenter extends CommonPresenter<IInformationDetailsView> {


    public void getHttpData(final boolean fist, int id) {
        if (fist) {
            mvpView.clearPagingData();
        }

        HttpCallBack.Buider buider = new HttpCallBack.Buider(this).
                setLoadingAndRetryManager(mvpView.getLoadingAndRetryManager()).
                setShowLoadding(false).
                setSwipeRefreshLayout(mvpView.getSwipeRefreshLayout());

        final HttpCallBack httpCallBack = new HttpCallBack<InformationDetailsData>(buider) {
            @Override
            public void onNext(InformationDetailsData result) {
                super.onNext(result);
                if (fist) {
                    mvpView.clearData();
                }
                //进行data处理
                if (result.getResult() == 0) {
                    mvpView.upDataUi(result);
                } else {
                    mvpView.showWarningSnackbar(result.getMsg(), true);
                }
            }
        };
        HttpRequest.CaterersRwzDetail(httpCallBack, id);
    }





}
