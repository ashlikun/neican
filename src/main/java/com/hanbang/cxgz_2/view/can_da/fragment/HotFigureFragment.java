package com.hanbang.cxgz_2.view.can_da.fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseSwipeFragment;
import com.hanbang.cxgz_2.mode.enumeration.FigureClassifyEnum;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureResponse;
import com.hanbang.cxgz_2.mode.javabean.can_da.MealAnswerClassifyData;
import com.hanbang.cxgz_2.pressenter.can_da.HomeHotFigurePresenter;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.can_da.activity.HotFigureActivity;
import com.hanbang.cxgz_2.view.can_da.activity.MealAnswerDetailsActivity;
import com.hanbang.cxgz_2.view.can_da.adapter.HotFigureAdapter;
import com.hanbang.cxgz_2.view.can_da.iview.IHomeHotFigureView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 餐答首页热门用户
 */
public class HotFigureFragment extends BaseSwipeFragment<IHomeHotFigureView, HomeHotFigurePresenter> implements IHomeHotFigureView {
    @BindView(R.id.fragment_hot_figure_GridView_classify)
    GridView classify;
    @BindView(R.id.fragment_hot_figure_GridView_recommend)
    GridView recommend;


    private HotFigureResponse response = HotFigureResponse.init();
    private HotFigureAdapter adapter = new HotFigureAdapter(getActivity(), response);


    public static HotFigureFragment newInstance() {
        HotFigureFragment listFragment = new HotFigureFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_hot_figure;
    }

    @Override
    public void initView() {
        super.initView();


        setAdapter();
        presenter.getHttpData(true);
    }


    /**
     * 获取下拉刷新控件
     */
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    //数据加载失败点击重新加载
    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    //数据为空点击重新加载
    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


    public void setAdapter() {
        adapter = new HotFigureAdapter(activity, response);
        recommend.setAdapter(adapter.mingRenTuiJian);
        final ArrayList<MealAnswerClassifyData> Datas = response.getClassifyListData();

        classify.setAdapter(new CommonAdapter<MealAnswerClassifyData>(getActivity(), R.layout.item_hot_figure_classify, Datas) {

            @Override
            public void convert(ViewHolder holder, MealAnswerClassifyData itmeMeGridViewData) {
                try {
                    holder.setImageResource(R.id.item_hot_figure_classify_image, itmeMeGridViewData.getPictureId());
                    holder.setText(R.id.item_hot_figure_classify_content, itmeMeGridViewData.getClassify().getValuse());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        classify.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                HotFigureActivity.startUi(activity, Datas.get(position).getClassify());
            }
        });

        recommend.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MealAnswerDetailsActivity.startUI(activity, response.list.get(position).getGuid());
            }
        });


    }

    private void refreshHeight() {
        UiUtils.setListViewHeightBasedOnChildren(classify, 4, null);
        UiUtils.setListViewHeightBasedOnChildren(recommend, 1, null);
    }


    @Override
    public void clearData() {
        response.clearAll();
    }

    @Override
    public HomeHotFigurePresenter initPressenter() {
        return new HomeHotFigurePresenter();
    }


    @Override
    public void upDataUi(HotFigureResponse datas) {
        response.addData(datas);
        adapter.notifyDataSetChanged();
        refreshHeight();
    }


    @OnClick(R.id.fragment_hot_figure_recommend)
    public void onClick() {
        HotFigureActivity.startUi(activity, FigureClassifyEnum.MING_REN_TUI_JIAN);
    }
}
