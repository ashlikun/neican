package com.hanbang.cxgz_2.view.can_da.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseSwipeActivity;
import com.hanbang.cxgz_2.mode.javabean.can_da.MealAnswerDetailsData;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.pressenter.can_da.MealAnserDetailsPresenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.view.adapter.MealAnswerQuetionAdapter;
import com.hanbang.cxgz_2.view.can_da.iview.IMealAnswerDetailsView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;
import com.hanbang.videoplay.view.VideoPlayer;

import java.util.ArrayList;

import butterknife.BindView;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 17:29
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：餐答详情页
 */

public class MealAnswerDetailsActivity extends BaseSwipeActivity<IMealAnswerDetailsView, MealAnserDetailsPresenter> implements IMealAnswerDetailsView {

    ArrayList<HotAnswerData> datas = new ArrayList<>();
    @BindView(R.id.activity_meal_answer_details_portrait)
    ImageView portrait;
    @BindView(R.id.activity_meal_answer_details_name)
    TextView name;
    @BindView(R.id.activity_meal_answer_details_job)
    TextView job;
    @BindView(R.id.activity_meal_answer_details_brief)
    TextView brief;
    @BindView(R.id.activity_meal_answer_details_content)
    EditText content;
    @BindView(R.id.activity_meal_answer_details_price)
    TextView price;
    @BindView(R.id.activity_meal_answer_details_quetion)
    TextView quetion;

    @BindView(R.id.activity_meal_answer_details_correlation)
    SuperGridLayout gridLayout;
    CommonAdapter adapter;
    private int UserId = -1;

    @Override
    public int getContentView() {
        return R.layout.activity_meal_answer_details;
    }

    public static void startUI(Context context, int UserId) {
        Intent intent = new Intent(context, MealAnswerDetailsActivity.class);
        intent.putExtra("UserId", UserId);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void parseIntent(Intent intent) {
        UserId = intent.getIntExtra("UserId", -1);
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setBack(this);
        toolbar.setTitle("热门回答");

        gridLayout.setAdapter(adapter = new MealAnswerQuetionAdapter(this, datas));
        presenter.getHttpData(UserId);


    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    @Override
    public void onRefresh() {
        datas.clear();
        presenter.getHttpData(UserId);
    }

    @Override
    public void clearData() {

    }

    @Override
    public MealAnserDetailsPresenter initPressenter() {
        return new MealAnserDetailsPresenter();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(UserId);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(UserId);
    }


    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onBackPressed() {
        if (VideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    /**
     * @param data
     */
    @Override
    public void upDataUi(MealAnswerDetailsData data) {
        GlideUtils.showCircle(portrait, data.getAvatar());
        name.setText(data.getUser_name());
        job.setText(data.getJobCN() + " | " + data.getCompany());
        brief.setText(data.getCanDaJianJie());
        price.setText(data.getCanDaTiWenMoney());
        quetion.setText(data.getWithQuestionCount());
        datas.addAll(data.getWithQuestion());

        adapter.notifyDataSetChanged();


    }


}
