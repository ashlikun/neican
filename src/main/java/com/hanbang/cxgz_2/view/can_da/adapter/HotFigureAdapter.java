package com.hanbang.cxgz_2.view.can_da.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureResponse;
import com.hanbang.cxgz_2.mode.javabean.home.HotFigureData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * Created by yang on 2016/8/17.
 */
public class HotFigureAdapter {
    private Context context;
    private HotFigureResponse response;
    //1 //推荐人物
    public CommonAdapter mingRenTuiJian = null;

    public HotFigureAdapter(Context context, HotFigureResponse response) {
        this.response = response;
        this.context = context;
        initAdapter();
    }


    public void notifyDataSetChanged() {
        mingRenTuiJian.notifyDataSetChanged();
    }


    private void initAdapter() {
        //推荐人物
        mingRenTuiJian = new CommonAdapter<HotFigureData>(context, R.layout.item_hot_firure_recommend, response.getList()) {
            @Override
            public void convert(ViewHolder helper, HotFigureData item) {
                try {
                    if (!StringUtils.isBlank(item.getAvatarimgSmallurl())) {
                        helper.setImageBitmapCircle(R.id.item_hot_firure_recommend_hotFigure, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatarimgSmallurl())));
                    } else {
                        helper.setImageBitmapCircle(R.id.item_hot_firure_recommend_hotFigure, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar())));
                    }
                    helper.setText(R.id.iitem_hot_firure_recommend_item1, StringUtils.isNullToConvert(item.getUser_name()));
                    helper.setText(R.id.iitem_hot_firure_recommend_item2, StringUtils.isNullToConvert(item.getJobCN()));
                    helper.setText(R.id.iitem_hot_firure_recommend_item3, StringUtils.isNullToConvert(item.getCompany()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };


    }


}
