package com.hanbang.cxgz_2.view.can_da.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.pressenter.can_da.HotAnswerPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.adapter.MealAnswerQuetionAdapter;
import com.hanbang.cxgz_2.view.can_da.iview.IHotAnswer;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * 热门问题列表
 */
public class HotAnswerActivity extends BaseMvpActivity<IHotAnswer, HotAnswerPresenter> implements IHotAnswer, ListSwipeView.ListSwipeViewListener {
    @BindView(R.id.switchRoot)
    ListSwipeView switchRoot;
    MealAnswerQuetionAdapter adapter;
    private ArrayList<HotAnswerData> datas = new ArrayList<>();
    private String title;


    /**
     * 启动信息详情
     *
     * @param context
     */
    public static void startUi(Context context, String title) {
        Intent intent = new Intent(context, HotAnswerActivity.class);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }


    @Override
    public HotAnswerPresenter initPressenter() {
        return new HotAnswerPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        title = getIntent().getStringExtra("title");
    }

    @Override
    public int getContentView() {
        return R.layout.activity_common_list;
    }

    @Override
    public void initView() {
        toolbar.setTitle(title);
        toolbar.setBack(this);


        switchRoot.getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        switchRoot.setOnRefreshListener(this);
        switchRoot.setOnLoaddingListener(this);
        switchRoot.getRecyclerView().addItemDecoration(new HorizontalDividerItemDecoration.
                Builder(this).sizeResId(R.dimen.dp_5).colorResId(R.color.gray_ee).build());
        presenter.getHttpData(true);


        switchRoot.setAdapter(adapter = new MealAnswerQuetionAdapter(this, datas));

    }


    @Override
    public void upDataUi(List<HotAnswerData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return switchRoot.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return switchRoot.getPagingHelp().getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        switchRoot.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return switchRoot.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return switchRoot.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


}
