package com.hanbang.cxgz_2.view.can_da.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseSwipeView;
import com.hanbang.cxgz_2.mode.httpresponse.home.mead_answer.HotFigureResponse;

/**
 * Created by yang on 2016/8/17.
 */
public interface IHomeHotFigureView extends BaseSwipeView {
    void upDataUi(HotFigureResponse datas);
}
