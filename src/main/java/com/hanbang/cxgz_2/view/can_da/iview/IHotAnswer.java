package com.hanbang.cxgz_2.view.can_da.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IHotAnswer extends BaseListView{
    void upDataUi(List<HotAnswerData> datas);
}
