package com.hanbang.cxgz_2.view.can_da.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.SearchEnum;
import com.hanbang.cxgz_2.mode.javabean.home.FigureData;
import com.hanbang.cxgz_2.pressenter.can_da.CateringFigurePresenter;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.can_da.iview.ICateringFigure;
import com.hanbang.cxgz_2.view.other.SearchActivity;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.xin_xi.acitvity.InformationDetailsActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/19 17:16
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：餐饮人物志列表
 */

public class CateringFigureActivity extends BaseMvpActivity<ICateringFigure, CateringFigurePresenter> implements ICateringFigure, ListSwipeView.ListSwipeViewListener {
    @BindView(R.id.switchRoot)
    ListSwipeView switchRoot;
    CommonAdapter adapter;
    private ArrayList<FigureData> datas = new ArrayList<>();
    private String title;


    public static void startUi(Context context, String title) {
        Intent intent = new Intent(context, CateringFigureActivity.class);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }


    @Override
    public CateringFigurePresenter initPressenter() {
        return new CateringFigurePresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        title = getIntent().getStringExtra("title");
    }

    @Override
    public int getContentView() {
        return R.layout.activity_common_list;
    }

    @Override
    public void initView() {
        toolbar.setTitle(title);
        toolbar.setBack(this);
        toolbar.addAction(1, "搜索", R.drawable.material_search);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SearchActivity.startUi(CateringFigureActivity.this, SearchEnum.renWuZhi);
                return false;
            }
        });


        switchRoot.getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        switchRoot.setOnRefreshListener(this);
        switchRoot.setOnLoaddingListener(this);

        switchRoot.getRecyclerView().addItemDecoration(new HorizontalDividerItemDecoration.
                Builder(this).sizeResId(R.dimen.dp_5).colorResId(R.color.gray_ee).build());

        presenter.getHttpData(true);

        switchRoot.setAdapter(adapter = new CommonAdapter<FigureData>(this, R.layout.item_find_classroom_data, datas) {
            @Override
            public void convert(ViewHolder holder, FigureData data) {
                try {
                    holder.setImageBitmap(R.id.find_classroom_data_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(data.getImg_url())));
                    if (data.is_vip()) {
                        holder.setVisible(R.id.find_classroom_data_whetherVip, true);
                    } else {
                        holder.setVisible(R.id.find_classroom_data_whetherVip, false);
                    }
                    holder.setText(R.id.find_classroom_data_title, StringUtils.isNullToConvert(data.getTitle()));
                    holder.setText(R.id.find_classroom_data_nameJobCompany, StringUtils.isNullToConvert(data.getAuthor() + " | " + data.getJob_CN() + " | " + data.getCompany()));
                    holder.setText(R.id.find_classroom_data_time, "偷听:" + DateUtils.getTime(data.getAdd_time()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View view, Object data, int position) {
                InformationDetailsActivity.startUI(CateringFigureActivity.this, datas.get(position).getId());
            }
        });

    }


    @Override
    public void upDataUi(List<FigureData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return switchRoot.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return switchRoot.getPagingHelp().getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        switchRoot.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return switchRoot.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return switchRoot.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


}
