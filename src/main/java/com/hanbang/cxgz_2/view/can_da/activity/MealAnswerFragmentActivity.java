package com.hanbang.cxgz_2.view.can_da.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.SearchEnum;
import com.hanbang.cxgz_2.mode.javabean.base.BannerAdData;
import com.hanbang.cxgz_2.pressenter.can_da.MealAnswerPresenter;
import com.hanbang.cxgz_2.view.can_da.fragment.HotAnswerFragment;
import com.hanbang.cxgz_2.view.can_da.fragment.HotFigureFragment;
import com.hanbang.cxgz_2.view.can_da.iview.IMealAnswerView;
import com.hanbang.cxgz_2.view.jieyou.adapter.ScrollableViewPageAdapter;
import com.hanbang.cxgz_2.view.other.SearchActivity;
import com.hanbang.cxgz_2.view.widget.banner.ConvenientBanner;
import com.hanbang.cxgz_2.view.widget.banner.NetworkImageHolderView;
import com.hanbang.cxgz_2.view.widget.banner.holder.CBViewHolderCreator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 餐答首页
 */
public class MealAnswerFragmentActivity extends BaseMvpActivity<IMealAnswerView, MealAnswerPresenter> implements IMealAnswerView, CBViewHolderCreator {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.fragment_allay_shop_header_viewpager)
    ViewPager viewPager;

    @BindView(R.id.advertisement)
    ConvenientBanner advertisement;
    private List<BannerAdData> advertisementDatas;

    private String[] title = new String[]{"热门人物", "热门问题"};
    private ArrayList<Fragment> fragmentList = new ArrayList<>();

    public static void startUi(Context context) {
        Intent intent = new Intent(context, MealAnswerFragmentActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public MealAnswerPresenter initPressenter() {
        return null;
    }

    @Override
    public void initView() {
        fragmentList.add(HotFigureFragment.newInstance());
        fragmentList.add(HotAnswerFragment.newInstance());
        ScrollableViewPageAdapter.initFragmentPager(getSupportFragmentManager(), viewPager, fragmentList, tabLayout, title, 0);

        advertisementDatas = new ArrayList<>();
        advertisementDatas.add(new BannerAdData(1, "http://img0.imgtn.bdimg.com/it/u=1458042595,2336590661&fm=206&gp=0.jpg"));
        advertisementDatas.add(new BannerAdData(2, "http://img4.imgtn.bdimg.com/it/u=3646491133,3402401671&fm=21&gp=0.jpg"));
        advertisement.setPages(this, advertisementDatas);
        advertisement.setWHBili();
        advertisement.startTurning(2000);
    }

    @Override
    public void parseIntent(Intent intent) {

    }


    @Override
    public int getContentView() {

        return R.layout.activity_deman_answer;
    }


    @Override
    public void clearData() {

    }

    @Override
    public Object createHolder() {
        return new NetworkImageHolderView() {

            @Override
            public void onItemClicklistener(View item, int position, BannerAdData data) {

            }
        };
    }

    @OnClick({R.id.activity_deman_answer_back, R.id.bar_search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_deman_answer_back:
                finish();
                break;
            case R.id.bar_search:
                SearchActivity.startUi(this, SearchEnum.canDa);
                break;
        }
    }
}
