package com.hanbang.cxgz_2.view.can_da.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseSwipeView;
import com.hanbang.cxgz_2.mode.javabean.can_da.MealAnswerDetailsData;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/12 14:45
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：餐答详情接口
 */

public interface IMealAnswerDetailsView extends BaseSwipeView {
    void upDataUi(MealAnswerDetailsData datas);
}
