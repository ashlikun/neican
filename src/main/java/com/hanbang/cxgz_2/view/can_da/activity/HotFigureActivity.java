package com.hanbang.cxgz_2.view.can_da.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.FigureClassifyEnum;
import com.hanbang.cxgz_2.mode.javabean.home.HotFigureData;
import com.hanbang.cxgz_2.pressenter.can_da.HotFigurePresenter;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.can_da.iview.IHotFigur;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * 热门用户列表
 */
public class HotFigureActivity extends BaseMvpActivity<IHotFigur, HotFigurePresenter> implements IHotFigur, ListSwipeView.ListSwipeViewListener {
    @BindView(R.id.switchRoot)
    ListSwipeView switchRoot;
    CommonAdapter adapter;
    private ArrayList<HotFigureData> datas = new ArrayList<>();
    private FigureClassifyEnum classify;


    /**
     * 启动信息详情
     *
     * @param context
     */
    public static void startUi(Context context, FigureClassifyEnum classify) {
        Intent intent = new Intent(context, HotFigureActivity.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("classify",classify);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }


    @Override
    public HotFigurePresenter initPressenter() {
        return new HotFigurePresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        classify = (FigureClassifyEnum) getIntent().getSerializableExtra("classify");
    }

    @Override
    public int getContentView() {
        return R.layout.activity_common_list;
    }

    @Override
    public void initView() {
        toolbar.setTitle(classify.getValuse());
        toolbar.setBack(this);


        switchRoot.getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        switchRoot.setOnRefreshListener(this);
        switchRoot.setOnLoaddingListener(this);
        presenter.getHttpData(true);

        switchRoot.setAdapter(adapter = new CommonAdapter<HotFigureData>(this, R.layout.item_hot_firure_recommend, datas) {
            @Override
            public void convert(ViewHolder holder, HotFigureData data) {
                try {
                    if (!StringUtils.isBlank(data.getAvatarimgSmallurl())) {
                        holder.setImageBitmapCircle(R.id.item_hot_firure_recommend_hotFigure, HttpLocalUtils.getHttpFileUrl((StringUtils.isNullToConvert(data.getAvatarimgSmallurl()))));
                    } else {
                        holder.setImageBitmapCircle(R.id.item_hot_firure_recommend_hotFigure, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(data.getAvatar())));
                    }
                    holder.setText(R.id.iitem_hot_firure_recommend_item1, StringUtils.isNullToConvert(data.getUser_name()));
                    holder.setText(R.id.iitem_hot_firure_recommend_item2, StringUtils.isNullToConvert(data.getJobCN()));
                    holder.setText(R.id.iitem_hot_firure_recommend_item3, StringUtils.isNullToConvert(data.getCompany()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });


    }


    @Override
    public void upDataUi(List<HotFigureData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return switchRoot.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return switchRoot.getPagingHelp().getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        switchRoot.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return switchRoot.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return switchRoot.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


}
