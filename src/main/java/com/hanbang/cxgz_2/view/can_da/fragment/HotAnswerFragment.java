package com.hanbang.cxgz_2.view.can_da.fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseMvpFragment;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.pressenter.can_da.HomeHotAnswerPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.adapter.MealAnswerQuetionAdapter;
import com.hanbang.cxgz_2.view.can_da.iview.IHomeHotAnswerView;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

import static com.hanbang.cxgz_2.R.id.switchRoot;

/**
 * 餐答首页热门用户
 */
public class HotAnswerFragment extends BaseMvpFragment<IHomeHotAnswerView, HomeHotAnswerPresenter> implements IHomeHotAnswerView, ListSwipeView.ListSwipeViewListener {
    @BindView(switchRoot)
    ListSwipeView listSwipeView;
    MealAnswerQuetionAdapter adapter;

    private ArrayList<HotAnswerData> datas = new ArrayList<>();

    public static HotAnswerFragment newInstance() {
        HotAnswerFragment listFragment = new HotAnswerFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_hot_answer;
    }

    @Override
    public void initView() {

        listSwipeView.getRecyclerView().setLayoutManager(new LinearLayoutManager(getActivity()));
        listSwipeView.setOnRefreshListener(this);
        listSwipeView.setOnLoaddingListener(this);
        listSwipeView.getRecyclerView().addItemDecoration(new HorizontalDividerItemDecoration.
                Builder(getContext()).sizeResId(R.dimen.dp_5).colorResId(R.color.gray_ee).build());
        presenter.getHttpData(true);


        listSwipeView.setAdapter(adapter = new MealAnswerQuetionAdapter(getActivity(), datas));
    }


    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.getSwipeRefreshLayout();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void upDataUi(List<HotAnswerData> datas) {
        this.datas.addAll(datas);

        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());

        }
        adapter.notifyDataSetChanged();

    }

    @Override
    public StatusChangListener getStatusChangListener() {
        return listSwipeView.getStatusChangListener();
    }

    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {
        return listSwipeView.getPagingHelp().getValidData(c);
    }

    @Override
    public void clearPagingData() {
        listSwipeView.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return listSwipeView.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return listSwipeView.getPagingHelp().getPageCount();
    }

    @Override
    public HomeHotAnswerPresenter initPressenter() {
        return new HomeHotAnswerPresenter();
    }
}
