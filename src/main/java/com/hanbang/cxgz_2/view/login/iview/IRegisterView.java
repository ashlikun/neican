package com.hanbang.cxgz_2.view.login.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;

/**
 * Created by yang on 2016/8/17.
 */
public interface IRegisterView extends BaseView {
    /**
     * 获取验证码
     *
     * @param whetherVerification
     */
    void isVerification(boolean whetherVerification);

    /**
     * 注册成功后接收UserData对象
     *
     * @param
     */
    void receiverUserData(UserData UserData);
}
