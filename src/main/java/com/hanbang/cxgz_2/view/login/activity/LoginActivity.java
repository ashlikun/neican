package com.hanbang.cxgz_2.view.login.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.common.TheCountdownBiz;
import com.hanbang.cxgz_2.pressenter.othre.LoginPresenter;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.home.activity.HomeActivity;
import com.hanbang.cxgz_2.view.login.iview.IloginView;

import butterknife.BindView;
import butterknife.OnClick;

import static com.hanbang.cxgz_2.appliction.Global.LOGIN_BROADCAST;


/**
 * 登录页面
 * *
 */
public class LoginActivity extends BaseMvpActivity<IloginView, LoginPresenter> implements IloginView {
    public EditHelper editHelper = new EditHelper(this);
    boolean isSkipHome;

    @Override
    public int getContentView() {
        return R.layout.activity_login;
    }

    /**
     * 跳转到主页
     *
     * @param context
     */
    public static void startUI(Context context) {
        startUI(context, true);
    }

    /**
     * 返回到当前页
     *
     * @param context
     * @param isSkipHome
     */
    public static void startUI(Context context, boolean isSkipHome) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("isSkipHome", isSkipHome);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    /*
     * 初始化view
     */
    public void initView() {
        toolbar.setTitle("登录");
        toolbar.setBack(this);
        editHelper.addEditHelperData(new EditHelper.EditHelperData(loginName, Validators.getFixLength(11), R.string.inputPhone));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(loginPassword, Validators.getLenghMinRegex(3), R.string.inputPassword));


    }


    @Override
    public void parseIntent(Intent intent) {
        isSkipHome = intent.getBooleanExtra("isSkipHome", true);
    }


    /**
     * 获取验证码
     */
    @OnClick(value = {R.id.login_send_verification, R.id.user_login_row, R.id.user_login_register_right})
    protected void onClickListener(View v) {
        UiUtils.exitInput(LoginActivity.this);
        switch (v.getId()) {

            //获取验证码
            case R.id.login_send_verification:
                if (!editHelper.check(R.id.login_name)) {
                    return;
                }
                presenter.getHttpData(LoginActivity.this, editHelper.getText(R.id.login_name));
                break;

            //登录
            case R.id.user_login_row:
                if (editHelper.check()) {
                    presenter.login(editHelper.getText(R.id.login_name), editHelper.getText(R.id.login_password));
                }
                break;

            //注册
            case R.id.user_login_register_right:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public LoginPresenter initPressenter() {
        return new LoginPresenter();
    }


    @Override
    public void clearData() {

    }


    @Override
    public void isVerification(boolean whetherVerification) {

        if (whetherVerification) {
            SnackbarUtil.showLong(this, "发送成功", SnackbarUtil.Info).show();
            TheCountdownBiz.recordTime((TextView) findViewById(R.id.login_send_verification));
        }
    }

    @Override
    public void login(UserData UserData) {

        if (UserData.setDbUserData(UserData)) {

            Intent intent = new Intent(LOGIN_BROADCAST);
            sendBroadcast(intent);

            showSnackbar("登录成功", SnackbarUtil.Info, true, new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    super.onDismissed(snackbar, event);
                    if (isSkipHome) {
                        HomeActivity.startUI(LoginActivity.this);
                    }
                    finish();
                }
            });
        } else {
            showWarningMessage("登录失败");
        }


    }

    @BindView(R.id.login_name)
    EditText loginName;
    @BindView(R.id.login_password)
    EditText loginPassword;
    @BindView(R.id.login_send_verification)
    TextView loginSendVerification;
    @BindView(R.id.user_login_row)
    Button userLoginRow;
    @BindView(R.id.user_login_register_right)
    TextView userLoginRegisterRight;

}
