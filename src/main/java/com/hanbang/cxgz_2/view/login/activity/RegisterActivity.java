package com.hanbang.cxgz_2.view.login.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.AgreementEnum;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.common.TheCountdownBiz;
import com.hanbang.cxgz_2.pressenter.othre.RegisterPresenter;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.login.iview.IRegisterView;
import com.hanbang.cxgz_2.view.other.AgreementActivity;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 注册
 */
public class RegisterActivity extends BaseMvpActivity<IRegisterView, RegisterPresenter> implements IRegisterView {
    public EditHelper editHelper = new EditHelper(this);
    @BindView(R.id.activity_register_phone)
    EditText activityRegisterPhone;
    @BindView(R.id.activity_register_password)
    EditText activityRegisterPassword;

    public static void startUI(Context context) {
        Intent intent = new Intent(context, RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initView();
        changeViewStatus(R.id.activity_register_manager);
    }


    /*
     * 初始化view
     */

    public void initView() {
        toolbar.setTitle("注册");
        toolbar.setBack(this);

        editHelper.addEditHelperData(new EditHelper.EditHelperData(activityRegisterPhone, Validators.getFixLength(11), R.string.inputPhone));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(activityRegisterPassword, Validators.getLengthSRegex(4, 8), R.string.inputCode));

    }


    @Override
    public void parseIntent(Intent intent) {

    }


    @Override
    public int getContentView() {
        return R.layout.activity_register;
    }

    /**
     * 获取验证码
     */
    @OnClick(value = {R.id.antivity_register_login, R.id.activity_register_send_verification, R.id.activity_register_manager, R.id.activity_register_chef, R.id.activity_register_agreement})
    protected void onClickListener(View v) {
        UiUtils.exitInput(RegisterActivity.this);
        switch (v.getId()) {

            //获取验证码
            case R.id.activity_register_send_verification:
                if (!editHelper.check(R.id.activity_register_phone)) {
                    return;
                }

                presenter.getHttpData(this, editHelper.getText(R.id.activity_register_phone));
                break;

            //注册
            case R.id.antivity_register_login:

                if (editHelper.check()) {

                    presenter.getHttpUserData(this, editHelper.getText(R.id.activity_register_phone),
                            editHelper.getText(R.id.activity_register_password),
                            (findViewById(R.id.activity_register_manager).isSelected() == true ? "2" : "1"),
                            "未知",
                            ((EditText) findViewById(R.id.activity_register_inviteCode)).getText().toString()
                    );


                }
                break;

            //查看注册协议
            case R.id.activity_register_agreement:
                AgreementActivity.startUI(this, AgreementEnum.PU_TONG_XIE_YE);
                break;

            //选择经理人
            case R.id.activity_register_manager:
                changeViewStatus(R.id.activity_register_manager);
                break;

            //选择厨师
            case R.id.activity_register_chef:
                changeViewStatus(R.id.activity_register_chef);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public RegisterPresenter initPressenter() {
        return new RegisterPresenter();
    }

    @Override
    public void isVerification(boolean whetherVerification) {
        if (whetherVerification) {
            TheCountdownBiz.recordTime((TextView) findViewById(R.id.activity_register_send_verification));
        }

    }

    @Override
    public void receiverUserData(UserData UserData) {

        UserData.setDbUserData(UserData);
    }

    @Override
    public void clearData() {

    }


    /**
     * 改变选择状态
     *
     * @param viewId
     */
    private void changeViewStatus(int viewId) {
        ViewGroup viewGroup1 = (ViewGroup) findViewById(R.id.activity_register_manager);
        ViewGroup viewGroup2 = (ViewGroup) findViewById(R.id.activity_register_chef);

        viewGroup1.setSelected(viewId == viewGroup1.getId() ? true : false);
        viewGroup1.getChildAt(0).setSelected(viewId == viewGroup1.getId() ? true : false);
        viewGroup1.getChildAt(1).setSelected(viewId == viewGroup1.getId() ? true : false);

        viewGroup2.setSelected(viewId == viewGroup2.getId() ? true : false);
        viewGroup2.getChildAt(0).setSelected(viewId == viewGroup2.getId() ? true : false);
        viewGroup2.getChildAt(1).setSelected(viewId == viewGroup2.getId() ? true : false);
    }


}
