package com.hanbang.cxgz_2.view.home.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.GridLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMainActivity;
import com.hanbang.cxgz_2.mode.httpresponse.home.HomeHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.base.BannerAdData;
import com.hanbang.cxgz_2.pressenter.home.HomePresenter;
import com.hanbang.cxgz_2.view.about_me.activity.StudioActivity;
import com.hanbang.cxgz_2.view.can_da.activity.CateringFigureActivity;
import com.hanbang.cxgz_2.view.can_da.activity.HotAnswerActivity;
import com.hanbang.cxgz_2.view.can_da.activity.MealAnswerDetailsActivity;
import com.hanbang.cxgz_2.view.can_da.activity.MealAnswerFragmentActivity;
import com.hanbang.cxgz_2.view.home.adapter.HomeAdapter;
import com.hanbang.cxgz_2.view.home.iview.IHomeView;
import com.hanbang.cxgz_2.view.jianghu.activity.JianghuActivity;
import com.hanbang.cxgz_2.view.jieyou.activity.DemandRewardDetailsActivity;
import com.hanbang.cxgz_2.view.jieyou.activity.EsotericDetails_K_Activity;
import com.hanbang.cxgz_2.view.jieyou.activity.JieyouActivity;
import com.hanbang.cxgz_2.view.jieyou.activity.StudyClassroomDetailsActivity;
import com.hanbang.cxgz_2.view.widget.banner.ConvenientBanner;
import com.hanbang.cxgz_2.view.widget.banner.NetworkImageHolderView;
import com.hanbang.cxgz_2.view.widget.banner.holder.CBViewHolderCreator;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.empty_layout.OnRetryClickListion;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;
import com.hanbang.cxgz_2.view.xin_xi.acitvity.InformationActivity;
import com.hanbang.cxgz_2.view.xin_xi.acitvity.InformationDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * * 主页
 * Created by Administrator on 2016/8/15.
 */

public class HomeActivity extends BaseMainActivity<IHomeView, HomePresenter> implements IHomeView,
        SwipeRefreshLayout.OnRefreshListener, OnRetryClickListion, CBViewHolderCreator {

    @BindView(R.id.advertisement)
    ConvenientBanner advertisement;
    private List<BannerAdData> advertisementDatas;


    @BindView(R.id.switchRoot)
    SwipeRefreshLayout switchRoot;
    private HomeHttpResponse homeHttpResponse = HomeHttpResponse.init();
    private HomeAdapter adapter = new HomeAdapter(this, homeHttpResponse);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.getHttpData();

    }

    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    public int getStatusBarResource() {
        return R.color.translucent;
    }

    @Override
    public int currentItem() {
        return 0;
    }

    @Override
    public HomePresenter initPressenter() {
        return new HomePresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main_home;
    }

    public static void startUI(Activity activity) {
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(intent);
    }

    @Override
    public void initView() {
        super.initView();
        advertisementDatas = new ArrayList<>();
        advertisementDatas.add(new BannerAdData(1, "http://img0.imgtn.bdimg.com/it/u=1458042595,2336590661&fm=206&gp=0.jpg"));
        advertisementDatas.add(new BannerAdData(2, "http://img4.imgtn.bdimg.com/it/u=3646491133,3402401671&fm=21&gp=0.jpg"));
        advertisement.setPages(this, advertisementDatas);
        advertisement.setWHBili();
        advertisement.startTurning(2000);

        switchRoot.setOnRefreshListener(this);
        setAdapter();
        setGridViewOnItemClickListener();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData();
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData();
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData();
    }

    @Override
    public void upDataUi(HomeHttpResponse datas) {
        homeHttpResponse.addData(datas);
        adapter.notifyDataSetChanged();
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot;
    }

    @Override
    public void clearData() {
        homeHttpResponse.clearAll();
    }

    private void setGridViewOnItemClickListener() {

        //热门人物
        hotFigure.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                MealAnswerDetailsActivity.startUI(HomeActivity.this, homeHttpResponse.getHotFigure().get(position).getGuid());
            }
        });

        //热门问答
        hotAnswer.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                MealAnswerDetailsActivity.startUI(HomeActivity.this, homeHttpResponse.getHotWenda().get(position).getHuiDaUserID());

            }
        });

        //人物志
        cateringFigure.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                InformationDetailsActivity.startUI(HomeActivity.this, homeHttpResponse.getCaterersRwz().get(position).getId());
            }
        });

        //资讯
        cateringInformation.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                InformationDetailsActivity.startUI(HomeActivity.this, homeHttpResponse.getCaterersInfoHead().get(position).getId());
            }
        });
        //秘籍
        esoteric.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_K_Activity.startUI(HomeActivity.this, homeHttpResponse.getCaiPMiji().get(position).getId());
            }
        });
        //悬赏
        demandReward.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardDetailsActivity.startUI(HomeActivity.this, homeHttpResponse.getNeedOffered().get(position).getID());
            }
        });

        //学习课堂
        studyClassroom.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {

                StudyClassroomDetailsActivity.startUI(HomeActivity.this, homeHttpResponse.getLearnClass().get(position).getId());
            }
        });

        //大厨
        chef.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                StudioActivity.startUI(HomeActivity.this, homeHttpResponse.getChef().get(position).getId(), StudioActivity.CHUSHI);
            }
        });

        //掌柜
        manager.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                StudioActivity.startUI(HomeActivity.this, homeHttpResponse.getManager().get(position).getId(), StudioActivity.ZHANGGUI);

            }
        });

    }

    public void setAdapter() {
        hotFigure.setAdapter(adapter.hotFigureAdapter);
        hotAnswer.setAdapter(adapter.hotAnswerAdapter);
        cateringFigure.setAdapter(adapter.cateringFigureAdapter);
        cateringInformation.setAdapter(adapter.cateringInformationAdapter);
        esoteric.setAdapter(adapter.esotericAdapter);
        demandReward.setAdapter(adapter.demandRewardAdapter);
        studyClassroom.setAdapter(adapter.studyClassroomAdapter);
        chef.setAdapter(adapter.chefAdapter);
        manager.setAdapter(adapter.managerAdapter);
    }


    @OnClick(value = {R.id.activity_homepage2_hot_figure, R.id.activity_homepage2_mealAnswer, R.id.activity_homepage2_cateringFigure, R.id.activity_homepage2_hot_mealAnswer,
            R.id.activity_homepage2_information, R.id.activity_homepage2_esoterica, R.id.activity_homepage2_reward,
            R.id.activity_homepage2_studyClassroom, R.id.activity_homepage2_chef, R.id.activity_homepage2_manager
    })
    public void onClickLister(View view) {
        switch (view.getId()) {
            //餐答
            case R.id.activity_homepage2_mealAnswer:
                MealAnswerFragmentActivity.startUi(this);
                break;
            case R.id.activity_homepage2_hot_figure:
                MealAnswerFragmentActivity.startUi(this);

                break;
            case R.id.activity_homepage2_hot_mealAnswer:
                HotAnswerActivity.startUi(this, "热门问答");
                break;
            case R.id.activity_homepage2_cateringFigure:
                CateringFigureActivity.startUi(this, "人物志");
                break;

            case R.id.activity_homepage2_information:
                InformationActivity.startUi(this, "资讯");
                break;

            case R.id.activity_homepage2_esoterica:
                JieyouActivity.startUI(this, 0);
                break;

            case R.id.activity_homepage2_reward:
                JieyouActivity.startUI(this, 2);
                break;
            case R.id.activity_homepage2_studyClassroom:
                JieyouActivity.startUI(this, 1);
                break;
            case R.id.activity_homepage2_chef:
                JianghuActivity.startUI(HomeActivity.this, 1);
                break;
            case R.id.activity_homepage2_manager:
                JianghuActivity.startUI(HomeActivity.this, 2);
                break;
        }
    }

    @BindView(R.id.activity_homepage_GridView_hotFigure)
    protected SuperGridLayout hotFigure;
    @BindView(R.id.activity_homepage_GridView_hotAnswer)
    protected SuperGridLayout hotAnswer;
    @BindView(R.id.activity_homepage_GridView_cateringFigure)
    protected SuperGridLayout cateringFigure;
    @BindView(R.id.activity_homepage_GridView_cateringInformation)
    protected SuperGridLayout cateringInformation;
    @BindView(R.id.activity_homepage_GridView_esoteric)
    protected SuperGridLayout esoteric;
    @BindView(R.id.activity_homepage_GridView_demandReward)
    protected SuperGridLayout demandReward;
    @BindView(R.id.activity_homepage_GridView_studyClassroom)
    protected SuperGridLayout studyClassroom;
    @BindView(R.id.activity_homepage_GridView_chef)
    protected SuperGridLayout chef;
    @BindView(R.id.activity_homepage_GridView_manager)
    protected SuperGridLayout manager;


    @Override
    public Object createHolder() {
        return new NetworkImageHolderView() {

            @Override
            public void onItemClicklistener(View item, int position, BannerAdData data) {

            }
        };

    }
}
