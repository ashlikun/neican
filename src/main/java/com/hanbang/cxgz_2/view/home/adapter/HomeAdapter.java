package com.hanbang.cxgz_2.view.home.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.httpresponse.home.HomeHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.mode.javabean.home.FigureData;
import com.hanbang.cxgz_2.mode.javabean.home.HotFigureData;
import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.adapter.KeTangAdapter;
import com.hanbang.cxgz_2.view.adapter.MealAnswerQuetionAdapter;
import com.hanbang.cxgz_2.view.adapter.MiJiAdapter;

/**
 * Created by Administrator on 2016/8/17.
 */

public class HomeAdapter {
    private Context context;
    private HomeHttpResponse homeHttpRequest;

    public HomeAdapter(Context context, HomeHttpResponse homeHttpRequest) {
        this.homeHttpRequest = homeHttpRequest;
        this.context = context;
        initAdapter();
    }


    public void notifyDataSetChanged() {
        hotFigureAdapter.notifyDataSetChanged();
        hotAnswerAdapter.notifyDataSetChanged();
        cateringFigureAdapter.notifyDataSetChanged();
        cateringInformationAdapter.notifyDataSetChanged();
        esotericAdapter.notifyDataSetChanged();
        demandRewardAdapter.notifyDataSetChanged();
        studyClassroomAdapter.notifyDataSetChanged();
        chefAdapter.notifyDataSetChanged();
        managerAdapter.notifyDataSetChanged();
    }

    private void initAdapter() {
        hotFigureAdapter = new CommonAdapter<HotFigureData>(context, R.layout.item_homepage_hot_figure, homeHttpRequest.getHotFigure()) {
            @Override
            public void convert(ViewHolder helper, HotFigureData item) {
                try {
                    if (!StringUtils.isBlank(item.getAvatarimgSmallurl())) {
                        helper.setImageBitmapCircle(R.id.item_homepage_hot_figure_hotFigure,
                                HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatarimgSmallurl()))
                        );
                    } else {
                        helper.setImageBitmapCircle(R.id.item_homepage_hot_figure_hotFigure,
                                HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar()))
                        );
                    }
                    helper.setText(R.id.iitem_homepage_hot_figure_item1, StringUtils.isNullToConvert(item.getUser_name()));
                    helper.setText(R.id.iitem_homepage_hot_figure_item2, StringUtils.isNullToConvert(item.getJobCN()));
                    helper.setText(R.id.iitem_homepage_hot_figure_item3, StringUtils.isNullToConvert(item.getCompany()));




                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        hotAnswerAdapter = new MealAnswerQuetionAdapter(context, homeHttpRequest.getHotWenda());


        cateringFigureAdapter = new CommonAdapter<FigureData>(context, R.layout.item_find_classroom_data, homeHttpRequest.getCaterersRwz()) {
            @Override
            public void convert(ViewHolder helper, FigureData item) {
                helper.setImageBitmap(R.id.find_classroom_data_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                helper.setVisible(R.id.find_classroom_data_whetherVip, item.is_vip());
                helper.setText(R.id.find_classroom_data_title, StringUtils.isNullToConvert(item.getTitle()));
                helper.setText(R.id.find_classroom_data_nameJobCompany, StringUtils.isNullToConvert(item.getAuthor() + " | " + item.getJob_CN() + " | " + item.getCompany()));
                helper.setText(R.id.find_classroom_data_time, "偷听:" + DateUtils.getTime(item.getAdd_time()));
            }
        };
        cateringInformationAdapter = new CommonAdapter<InformationData>(context, R.layout.item_information, homeHttpRequest.getCaterersInfoHead()) {
            @Override
            public void convert(ViewHolder helper, InformationData item) {
                helper.setImageBitmap(R.id.item_information_topImg, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                helper.setText(R.id.item_information_title, StringUtils.isNullToConvert(item.getTitle()));
                helper.setText(R.id.item_information_from, StringUtils.isNullToConvert(item.getSource()));
                helper.setText(R.id.item_information_comment, item.getCommentcount());
                helper.setText(R.id.item_information_date, DateUtils.getTime(item.getAdd_time()));
                helper.setText(R.id.item_information_praise, item.getLikecount());
            }
        };


        esotericAdapter = new MiJiAdapter(context, homeHttpRequest.getCaiPMiji());


        demandRewardAdapter = new CommonAdapter<DemandRewardData>(context, R.layout.item_homepage_reward, homeHttpRequest.getNeedOffered()) {
            @Override
            public void convert(ViewHolder helper, DemandRewardData item) {
                helper.setImageBitmapCircle(R.id.item_homepage_reward_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar())));
                helper.setText(R.id.item_homepage_reward_name, StringUtils.isNullToConvert(item.getUser_name()));
                helper.setText(R.id.item_homepage_reward_jobCompany, StringUtils.isNullToConvert(item.getJobCN() + " | " + item.getCompany()));
                helper.setText(R.id.content, StringUtils.isNullToConvert(item.getXuQiuContent()));
                helper.setText(R.id.item_homepage_reward_participation, "参与:" + StringUtils.isNullToConvert(item.getJoinCount()));
                if (item.getStatus() == 2) {
                    helper.getView(R.id.item_homepage_reward_status).setSelected(true);
                } else {
                    helper.getView(R.id.item_homepage_reward_status).setSelected(false);
                }
                helper.setText(R.id.time, item.getCjsj());
                helper.setText(R.id.price, StringUtils.isNullToConvert(item.getXunShangPrice()));
            }
        };

        studyClassroomAdapter = new KeTangAdapter(context, homeHttpRequest.getLearnClass());


        chefAdapter = new CommonAdapter<ChefData>(context, R.layout.item_chef_manager, homeHttpRequest.getChef()) {
            @Override
            public void convert(ViewHolder helper, ChefData item) {
                if (!StringUtils.isBlank(item.getAvatarimgSmallurl())) {
                    helper.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatarimgSmallurl())));
                } else {
                    helper.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar())));
                }
                helper.setVisible(R.id.item_chef_manager_isVip, item.is_vip());
                helper.setText(R.id.item_chef_manager_attention, item.getMembershipcount());
                helper.setText(R.id.item_chef_manager_company, StringUtils.isNullToConvert(item.getCompany()));
                helper.setText(R.id.item_chef_manager_name, StringUtils.isNullToConvert(item.getUser_name()));
                helper.setText(R.id.item_chef_manager_job, StringUtils.isNullToConvert(item.getJob()));
            }
        };
        managerAdapter = new CommonAdapter<ManagerData>(context, R.layout.item_chef_manager, homeHttpRequest.getManager()) {
            @Override
            public void convert(ViewHolder helper, ManagerData item) {
                if (!StringUtils.isBlank(item.getAvatarimgSmallurl())) {
                    helper.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatarimgSmallurl())));
                } else {
                    helper.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar())));
                }
                helper.setVisible(R.id.item_chef_manager_isVip, item.is_vip());
                helper.setText(R.id.item_chef_manager_attention, StringUtils.isNullToConvert(item.getIsguanzhu()));
                helper.setText(R.id.item_chef_manager_company, StringUtils.isNullToConvert(item.getCompany()));
                helper.setText(R.id.item_chef_manager_name, StringUtils.isNullToConvert(item.getUser_name()));
                helper.setText(R.id.item_chef_manager_job, StringUtils.isNullToConvert(item.getJob()));
            }
        };

    }

    //1热门用户
    public CommonAdapter hotFigureAdapter = null;
    //2热门问答

    public MealAnswerQuetionAdapter hotAnswerAdapter = null;
    //3餐答人物志
    public CommonAdapter cateringFigureAdapter = null;
    //4资讯
    public CommonAdapter cateringInformationAdapter = null;
    //5秘籍
    public CommonAdapter esotericAdapter = null;

    //6悬赏需求

    public CommonAdapter demandRewardAdapter = null;

    //7学习
    public CommonAdapter studyClassroomAdapter = null;

    //8厨师
    public CommonAdapter chefAdapter = null;

    //9掌柜
    public CommonAdapter managerAdapter = null;
}
