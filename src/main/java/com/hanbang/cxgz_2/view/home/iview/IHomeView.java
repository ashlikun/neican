package com.hanbang.cxgz_2.view.home.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseSwipeView;
import com.hanbang.cxgz_2.mode.httpresponse.home.HomeHttpResponse;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IHomeView extends BaseSwipeView {
    void upDataUi(HomeHttpResponse datas);
}
