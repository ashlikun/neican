package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.view.about_me.fragment.MeMiJIFragment;
import com.hanbang.cxgz_2.view.adapter.SectionsPagerAdapter;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/22　14:28
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：我的秘籍
 */

public class MeMijiActivity extends BaseActivity {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPage)
    ViewPager viewPager;
    private String[] title = new String[]{"我上传的秘籍", "我购买的秘籍"};
    private ArrayList<Fragment> fragmentList = new ArrayList<>();

    public static void startUI(BaseActivity activity) {
        if (activity.isLogin(true)) {
            Intent intent = new Intent(activity, MeMijiActivity.class);
            activity.startActivity(intent);
        }
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     * <p>
     * 方法功能：获取布局id
     */
    @Override
    public int getContentView() {
        return R.layout.activity_or_fragment_viewpage;
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:16
     * <p>
     * 方法功能：初始化view
     */
    @Override
    public void initView() {
        toolbar.setBack(this);
        toolbar.setTitle("我的秘籍");
        setDefaultFragment();
        fragmentList.add(new MeMiJIFragment(1));
        fragmentList.add(new MeMiJIFragment(2));
        //为viewPage设置adapter
        SectionsPagerAdapter mAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragmentList, title);
        viewPager.setAdapter(mAdapter);
        //tabLayout关联ViewPage
        tabLayout.setupWithViewPager(viewPager);

    }


    private void setDefaultFragment() {
//        FragmentManager fm = getSupportFragmentManager();
//        FragmentTransaction transaction = fm.beginTransaction();
//        studioMiJIFragment = new StudioMiJIFragment();
//        transaction.replace(R.id.content, studioMiJIFragment);
//        transaction.commit();
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:16
     * <p>
     * 方法功能：解析意图
     *
     * @param intent
     */
    @Override
    public void parseIntent(Intent intent) {

    }
}
