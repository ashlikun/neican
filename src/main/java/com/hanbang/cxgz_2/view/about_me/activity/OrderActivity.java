package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseListActivity;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeOrderData;
import com.hanbang.cxgz_2.pressenter.about_me.OrderPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.iview.IOrderView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/21 14:53
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的订单
 */

public class OrderActivity extends BaseListActivity<IOrderView, OrderPresenter> implements IOrderView {
    CommonAdapter adapter;
    private ArrayList<MeOrderData> datas = new ArrayList<>();


    public static void startUi(BaseActivity context) {
        if (context.isLogin(true)) {
            Intent intent = new Intent(context, OrderActivity.class);
            context.startActivity(intent);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }

    @Override
    public OrderPresenter initPressenter() {
        return new OrderPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {


    }

    @Override
    public int getContentView() {
        return R.layout.activity_or_fragment_list;
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setTitle("我的订单");
        toolbar.setBack(this);
        presenter.getHttpData(true);
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(this).sizeResId(R.dimen.dp_10)
                .colorResId(R.color.gray_ee).build();
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    public Object getAdapter() {
        return adapter = new CommonAdapter<MeOrderData>(this, R.layout.item_order, datas) {
            @Override
            public void convert(ViewHolder holder, MeOrderData item) {
                try {

                    holder.setText(R.id.activity_order_name, item.getTitle());
                    holder.setText(R.id.order_details_orderNum, item.getShopingNum());
                    holder.setText(R.id.order_details_orderTime, item.getAdd_time());
                    holder.setText(R.id.order_details_price, item.getTotalPrice());
                    holder.setImageBitmap(R.id.activity_order_picture, item.getImgurl());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }


    @Override
    public void upDataUi(List<MeOrderData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return listSwipeView.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return listSwipeView.getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        listSwipeView.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return listSwipeView.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return listSwipeView.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


}
