package com.hanbang.cxgz_2.view.about_me.activity;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;
import com.hanbang.cxgz_2.pressenter.about_me.MeSetDataPresenter;
import com.hanbang.cxgz_2.utils.ui.AndroidBug5497Workaround;
import com.hanbang.cxgz_2.view.about_me.fragment.MeSetDataEditFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.MeSetDataShowFragment;
import com.hanbang.cxgz_2.view.about_me.iview.IMeSetDAtaView;

import java.util.ArrayList;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 11:43
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：设置资料
 */

public class MeSetDataActivity extends BaseMvpActivity<IMeSetDAtaView,MeSetDataPresenter>implements IMeSetDAtaView {
    FragmentManager fm;
    MeSetDataShowFragment ShowFragment;
    MeSetDataEditFragment editFragment;
    boolean conver = true;

    /**
     * @param context
     */
    public static void startUi(BaseActivity context) {
        if (context.isLogin(true)) {
            Intent intent = new Intent(context, MeSetDataActivity.class);
            context.startActivity(intent);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.getUserData(UserData.userData.getId());
    }


    @Override
    public int getContentView() {
        return R.layout.activity_nest_fragment;


    }

    @Override
    public void initView() {
        AndroidBug5497Workaround androidBug5497Workaround = AndroidBug5497Workaround.assistActivity(this);
        ShowFragment = MeSetDataShowFragment.newIntences();
        editFragment = MeSetDataEditFragment.newIntences();


        toolbar.setTitle("我的资料");
        toolbar.setBack(this);
        toolbar.addAction(1, "编辑", R.drawable.material_set);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                toolbar.removeAction(1);
                FragmentTransaction ft = fm.beginTransaction();
                if (conver) {
                    dataEdit(ft);
                } else {
                    dataShow(ft);
                }

                ft.commit();
                return true;
            }
        });

        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content, editFragment);
        ft.add(R.id.content, ShowFragment);
        ft.commit();
    }

    //编辑
    private void dataEdit(FragmentTransaction ft) {
        animator(editFragment.view);

        ft.show(editFragment);
        ft.hide(ShowFragment);
        conver = false;
        toolbar.addAction(1, "取消");
    }

    //展示
    private void dataShow(FragmentTransaction ft) {
        animator(ShowFragment.view);

        ft.show(ShowFragment);
        ft.hide(editFragment);
        conver = true;
        toolbar.addAction(1, "编辑", R.drawable.material_set);

    }

    //动画
    private void animator(final View view) {

        view.setScaleX(0.5f);
        view.setScaleY(0.5f);
        ObjectAnimator rotationY = ObjectAnimator.ofFloat(view, "rotationY", 180, 360).setDuration(1000);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", 0.3f, 1.0f).setDuration(300);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", 0.3f, 1f).setDuration(300);
        AnimatorSet set = new AnimatorSet();

        set.play(rotationY).before(scaleY).before(scaleX);
        set.start();


    }

    @Override
    public int getStatusBarResource() {

        return R.color.translucent;
    }

    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    public void parseIntent(Intent intent) {

    }


    @Override
    public void upLoading(boolean whetherSucceed) {

    }

    @Override
    public void clearData() {

    }

    @Override
    public MeSetDataPresenter initPressenter() {
        return new MeSetDataPresenter();
    }

    @Override
    public void getCaiXiData(ArrayList<CaiXiData> datas) {

    }



    @Override
    public void getUserData(UserData data) {

    }
}
