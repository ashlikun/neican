package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hanbang.audiorecorder.RecordActivity;
import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.MyApplication;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;
import com.hanbang.cxgz_2.pressenter.about_me.IssuerEsoteric_D_Presenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.FileUtils;
import com.hanbang.cxgz_2.utils.other.MediaFile;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.utils.ui.AndroidBug5497Workaround;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.utils.ui.ToastUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.about_me.iview.IssuerEsoteric_K_View;
import com.hanbang.cxgz_2.view.widget.PopupSelect;
import com.hanbang.cxgz_2.view.widget.dialog.DialogDateTime;
import com.hanbang.cxgz_2.view.widget.dialog.DialogProgress;
import com.hanbang.cxgz_2.view.widget.dialog.DialogSelectMode;
import com.hanbang.cxgz_2.view.widget.media.MusicPlayView;
import com.hanbang.cxgz_2.view.widget.media.VideoPlayView;
import com.hanbang.videoplay.view.VideoCameraActivity;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/2 10:44
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：买断秘籍上传页面
 */

public class IssuerEsoteric_D_Activity extends BaseMvpActivity<IssuerEsoteric_K_View, IssuerEsoteric_D_Presenter> implements IssuerEsoteric_K_View {

    @BindView(R.id.find_uploading_k_activity_root)
    RelativeLayout viewRoot;
    @BindView(R.id.haiBao)
    ImageView haiBao;
    @BindView(R.id.esotericName)
    TextInputLayout esotericName;
    @BindView(R.id.maiDian)
    TextInputLayout maiDian;
    @BindView(R.id.caiXi)
    TextView caiXi;

    @BindView(R.id.inputPrice)
    TextInputLayout inputPrice;
    @BindView(R.id.pingJia)
    TextInputLayout pingJia;
    @BindView(R.id.maiDuanLiuCheng)
    TextInputLayout maiDuanLiuCheng;
    @BindView(R.id.maiDunaShengMing)
    TextInputLayout maiDunaShengMing;
    @BindView(R.id.ZhongChou)
    TextInputLayout ZhongChou;
    @BindView(R.id.startTiem)
    TextView startTiem;
    @BindView(R.id.endTiem)
    TextView endTiem;
    //视频路径
    private String videoPlayPath;
    @BindView(R.id.find_uploading_k_activity_videoPlay)
    VideoPlayView videoPlay;
    //音频路径
    private String musicPlayPath;
    @BindView(R.id.music_play)
    MusicPlayView musicPlay;

    //海报路径
    private String haiBaoPath;
    private EditHelper editHelper = new EditHelper(this);
    private int caiXiId;

    @Override
    public int getContentView() {
        return R.layout.activity_uploading_d_activity;
    }


    @Override
    public void clearData() {

    }

    @Override
    public IssuerEsoteric_D_Presenter initPressenter() {
        return new IssuerEsoteric_D_Presenter();
    }

    @Override
    public void initView() {
        AndroidBug5497Workaround androidBug5497Workaround = AndroidBug5497Workaround.assistActivity(this);
        viewRoot.setPadding(0, UiUtils.getStatusHeight(this) - 2, 0, 0);
        toolbar.setBack(this);
        toolbar.setTitle("上传买断式秘籍");


        musicPlay.setVisibility(View.GONE);
        videoPlay.setVisibility(View.GONE);

        editHelper.addEditHelperData(new EditHelper.EditHelperData(esotericName, Validators.getLengthSRegex(1, 20), "请输入秘籍名称"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(caiXi, Validators.getLengthSRegex(1, 20), "请选择菜系"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(maiDian, Validators.getLengthSRegex(1, 100), "请输入卖点特色"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(startTiem, Validators.getLengthSRegex(1, 100), "请选择开始时间"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(endTiem, Validators.getLengthSRegex(1, 100), "请选择结束时间"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(inputPrice, Validators.getLengthSRegex(1, 7), "请输入价格"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(pingJia, Validators.getLengthSRegex(1, 100), "请输入价格"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(ZhongChou, Validators.getLengthSRegex(1, 100), "欢迎众筹买断"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(maiDuanLiuCheng, Validators.getLengthSRegex(1, 100), "请输入买断流程"));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(maiDunaShengMing, Validators.getLengthSRegex(1, 100), "请输入买断声明"));


    }


    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    public int getStatusBarResource() {
        return R.color.yellow_top;
    }

    @Override
    public void parseIntent(Intent intent) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //获取海报路径
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            haiBaoPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT).get(0);
            GlideUtils.show(haiBao, haiBaoPath);
        }


        //获取音频路径
        if (requestCode == REQUEST_CODE + 2 && resultCode == RESULT_OK) {
            musicPlayPath = FileUtils.getFileSelectPath(data);
            if (!MediaFile.JudgeFileType(musicPlayPath, "mp3")) {
                ToastUtils.getToastShort("请选择MP3,WMA格式的音频文件");
                return;
            }
            musicPlay.setFilePath(musicPlayPath);
            musicPlay.setVisibility(View.VISIBLE);
        }
        //获取视频路径
        if (requestCode == REQUEST_CODE + 3 && resultCode == RESULT_OK) {
            videoPlayPath = FileUtils.getFileSelectPath(data);
            if (!MediaFile.JudgeFileType(videoPlayPath, "mp4")) {
                ToastUtils.getToastShort("请选择MP4格式的视频文件");
                return;
            }
            videoPlay.setUpDefault(videoPlayPath, "本地视频");
            videoPlay.setThumbRes(R.mipmap.logo);
            videoPlay.setVisibility(View.VISIBLE);
        }


        //录制视频路径
        if (requestCode == REQUEST_CODE + 4 && resultCode == RESULT_OK) {
            videoPlayPath = data.getStringExtra(VideoCameraActivity.RESULT);
            videoPlay.setUpDefault(videoPlayPath, "录制视频");
            videoPlay.setThumbRes(R.mipmap.logo);
            videoPlay.setVisibility(View.VISIBLE);
        }


        //录制音频路径
        if (requestCode == REQUEST_CODE + 5 && resultCode == RESULT_OK) {
            musicPlayPath = data.getStringExtra(RecordActivity.MP3PATH);
            musicPlay.setFilePath(musicPlayPath);
            musicPlay.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (musicPlay != null) {
            musicPlay.pause();

        }
        if (videoPlay != null) {
            videoPlay.pause();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (musicPlay != null) {
            musicPlay.release();
        }

        if (viewRoot != null) {
            videoPlay.release();
        }


    }


    private Calendar startTime;
    private Calendar endTime;

    @OnClick({R.id.selectHaiBao, R.id.commit, R.id.selectAudio, R.id.selectVideo, R.id.caiXi_select, R.id.ll_startTiem, R.id.ll_endTiem})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.selectHaiBao:
                MultiImageSelector.create()
                        .showCamera(true)
                        .single()
                        .start(this, REQUEST_CODE);
                break;

            case R.id.commit:
                if (editHelper.check()) {
                    if (TextUtils.isEmpty(haiBaoPath)) {
                        SnackbarUtil.showShort(this, "海报不能为空", SnackbarUtil.Warning).show();
                        return;
                    }

                    presenter.getHttpData(
                            String.valueOf(2),
                            editHelper.getText(R.id.esotericName),
                            editHelper.getText(R.id.maiDian),
                            editHelper.getText(R.id.inputPrice),
                            String.valueOf(caiXiId),
                            haiBaoPath,
                            videoPlayPath,
                            editHelper.getText(R.id.startTiem),
                            editHelper.getText(R.id.endTiem),
                            editHelper.getText(R.id.pingJia),
                            editHelper.getText(R.id.maiDuanLiuCheng),
                            editHelper.getText(R.id.maiDunaShengMing),
                            editHelper.getText(R.id.ZhongChou),
                            musicPlayPath
                    );
                }
                break;

            case R.id.selectAudio:
                final DialogSelectMode dialog = new DialogSelectMode(IssuerEsoteric_D_Activity.this, "录音", "选择音频");
                dialog.show();
                dialog.setClickCallback(new DialogSelectMode.OnClickCallback() {
                    @Override
                    public void onClick(int type) {
                        if (type == 1) {
                            RecordActivity.startUi(IssuerEsoteric_D_Activity.this, REQUEST_CODE + 5, MyApplication.appSDFilePath);
                            dialog.dismiss();
                        } else if (type == 2) {
                            UiUtils.startFileSelect(IssuerEsoteric_D_Activity.this, "audio/*", REQUEST_CODE + 2);
                            dialog.dismiss();
                        } else if (type == 0) {
                            dialog.dismiss();
                        }
                    }
                });
                break;
            case R.id.selectVideo:
                final DialogSelectMode dialog2 = new DialogSelectMode(IssuerEsoteric_D_Activity.this, "录制", "选择视频");
                dialog2.show();
                dialog2.setClickCallback(new DialogSelectMode.OnClickCallback() {
                    @Override
                    public void onClick(int type) {
                        if (type == 1) {
                            VideoCameraActivity.startActivity(IssuerEsoteric_D_Activity.this, MyApplication.appSDFilePath, IssuerEsoteric_D_Activity.this.REQUEST_CODE + 4);
                            dialog2.dismiss();
                        } else if (type == 2) {
                            UiUtils.startFileSelect(IssuerEsoteric_D_Activity.this, "video/*", REQUEST_CODE + 3);
                            dialog2.dismiss();
                        } else if (type == 0) {
                            dialog2.dismiss();
                        }
                    }
                });
                break;

            case R.id.caiXi_select:
                presenter.getCaiXiData(false);
                break;

            case R.id.ll_startTiem:
                DialogDateTime dialogDateTime = new DialogDateTime(IssuerEsoteric_D_Activity.this, DialogDateTime.MODE.DATE_Y_M_D);
                dialogDateTime.setTitleMain("开始日期");
                final Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.YEAR, 1);
                if (endTime != null) {
                    dialogDateTime.setMaxCalendar(endTime);
                } else {
                    dialogDateTime.setMaxCalendar(calendar);
                }

                Calendar calendar2 = Calendar.getInstance();
                dialogDateTime.setMinCalendar(calendar2);
                dialogDateTime.show();
                dialogDateTime.setOnClickCallback(new DialogDateTime.OnClickCallback() {
                    @Override
                    public void onClick(DialogDateTime dialog, int number, Calendar content) {
                        startTime = content;
                        startTiem.setText(content.get(Calendar.YEAR) + "-" + (content.get(Calendar.MONTH) + 1) + "-" + content.get(Calendar.DAY_OF_MONTH));
                    }
                });

                break;


            case R.id.ll_endTiem:
                DialogDateTime dialogDateTime2 = new DialogDateTime(IssuerEsoteric_D_Activity.this, DialogDateTime.MODE.DATE_Y_M_D);
                if (startTime == null) {
                    SnackbarUtil.showShort(this, "请先选择开始时间", SnackbarUtil.Info).show();
                    return;
                }
                dialogDateTime2.setTitleMain("结束日期");


                dialogDateTime2.setMinCalendar(startTime);

                dialogDateTime2.show();
                dialogDateTime2.setOnClickCallback(new DialogDateTime.OnClickCallback() {
                    @Override
                    public void onClick(DialogDateTime dialog, int number, Calendar content) {
                        endTime = content;
                        endTiem.setText(content.get(Calendar.YEAR) + "-" + (content.get(Calendar.MONTH) + 1) + "-" + content.get(Calendar.DAY_OF_MONTH));
                    }
                });

//                CalendarPickerUtil calendarPickerUtil2 = new CalendarPickerUtil(this);
//                calendarPickerUtil2.setMinDate(0);
//                calendarPickerUtil2.setOnClickCallback(new CalendarPickerUtil.OnClickCallback() {
//                    @Override
//                    public void onclick(int year, int month, int day, int hour, int minute) {
//                        endTiem.setText(year + "-" + month + "-" + day);
//                    }
//                });

                break;
        }
    }


    @Override
    public void upLoading(boolean whetherSucceed) {

    }

    DialogProgress dialogProgress;

    @Override
    public void upLoading(int progress, boolean done, boolean isUpdate, boolean isCompress) {
        if (dialogProgress == null) {
            dialogProgress = new DialogProgress(this);
        }
        dialogProgress.setTitleText(isCompress ? "压缩中" : "上传中");
        dialogProgress.setProgress(progress);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

    PopupSelect popupSelect;

    @Override
    public void getCaiXiData(ArrayList<CaiXiData> datas) {

        if (popupSelect == null) {
            popupSelect = new PopupSelect(this, datas, 8, 100);
        }

        popupSelect.showPopupWindow(caiXi);
        popupSelect.setOnClickCallback(new PopupSelect.OnClickCallback() {
            @Override
            public void onClick(int id, String content, int type) {
                caiXi.setText(content);
                caiXiId = id;
            }
        });
    }


}
