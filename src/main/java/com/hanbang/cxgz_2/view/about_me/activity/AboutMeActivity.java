package com.hanbang.cxgz_2.view.about_me.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMainActivity;
import com.hanbang.cxgz_2.mode.enumeration.AboutMeTypeEnum;
import com.hanbang.cxgz_2.mode.enumeration.AgreementEnum;
import com.hanbang.cxgz_2.mode.javabean.about_me.AboutMeTypeData;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.pressenter.about_me.AboutMePresenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.login.activity.LoginActivity;
import com.hanbang.cxgz_2.view.login.activity.RegisterActivity;
import com.hanbang.cxgz_2.view.other.AgreementActivity;
import com.hanbang.cxgz_2.view.other.TestActivity;
import com.hanbang.cxgz_2.view.widget.dialog.DialogIssue;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2016/8/15.
 */

public class AboutMeActivity extends BaseMainActivity<BaseView, AboutMePresenter> implements BaseView {
    @BindView(R.id.view_gridView)
    GridView viewGridView;
    ArrayList<AboutMeTypeData> items = new ArrayList<>();

    MyBroadcast myBroadcast;
    IntentFilter intentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int currentItem() {
        return 4;
    }

    @Override
    public AboutMePresenter initPressenter() {
        return new AboutMePresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main_me;
    }

    public static void startUI(Activity activity) {
        Intent intent = new Intent(activity, AboutMeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(intent);
    }

    @Override
    public void initView() {
        super.initView();
        myBroadcast = new MyBroadcast();
        intentFilter = new IntentFilter();
        intentFilter.addAction(Global.LOGIN_BROADCAST);
        registerReceiver(myBroadcast, intentFilter);

        setAdapter();
        setOnclickListener();
        addData();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myBroadcast);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void addData() {
        if (UserData.getUserData() != null) {
            GlideUtils.showCircle(findViewById(R.id.activity_main_me_portrait), UserData.userData.getAvatar());
            UiUtils.setTextViewContent(findViewById(R.id.activity_main_me_name), UserData.userData.getUser_name());
            UiUtils.setTextViewContent(findViewById(R.id.activity_main_me_job), UserData.userData.getJob_CN());
            UiUtils.setTextViewContent(findViewById(R.id.activity_main_me_commpany), UserData.userData.getCompany());
            if (UserData.userData.is_vip()) {
                findViewById(R.id.activity_main_me_vip).setBackgroundResource(R.mipmap.vip_icon);
            } else {
                findViewById(R.id.activity_main_me_vip).setVisibility(View.GONE);
            }

        }
    }


    @OnClick(value = {R.id.activity_my_issue, R.id.activity_my_set})
    public void onClickListener(View v) {
        switch (v.getId()) {
            case R.id.activity_my_issue:
                DialogIssue dialogIssue = new DialogIssue(this);
                dialogIssue.show();

                dialogIssue.setClickCallback(new DialogIssue.OnClickCallback() {
                    @Override
                    public void onClick(String title, String className) {
                        Intent intent = new Intent();
                        intent.setClassName(AboutMeActivity.this, className);
                        startActivity(intent);
                    }
                });

                break;

            case R.id.activity_my_set:
                MeSetActivity.startUi(this);

                break;

        }
    }

    @Override
    public void clearData() {

    }

    private void setAdapter() {
        //设置adapter
        items.clear();
        items.addAll(presenter.getGridData());
        viewGridView.setAdapter(new CommonAdapter<AboutMeTypeData>(this, R.layout.me_activity_item, items) {
            @Override
            public void convert(ViewHolder helper, AboutMeTypeData item) {
                helper.setImageResource(R.id.me_acticity_item_image, item.getType().getPictureId());
                helper.setText(R.id.me_acticity_item_text, item.getType().getTitle());
                helper.setVisible(R.id.me_acticity_item_image_messageNumber, item.showHint());
            }
        });
    }

    private void setOnclickListener() {
        viewGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //我的餐答
                if (items.get(position).getType() == AboutMeTypeEnum.CHAN_DA) {
                    MeCanDaActivity.startUI(AboutMeActivity.this, 0);

                    //订单
                } else if (items.get(position).getType() == AboutMeTypeEnum.DING_DAN) {
                    OrderActivity.startUi(AboutMeActivity.this);

                    //秀场
                } else if (items.get(position).getType() == AboutMeTypeEnum.XIU_CHANG) {
                    MeXiuChangActivity.startUi(AboutMeActivity.this);

                    //老师学员
                } else if (items.get(position).getType() == AboutMeTypeEnum.LAO_SHI_XUE_YUAN) {
                    SnackbarUtil.showShort(AboutMeActivity.this, "功能添加中", SnackbarUtil.Info).show();

                    //邀请好友
                } else if (items.get(position).getType() == AboutMeTypeEnum.YAO_QING_HAO_YOU) {
                    SnackbarUtil.showShort(AboutMeActivity.this, "功能添加中", SnackbarUtil.Info).show();

                    //多的收藏
                } else if (items.get(position).getType() == AboutMeTypeEnum.SHOU_CANG) {
                    SnackbarUtil.showShort(AboutMeActivity.this, "功能添加中", SnackbarUtil.Info).show();

                    //我的工作室
                } else if (items.get(position).getType() == AboutMeTypeEnum.GONG_ZUO_SHI) {
                    StudioActivity.startUI(AboutMeActivity.this, UserData.userData.getId(), UserData.userData.getGroup_id());

                    //我的余额
                } else if (items.get(position).getType() == AboutMeTypeEnum.YU_E) {
                    YuEActivity.startUi(AboutMeActivity.this);

                    //我的秘籍
                } else if (items.get(position).getType() == AboutMeTypeEnum.MI_JI) {
                    MeMijiActivity.startUI(AboutMeActivity.this);
                    //我的课堂
                } else if (items.get(position).getType() == AboutMeTypeEnum.KE_TANG) {
                    MeKeTangActivity.startUI(AboutMeActivity.this);
                    //关于内餐
                } else if (items.get(position).getType() == AboutMeTypeEnum.GUAN_YU) {
                    AgreementActivity.startUI(AboutMeActivity.this, AgreementEnum.GUANG_YU_NEI_CAN);

                    //使用说明
                } else if (items.get(position).getType() == AboutMeTypeEnum.SHUO_MING) {
                    AgreementActivity.startUI(AboutMeActivity.this, AgreementEnum.SHR_YONG_SHUO_MING);

                    //登录
                } else if (items.get(position).getType() == AboutMeTypeEnum.DENG_LU) {
                    LoginActivity.startUI(AboutMeActivity.this, false);

                    //注册
                } else if (items.get(position).getType() == AboutMeTypeEnum.ZHU_CE) {
                    RegisterActivity.startUI(AboutMeActivity.this);

                    //测试
                } else if (items.get(position).getType() == AboutMeTypeEnum.CE_SHI) {
                    TestActivity.startUI(AboutMeActivity.this);
                }


            }
        });


    }


    /**
     * 我的广播
     */
    class MyBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //登录成功
            if (intent.getAction().equals(Global.LOGIN_BROADCAST)) {
                addData();
            }

        }
    }
}
