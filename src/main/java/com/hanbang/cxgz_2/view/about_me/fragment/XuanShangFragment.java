package com.hanbang.cxgz_2.view.about_me.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseListFragment;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.pressenter.about_me.MeXuanShangPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.adapter.MeXuanShangAdapter;
import com.hanbang.cxgz_2.view.about_me.iview.IMeXuanShangView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/10　12:41
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class XuanShangFragment extends BaseListFragment<IMeXuanShangView, MeXuanShangPresenter> implements IMeXuanShangView {
    private ArrayList<DemandRewardData> listDatas = new ArrayList<>();
    private MeXuanShangAdapter adapter = null;
    private int id;

    public XuanShangFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public int getContentView() {
        return R.layout.listswipeview;
    }

    @Override
    public void initView() {
        super.initView();
        listSwipeView.getRecyclerView().setAutoloaddingCompleData("共发布了 %d 条悬赏");
        presenter.getHttpData(true);
    }

    @Override
    public Object getAdapter() {
        return adapter = new MeXuanShangAdapter(activity, listDatas);
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration
                .Builder(activity)
                .sizeResId(R.dimen.dp_10)
                .colorResId(R.color.main_black)
                .build();
    }

    @Override
    public MeXuanShangPresenter initPressenter() {
        return new MeXuanShangPresenter();
    }

    @Override
    public void clearData() {
        listDatas.clear();
    }


    @Override
    public void upDataUi(List<DemandRewardData> datas) {
        listDatas.addAll(datas);
        adapter.notifyDataSetChanged();
        if (listDatas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData("暂未发布秘籍"));
        }
    }


    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


}
