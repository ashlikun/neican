package com.hanbang.cxgz_2.view.about_me.iview;


import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeBalanceData;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 14:38
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的余额
 */
public interface IYuEView extends BaseListView {
    void upDataUi(MeBalanceData datas);
}
