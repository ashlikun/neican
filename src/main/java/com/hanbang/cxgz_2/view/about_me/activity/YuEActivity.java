package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseListActivity;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeBalanceData;
import com.hanbang.cxgz_2.pressenter.about_me.YuEPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.iview.IYuEView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 14:38
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的余额
 */

public class YuEActivity extends BaseListActivity<IYuEView, YuEPresenter> implements IYuEView {
    CommonAdapter adapter;
    @BindView(R.id.activity_yu_e_price)
    TextView yuE;
    private ArrayList<MeBalanceData.Item> datas = new ArrayList<>();
    MeBalanceData data;

    public static void startUi(BaseActivity context) {
        if (context.isLogin(true)) {
            Intent intent = new Intent(context, YuEActivity.class);
            context.startActivity(intent);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }

    @Override
    public YuEPresenter initPressenter() {
        return new YuEPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {


    }

    @Override
    public int getContentView() {
        return R.layout.activity_yu_e;
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setTitle("我的余额");
        toolbar.setBack(this);
        presenter.getHttpData(true);
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(this).sizeResId(R.dimen.dp_1)
                .colorResId(R.color.gray_ee).build();
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    public Object getAdapter() {
        return adapter = new CommonAdapter<MeBalanceData.Item>(this, R.layout.item_activity_yu_e, datas) {
            @Override
            public void convert(ViewHolder holder, MeBalanceData.Item item) {
                try {

                    holder.setText(R.id.item_activity_yu_e_source, item.getBankuaiCN());
                    holder.setText(R.id.item_activity_yu_e_price, item.getPrice());
                    holder.setText(R.id.item_activity_yu_e_time, item.getCjsj());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }


    @Override
    public void upDataUi(MeBalanceData d) {
        datas.addAll(d.getList());
        data = d;
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();
        yuE.setText("￥" + d.getAccountAll());

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return listSwipeView.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return listSwipeView.getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        listSwipeView.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return listSwipeView.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return listSwipeView.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


    @OnClick(R.id.TiXian)
    public void onClick() {
        if (data != null) {
            YuETiXianActivity.startUi(this, data.getTiXianRemark(), data.getYongJinRemark());
        }
    }
}
