package com.hanbang.cxgz_2.view.about_me.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.MultiItemCommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.MultiItemTypeSupport;
import com.hanbang.cxgz_2.mode.enumeration.RewardTypeEnum;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/21　15:33
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class MeXuanShangAdapter extends MultiItemCommonAdapter<DemandRewardData> {


    public MeXuanShangAdapter(Context context, List<DemandRewardData> datas) {

        super(context, datas, new MultiItemTypeSupport<DemandRewardData>() {
            @Override
            public int getLayoutId(int itemType) {
                return itemType;
            }

            @Override
            public int getItemViewType(int position, DemandRewardData demandRewardData) {
                return demandRewardData.getLayoutId();
            }
        });
    }


    @Override
    public void convert(ViewHolder holder, DemandRewardData o) {
        holder.setText(R.id.content, StringUtils.isNullToConvert(o.getXuQiuContent()));
        holder.setText(R.id.price, StringUtils.isNullToConvert(o.getXunShangPrice() + "元"));
        holder.setText(R.id.time, o.getCjsj());

        if (o.getXuQiuType() == RewardTypeEnum.CAI_PIN_YAN_FA) {
            holder.setImageBitmapCircle(R.id.item_homepage_reward_portrait, o.getAvatar());
            holder.setText(R.id.item_homepage_reward_name, StringUtils.isNullToConvert(o.getUser_name()));
            holder.setText(R.id.item_homepage_reward_jobCompany, StringUtils.isNullToConvert(o.getJobCN() + " | " + o.getCompany()));
            holder.setText(R.id.item_homepage_reward_participation, "参与:" + StringUtils.isNullToConvert(o.getJoinCount()));
            if (o.getStatus() == 2) {
                holder.getView(R.id.item_homepage_reward_status).setSelected(true);
            } else {
                holder.getView(R.id.item_homepage_reward_status).setSelected(false);
            }
        } else {
            holder.setText(R.id.address, StringUtils.isNullToConvert(o.getJobCN() + " | " + o.getCompany()));
        }

        if (o.getXuQiuType() == RewardTypeEnum.DIAN_PU_ZHUAN_RANG ||
                o.getXuQiuType() == RewardTypeEnum.ER_SHOU_SHE_BEI ||
                o.getXuQiuType() == RewardTypeEnum.JIA_MENG_DAI_LI
                ) {
            holder.setImageBitmap(R.id.image, o.getXuQiuImg());
        }

    }


}
