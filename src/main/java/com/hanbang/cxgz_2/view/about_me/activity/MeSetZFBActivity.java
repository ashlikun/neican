package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;

import butterknife.OnClick;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 18:18
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：设置支付宝帐号
 */

public class MeSetZFBActivity extends BaseActivity {

    /**
     * @param context
     */
    public static void startUi(BaseActivity context) {
        if (context.isLogin(true)) {
            Intent intent = new Intent(context, MeSetZFBActivity.class);
            context.startActivity(intent);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }


    @Override
    public int getContentView() {
        return R.layout.activity_me_set_zfb;
    }

    @Override
    public void initView() {
        toolbar.setTitle("设置支付宝帐号");
        toolbar.setBack(this);


    }


    @Override
    public void parseIntent(Intent intent) {

    }

    @OnClick(R.id.commit)
    public void onClick() {
        SnackbarUtil.showShort(this, "功能添加中", SnackbarUtil.Info).show();

    }

}
