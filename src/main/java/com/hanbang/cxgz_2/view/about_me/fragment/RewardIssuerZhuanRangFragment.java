package com.hanbang.cxgz_2.view.about_me.fragment;

import android.content.Intent;
import android.widget.EditText;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.view.IUpdataUIView;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseMvpFragment;
import com.hanbang.cxgz_2.pressenter.about_me.RewardIssuerPresenter;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerRewardFragmentActivity;
import com.hanbang.cxgz_2.view.other.CitySelectActivity;
import com.hanbang.cxgz_2.view.widget.dialog.DialogProgress;

import butterknife.BindView;
import butterknife.OnClick;

;
;import static android.app.Activity.RESULT_OK;

/**
 * 店铺转让
 * Created by yang on 2016/8/24.
 */

public class RewardIssuerZhuanRangFragment extends BaseMvpFragment<IUpdataUIView<Boolean>, RewardIssuerPresenter> implements IUpdataUIView<Boolean> {
    @BindView(R.id.fragment_reward_issuer_dian_pu_name)
    EditText name;
    @BindView(R.id.fragment_reward_issuer_dian_pu_price)
    EditText price;
    @BindView(R.id.fragment_reward_issuer_zao_pin_selectCity)
    TextView city;
    @BindView(R.id.fragment_reward_issuer_zao_pin_address)
    EditText address;

    private EditHelper editHelper;
    private IssuerRewardFragmentActivity activity;
    private String cityId = "-1";

    public static RewardIssuerZhuanRangFragment newInstance() {
        RewardIssuerZhuanRangFragment listFragment = new RewardIssuerZhuanRangFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_reward_issuer_dian_pu;
    }

    @Override
    public void initView() {
        activity = (IssuerRewardFragmentActivity) getActivity();
        editHelper = new EditHelper(activity);
        editHelper.addEditHelperData(new EditHelper.EditHelperData(activity.content, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_CONTENT), activity.getResources().getString(R.string.hintContent)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(city, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_NAME), activity.getResources().getString(R.string.hintCity)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(address, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_ADDRESS), activity.getResources().getString(R.string.hintAddress)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(price, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_PRICE), getActivity().getResources().getString(R.string.hintPrice)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(name, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_NAME), getActivity().getResources().getString(R.string.hintName)));

    }

    @OnClick(R.id.fragment_reward_issuer_zao_pin_selectCity)
    public void onClick() {
        CitySelectActivity.startUI(this, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            cityId = data.getStringExtra("cityId");
            city.setText(data.getStringExtra("cityName"));
        }
    }

    @OnClick(R.id.fragment_reward_issuer_dian_pu_commit)
    public void commit() {
        if (editHelper.check()) {
            presenter.getHttpData(
                    String.valueOf(activity.type),
                    editHelper.getText(activity.content.getId()),
                    editHelper.getText(price.getId()),
                    cityId,
                    editHelper.getText(address.getId()),
                    editHelper.getText(name.getId()),
                    null,
                    null

            );
        }

    }


    @Override
    public RewardIssuerPresenter initPressenter() {
        return new RewardIssuerPresenter();
    }

    @Override
    public void upDataUi(Boolean datas) {

    }

    @Override
    public void clearData() {

    }
    DialogProgress dialogProgress;
    @Override
    public void upLoading(int progress, boolean done, boolean isUpdate, boolean isCompress) {
        if (dialogProgress == null) {
            dialogProgress = new DialogProgress(activity);
        }
        dialogProgress.setTitleText("上传中");
        dialogProgress.setProgress(progress);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }


}
