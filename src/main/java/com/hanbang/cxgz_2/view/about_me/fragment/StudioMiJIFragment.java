package com.hanbang.cxgz_2.view.about_me.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseListFragment;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.pressenter.about_me.StudioFPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.utils.ui.divider.VerticalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.iview.IStudioFView;
import com.hanbang.cxgz_2.view.adapter.MiJiAdapter;
import com.hanbang.cxgz_2.view.jieyou.activity.EsotericDetails_D_Activity;
import com.hanbang.cxgz_2.view.jieyou.activity.EsotericDetails_K_Activity;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/10　12:41
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioMiJIFragment extends BaseListFragment<IStudioFView, StudioFPresenter> implements IStudioFView {
    private ArrayList<EsotericaData> listDatas = new ArrayList<>();
    private MiJiAdapter adapter = null;
    private int id;
    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 14:26
     * <p>
     * 方法功能：是否是当前登录账户的本人
     */
    private boolean isSelf = false;


    public StudioMiJIFragment() {
        this(UserData.getUserData() == null ? 0 : UserData.getUserData().getId());
    }

    public StudioMiJIFragment(int id) {
        isSelf = UserData.getUserData() != null && UserData.getUserData().getId() == id;
        this.id = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public int getContentView() {
        return R.layout.listswipeview;
    }

    @Override
    public void initView() {
        super.initView();

        listSwipeView.getRecyclerView().setAutoloaddingCompleData("共发布了 %d 条秘籍");
        listSwipeView.getRecyclerView().addItemDecoration(
                new VerticalDividerItemDecoration.Builder(activity)
                        .sizeResId(R.dimen.dp_1)
                        .colorResId(R.color.gray_ee)
                        .build());
        presenter.getMiJiHttpData(true, id);
        if (!isSelf) {
            adapter.setOnItemClickListener(new OnItemClickListener<EsotericaData>() {
                @Override
                public void onItemClick(ViewGroup parent, View view, EsotericaData data, int position) {
                    if (data.getType() == 1) {
                        EsotericDetails_K_Activity.startUI(activity, data.getId());
                    } else if (data.getType() == 2) {
                        EsotericDetails_D_Activity.startUI(activity, data.getId());
                    }

                }
            });
        }
    }

    @Override
    public Object getAdapter() {
        return adapter = new MiJiAdapter(activity, listDatas);
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration
                .Builder(activity)
                .sizeResId(R.dimen.dp_1)
                .colorResId(R.color.gray_ee)
                .build();
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(activity, 2);
    }

    @Override
    public StudioFPresenter initPressenter() {
        return new StudioFPresenter();
    }

    @Override
    public void clearData() {
        listDatas.clear();
    }


    @Override
    public void UpDataMiJi(ArrayList<EsotericaData> datas) {
        listDatas.addAll(datas);
        adapter.notifyDataSetChanged();
        if (listDatas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData("暂未发布秘籍"));
        }
    }


    @Override
    public void onRefresh() {
        presenter.getMiJiHttpData(true, id);
    }

    @Override
    public void onLoadding() {
        presenter.getMiJiHttpData(false, id);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getMiJiHttpData(true, id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getMiJiHttpData(true, id);
    }


    @Override
    public void UpDataKeTang(ArrayList<StudyClassroomData> datas) {

    }

    @Override
    public void UpDataXiuChang(ArrayList<XiuChangItemData> datas) {

    }

    @Override
    public void itemChanged(int position) {

    }
}
