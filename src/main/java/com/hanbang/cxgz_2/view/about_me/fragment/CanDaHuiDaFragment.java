package com.hanbang.cxgz_2.view.about_me.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseListFragment;
import com.hanbang.cxgz_2.mode.javabean.about_me.MyAnswerListData;
import com.hanbang.cxgz_2.pressenter.about_me.MeCanDaHuiDaPresenter;
import com.hanbang.cxgz_2.utils.other.AESEncrypt;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.iview.IMeCanDaHuiDaView;
import com.hanbang.cxgz_2.view.widget.TouTingMusicPlayView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.List;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/21 10:16
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的回答
 */

public class CanDaHuiDaFragment extends BaseListFragment<IMeCanDaHuiDaView, MeCanDaHuiDaPresenter> implements IMeCanDaHuiDaView {
    private ArrayList<MyAnswerListData> datas = new ArrayList();
    CommonAdapter adapter;

    public static CanDaHuiDaFragment newInstance() {
        CanDaHuiDaFragment fragment = new CanDaHuiDaFragment();
        return fragment;
    }

    @Override
    public void initView() {
        super.initView();
        listSwipeView.getRecyclerView().setLayoutManager(new LinearLayoutManager(getActivity()));
        presenter.getHttpData(true);
    }

    @Override
    public Object getAdapter() {
        return adapter = new CommonAdapter<MyAnswerListData>(getActivity(), R.layout.item_me_can_da, datas) {
            @Override
            public void convert(ViewHolder helper, MyAnswerListData item) {

                //提问的人
                if (!StringUtils.isBlank(item.getTw_avatarimgSmallurl())) {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitTW, item.getTw_avatarimgSmallurl());
                } else {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitTW, item.getTw_avatar());
                }
                //回答的人
                if (!StringUtils.isBlank(item.getHd_avatarimgSmallurl())) {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitHD,item.getHd_avatarimgSmallurl());
                } else {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitHD, item.getHd_avatar());
                }

                helper.setText(R.id.item_me_can_da_question, item.getTiWenQuestion());
                helper.setText(R.id.item_me_can_da_voiceTime, item.getHuiDaSoundTime());
                helper.setText(R.id.item_me_can_da_status, item.getIsHuiDaCN());
                helper.setText(R.id.item_me_can_da_date, item.getTiWenCjsj());


                View voiceParent = helper.getView(R.id.item_me_can_da_voiceParent);
                if (item.getIsHuiDa()) {
                    voiceParent.setVisibility(View.VISIBLE);
                    //播放
                    TouTingMusicPlayView playView = helper.getView(R.id.TouTingMusicPlayView);
                    String str = null;
                    try {
                        str = AESEncrypt.getInstance().decrypt(item.getHuiDaResultUrl());


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    playView.setUrl(str);
                    playView.setBuy(true);
                    playView.setOnCallListener(new TouTingMusicPlayView.OnCallListener() {
                        @Override
                        public void onBuy(boolean isBuy) {

                        }
                    });

                } else {
                    voiceParent.setVisibility(View.GONE);
                }


            }
        };
    }




    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(getActivity()).sizeResId(R.dimen.dp_5).colorResId(R.color.gray_ee).build();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public MeCanDaHuiDaPresenter initPressenter() {
        return new MeCanDaHuiDaPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.view_listview_swipe;
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void clearData() {
        datas.clear();
    }

    @Override
    public void upDataUi(List<MyAnswerListData> datas) {
        this.datas.addAll(datas);
        adapter.notifyDataSetChanged();

    }
}
