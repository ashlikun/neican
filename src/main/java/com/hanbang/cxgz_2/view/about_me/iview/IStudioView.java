package com.hanbang.cxgz_2.view.about_me.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioBeiJinData;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioData;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/3　9:00
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public interface IStudioView extends BaseView {
    void upDataUI(StudioData data);

    void guanzhuChanged();
    void beijingCHang(StudioBeiJinData data);
}
