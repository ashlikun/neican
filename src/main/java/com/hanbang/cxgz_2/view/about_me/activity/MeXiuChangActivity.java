package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.view.about_me.fragment.StudioXiuChangFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.XuanShangFragment;
import com.hanbang.cxgz_2.view.adapter.SectionsPagerAdapter;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/21　14:29
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：我的秀场和悬赏
 */

public class MeXiuChangActivity extends BaseActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPage)
    ViewPager viewPager;
    private String[] title = new String[]{"我的秀场", "需求悬赏"};
    private ArrayList<Fragment> fragmentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 启动信息详情
     *
     * @param context
     */
    public static void startUi(BaseActivity context) {
        if (context.isLogin(true)) {
            Intent intent = new Intent(context, MeXiuChangActivity.class);
            context.startActivity(intent);
        }
    }

    @Override
    public void initView() {
        toolbar.setBack(this);
        toolbar.setTitle("秀场悬赏");

        fragmentList.add(new StudioXiuChangFragment(UserData.getUserData().getId()));
        fragmentList.add(new XuanShangFragment());
        //为viewPage设置adapter
        SectionsPagerAdapter mAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragmentList, title);
        viewPager.setAdapter(mAdapter);
        //tabLayout关联ViewPage
        tabLayout.setupWithViewPager(viewPager);


    }

    @Override
    public void parseIntent(Intent intent) {

    }

    @Override
    public int getContentView() {
        return R.layout.activity_or_fragment_viewpage;
    }


}
