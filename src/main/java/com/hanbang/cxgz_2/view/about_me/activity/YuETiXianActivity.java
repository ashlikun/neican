package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.EditText;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.javabean.about_me.MeBalanceData;
import com.hanbang.cxgz_2.pressenter.about_me.YuEPresenter;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.iview.IYuEView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;

import java.util.Collection;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 14:38
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的余额-提现
 */

public class YuETiXianActivity extends BaseMvpActivity<IYuEView, YuEPresenter> implements IYuEView {
    @BindView(R.id.price)
    EditText price;
    @BindView(R.id.account)
    EditText account;
    @BindView(R.id.TXSM)
    TextView TvTXSM;
    @BindView(R.id.YJSM)
    TextView TvYJSM;
    private String TXSM;
    private String YJSM;

    /**
     * @param context
     * @param TXSM    提现说明
     * @param YJSM    佣金说明
     */
    public static void startUi(BaseActivity context, String TXSM, String YJSM) {
        if (context.isLogin(true)) {
            Intent intent = new Intent(context, YuETiXianActivity.class);
            intent.putExtra("TXSM", TXSM);
            intent.putExtra("YJSM", YJSM);
            context.startActivity(intent);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }

    @Override
    public YuEPresenter initPressenter() {
        return new YuEPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        TXSM = intent.getStringExtra("TXSM");
        YJSM = intent.getStringExtra("YJSM");

    }

    @Override
    public int getContentView() {
        return R.layout.activity_ti_xian;
    }

    @Override
    public void initView() {
        toolbar.setTitle("余额提现");
        toolbar.setBack(this);
        presenter.getHttpData(true);

        TvTXSM.setText(TXSM);
        TvYJSM.setText(YJSM);

    }


    @Override
    public void upDataUi(MeBalanceData d) {


    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return null;
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return null;
    }

    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {
        return null;
    }


    @Override
    public void clearData() {

    }


    @Override
    public void clearPagingData() {

    }

    @Override
    public int getPageindex() {
        return 0;
    }

    @Override
    public int getPageCount() {
        return 0;
    }


    @Override
    public void onRefresh() {

    }


    @OnClick(R.id.commit)
    public void onClick() {
        SnackbarUtil.showShort(this, "功能添加中", SnackbarUtil.Info).show();

    }
}
