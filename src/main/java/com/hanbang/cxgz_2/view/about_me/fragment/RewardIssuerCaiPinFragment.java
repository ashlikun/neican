package com.hanbang.cxgz_2.view.about_me.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.IUpdataUIView;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseMvpFragment;
import com.hanbang.cxgz_2.pressenter.about_me.RewardIssuerPresenter;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerRewardFragmentActivity;
import com.hanbang.cxgz_2.view.widget.GridViewForScrollView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogProgress;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

import static android.app.Activity.RESULT_OK;


/**
 * Created by yang on 2016/8/24.
 */

public class RewardIssuerCaiPinFragment extends BaseMvpFragment<IUpdataUIView<Boolean>, RewardIssuerPresenter> implements IUpdataUIView<Boolean>  {
    @BindView(R.id.rewardIssuerPicture)
    GridViewForScrollView selectPicture;
    CommonAdapter<String> adapter;
    @BindView(R.id.price)
    EditText price;
    private EditHelper editHelper;
    private ArrayList<String> picturePath = new ArrayList<>();
    private IssuerRewardFragmentActivity activityManager;

    public static RewardIssuerCaiPinFragment newInstance() {
        RewardIssuerCaiPinFragment listFragment = new RewardIssuerCaiPinFragment();
        return listFragment;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter();
    }

    @Override
    public int getContentView() {
        return R.layout.fragment_reward_issuer_cai_pin;
    }

    @Override
    public void initView() {
        activityManager = (IssuerRewardFragmentActivity) getActivity();
        editHelper = new EditHelper(activityManager);
        editHelper.addEditHelperData(new EditHelper.EditHelperData(price, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_PRICE), getActivity().getResources().getString(R.string.hintPrice)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(activityManager.content, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_CONTENT), getActivity().getResources().getString(R.string.hintPrice)));

    }

    private void setAdapter() {
        selectPicture.setAdapter(adapter = new CommonAdapter<String>(getActivity(), R.layout.item_photo_select, picturePath) {
            @Override
            public void convert(final ViewHolder holder, String s) {

                holder.setImageBitmap(R.id.item_photo_selectImage, s);
                holder.setOnClickListener(R.id.item_photo_selectDelete, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        picturePath.remove(holder.getmPosition());
                        notifyDataSetChanged();
                    }
                });

                holder.setOnClickListener(R.id.item_photo_selectImage, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectPicture();
                    }
                });

            }
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE&&resultCode==RESULT_OK) {
            picturePath.clear();
            picturePath.addAll(data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT));
            adapter.notifyDataSetChanged();
        }

    }

    /**
     *
     */
    @OnClick(R.id.selectPicture)
    public void onClick() {
        selectPicture();
    }

    @OnClick(R.id.commit)
    public void commit() {
        if (editHelper.check()) {
            presenter.getHttpData(
                    String.valueOf(activityManager.type),
                    activityManager.content.getText().toString(),
                    price.getText().toString(),
                    "",
                    "",
                    "",
                    "",
                    picturePath);
        }
    }

    private void selectPicture() {
        MultiImageSelector.create()
                .showCamera(true) // show camera or not. true by default
                .count(9) // max select image size, 9 by default. used width #.multi()
                .multi() // multi mode, default mode;
                .origin(picturePath) // original select data set, used width #.multi()
                .start(this, REQUEST_CODE);
    }





    @Override
    public RewardIssuerPresenter initPressenter() {
        return new RewardIssuerPresenter();
    }




    @Override
    public void upDataUi(Boolean datas) {

    }

    @Override
    public void clearData() {

    }

    DialogProgress dialogProgress;
    @Override
    public void upLoading(int progress, boolean done, boolean isUpdate, boolean isCompress) {
        if (dialogProgress == null) {
            dialogProgress = new DialogProgress(activity);
        }
        dialogProgress.setTitleText("上传中");
        dialogProgress.setProgress(progress);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

}
