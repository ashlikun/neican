package com.hanbang.cxgz_2.view.about_me.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.appliction.iview.common_view.IProgressView;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/8 16:59
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：课堂上传页面
 */

public interface IssuerClassroomView extends BaseView, IProgressView {

    void upLoading(boolean whetherSucceed);


}
