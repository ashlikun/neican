package com.hanbang.cxgz_2.view.about_me.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.view.IUpdataUIView;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseMvpFragment;
import com.hanbang.cxgz_2.pressenter.about_me.RewardIssuerPresenter;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerRewardFragmentActivity;
import com.hanbang.cxgz_2.view.other.CitySelectActivity;
import com.hanbang.cxgz_2.view.widget.dialog.DialogProgress;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 求职的上传
 * Created by yang on 2016/8/24.
 */

public class RewardIssuerQiuZhiFragment extends BaseMvpFragment<IUpdataUIView<Boolean>, RewardIssuerPresenter> implements IUpdataUIView {
    @BindView(R.id.fragment_reward_issuer_qiu_zhi_CitySelect)
    TextView citySelect;
    @BindView(R.id.fragment_reward_issuer_qiu_zhi_name)
    EditText name;
    @BindView(R.id.fragment_reward_issuer_qiu_zhi_price)
    EditText price;

    private IssuerRewardFragmentActivity activity;
    private EditHelper editHelper;
    private String cityId = "-1";

    public static RewardIssuerQiuZhiFragment newInstance() {
        RewardIssuerQiuZhiFragment listFragment = new RewardIssuerQiuZhiFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_reward_issuer_qiu_zhi;
    }

    @Override
    public void initView() {
        activity = (IssuerRewardFragmentActivity) getActivity();

        editHelper = new EditHelper(activity);
        editHelper.addEditHelperData(new EditHelper.EditHelperData(activity.content, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_CONTENT), activity.getResources().getString(R.string.hintContent)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(citySelect, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_NAME), activity.getResources().getString(R.string.hintCity)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(name, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_NAME), activity.getResources().getString(R.string.hintName)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(price, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_PRICE), activity.getResources().getString(R.string.hintPrice)));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE&&resultCode==activity.RESULT_OK) {
            cityId = data.getStringExtra("cityId");
            citySelect.setText(data.getStringExtra("cityName"));
        }
    }

    @OnClick({R.id.fragment_reward_issuer_qiu_zhi_CitySelect, R.id.fragment_reward_issuer_qiu_zhi_commit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_reward_issuer_qiu_zhi_CitySelect:
                CitySelectActivity.startUI(this, REQUEST_CODE);
                break;
            case R.id.fragment_reward_issuer_qiu_zhi_commit:
                if (editHelper.check()) {
                    presenter.getHttpData(
                            String.valueOf(activity.type),
                            editHelper.getText(activity.content.getId()),
                            editHelper.getText(price.getId()),
                            cityId,
                            null,
                            null,
                            editHelper.getText(name.getId()),
                            null

                    );
                }
                break;
        }
    }

    @Override
    public void upDataUi(Object datas) {

    }

    @Override
    public void clearData() {

    }

    @Override
    public RewardIssuerPresenter initPressenter() {
        return new RewardIssuerPresenter();
    }

    DialogProgress dialogProgress;
    @Override
    public void upLoading(int progress, boolean done, boolean isUpdate, boolean isCompress) {
        if (dialogProgress == null) {
            dialogProgress = new DialogProgress(activity);
        }
        dialogProgress.setTitleText("上传中");
        dialogProgress.setProgress(progress);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

}
