package com.hanbang.cxgz_2.view.about_me.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.IUpdataUIView;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseMvpFragment;
import com.hanbang.cxgz_2.pressenter.about_me.RewardIssuerPresenter;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerRewardFragmentActivity;
import com.hanbang.cxgz_2.view.other.CitySelectActivity;
import com.hanbang.cxgz_2.view.widget.GridViewForScrollView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogProgress;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

import static android.app.Activity.RESULT_OK;


/**
 * Created by yang on 2016/8/24.
 */

public class RewardIssuerSheBeiFragment extends BaseMvpFragment<IUpdataUIView<Boolean>, RewardIssuerPresenter> implements IUpdataUIView {
    @BindView(R.id.fragment_reward_issuer_she_bei_gridView)
    GridViewForScrollView gridView;
    @BindView(R.id.fragment_reward_issuer_she_bei_city)
    TextView city;
    @BindView(R.id.fragment_reward_issuer_she_bei_name)
    EditText name;
    @BindView(R.id.fragment_reward_issuer_she_bei_address)
    EditText address;
    @BindView(R.id.fragment_reward_issuer_she_bei_price)
    EditText price;
    CommonAdapter<String> adapter;
    private ArrayList<String> picturePath = new ArrayList<>();

    private EditHelper editHelper;
    private IssuerRewardFragmentActivity activity;
    private String cityId = "-1";

    public static RewardIssuerSheBeiFragment newInstance() {
        RewardIssuerSheBeiFragment listFragment = new RewardIssuerSheBeiFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_reward_issuer_she_bei;
    }

    @Override
    public void initView() {
        activity = (IssuerRewardFragmentActivity) getActivity();
        editHelper = new EditHelper(activity);
        editHelper.addEditHelperData(new EditHelper.EditHelperData(activity.content, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_CONTENT), activity.getResources().getString(R.string.hintContent)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(city, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_NAME), activity.getResources().getString(R.string.hintCity)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(address, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_ADDRESS), activity.getResources().getString(R.string.hintAddress)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(price, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_PRICE), getActivity().getResources().getString(R.string.hintPrice)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(name, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_NAME), getActivity().getResources().getString(R.string.hintName)));

        setAdapter();

    }


    @Override
    public void upDataUi(Object datas) {

    }

    @Override
    public void clearData() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE&&resultCode==RESULT_OK) {
            picturePath.clear();
            picturePath.addAll(data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT));
            adapter.notifyDataSetChanged();
        }

        if (requestCode == REQUEST_CODE + 1 && resultCode == RESULT_OK) {
            cityId = data.getStringExtra("cityId");
            city.setText(data.getStringExtra("cityName"));
        }

    }

    private void setAdapter() {
        gridView.setAdapter(adapter = new CommonAdapter<String>(getActivity(), R.layout.item_photo_select, picturePath) {
            @Override
            public void convert(final ViewHolder holder, String s) {

                holder.setImageBitmap(R.id.item_photo_selectImage, s);
                holder.setOnClickListener(R.id.item_photo_selectDelete, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        picturePath.remove(holder.getmPosition());
                        notifyDataSetChanged();
                    }
                });

                holder.setOnClickListener(R.id.item_photo_selectImage, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectPicture();
                    }
                });

            }
        });


    }

    @Override
    public RewardIssuerPresenter initPressenter() {
        return new RewardIssuerPresenter();
    }


    @OnClick({R.id.fragment_reward_issuer_she_bei_selectPicture, R.id.fragment_reward_issuer_she_bei_city, R.id.fragment_reward_issuer_she_bei_commit})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.fragment_reward_issuer_she_bei_selectPicture:
                selectPicture();
                break;
            case R.id.fragment_reward_issuer_she_bei_city:
                CitySelectActivity.startUI(this, REQUEST_CODE + 1);
                break;
            case R.id.fragment_reward_issuer_she_bei_commit:

                if (editHelper.check()) {
                    presenter.getHttpData(
                            String.valueOf(activity.type),
                            editHelper.getText(activity.content.getId()),
                            editHelper.getText(price.getId()),
                            cityId,
                            editHelper.getText(address.getId()),
                            editHelper.getText(name.getId()),
                            null,
                            picturePath

                    );
                }
                break;
        }
    }


    private void selectPicture() {
        MultiImageSelector.create()
                .showCamera(true) // show camera or not. true by default
                .count(9) // max select image size, 9 by default. used width #.multi()
                .multi() // multi mode, default mode;
                .origin(picturePath) // original select data set, used width #.multi()
                .start(this, REQUEST_CODE);
    }

    DialogProgress dialogProgress;
    @Override
    public void upLoading(int progress, boolean done, boolean isUpdate, boolean isCompress) {
        if (dialogProgress == null) {
            dialogProgress = new DialogProgress(activity);
        }
        dialogProgress.setTitleText("上传中");
        dialogProgress.setProgress(progress);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }


}
