package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.pressenter.about_me.IssuerXiuChangPresenter;
import com.hanbang.cxgz_2.utils.ui.AndroidBug5497Workaround;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.about_me.iview.IssuerClassroomView;
import com.hanbang.cxgz_2.view.other.PhotoActivity;
import com.hanbang.cxgz_2.view.widget.GridViewForScrollView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogProgress;
import com.hanbang.cxgz_2.view.widget.dialog.DialogSelectEmoji;
import com.hanbang.cxgz_2.view.widget.emoj.EmojiconEditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/8 17:20
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：发布秀场
 */

public class IssuerXiuChangActivity extends BaseMvpActivity<IssuerClassroomView, IssuerXiuChangPresenter> implements IssuerClassroomView {
    @BindView(R.id.find_uploading_k_activity_root)
    RelativeLayout viewRoot;

    //显示过程图片
    @BindView(R.id.showProcessPicture)
    GridViewForScrollView showProcessPicture;
    @BindView(R.id.issuerContent)
    TextInputLayout issuerContent;
    private ArrayList<String> processPicturePhat = new ArrayList<>();
    @BindView(R.id.emojiconEditText)
    EmojiconEditText emojiconEditText;


    @Override
    public int getContentView() {
        return R.layout.activity_uploading_xiu_chang;
    }


    @Override
    public void clearData() {


    }

    @Override
    public IssuerXiuChangPresenter initPressenter() {
        return new IssuerXiuChangPresenter();
    }

    @Override
    public void initView() {
        AndroidBug5497Workaround androidBug5497Workaround = AndroidBug5497Workaround.assistActivity(this);
        viewRoot.setPadding(0, UiUtils.getStatusHeight(this) - 2, 0, 0);
        toolbar.setBack(this);
        toolbar.setTitle("发布秀场");
        toolbar.addAction(1, "发布");

        processPicturePhat.add("");
        showProcessPicture.setAdapter(adapter);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (TextUtils.isEmpty(emojiconEditText.getText().toString())) {

                    SnackbarUtil.showShort(IssuerXiuChangActivity.this, "内容不可为空", SnackbarUtil.Info).show();

                } else {

                    presenter.issuer("1", "苏州", emojiconEditText.getText().toString(), processPicturePhat);
                }
                return false;

            }
        });

    }


    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    public int getStatusBarResource() {
        return R.color.yellow_top;
    }

    @Override
    public void parseIntent(Intent intent) {

    }

    CommonAdapter adapter = new CommonAdapter<String>(this, R.layout.item_photo_select, processPicturePhat) {
        @Override
        public void convert(final ViewHolder holder, String item) {

            if (holder.getmPosition() == processPicturePhat.size() - 1) {
                holder.setVisible(R.id.item_photo_selectDelete, false);
                //因为有缓存所以增加延时加载
                holder.getView(R.id.item_photo_selectDelete).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.setImageResource(R.id.item_photo_selectImage, R.mipmap.my_release_ico_addpic);
                    }
                }, 100);


            } else if (!TextUtils.isEmpty(item)) {
                holder.setImageBitmap(R.id.item_photo_selectImage, item);
                holder.setVisible(R.id.item_photo_selectDelete, true);
            }

            holder.setOnClickListener(R.id.item_photo_selectImage, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (processPicturePhat.size() - 1 == holder.getmPosition()) {

                        processPicturePhat.remove(processPicturePhat.size() - 1);
                        MultiImageSelector.create()
                                .showCamera(true)
                                .count(9)
                                .multi()
                                .origin(processPicturePhat)
                                .start(IssuerXiuChangActivity.this, REQUEST_CODE + 1);
                    } else {

                        if (processPicturePhat.size() > 9) {
                            processPicturePhat.remove(processPicturePhat.size() - 1);
                        }
                        PhotoActivity.startUI(IssuerXiuChangActivity.this, processPicturePhat, holder.getmPosition());
                    }
                }
            });

            holder.setOnClickListener(R.id.item_photo_selectDelete, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    processPicturePhat.remove(holder.getmPosition());
                    notifyDataSetChanged();
                }
            });
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //过程图片路径
        if (requestCode == REQUEST_CODE + 1 && resultCode == RESULT_OK) {
            processPicturePhat.clear();
            processPicturePhat.addAll(data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT));
            processPicturePhat.add("");
            adapter.notifyDataSetChanged();
        }


    }


    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }


    @Override
    public void upLoading(boolean whetherSucceed) {

    }


    DialogProgress dialogProgress;

    @Override
    public void upLoading(int progress, boolean done, boolean isUpdate, boolean isCompress) {
        if (dialogProgress == null) {
            dialogProgress = new DialogProgress(this);
        }
        dialogProgress.setTitleText("上传中");
        dialogProgress.setProgress(progress);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }


    @OnClick(R.id.ll_face)
    public void onClick() {


        DialogSelectEmoji dialog = new DialogSelectEmoji(this, R.style.Dialog_emoji);
        dialog.setListener(emojiconEditText);
        dialog.show();
        dialog.setClickCallback(new DialogSelectEmoji.OnClickCallback() {


            @Override
            public void onClick(String name) {

                if (name.equals("-1")) {
                    emojiconEditText.backspace();
                } else {
                    emojiconEditText.insertEmoji(name);

                }
            }
        });


    }


}
