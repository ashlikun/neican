package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioBeiJinData;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioData;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.pressenter.about_me.StudioPresenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.ui.DrawableUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.about_me.fragment.StudioKeTangFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.StudioMiJIFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.StudioXiuChangFragment;
import com.hanbang.cxgz_2.view.about_me.iview.IStudioView;
import com.hanbang.cxgz_2.view.adapter.SectionsPagerAdapter;
import com.hanbang.cxgz_2.view.can_da.activity.MealAnswerDetailsActivity;
import com.hanbang.cxgz_2.view.widget.appbarlayout.AppBarStateChangeListener;
import com.hanbang.cxgz_2.view.widget.studio_tablayout.StudioTabItem;
import com.hanbang.cxgz_2.view.widget.studio_tablayout.StudioTabLayout;
import com.hanbang.cxgz_2.view.widget.studio_tablayout.StudioTopLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/3　8:58
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioActivity extends BaseMvpActivity<IStudioView, StudioPresenter> implements IStudioView
        , StudioTopLayout.OnClickListener {

    private StudioData data;
    public static int CHUSHI = 1;//厨师个人工作室
    public static int ZHANGGUI = 2;//掌柜个人工作室
    @BindView(R.id.beijing)
    ImageView beijing;
    @BindView(R.id.studioTopLayout)
    StudioTopLayout studioTopLayout;
    private int mode;
    private int id;
    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 14:26
     * <p>
     * 方法功能：是否是当前登录账户的本人
     */
    private boolean isSelf = false;
    private List<Fragment> fragment = new ArrayList<>();

    @Override
    public int getContentView() {
        return R.layout.activity_studio;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static void startUI(BaseActivity activity, int id, int mode) {

        Intent intent = new Intent(activity, StudioActivity.class);
        intent.putExtra("mode", mode);
        intent.putExtra("id", id);
        activity.startActivity(intent);
    }

    @Override
    public void parseIntent(Intent intent) {
        mode = intent.getIntExtra("mode", 1);
        id = intent.getIntExtra("id", id);
        isSelf = UserData.getUserData() != null && UserData.getUserData().getId() == id;
    }

    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }


    @Override
    public int getStatusBarResource() {
        return R.color.transparency;
    }

    @Override
    public void initView() {
        toolbar.setBack(this);
        toolbar.addAction(0, "分享", R.drawable.material_ic_share);
        toolbar.setTitle("工作室");
        ((CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams()).topMargin = UiUtils.getStatusHeight(this);
        if (mode == CHUSHI) {
            studioTabLayout.addSubMenus(
                    new StudioTabItem("个人简介", 4, DrawableUtils.getStateSelectDrawable(R.mipmap.studio_icon_introduce_on, R.mipmap.studio_icon_introduce)).setClickUp(true)
                    , new StudioTabItem("秀场", 1, DrawableUtils.getStateSelectDrawable(R.mipmap.studio_icon_show_on, R.mipmap.studio_icon_show))
                    , new StudioTabItem("餐答", 3, R.mipmap.my_studio_answer, true).setClickUp(true)
                    , new StudioTabItem("课堂", 2, DrawableUtils.getStateSelectDrawable(R.mipmap.studio_icon_class_on, R.mipmap.studio_icon_class))
                    , new StudioTabItem("菜品秘籍", 5, DrawableUtils.getStateSelectDrawable(R.mipmap.studio_icon_cheats_on, R.mipmap.studio_icon_cheats)));
            fragment.add(new StudioXiuChangFragment(id));
            fragment.add(new StudioKeTangFragment(id));
            fragment.add(new StudioMiJIFragment(id));

        } else if (mode == ZHANGGUI) {
            studioTabLayout.addSubMenus(new StudioTabItem("餐答", 3, R.mipmap.my_studio_answer, true).setClickUp(true),
                    new StudioTabItem("个人简介", 4, DrawableUtils.getStateSelectDrawable(R.mipmap.studio_icon_introduce_on, R.mipmap.studio_icon_introduce)).setClickUp(true),
                    new StudioTabItem("秀场", 1, DrawableUtils.getStateSelectDrawable(R.mipmap.studio_icon_show_on, R.mipmap.studio_icon_show)),
                    new StudioTabItem("课堂", 2, DrawableUtils.getStateSelectDrawable(R.mipmap.studio_icon_class_on, R.mipmap.studio_icon_class)));
            fragment.add(new StudioXiuChangFragment(id));
            fragment.add(new StudioKeTangFragment(id));
        }
        studioTabLayout.setOnItemClickListener(new StudioTabLayout.OnItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, StudioTabItem data) {
                if (data.isClickUp()) {
                    //跳转
                    if (data.getGroup() == 3) {
                        if (isSelf) {
                            MeCanDaActivity.startUI(StudioActivity.this);
                        } else {
                            MealAnswerDetailsActivity.startUI(StudioActivity.this, id);
                        }
                    }

                    return false;
                } else {
                    viewpager.setCurrentItem(position - 1);
                    return true;
                }

            }
        });
        appbarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state, int position, int total) {
                toolbar.setTitleAlpha(position * 1.0f / total);
            }
        });

        viewpager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager(), fragment));

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                studioTabLayout.selectItem(position + 1);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        studioTabLayout.selectItem(1);

        studioTopLayout.setOnClickListener(this);

        presenter.getHttpData(id);
    }

    @Override
    public void onClick(View v, int item) {
        if (item == studioTopLayout.FENSHI) {

        } else if (item == studioTopLayout.GUANZHU) {
            presenter.guanzhu(data);
        } else if (item == studioTopLayout.LIAOTIAN) {

        } else if (item == studioTopLayout.SQBS) {

        }
    }

    @Override
    public void guanzhuChanged() {
        studioTopLayout.guanzhuChanged(data);
    }


    @Override
    public StudioPresenter initPressenter() {
        return new StudioPresenter();
    }


    @Override
    public void clearData() {

    }


    @Override
    public void upDataUI(StudioData data) {

        this.data = data;
        if (data != null) {
            toolbar.setTitle(data.getUser_name());
            GlideUtils.show(beijing, data.getHome_img());
            studioTopLayout.upDataUI(data);
        }
    }

    @Override
    public void beijingCHang(StudioBeiJinData data) {

        if (data != null) {

            GlideUtils.show(beijing, data.getImg_urlMiddle());
        }

    }

    @OnClick(R.id.beijing)
    protected void beijingClick() {
        if (isSelf) {
            StudioBeiJingActivity.startUI(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

        }

    }

    @BindView(R.id.studioTabLayout)
    StudioTabLayout studioTabLayout;

    @BindView(R.id.appbarLayout)
    AppBarLayout appbarLayout;

    @BindView(R.id.viewpager)
    ViewPager viewpager;


}
