package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.mode.enumeration.MeSetEnum;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.hanbang.cxgz_2.R.id.switchRoot;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 14:38
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的余额-提现
 */

public class MeSetActivity extends BaseActivity {
    @BindView(switchRoot)
    RecyclerView recyclerView;
    ArrayList<MeSetEnum> datas = new ArrayList<>();


    /**
     * @param context
     */
    public static void startUi(BaseActivity context) {
        if (context.isLogin(true)) {
            Intent intent = new Intent(context, MeSetActivity.class);
            context.startActivity(intent);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }


    @Override
    public int getContentView() {
        return R.layout.activity_me_set;
    }

    @Override
    public void initView() {
        toolbar.setTitle("个人设置");
        toolbar.setBack(this);

        datas.add(MeSetEnum.GE_REN);
        datas.add(MeSetEnum.ZFB);
        datas.add(MeSetEnum.CAN_DA);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this).sizeResId(R.dimen.dp_5).colorResId(R.color.gray_ee).build());
        recyclerView.setAdapter(adapter);


        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View view, Object data, int position) {
                if (datas.get(position) == MeSetEnum.GE_REN) {
                    MeSetDataActivity.startUi(MeSetActivity.this);

                } else if (datas.get(position) == MeSetEnum.ZFB) {
                    MeSetZFBActivity.startUi(MeSetActivity.this);

                } else if (datas.get(position) == MeSetEnum.CAN_DA) {
                    MeSetCdActivity.startUi(MeSetActivity.this);
                }
            }
        });

    }


    CommonAdapter adapter = new CommonAdapter<MeSetEnum>(this, R.layout.item_me_set, datas) {
        @Override
        public void convert(ViewHolder holder, MeSetEnum o) {
            holder.setText(R.id.item_me_set_name, o.getTitle());
            holder.setImageResource(R.id.item_me_set_picture, o.getPictureId());


        }


    };


    @Override
    public void parseIntent(Intent intent) {

    }

    @OnClick(R.id.exit)
    public void onClick() {
        UserData.exit(MeSetActivity.this);
    }


}
