package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.presenter.ListPresenter;
import com.hanbang.cxgz_2.appliction.base.view.IListView;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseListActivity;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioBeiJinData;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.utils.ui.divider.VerticalDividerItemDecoration;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/22　10:56
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioBeiJingActivity extends BaseListActivity<IListView<StudioBeiJinData>, ListPresenter<StudioBeiJinData>> implements IListView<StudioBeiJinData> {

    public static String RESULT_DATA = "RESULT_DATA";

    private List<StudioBeiJinData> listDatas = new ArrayList<>();

    private CommonAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.getHttpData(true);
    }

    public static void startUI(BaseActivity activity) {
        Intent intent = new Intent(activity, StudioBeiJingActivity.class);
        activity.startActivityForResult(intent, activity.REQUEST_CODE);
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setBack(this);
        toolbar.setTitle("选择图片");
        listSwipeView.getRecyclerView().addItemDecoration(new VerticalDividerItemDecoration.Builder(this)
                .sizeResId(R.dimen.dp_3)
                .colorResId(R.color.main_black).build());
        listSwipeView.getRecyclerView().setLoadMoreEnabled(false);

        adapter.setOnItemClickListener(new OnItemClickListener<StudioBeiJinData>() {
            @Override
            public void onItemClick(ViewGroup parent, View view, StudioBeiJinData data, int position) {
                Intent intent = new Intent();
                intent.putExtra(RESULT_DATA, data);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    /**
     * 实例化Presenter对象
     *
     * @return
     */
    @Override
    public ListPresenter initPressenter() {
        return new ListPresenter();
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     * <p>
     * 方法功能：获取布局id
     */
    @Override
    public int getContentView() {
        return R.layout.activity_or_fragment_list;
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:16
     * <p>
     * 方法功能：解析意图
     *
     * @param intent
     */
    @Override
    public void parseIntent(Intent intent) {

    }


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:01
     * <p>
     * 方法功能：更新界面
     *
     * @param datas
     */
    @Override
    public void upDataUi(List<StudioBeiJinData> datas) {
        if (datas != null && datas.size() > 0) {

            listDatas.addAll(datas);
            if (listDatas.size() == 0) {
                loadingAndRetryManager.showEmpty(new ContextData("暂无背景图可选择"));
            }
            adapter.notifyDataSetChanged();
        }


    }

    public <T> Collection<T> getValidData(Collection<T> c) {
        return c;
    }

    @Override
    public ListPresenter.ApiType getApiType() {
        return ListPresenter.ApiType.GetHomeImg;
    }

    /**
     * Called when a swipe gesture triggers a refresh.
     */
    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    /**
     * 清空数据  一般在列表使用  但是也可以作为其他的界面使用
     */
    @Override
    public void clearData() {
        listDatas.clear();
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(this).sizeResId(R.dimen.dp_3)
                .colorResId(R.color.main_black).build();
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, 2);
    }

    @Override
    public Object getAdapter() {
        return adapter = new CommonAdapter<StudioBeiJinData>(this, R.layout.item_imageview, listDatas) {
            @Override
            public void convert(ViewHolder holder, StudioBeiJinData o) {
                holder.setImageBitmap(R.id.imageView, o.getImg_urlMiddle());
            }

        };
    }


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     * <p>
     * 方法功能：分页加载回调
     */
    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     * <p>
     * 方法功能：数据加载失败点击重新加载
     *
     * @param data
     */
    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     * <p>
     * 方法功能：数据为空点击重新加载
     *
     * @param data
     */
    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }
}
