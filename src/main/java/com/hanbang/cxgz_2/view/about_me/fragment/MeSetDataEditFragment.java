package com.hanbang.cxgz_2.view.about_me.fragment;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseMvpFragment;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;
import com.hanbang.cxgz_2.pressenter.about_me.MeSetDataPresenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.about_me.iview.IMeSetDAtaView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogSelectCaiXi;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

import static android.app.Activity.RESULT_OK;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 11:43
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：设置资料
 */

public class MeSetDataEditFragment extends BaseMvpFragment<IMeSetDAtaView, MeSetDataPresenter> implements IMeSetDAtaView {

    @BindView(R.id.portrait)
    ImageView portrait;
    @BindView(R.id.name)
    TextInputLayout name;
    @BindView(R.id.job)
    EditText job;
    @BindView(R.id.commpany)
    TextInputLayout commpany;

    @BindView(R.id.janJie)
    TextInputLayout janJie;

    @BindView(R.id.cia_xi_type)
    EditText cia_xi;

    //头像路径
    private String touXiangPath;


    public static MeSetDataEditFragment newIntences() {

        return new MeSetDataEditFragment();
    }


    @Override
    public int getContentView() {
        return R.layout.activity_me_set_data_edit;
    }

    @Override
    public void initView() {


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //头像路径
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            touXiangPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT).get(0);
            GlideUtils.show(portrait, touXiangPath);
        }


    }

    @Override
    public MeSetDataPresenter initPressenter() {
        return new MeSetDataPresenter();
    }


    @OnClick({R.id.cia_xi_type, R.id.commit, R.id.portrait, R.id.job})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cia_xi_type:
                presenter.getCaiXiData(false);

                break;
            case R.id.commit:
                SnackbarUtil.showShort(activity, "功能添加中", SnackbarUtil.Info).show();
                break;
            case R.id.portrait:
                MultiImageSelector.create()
                        .showCamera(true)
                        .single()
                        .start(this, REQUEST_CODE);

                break;

            case R.id.job:


                break;
        }
    }


    @Override
    public void upLoading(boolean whetherSucceed) {

    }

    @Override
    public void clearData() {

    }

    @Override
    public void getCaiXiData(ArrayList<CaiXiData> datas) {
        DialogSelectCaiXi dialog = new DialogSelectCaiXi(activity, datas);
        dialog.setClickCallback(new DialogSelectCaiXi.OnClickCallback() {
            @Override
            public void onClick(String names, String ids) {
                cia_xi.setText(names);
            }
        });
        dialog.show();
    }

    @Override
    public void getUserData(UserData data) {

    }
}
