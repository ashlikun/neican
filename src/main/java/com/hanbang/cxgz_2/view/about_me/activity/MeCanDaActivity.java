package com.hanbang.cxgz_2.view.about_me.activity;


import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.pressenter.about_me.MeCanDaPresenter;
import com.hanbang.cxgz_2.view.about_me.fragment.CanDaHuiDaFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.CanDaTouTingFragment;
import com.hanbang.cxgz_2.view.about_me.iview.IMeCanDaView;
import com.hanbang.cxgz_2.view.jieyou.adapter.ScrollableViewPageAdapter;
import com.hanbang.cxgz_2.view.widget.TouTingMusicPlayView;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/21 9:01
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：我的餐答
 */
public class MeCanDaActivity extends BaseMvpActivity<IMeCanDaView, MeCanDaPresenter> implements IMeCanDaView {
    @BindView(R.id.viewPage)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    private int index;

    private String[] title = new String[]{"提问和偷听", "我的问答"};
    private ArrayList<Fragment> fragmentList = new ArrayList<>();

    @Override
    public int getContentView() {
        return R.layout.activity_or_fragment_viewpage;
    }

    public static void startUI(Context context, int index) {
        Intent intent = new Intent(context, MeCanDaActivity.class);
        intent.putExtra("index", index);
        context.startActivity(intent);
    }

    public static void startUI(Context context) {
        startUI(context, 0);
    }


    @Override
    public void initView() {
        toolbar.setTitle("我的餐答");
        toolbar.setBack(this);

        fragmentList.add(CanDaTouTingFragment.newInstance());
        fragmentList.add(CanDaHuiDaFragment.newInstance());

        ScrollableViewPageAdapter.initFragmentPager(getSupportFragmentManager(), viewPager, fragmentList, tabLayout, title, 0);
        viewPager.setCurrentItem(index);
    }

    @Override
    public MeCanDaPresenter initPressenter() {
        return new MeCanDaPresenter();
    }


    @Override
    public void parseIntent(Intent intent) {

    }


    @Override
    public void clearData() {

    }


    @Override
    protected void onPause() {
        super.onPause();
        TouTingMusicPlayView.release();
    }


}
