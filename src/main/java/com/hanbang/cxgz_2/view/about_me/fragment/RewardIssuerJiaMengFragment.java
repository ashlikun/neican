package com.hanbang.cxgz_2.view.about_me.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.IUpdataUIView;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseMvpFragment;
import com.hanbang.cxgz_2.pressenter.about_me.RewardIssuerPresenter;
import com.hanbang.cxgz_2.utils.other.EditHelper;
import com.hanbang.cxgz_2.utils.other.Validators;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerRewardFragmentActivity;
import com.hanbang.cxgz_2.view.widget.GridViewForScrollView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogProgress;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

;
;import static android.app.Activity.RESULT_OK;

/**
 * Created by yang on 2016/8/24.
 */

public class RewardIssuerJiaMengFragment extends BaseMvpFragment<IUpdataUIView<Boolean>, RewardIssuerPresenter> implements IUpdataUIView {
    @BindView(R.id.fragment_reward_issuer_jia_meng_gridView)
    GridViewForScrollView gridView;
    @BindView(R.id.fragment_reward_issuer_jia_meng_companyName)
    EditText companyName;
    @BindView(R.id.fragment_reward_issuer_jia_meng_price)
    EditText price;

    CommonAdapter<String> adapter;
    private ArrayList<String> picturePath = new ArrayList<>();
    private EditHelper editHelper;
    private IssuerRewardFragmentActivity activity;


    public static RewardIssuerJiaMengFragment newInstance() {
        RewardIssuerJiaMengFragment listFragment = new RewardIssuerJiaMengFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_reward_issuer_jia_meng;
    }

    @Override
    public void initView() {
        activity = (IssuerRewardFragmentActivity) getActivity();
        editHelper = new EditHelper(activity);
        editHelper.addEditHelperData(new EditHelper.EditHelperData(activity.content, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_CONTENT), activity.getResources().getString(R.string.hintContent)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(companyName, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_NAME), activity.getResources().getString(R.string.hintAddress)));
        editHelper.addEditHelperData(new EditHelper.EditHelperData(price, Validators.getLengthSRegex(1, Global.TEXT_LENGTH_PRICE), getActivity().getResources().getString(R.string.hintPrice)));
        setAdapter();
    }


    @Override
    public void upDataUi(Object datas) {

    }

    @Override
    public void clearData() {

    }

    @Override
    public RewardIssuerPresenter initPressenter() {
        return new RewardIssuerPresenter();
    }


    @OnClick({R.id.fragment_reward_issuer_jia_meng_pictureSelect, R.id.fragment_reward_issuer_jia_meng_commit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_reward_issuer_jia_meng_pictureSelect:
                selectPicture();
                break;
            case R.id.fragment_reward_issuer_jia_meng_commit:
                if (editHelper.check()) {
                    presenter.getHttpData(
                            String.valueOf(activity.type),
                            editHelper.getText(activity.content.getId()),
                            editHelper.getText(price.getId()),
                            null,
                            null,
                            null,
                            editHelper.getText(companyName.getId()),
                            picturePath

                    );
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE&&resultCode==RESULT_OK) {
            picturePath.clear();
            picturePath.addAll(data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT));
            adapter.notifyDataSetChanged();
        }

    }

    private void setAdapter() {
        gridView.setAdapter(adapter = new CommonAdapter<String>(getActivity(), R.layout.item_photo_select, picturePath) {
            @Override
            public void convert(final ViewHolder holder, String s) {

                holder.setImageBitmap(R.id.item_photo_selectImage, s);
                holder.setOnClickListener(R.id.item_photo_selectDelete, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        picturePath.remove(holder.getmPosition());
                        notifyDataSetChanged();
                    }
                });

                holder.setOnClickListener(R.id.item_photo_selectImage, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectPicture();
                    }
                });

            }
        });


    }

    private void selectPicture() {
        MultiImageSelector.create()
                .showCamera(true) // show camera or not. true by default
                .count(9) // max select image size, 9 by default. used width #.multi()
                .multi() // multi mode, default mode;
                .origin(picturePath) // original select data set, used width #.multi()
                .start(this, REQUEST_CODE);
    }

    DialogProgress dialogProgress;
    @Override
    public void upLoading(int progress, boolean done, boolean isUpdate, boolean isCompress) {
        if (dialogProgress == null) {
            dialogProgress = new DialogProgress(activity);
        }
        dialogProgress.setTitleText("上传中");
        dialogProgress.setProgress(progress);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null && dialogProgress.isShowing()) {
            dialogProgress.dismiss();
        }
    }

}
