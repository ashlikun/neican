package com.hanbang.cxgz_2.view.about_me.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.appliction.iview.common_view.ICaiXiView;
import com.hanbang.cxgz_2.appliction.iview.common_view.IProgressView;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/3　9:00
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public interface IssuerEsoteric_K_View extends BaseView, ICaiXiView,IProgressView {

    /**
     * 开放秘籍的上传
     *
     * @param whetherSucceed
     */
    void upLoading(boolean whetherSucceed);


}
