package com.hanbang.cxgz_2.view.about_me.fragment;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseFragment;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 11:43
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：设置资料
 */

public class MeSetDataShowFragment extends BaseFragment {


    public static MeSetDataShowFragment newIntences() {

        return new MeSetDataShowFragment();
    }


    @Override
    public int getContentView() {
        return R.layout.activity_me_set_data_show;
    }

    @Override
    public void initView() {


    }


}
