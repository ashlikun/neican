package com.hanbang.cxgz_2.view.about_me.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseListFragment;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.pressenter.about_me.StudioFPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.iview.IStudioFView;
import com.hanbang.cxgz_2.view.jianghu.adapter.XiuChangAdapter;
import com.hanbang.cxgz_2.view.jianghu.iview.IDianZanView;
import com.hanbang.cxgz_2.view.jianghu.iview.IGuanZhuView;
import com.hanbang.cxgz_2.view.jianghu.iview.IPingLunView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogComment;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/10　12:41
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioXiuChangFragment extends BaseListFragment<IStudioFView, StudioFPresenter> implements IStudioFView,
        IDianZanView, IGuanZhuView, IPingLunView {
    private ArrayList<XiuChangItemData> listDatas = new ArrayList<>();
    private XiuChangAdapter adapter = null;
    private int id;

    public StudioXiuChangFragment() {
    }

    public StudioXiuChangFragment(int id) {
        this.id = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public int getContentView() {
        return R.layout.listswipeview;
    }

    @Override
    public void initView() {
        super.initView();
        listSwipeView.getRecyclerView().setAutoloaddingCompleData("共发布了 %d 条秀场");
        if (listDatas.size() == 0) {
            presenter.getXiuChangHttpData(true, id);
        }
    }

    @Override
    public Object getAdapter() {
        return adapter = new XiuChangAdapter(activity, this, listDatas, false);
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration
                .Builder(activity)
                .sizeResId(R.dimen.dp_10)
                .colorResId(R.color.main_black)
                .build();
    }

    @Override
    public StudioFPresenter initPressenter() {
        return new StudioFPresenter();
    }

    @Override
    public void clearData() {
        listDatas.clear();
    }

    @Override
    public void UpDataXiuChang(ArrayList<XiuChangItemData> datas) {
        listDatas.addAll(datas);
        adapter.notifyDataSetChanged();
        if (listDatas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData("暂未发布秀场"));
        }
    }


    @Override
    public void onRefresh() {
        presenter.getXiuChangHttpData(true, id);
    }

    @Override
    public void onLoadding() {
        presenter.getXiuChangHttpData(false, id);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getXiuChangHttpData(true, id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getXiuChangHttpData(true, id);
    }

    @Override
    public void onDianzhanLick(XiuChangItemData daChuHttpResponse, int position) {
        presenter.dianzhan(daChuHttpResponse, position);
    }

    @Override
    public void onGuanzhuLick(XiuChangItemData daChuHttpResponse, int position, GuanZhu isGuanzhu) {
        presenter.guanzhu(daChuHttpResponse, position, isGuanzhu);
    }

    @Override
    public void itemChanged(int position) {
        adapter.itemChanged(position);
    }

    @Override
    public void UpDataKeTang(ArrayList<StudyClassroomData> datas) {

    }

    @Override
    public void UpDataMiJi(ArrayList<EsotericaData> datas) {

    }


    @Override
    public void onPingLunLick(final XiuChangItemData daChuHttpResponse, final int position) {
        DialogComment dialogComment = new DialogComment(activity);
        dialogComment.setSendCallback(new DialogComment.OnSendCallback() {
            @Override
            public void onSend(String content) {
                presenter.pinglun(daChuHttpResponse, position, content);
            }
        });
        dialogComment.show();
    }
}
