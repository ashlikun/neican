package com.hanbang.cxgz_2.view.about_me.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseListFragment;
import com.hanbang.cxgz_2.mode.javabean.about_me.MyAnswerEavesdropListData;
import com.hanbang.cxgz_2.pressenter.about_me.MeCanDaTouTingPresenter;
import com.hanbang.cxgz_2.utils.other.AESEncrypt;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.iview.IMeCanDaTouTingView;
import com.hanbang.cxgz_2.view.widget.TouTingMusicPlayView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.List;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/21 10:15
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：提问与偷听列表
 */
public class CanDaTouTingFragment extends BaseListFragment<IMeCanDaTouTingView, MeCanDaTouTingPresenter> implements IMeCanDaTouTingView {

    private ArrayList<MyAnswerEavesdropListData> datas = new ArrayList();
    CommonAdapter adapter;

    public static CanDaTouTingFragment newInstance() {
        CanDaTouTingFragment fragment = new CanDaTouTingFragment();
        return fragment;
    }

    @Override
    public void initView() {
        super.initView();
        listSwipeView.getRecyclerView().setLayoutManager(new LinearLayoutManager(getActivity()));
        presenter.getHttpData(true);
    }

    @Override
    public Object getAdapter() {
        return adapter = new CommonAdapter<MyAnswerEavesdropListData>(getActivity(), R.layout.item_me_can_da, datas) {
            @Override
            public void convert(ViewHolder helper, MyAnswerEavesdropListData item) {
                //提问的人
                if (!StringUtils.isBlank(item.getAvatarimgSmallurl_tw())) {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitTW, item.getAvatarimgSmallurl_tw());
                } else {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitTW, item.getAvatar_tw());
                }
                //回答的人
                if (!StringUtils.isBlank(item.getAvatarimgSmallurl_hd())) {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitHD,item.getAvatarimgSmallurl_hd());
                } else {
                    helper.setImageBitmapCircle(R.id.item_me_can_da_portraitHD, item.getAvatar_hd());
                }

                helper.setText(R.id.item_me_can_da_question, item.getTiWenQuestion_tw());
                helper.setText(R.id.item_me_can_da_voiceTime, item.getHuiDaSoundTime());
                helper.setText(R.id.item_me_can_da_status, item.getStatus());
                helper.setText(R.id.item_me_can_da_date, item.getCjsj());


                View voiceParent = helper.getView(R.id.item_me_can_da_voiceParent);
                if (item.getIsHuiDa()) {
                    voiceParent.setVisibility(View.VISIBLE);
                    //播放
                    TouTingMusicPlayView playView = helper.getView(R.id.TouTingMusicPlayView);
                    String str = null;
                    try {
                        str = AESEncrypt.getInstance().decrypt(item.getHuiDaResultUrl());


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    playView.setUrl(str);
                    playView.setBuy(true);
                    playView.setOnCallListener(new TouTingMusicPlayView.OnCallListener() {
                        @Override
                        public void onBuy(boolean isBuy) {

                        }
                    });

                } else {
                    voiceParent.setVisibility(View.GONE);
                }


            }
        };
    }




    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(getActivity()).sizeResId(R.dimen.dp_5).colorResId(R.color.gray_ee).build();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public MeCanDaTouTingPresenter initPressenter() {
        return new MeCanDaTouTingPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.view_listview_swipe;
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void clearData() {
        datas.clear();
    }

    @Override
    public void upDataUi(List<MyAnswerEavesdropListData> datas) {
        this.datas.addAll(datas);
        adapter.notifyDataSetChanged();

    }
}
