package com.hanbang.cxgz_2.view.about_me.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.EditText;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.utils.ui.AndroidBug5497Workaround;
import com.hanbang.cxgz_2.view.about_me.fragment.RewardIssuerCaiPinFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.RewardIssuerJiaMengFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.RewardIssuerQiuZhiFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.RewardIssuerSheBeiFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.RewardIssuerZhaoPinFragment;
import com.hanbang.cxgz_2.view.about_me.fragment.RewardIssuerZhuanRangFragment;
import com.hanbang.cxgz_2.view.adapter.SectionsPagerAdapter;
import com.hanbang.cxgz_2.view.widget.AnimCheckBox;
import com.hanbang.cxgz_2.view.widget.ViewPagerForScroll;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by yang on 2016/8/24.
 */

public class IssuerRewardFragmentActivity extends BaseMvpActivity implements AnimCheckBox.OnCheckedChangeListener {

    @Override
    public int getContentView() {
        return R.layout.activity_reward_issuer;
    }

    @BindView(R.id.activity_reward_issuer_viewPage)
    ViewPagerForScroll viewPager;
    @BindView(R.id.activity_reward_issuerCPYF)
    AnimCheckBox caiPinYanFa;
    @BindView(R.id.activity_reward_issuerZP)
    AnimCheckBox zaoPin;
    @BindView(R.id.activity_reward_issuerQZ)
    AnimCheckBox qiuZhi;
    @BindView(R.id.activity_reward_issuerDPZR)
    AnimCheckBox dianPuZhuanRang;
    @BindView(R.id.activity_reward_issuerESSB)
    AnimCheckBox erShouSheBei;
    @BindView(R.id.activity_reward_issuerJMDL)
    AnimCheckBox jiaMengDaiLi;
    @BindView(R.id.activity_reward_issuerContent)
    public EditText content;
    public int type = 0;

    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private ArrayList<AnimCheckBox> viewGroup = new ArrayList<>();

    @Override
    public BasePresenter initPressenter() {
        return null;
    }

    @Override
    public void initView() {
        AndroidBug5497Workaround androidBug5497Workaround = AndroidBug5497Workaround.assistActivity(this);


        caiPinYanFa.setOnCheckedChangeListener(this);
        zaoPin.setOnCheckedChangeListener(this);
        qiuZhi.setOnCheckedChangeListener(this);
        dianPuZhuanRang.setOnCheckedChangeListener(this);
        erShouSheBei.setOnCheckedChangeListener(this);
        jiaMengDaiLi.setOnCheckedChangeListener(this);

        viewGroup.add(caiPinYanFa);
        viewGroup.add(zaoPin);
        viewGroup.add(qiuZhi);
        viewGroup.add(dianPuZhuanRang);
        viewGroup.add(erShouSheBei);
        viewGroup.add(jiaMengDaiLi);

        setAdapter();

        toolbar.setTitle("发布需求");
        toolbar.setBack(this);
    }

    private void setAdapter() {
        fragmentList.add(RewardIssuerCaiPinFragment.newInstance());
        fragmentList.add(RewardIssuerZhaoPinFragment.newInstance());
        fragmentList.add(RewardIssuerQiuZhiFragment.newInstance());
        fragmentList.add(RewardIssuerZhuanRangFragment.newInstance());
        fragmentList.add(RewardIssuerSheBeiFragment.newInstance());
        fragmentList.add(RewardIssuerJiaMengFragment.newInstance());

        SectionsPagerAdapter mAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(mAdapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewGroup.get(position).setChecked(true);
                onChange(viewGroup.get(position), true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void parseIntent(Intent intent) {

    }


    @Override
    public void onChange(AnimCheckBox checkBox, boolean checked) {
        if (!checkBox.isChecked()) {
            checkBox.setChecked(true);
            return;
        }
        for (int i = 0; i < viewGroup.size(); i++) {
            if (checkBox.getId() == viewGroup.get(i).getId()) {
                viewGroup.get(i).setChecked(checked, true);
                viewPager.setCurrentItem(i);
                type = i;
                if (i > 0) {
                    type = i + 1;
                }
            } else {
                viewGroup.get(i).setChecked(!checked, true);
            }
        }
    }


}
