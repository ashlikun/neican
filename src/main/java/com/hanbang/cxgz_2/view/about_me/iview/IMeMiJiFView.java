package com.hanbang.cxgz_2.view.about_me.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;

import java.util.ArrayList;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/3　9:00
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public interface IMeMiJiFView extends BaseListView {
    void UpDataMiJi(ArrayList<EsotericaData> datas);
}
