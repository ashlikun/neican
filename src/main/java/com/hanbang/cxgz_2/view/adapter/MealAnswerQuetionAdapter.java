package com.hanbang.cxgz_2.view.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.AESEncrypt;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.widget.TouTingMusicPlayView;

import java.util.List;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/19 10:12
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：餐答问题的Adapter
 */

public class MealAnswerQuetionAdapter extends CommonAdapter<HotAnswerData> {

    public MealAnswerQuetionAdapter(Context context, List<HotAnswerData> datas) {
        super(context, R.layout.item_homepage_answer, datas);
    }

    @Override
    public void convert(ViewHolder helper, HotAnswerData item) {
        if (!StringUtils.isBlank(item.getAvatarimgSmallurl_hd())) {
            helper.setImageBitmapCircle(R.id.item_homepage_answer_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatarimgSmallurl_hd())));
        } else {
            helper.setImageBitmapCircle(R.id.item_homepage_answer_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar_hd())));
        }
        helper.setText(R.id.item_homepage_answer_question, StringUtils.isNullToConvert(item.getTiWenQuestion()));
        helper.setText(R.id.item_homepage_answer_nameJob, StringUtils.isNullToConvert(item.getJob_hd() + " | " + item.getCompany_hd()));
        helper.setText(R.id.item_homepage_answer_voiceTime, DateUtils.showEavesdropTime(item.getHuiDaSoundTime()));
        helper.setText(R.id.item_homepage_answer_eavesdrop, "偷听:" + item.getTtingCount());

        TouTingMusicPlayView playView = helper.getView(R.id.TouTingMusicPlayView);
        String str=null;
        try {
            str = AESEncrypt.getInstance().decrypt(item.getHuiDaResultUrl());


        } catch (Exception e) {
            e.printStackTrace();
        }
        playView.setUrl(str);
        playView.setBuy(true);
        playView.setOnCallListener(new TouTingMusicPlayView.OnCallListener() {
            @Override
            public void onBuy(boolean isBuy) {

            }
        });


    }
}