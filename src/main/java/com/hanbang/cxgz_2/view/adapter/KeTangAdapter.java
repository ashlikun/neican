package com.hanbang.cxgz_2.view.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/12　13:57
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：课堂的Item
 */

public class KeTangAdapter extends CommonAdapter<StudyClassroomData> {

    public KeTangAdapter(Context context, List<StudyClassroomData> datas) {
        super(context, R.layout.item_study_classroom, datas);
    }

    @Override
    public void convert(ViewHolder helper, StudyClassroomData item) {
        if (!StringUtils.isBlank(item.getImg_urlSmall())) {
            helper.setImageBitmap(R.id.item_study_classroom_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_urlSmall())));
        } else {
            helper.setImageBitmap(R.id.item_study_classroom_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
        }
        helper.setText(R.id.item_study_classroom_title, StringUtils.isNullToConvert(item.getTitle()));
        helper.setText(R.id.item_study_classroom_grade, StringUtils.isNullToConvert(item.getPingfen()) + "分");
        helper.setText(R.id.item_study_classroom_read, "阅读:" + StringUtils.isNullToConvert(item.getClick()));
        if (item.getPrice() == 0) {
            helper.setText(R.id.item_study_classroom_price, "免费");
        } else {
            helper.setText(R.id.item_study_classroom_price, "￥" + StringUtils.isNullToConvert(item.getPrice()));
        }
    }
}
