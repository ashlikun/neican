package com.hanbang.cxgz_2.view.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/12　14:03
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class MiJiAdapter extends CommonAdapter<EsotericaData> {

    public MiJiAdapter(Context context, List<EsotericaData> datas) {
        super(context, R.layout.item_homepage_esoterca, datas);
    }

    @Override
    public void convert(ViewHolder helper, EsotericaData item) {
        helper.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
        helper.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
        helper.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
        helper.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
        if (item.getPrice() == 0) {
            helper.setText(R.id.item_homepage_esoterca_price, "免费");
        } else {
            helper.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
        }
    }
}
