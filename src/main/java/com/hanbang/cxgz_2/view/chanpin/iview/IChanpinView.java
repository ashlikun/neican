package com.hanbang.cxgz_2.view.chanpin.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.chanpin.ItemProductShopData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IChanpinView extends BaseListView {

    void upDataUI(List<ItemProductShopData> datas);
}
