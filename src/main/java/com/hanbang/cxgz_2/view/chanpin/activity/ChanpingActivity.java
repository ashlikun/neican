package com.hanbang.cxgz_2.view.chanpin.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMainActivity;
import com.hanbang.cxgz_2.mode.javabean.chanpin.ItemProductShopData;
import com.hanbang.cxgz_2.pressenter.chanpin.ChanpinPresenter;
import com.hanbang.cxgz_2.utils.other.LogUtils;
import com.hanbang.cxgz_2.view.chanpin.iview.IChanpinView;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.banner.ConvenientBanner;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Administrator on 2016/8/15.
 */

public class ChanpingActivity extends BaseMainActivity<IChanpinView, ChanpinPresenter> implements IChanpinView, ListSwipeView.ListSwipeViewListener {


    @BindView(R.id.switchRoot)
    ListSwipeView switchRoot;
    private ArrayList<ItemProductShopData> datas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.getHttpData(true);
    }

    @Override
    public int currentItem() {
        return 3;
    }

    @Override
    public ChanpinPresenter initPressenter() {
        return new ChanpinPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main_chanpin;
    }

    public static void startUI(Activity activity) {
        Intent intent = new Intent(activity, ChanpingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(intent);
    }

    CommonAdapter adapter = new CommonAdapter<ItemProductShopData>(this, R.layout.item_product_shop, datas) {

        @Override
        public void convert(ViewHolder holder, ItemProductShopData itemProductShopData) {
            LogUtils.e(itemProductShopData.toString());
        }
    };

    @Override
    public void initView() {
        super.initView();
        switchRoot.getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        switchRoot.setAdapter(adapter);
        switchRoot.setOnRefreshListener(this);
        switchRoot.setOnLoaddingListener(this);
        switchRoot.getRecyclerView().addHeaderView(new ConvenientBanner<>(this));
    }


    @Override
    public void upDataUI(List<ItemProductShopData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void clearData() {
        datas.clear();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot.getSwipeRefreshLayout();
    }

    @Override
    public StatusChangListener getStatusChangListener() {
        return switchRoot.getStatusChangListener();
    }

    /**
     * 获取分页的有效数据
     */
    public <T> Collection<T> getValidData(Collection<T> c) {
        return switchRoot.getPagingHelp().getValidData(c);
    }

    public void clearPagingData() {
        switchRoot.getPagingHelp().clear();
    }

    public int getPageindex() {
        return switchRoot.getPagingHelp().getPageindex();
    }

    public int getPageCount() {
        return switchRoot.getPagingHelp().getPageCount();
    }
}
