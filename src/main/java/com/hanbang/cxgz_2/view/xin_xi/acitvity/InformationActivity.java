package com.hanbang.cxgz_2.view.xin_xi.acitvity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.SearchEnum;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationData;
import com.hanbang.cxgz_2.pressenter.xin_xi.InformationPresenter;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.other.SearchActivity;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.xin_xi.iview.IInformation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * 信息资讯列表
 */
public class InformationActivity extends BaseMvpActivity<IInformation, InformationPresenter> implements IInformation, ListSwipeView.ListSwipeViewListener {
    @BindView(R.id.switchRoot)
    ListSwipeView switchRoot;
    CommonAdapter adapter;
    private ArrayList<InformationData> datas = new ArrayList<>();
    private String title;
    MyBroadcast myBroadcast;
    IntentFilter intentFilter;

    /**
     * 启动信息详情
     *
     * @param context
     */
    public static void startUi(Context context, String title) {
        Intent intent = new Intent(context, InformationActivity.class);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myBroadcast = new MyBroadcast();
        intentFilter = new IntentFilter();
        intentFilter.addAction(Global.COMMENT_INFORMATION);
        registerReceiver(myBroadcast, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myBroadcast);
    }

    @Override
    public InformationPresenter initPressenter() {
        return new InformationPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        title = getIntent().getStringExtra("title");
    }

    @Override
    public int getContentView() {
        return R.layout.activity_common_list;
    }

    @Override
    public void initView() {
        toolbar.setTitle(title);
        toolbar.setBack(this);
        toolbar.addAction(1, "搜索", R.drawable.material_search);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SearchActivity.startUi(InformationActivity.this, SearchEnum.ziXun);
                return false;
            }
        });


        switchRoot.getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        switchRoot.getRecyclerView().addItemDecoration(new HorizontalDividerItemDecoration.
                Builder(this).sizeResId(R.dimen.dp_5).colorResId(R.color.gray_ee).build());
        switchRoot.setOnRefreshListener(this);
        switchRoot.setOnLoaddingListener(this);
        presenter.getHttpData(true);


        switchRoot.setAdapter(adapter = new CommonAdapter<InformationData>(this, R.layout.item_information, datas) {
            @Override
            public void convert(ViewHolder holder, InformationData data) {
                try {
                    holder.setImageBitmap(R.id.item_information_topImg, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(data.getImg_url())));
                    holder.setText(R.id.item_information_title, StringUtils.isNullToConvert(data.getTitle()));
                    holder.setText(R.id.item_information_from, StringUtils.isNullToConvert(data.getSource()));
                    holder.setText(R.id.item_information_comment, data.getCommentcount());
                    holder.setText(R.id.item_information_date, DateUtils.getTime(data.getAdd_time()));
                    holder.setText(R.id.item_information_praise, data.getLikecount());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View view, Object data, int position) {
                InformationDetailsActivity.startUI(InformationActivity.this, datas.get(position).getId());
            }
        });


    }


    @Override
    public void upDataUi(List<InformationData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return switchRoot.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return switchRoot.getPagingHelp().getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        switchRoot.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return switchRoot.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return switchRoot.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


    /**
     * 我的广播
     */
    class MyBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //评论
            if (intent.getAction().equals(Global.COMMENT_INFORMATION)) {
                int id = intent.getIntExtra("id", -1);
                if (id != -1) {
                    for (int i = 0; i < datas.size(); i++) {
                        if (id == datas.get(i).getId()) {
                            datas.get(i).addCommentcount();
                            adapter.notifyDataSetChanged();
                            return;
                        }
                    }
                }
            }


        }
    }

}
