package com.hanbang.cxgz_2.view.xin_xi.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IInformation extends BaseListView {
    void upDataUi(List<InformationData> datas );
}
