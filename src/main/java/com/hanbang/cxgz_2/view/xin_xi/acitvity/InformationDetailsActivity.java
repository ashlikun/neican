package com.hanbang.cxgz_2.view.xin_xi.acitvity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseListActivity;
import com.hanbang.cxgz_2.mode.enumeration.ModeType;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationDetailsData;
import com.hanbang.cxgz_2.pressenter.xin_xi.InformationDetailsPresenter;
import com.hanbang.cxgz_2.utils.animator.AnimUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.widget.dialog.DialogComment;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.xin_xi.adapter.InformationDetailsAdapter;
import com.hanbang.cxgz_2.view.xin_xi.iview.IInformationDetailsView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/18 10:32
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：信息的详情面
 */

public class InformationDetailsActivity extends BaseListActivity<IInformationDetailsView, InformationDetailsPresenter> implements IInformationDetailsView {
    @BindView(R.id.tv_bar_bottom_reward_details_comment)
    TextView comment;
    @BindView(R.id.tv_bar_bottom_reward_details_praise)
    TextView praise;
    private int id = -1;

    InformationDetailsData data;

    private ArrayList<HotAnswerData> listDatas = new ArrayList<>();
    InformationDetailsAdapter adapter;

    @Override
    public int getContentView() {
        return R.layout.activity_information_details;
    }

    public static void startUI(Context context, int id) {
        Intent intent = new Intent(context, InformationDetailsActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void parseIntent(Intent intent) {
        id = intent.getIntExtra("id", -1);
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setBack(this);
        toolbar.addAction(1, "搜索", R.drawable.material_ic_share);
        presenter.getHttpData(true, id);
        listSwipeView.getRecyclerView().setLoadMoreEnabled(false);



    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(this).colorResId(R.color.gray_ee).sizeResId(R.dimen.dp_5).build();
    }

    @Override
    public Object getAdapter() {
        return adapter = new InformationDetailsAdapter(this, listSwipeView.getRecyclerView(), listDatas);
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.swipeView;
    }

    @Override
    public void upDataUi(InformationDetailsData datas) {
        data = datas;
        adapter.notifyDataSetChangedAll(datas);
        comment.setText(data.getRwzDetail().getCommentcount());
        praise.setText(data.getRwzDetail().getLikecount());


    }


    @Override
    public void onRefresh() {

        presenter.getHttpData(true, id);
    }

    @Override
    public void clearData() {
        adapter.clearData();
    }

    @Override
    public InformationDetailsPresenter initPressenter() {
        return new InformationDetailsPresenter();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true, id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true, id);
    }


    @Override
    public void onLoadding() {
        presenter.getHttpData(false, id);
    }

    @OnClick({R.id.bar_bottom_reward_details_transmit, R.id.bar_bottom_reward_details_comment, R.id.bar_bottom_reward_details_collect, R.id.bar_bottom_reward_details_praise, R.id.bar_bottom_reward_details_look})
    public void onClick(View view) {
        if (!isLogin(true)) {
            return;
        }

        switch (view.getId()) {
            //分享
            case R.id.bar_bottom_reward_details_transmit:
                UserData.isSLogin();

                AnimUtils.scaleAnim(findViewById(R.id.bar_bottom_reward_details_transmit));
//                SharedData sharedData = new SharedData(
//                        1,
//                        data.getRwzDetail().getId() + "",
//                        data.getRwzDetail().getTitle(),
//                        data.getRwzDetail().getCanDaJianJie());
//
//                SharedUtil.getHttpSharedUrlAndShared(InformationDetailsActivity.this, sharedData);


                break;
            //评论
            case R.id.bar_bottom_reward_details_comment:
                AnimUtils.scaleAnim(findViewById(R.id.bar_bottom_reward_details_comment));
                DialogComment dialogComment = new DialogComment(this);
                dialogComment.show();
                dialogComment.setSendCallback(new DialogComment.OnSendCallback() {
                    @Override
                    public void onSend(String content) {

                        presenter.pinglun(InformationDetailsActivity.this, id, ModeType.RDZX_D, content);
                    }
                });

                break;
            case R.id.bar_bottom_reward_details_collect:
                AnimUtils.scaleAnim(findViewById(R.id.bar_bottom_reward_details_collect));


                break;
            case R.id.bar_bottom_reward_details_praise:
                AnimUtils.scaleAnim(findViewById(R.id.bar_bottom_reward_details_praise));
                presenter.dianZan(this, id, ModeType.RDZX_D);

                break;
            case R.id.bar_bottom_reward_details_look:
                AnimUtils.scaleAnim(findViewById(R.id.bar_bottom_reward_details_look));
                break;
        }
    }


    @Override
    public void pingLun(int id) {

        comment.setText(data.getRwzDetail().addCommentcount());
    }

    @BindView(R.id.appbarLayout)
    AppBarLayout appbarLayout;
}
