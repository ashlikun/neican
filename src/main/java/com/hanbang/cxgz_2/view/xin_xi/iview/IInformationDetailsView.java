package com.hanbang.cxgz_2.view.xin_xi.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.appliction.iview.common_view.IPingLunView;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationDetailsData;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IInformationDetailsView extends BaseListView,IPingLunView{
    void upDataUi(InformationDetailsData datas);
}
