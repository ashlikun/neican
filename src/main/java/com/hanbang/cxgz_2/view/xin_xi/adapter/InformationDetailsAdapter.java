package com.hanbang.cxgz_2.view.xin_xi.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.home.HotAnswerData;
import com.hanbang.cxgz_2.mode.javabean.xin_xi.InformationDetailsData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.AESEncrypt;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.utils.ui.WebViewUtils;
import com.hanbang.cxgz_2.view.widget.TouTingMusicPlayView;
import com.hanbang.cxgz_2.view.widget.WebViewForScroll;
import com.hanbang.cxgz_2.view.widget.autoloadding.RecyclerViewWithHeaderAndFooter;

import java.util.List;

/**
 * Created by yang on 2016/9/18.
 */

public class InformationDetailsAdapter extends CommonAdapter<HotAnswerData> {
    InformationDetailsData data;
    Activity activity;
    RecyclerViewWithHeaderAndFooter recyclerView;


    public InformationDetailsAdapter(Activity context, RecyclerViewWithHeaderAndFooter recyclerView, final List<HotAnswerData> datas) {
        super(context, R.layout.item_homepage_answer, datas);
        activity = context;
        this.recyclerView = recyclerView;

        //添加头部
        final View headView = UiUtils.getInflaterView(activity, R.layout.activity_information_details_headview, recyclerView, false);
        recyclerView.addHeaderView(headView);

        // 注册数据源发生变化
        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if (data != null) {
                    convertHeader(new ViewHolder(mContext, headView, null, 0), data);
                }
            }
        });

    }

    @Override
    public void convert(ViewHolder helper, HotAnswerData item) {

        if (!StringUtils.isBlank(item.getAvatarimgSmallurl_hd())) {
            helper.setImageBitmapCircle(R.id.item_homepage_answer_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatarimgSmallurl_hd())));
        } else {
            helper.setImageBitmapCircle(R.id.item_homepage_answer_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar_hd())));
        }
        helper.setText(R.id.item_homepage_answer_question, StringUtils.isNullToConvert(item.getTiWenQuestion()));
        helper.setText(R.id.item_homepage_answer_nameJob, StringUtils.isNullToConvert(item.getJob_hd() + " | " + item.getCompany_hd()));
        helper.setText(R.id.item_homepage_answer_voiceTime, DateUtils.showEavesdropTime(item.getHuiDaSoundTime()));
        helper.setText(R.id.item_homepage_answer_eavesdrop, "偷听:" + item.getTtingCount());

        TouTingMusicPlayView playView = helper.getView(R.id.TouTingMusicPlayView);
        String str = null;
        try {
            str = AESEncrypt.getInstance().decrypt(item.getHuiDaResultUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }
        playView.setUrl(str);
        playView.setBuy(true);
        playView.setOnCallListener(new TouTingMusicPlayView.OnCallListener() {
            @Override
            public void onBuy(boolean isBuy) {


            }
        });


    }


    /**
     * 头部数据的赋值
     *
     * @param holder
     * @param data
     */
    public void convertHeader(final ViewHolder holder, InformationDetailsData data) {
        final WebViewForScroll webView = (WebViewForScroll) holder.getView(R.id.tv_information_details_content);
        final LinearLayout linearLayout = (LinearLayout) holder.getView(R.id.webViewParent);
        final TextView hint = (TextView) holder.getView(R.id.activity_emal_answer_headview_unfoldAndFold);
        View mealAnswerParent = holder.getView(R.id.activity_emal_answer_headview_mealAnswerParent);
        whetherOpen = false;

        holder.setImageBitmapCircle(R.id.activity_information_details_headview_portrait, data.getRwzDetail().getImg_url());
        holder.setText(R.id.activity_information_details_headview_name, data.getRwzDetail().getUser_name());
        holder.setText(R.id.activity_information_details_headview_source, data.getRwzDetail().getJob_hd() + " | " + data.getRwzDetail().getCompany());
        holder.setText(R.id.activity_information_details_headview_brief, data.getRwzDetail().getCanDaJianJie());
        holder.setText(R.id.activity_information_details_headview_price, data.getRwzDetail().getCanDaTiWenMoney());

        holder.setOnClickListener(R.id.activity_emal_answer_headview_unfoldAndFold, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webViewUnfoldAndfold(activity, linearLayout, hint);
            }
        });

        webViewUnfoldAndfold(activity, linearLayout, hint);
        setSettings(webView, linearLayout, hint);
        webView.loadUrl("http://api.cxgz8.com/app/article_show.aspx?" + "id=" + data.getRwzDetail().getId());

        //如果餐答没有开启的，并且有文章没有作者
        if (data.getRwzDetail().getCanDaKaiQi() && data.getRwzDetail().getAuthor()) {
            mealAnswerParent.setVisibility(View.VISIBLE);

        }else {
            mealAnswerParent.setVisibility(View.GONE);
            whetherOpen = true;
            webViewUnfoldAndfold(activity, linearLayout, hint);
        }


    }

    public void notifyDataSetChangedAll(InformationDetailsData datas) {
        mDatas.addAll(datas.getWithQuestion());
        data = datas;


        notifyDataSetChanged();

    }


    public void clearData() {
        mDatas.clear();
        data = null;
    }


    /**
     * webviow，的设置
     */
    private void setSettings(WebView webView, final View webViewParent, final TextView hint) {
        WebViewUtils.configWebview(webView);

        //监听webView加载完成,此方法暂时不使用
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

//                webViewUnfoldAndfold(activity, webViewParent, hint);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
    }

    //webView，打开状态
    private boolean whetherOpen = false;

    /**
     * webView,展开与折叠
     */
    private void webViewUnfoldAndfold(Activity activity, View view, TextView textView) {
        DisplayMetrics dm = new DisplayMetrics();//获取当前显示的界面大小
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;//获取当前界面的高度
        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) view.getLayoutParams();

        if (whetherOpen) {
            //折叠时设置高度为：linearParams.MATCH_PARENT
            linearParams.height = linearParams.MATCH_PARENT;
            view.setLayoutParams(linearParams);
            textView.setText("收起文章");

            //展开时设置高度为:屏幕高度的1/2;
        } else {
            linearParams.height = (int) (height / 1.5);
            view.setLayoutParams(linearParams);
            recyclerView.scrollToPosition(0);

            textView.setText("点击查看全文");
        }
        whetherOpen = !whetherOpen;

    }

}
