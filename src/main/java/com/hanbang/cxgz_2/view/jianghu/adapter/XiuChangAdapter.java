package com.hanbang.cxgz_2.view.jianghu.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.BaseView;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ImageItemData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.utils.animator.AnimUtils;
import com.hanbang.cxgz_2.utils.http.GlideOptions;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.view.jianghu.iview.IDianZanView;
import com.hanbang.cxgz_2.view.jianghu.iview.IGuanZhuView;
import com.hanbang.cxgz_2.view.jianghu.iview.IPingLunView;
import com.hanbang.cxgz_2.view.other.PhotoActivity;
import com.hanbang.cxgz_2.view.widget.ninegridimageview.NineGridImageView;
import com.hanbang.cxgz_2.view.widget.ninegridimageview.NineGridImageViewAdapter;

import java.util.List;

import static com.hanbang.cxgz_2.R.id.guanzhuLL;

/**
 * Created by Administrator on 2016/8/24.
 */

public class XiuChangAdapter extends CommonAdapter<XiuChangItemData> {

    BaseView baseView;
    BaseActivity activity;
    boolean isSHowFenshi;

    NineGridImageViewAdapter adapter = new NineGridImageViewAdapter<ImageItemData>() {
        @Override
        protected void onDisplayImage(Context context, ImageView imageView, ImageItemData o) {
            GlideUtils.show(imageView, o.getImgSmallurl(), new GlideOptions.Builder().bulider());
        }

        @Override
        protected void onItemImageClick(Context context, int index, List<ImageItemData> list) {
            PhotoActivity.startUI(((BaseActivity) mContext), ImageItemData.getStringList(list), index);
        }
    };

    public XiuChangAdapter(BaseActivity context, BaseView view, List<XiuChangItemData> datas) {

        this(context, view, datas, true);
    }

    public XiuChangAdapter(BaseActivity context, BaseView view, List<XiuChangItemData> datas, boolean isSHowFenshi) {

        super(context, R.layout.item_jianghu_xiuchang, datas);
        this.isSHowFenshi = isSHowFenshi;
        activity = context;
        baseView = view;
    }

    @Override
    public void convert(final ViewHolder holder, final XiuChangItemData o) {
        ((NineGridImageView<ImageItemData>) holder.getView(R.id.photosGv)).setAdapter(adapter);
        ((NineGridImageView<ImageItemData>) holder.getView(R.id.photosGv)).setImagesData(o.getList_img());
        holder.setImageBitmapCircle(R.id.touxiang, o.getAvatar());
        holder.setText(R.id.nameTv, o.getUser_name());
        holder.setText(R.id.zhiweiInfo, o.getZihweiInfo());
        holder.setText(R.id.timeTv, o.getTime());

        holder.setText(R.id.addressTv, String.valueOf(o.getPosition()));
        holder.setText(R.id.commentTv, String.valueOf(o.getPinglunCount()));
        holder.setText(R.id.likeTv, String.valueOf(o.getDianzanCount()));
        holder.setText(R.id.shareTv, String.valueOf(o.getSharecount()));
        holder.setText(R.id.contentTv, o.getText());
        holder.setVisible(R.id.vipIv, o.is_vip());


        if (isSHowFenshi) {
            holder.setText(R.id.guanzhiNumTv, String.valueOf(o.getFensiCount()));
            holder.setText(R.id.guanzhuTv, o.guanzhuText());
            holder.setBackgroundRes(R.id.guanzhuIv, o.getGuanzhuRes());
        } else {
            holder.setVisible(R.id.guanzhuLL, false);
            holder.setVisible(R.id.fenshiLL, false);
        }


        if (o.isClickDianzhan()) {
            AnimUtils.scaleAnim(holder.getView(R.id.liketLL));
            o.setClickDianzhan(false);
        }

        if (o.isClickGuanzhu()) {
            AnimUtils.rotationAnim(holder.getView(guanzhuLL));
            o.setClickGuanzhu(false);
        }
        if (o.isClickPinglun()) {
            AnimUtils.scaleAnim(holder.getView(R.id.commentLL));
            o.setClickPinglun(false);
        }
        holder.setOnClickListener(R.id.liketLL, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.isLogin(true)) {
                    if (baseView instanceof IDianZanView) {
                        ((IDianZanView) baseView).onDianzhanLick(o, holder.getmPosition());
                    }
                }
            }
        });
        holder.setOnClickListener(R.id.commentLL, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.isLogin(true)) {
                    if (baseView instanceof IPingLunView) {
                        ((IPingLunView) baseView).onPingLunLick(o, holder.getmPosition());
                    }
                }
            }
        });
        holder.setOnClickListener(guanzhuLL, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.isLogin(true)) {
                    if (baseView instanceof IGuanZhuView) {
                        ((IGuanZhuView) baseView).onGuanzhuLick(o, holder.getmPosition(), GuanZhu.getState(o.isguanzhu()));
                    }
                }
            }
        });
    }


    public void itemChanged(int position) {
        notifyItemChanged(position + getHeadSize(), false);
    }
}
