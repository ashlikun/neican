package com.hanbang.cxgz_2.view.jianghu.adapter;

import android.content.Context;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ZhangGuiItemData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.jianghu.activity.ZhangGuiListActivity;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import java.util.List;

/**
 * Created by Administrator on 2016/8/17.
 */

public class ZhangguiAdapter extends CommonAdapter<ZhangGuiItemData> {

    SuperGridLayout.OnItemClickListener onItemClickListener;

    public ZhangguiAdapter(Context context, SuperGridLayout.OnItemClickListener onItemClickListener, List<ZhangGuiItemData> datas) {

        super(context, R.layout.item_jianghu_zhanggui, datas);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void convert(ViewHolder holder, final ZhangGuiItemData data) {

        holder.setText(R.id.title, StringUtils.isNullToConvert(data.getTitle()));

        SuperGridLayout superGridLayout = holder.getView(R.id.hyzgGridView);
        CommonAdapter adapter = superGridLayout.getAdapter();
        holder.setOnClickListener(R.id.moreLL, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (data.getContent() != null && data.getContent().size() != 0) {
                            ZhangGuiListActivity.startUi(mContext, data.getBiaoqian(), data.getTitle());
                        }
                    }
                }
        );
        if (adapter != null) {
            adapter.setDatas(data.getContent());
            superGridLayout.setAdapter(adapter);
        } else {
            superGridLayout.setAdapter(new CommonAdapter<ManagerData>(mContext, R.layout.item_chef_manager, data.getContent()) {
                @Override
                public void convert(ViewHolder holder, ManagerData o) {
                    if (!StringUtils.isBlank(o.getAvatarimgSmallurl())) {
                        holder.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(o.getAvatarimgSmallurl())));
                    } else {
                        holder.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(o.getAvatar())));
                    }
                    holder.setVisible(R.id.item_chef_manager_isVip, o.is_vip());
                    holder.setText(R.id.item_chef_manager_attention, o.getMembershipcount());
                    holder.setText(R.id.item_chef_manager_company, StringUtils.isNullToConvert(o.getCompany()));
                    holder.setText(R.id.item_chef_manager_name, StringUtils.isNullToConvert(o.getUser_name()));
                    holder.setText(R.id.item_chef_manager_job, StringUtils.isNullToConvert(o.getJob()));
                }
            });
        }

        superGridLayout.setOnItemClickListener(onItemClickListener);


    }


}
