package com.hanbang.cxgz_2.view.jianghu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseListActivity;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;
import com.hanbang.cxgz_2.pressenter.jianghu.ChuShiListPresenter;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.utils.ui.divider.VerticalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.activity.StudioActivity;
import com.hanbang.cxgz_2.view.jianghu.iview.IChefView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间: 2016/9/21 9:53
 * <p>
 * 方法功能：厨师更多列表
 */

public class ChuShiListActivity extends BaseListActivity<IChefView, ChuShiListPresenter> implements IChefView
        , OnItemClickListener<ChefData> {
    CommonAdapter adapter;
    private ArrayList<ChefData> datas = new ArrayList<>();
    private String title;
    private int sortId;//分类id


    public static void startUi(Context context, int sortId, String title) {
        Intent intent = new Intent(context, ChuShiListActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("sortId", sortId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.getHttpData(true);
    }

    @Override
    public ChuShiListPresenter initPressenter() {
        return new ChuShiListPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        title = getIntent().getStringExtra("title");
        sortId = getIntent().getIntExtra("sortId", 0);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_or_fragment_list;
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setTitle(title);
        toolbar.setBack(this);
        adapter.setOnItemClickListener(this);
        listSwipeView.getRecyclerView().addItemDecoration(new VerticalDividerItemDecoration.Builder(this).sizeResId(R.dimen.dp_3)
                .colorResId(R.color.main_black).build());
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(this).sizeResId(R.dimen.dp_3)
                .colorResId(R.color.main_black).build();
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, 3);
    }

    @Override
    public Object getAdapter() {
        return adapter = new CommonAdapter<ChefData>(this, R.layout.item_chef_manager, datas) {
            @Override
            public void convert(ViewHolder holder, ChefData item) {
                try {
                    if (!StringUtils.isBlank(item.getAvatarimgSmallurl())) {
                        holder.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatarimgSmallurl())));
                    } else {
                        holder.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar())));
                    }
                    if (item.is_vip()) {
                        holder.setVisible(R.id.item_chef_manager_isVip, true);
                    } else {
                        holder.setVisible(R.id.item_chef_manager_isVip, false);
                    }
                    holder.setText(R.id.item_chef_manager_attention, item.getMembershipcount());
                    holder.setText(R.id.item_chef_manager_company, StringUtils.isNullToConvert(item.getCompany()));
                    holder.setText(R.id.item_chef_manager_name, StringUtils.isNullToConvert(item.getUser_name()));
                    holder.setText(R.id.item_chef_manager_job, StringUtils.isNullToConvert(item.getJob()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }


    @Override
    public void onItemClick(ViewGroup parent, View view, ChefData data, int position) {
        StudioActivity.startUI(this, data.getId(), StudioActivity.CHUSHI);
    }

    @Override
    public void upDataUi(List<ChefData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();

    }

    @Override
    public int getBiaoqian() {
        return sortId;
    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return listSwipeView.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return listSwipeView.getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        listSwipeView.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return listSwipeView.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return listSwipeView.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


}
