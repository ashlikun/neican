package com.hanbang.cxgz_2.view.jianghu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseListActivity;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangDetailsHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangCommentData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.pressenter.jianghu.XiuChangDetailsPresenter;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.jianghu.adapter.XiuChangDetailsAdapter;
import com.hanbang.cxgz_2.view.jianghu.iview.IDianZanView;
import com.hanbang.cxgz_2.view.jianghu.iview.IGuanZhuView;
import com.hanbang.cxgz_2.view.jianghu.iview.IPingLunView;
import com.hanbang.cxgz_2.view.jianghu.iview.IXiuChangDetailsView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogComment;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

/**
 * Created by Administrator on 2016/8/26.
 */

public class XiuChangDetailsActivity extends BaseListActivity<IXiuChangDetailsView, XiuChangDetailsPresenter> implements IXiuChangDetailsView
        , IDianZanView, IGuanZhuView, IPingLunView {

    private int id = 0;

    XiuChangDetailsHttpResponse response = XiuChangDetailsHttpResponse.init();

    XiuChangDetailsAdapter adapter = null;

    @Override
    public int getContentView() {
        return R.layout.activity_xiuchang_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.getHttpData(true, id);
    }

    /**
     * 启动秀场详情
     *
     * @param context
     */
    public static void startUi(Context context, int id) {
        Intent intent = new Intent(context, XiuChangDetailsActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }

    @Override
    public void parseIntent(Intent intent) {
        id = intent.getIntExtra("id", 0);


    }

    @Override
    public void initView() {
        adapter = new XiuChangDetailsAdapter(this, listSwipeView.getRecyclerView(), response);
        super.initView();
        toolbar.setTitle("秀场");
        toolbar.addAction(1, "", R.drawable.material_search);
        toolbar.setNavigationIcon(R.drawable.material_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == 1) {
                    showWarningSnackbar("  aaaa ");
                }
                return true;
            }
        });
        listSwipeView.getRecyclerView().setAutoloaddingNoData("暂无评论,赶紧抢占沙发");
        listSwipeView.getRecyclerView().setAutoloaddingCompleData("当前有%d条评论，需要你火速支援");
        loadingAndRetryManager.showRetry(new ContextData());

    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(this).build();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true, id);
    }

    @Override
    public void clearData() {
        response.clearAll();
    }

    @Override
    public Object getAdapter() {
        return adapter;
    }

    @Override
    public XiuChangDetailsPresenter initPressenter() {
        return new XiuChangDetailsPresenter();
    }


    @Override
    public void upDataUi(XiuChangDetailsHttpResponse datas) {
        response.addData(datas);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false, id);
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true, id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true, id);
    }

    @Override
    public void headChanged() {
        adapter.headChanged();
    }

    @Override
    public void pingLinSuccess() {
        adapter.headChanged();
        listSwipeView.setRefreshing(true);
    }


    @Override
    public void onDianzhanLick(XiuChangItemData daChuHttpResponse, int position) {
        presenter.dianzhan(daChuHttpResponse);
    }

    @Override
    public void onGuanzhuLick(XiuChangItemData daChuHttpResponse, int position, GuanZhu isGuanzhu) {
        presenter.guanzhu(daChuHttpResponse, isGuanzhu);
    }

    @Override
    public void onPingLunLick(final XiuChangItemData daChuHttpResponse, final int position) {
        DialogComment dialogComment = new DialogComment(this);
        dialogComment.setSendCallback(new DialogComment.OnSendCallback() {
            @Override
            public void onSend(String content) {
                presenter.pinglun(daChuHttpResponse, content);
            }
        });
        dialogComment.show();
    }

    public void onPingLunPersonLick(final XiuChangItemData daChuHttpResponse, final XiuChangCommentData data) {
        DialogComment dialogComment = new DialogComment(this);
        dialogComment.setSendCallback(new DialogComment.OnSendCallback() {
            @Override
            public void onSend(String content) {
                presenter.pinglunPerson(daChuHttpResponse, data, "@" + StringUtils.isNullToConvert(data.getUser_name()) + ":" + content);
            }
        });
        dialogComment.show();
    }
}
