package com.hanbang.cxgz_2.view.jianghu.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ChuShiItemData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.view.jianghu.activity.ChuShiListActivity;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import java.util.List;

/**
 * Created by Administrator on 2016/8/17.
 */

public class DaChuAdapter extends CommonAdapter<ChuShiItemData> {
    SuperGridLayout.OnItemClickListener onItemClickListener;

    public DaChuAdapter(Context context, SuperGridLayout.OnItemClickListener onItemClickListener, List<ChuShiItemData> datas) {
        super(context, R.layout.item_jianghu_chushi, datas);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void convert(ViewHolder holder, final ChuShiItemData data) {

        holder.setText(R.id.title, StringUtils.isNullToConvert(data.getTitle()));

        holder.setVisible(R.id.age, !StringUtils.isEmpty(data.getAge()));
        if (!StringUtils.isEmpty(data.getAge())) {
            holder.setText(R.id.age, StringUtils.isNullToConvert(data.getAge()));
        }


        holder.setOnClickListener(R.id.moreLL, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (data.getContent() != null && data.getContent().size() != 0) {
                            ChuShiListActivity.startUi(mContext, data.getBiaoqian(), data.getTitle());
                        }
                    }
                }
        );


        holder.setVisible(R.id.xiongmaoFl, data.getPandaCount() > 0);

        if (data.getPandaCount() > 0)

        {
            LinearLayout flexboxLayout = holder.getView(R.id.xiongmaoFl);
            flexboxLayout.removeAllViews();
            for (int i = 0; i < data.getPandaCount(); i++) {
                View v = new View(mContext);
                v.setBackgroundResource(R.mipmap.channel_jianghu_cook_icon_panda);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mContext.getResources().getDimensionPixelSize(R.dimen.dp_20)
                        , mContext.getResources().getDimensionPixelSize(R.dimen.dp_20));
                params.leftMargin = mContext.getResources().getDimensionPixelSize(R.dimen.dp_4);
                flexboxLayout.addView(v, params);
            }

        }

        SuperGridLayout superGridLayout = holder.getView(R.id.hyzgGridView);
        CommonAdapter adapter = superGridLayout.getAdapter();
        if (adapter != null)

        {
            adapter.setDatas(data.getContent());
            superGridLayout.setAdapter(adapter);
        } else

        {
            superGridLayout.setAdapter(new CommonAdapter<ChefData>(mContext, R.layout.item_chef_manager, data.getContent()) {
                @Override
                public void convert(ViewHolder holder, ChefData o) {
                    if (!StringUtils.isBlank(o.getAvatarimgSmallurl())) {
                        holder.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(o.getAvatarimgSmallurl())));
                    } else {
                        holder.setImageBitmap(R.id.item_chef_manager_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(o.getAvatar())));
                    }
                    holder.setVisible(R.id.item_chef_manager_isVip, o.is_vip());
                    holder.setText(R.id.item_chef_manager_attention, o.getMembershipcount());
                    holder.setText(R.id.item_chef_manager_company, StringUtils.isNullToConvert(o.getCompany()));
                    holder.setText(R.id.item_chef_manager_name, StringUtils.isNullToConvert(o.getUser_name()));
                    holder.setText(R.id.item_chef_manager_job, StringUtils.isNullToConvert(o.getJob()));
                }
            });

        }

        superGridLayout.setOnItemClickListener(onItemClickListener);
    }


}
