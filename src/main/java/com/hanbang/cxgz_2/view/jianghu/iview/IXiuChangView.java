package com.hanbang.cxgz_2.view.jianghu.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangHttpResponse;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IXiuChangView extends BaseListView {

    void upDataUI(XiuChangHttpResponse daChuHttpResponse);
    void itemChanged( int position);
}
