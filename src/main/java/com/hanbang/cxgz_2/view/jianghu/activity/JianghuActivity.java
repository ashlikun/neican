package com.hanbang.cxgz_2.view.jianghu.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMainActivity;
import com.hanbang.cxgz_2.pressenter.jianghu.JianghuPresenter;
import com.hanbang.cxgz_2.view.jianghu.fragment.DaChuFragment;
import com.hanbang.cxgz_2.view.jianghu.fragment.XiuChangFragment;
import com.hanbang.cxgz_2.view.jianghu.fragment.ZhangGuiFragment;
import com.hanbang.cxgz_2.view.jianghu.iview.IJianghuView;
import com.hanbang.cxgz_2.view.adapter.SectionsPagerAdapter;
import com.hanbang.cxgz_2.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 江湖
 * Created by Administrator on 2016/8/15.
 */

public class JianghuActivity extends BaseMainActivity<IJianghuView, JianghuPresenter> implements IJianghuView {
    @BindView(R.id.view_viewpage_tablayout_tabs)
    TabLayout tabLayout;
    @BindView(R.id.view_viewpage_tablayout_viewPager)
    ViewPager mViewPager;
    ArrayList<Fragment> fragments = new ArrayList<>();
    SectionsPagerAdapter mAdapter;

    private int initTab = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int currentItem() {
        return 1;
    }

    @Override
    public JianghuPresenter initPressenter() {
        return new JianghuPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main_jianghu;
    }


    public static void startUI(Activity activity) {
        startUI(activity, -1);
    }

    public static void startUI(Activity activity, int tab) {
        Intent intent = new Intent(activity, JianghuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("tab", tab);
        activity.startActivity(intent);
    }

    @Override
    public void parseIntent(Intent intent) {
        super.parseIntent(intent);
        initTab = intent.getIntExtra("tab", -1);
        if (initTab != -1) {
            mViewPager.setCurrentItem(initTab);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        parseIntent(intent);
    }

    @Override
    public void clearData() {

    }

    @Override
    public void initView() {
        super.initView();
        fragments.add(new XiuChangFragment());
        fragments.add(new DaChuFragment());
        fragments.add(new ZhangGuiFragment());
        fragments.add(new DaChuFragment());
        fragments.add(new DaChuFragment());

        mAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragments, new String[]{"秀场", "大厨", "掌柜", "附近", "好友"});
        mViewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        if (initTab != -1) {
            mViewPager.setCurrentItem(initTab);
        }
    }
}
