package com.hanbang.cxgz_2.view.jianghu.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseSwipeView;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ChuShiItemData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IDaChuView extends BaseSwipeView {

    void upDataUI(List<ChuShiItemData> datas);
}
