package com.hanbang.cxgz_2.view.jianghu.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.home.ManagerData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IManagerView extends BaseListView {
    void upDataUi(List<ManagerData> datas);
    int getBiaoqian();
}
