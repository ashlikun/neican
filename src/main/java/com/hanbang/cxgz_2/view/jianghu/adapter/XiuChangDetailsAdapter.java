package com.hanbang.cxgz_2.view.jianghu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangDetailsHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ImageItemData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangCommentData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.utils.animator.AnimUtils;
import com.hanbang.cxgz_2.utils.http.GlideOptions;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.jianghu.activity.XiuChangDetailsActivity;
import com.hanbang.cxgz_2.view.other.PhotoActivity;
import com.hanbang.cxgz_2.view.widget.autoloadding.RecyclerViewWithHeaderAndFooter;
import com.hanbang.cxgz_2.view.widget.ninegridimageview.NineGridImageView;
import com.hanbang.cxgz_2.view.widget.ninegridimageview.NineGridImageViewAdapter;

import java.util.List;

/**
 * Created by Administrator on 2016/8/26.
 */

public class XiuChangDetailsAdapter extends CommonAdapter<XiuChangCommentData> {

    XiuChangDetailsActivity activity;
    private XiuChangItemData headData;

    public XiuChangDetailsAdapter(final XiuChangDetailsActivity activity, RecyclerViewWithHeaderAndFooter recyclerView, final XiuChangDetailsHttpResponse datas) {
        super(activity, R.layout.item_jianghu_pinglun, datas.getPinglunList());
        this.activity = activity;

        final View view = UiUtils.getInflaterView(activity, R.layout.item_jianghu_xiuchang);
        NineGridImageView photosGv = (NineGridImageView) view.findViewById(R.id.photosGv);
        photosGv.setMaxSize(9);
        recyclerView.addHeaderView(view);
        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                convertHeader(new ViewHolder(mContext, view, null, 0), datas.getDynamicsDetail());
            }
        });
    }


    @Override
    public void convert(final ViewHolder holder, final XiuChangCommentData o) {

        holder.setImageBitmap(R.id.touxiang, o.getAvatarimgSmallurl());
        holder.setText(R.id.nameTv, o.getUser_name());
        holder.setText(R.id.content, o.getText());
        holder.setText(R.id.dianzhan, o.getLikecount());
        holder.setText(R.id.timeTv, DateUtils.dateToString(o.getAdd_time()));
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.isLogin(true)) {

                    activity.onPingLunPersonLick(headData,o);
                }
            }
        });

    }

    NineGridImageViewAdapter adapter = new NineGridImageViewAdapter<ImageItemData>() {
        @Override
        protected void onDisplayImage(Context context, ImageView imageView, ImageItemData o) {
            GlideUtils.show(imageView, o.getImgSmallurl(), new GlideOptions.Builder().bulider());
        }

        @Override
        protected void onItemImageClick(Context context, int index, List<ImageItemData> list) {
            PhotoActivity.startUI(((BaseActivity) mContext), ImageItemData.getStringList(list), index);
        }
    };


    public void convertHeader(final ViewHolder holder, List<XiuChangItemData> list) {
        if (list == null || list.size() <= 0) {
            return;
        }
        headData = list.get(0);

        ((NineGridImageView<ImageItemData>) holder.getView(R.id.photosGv)).setAdapter(adapter);
        ((NineGridImageView<ImageItemData>) holder.getView(R.id.photosGv)).setImagesData(headData.getList_img());
        holder.setImageBitmapCircle(R.id.touxiang, headData.getAvatar());
        holder.setText(R.id.nameTv, headData.getUser_name());
        holder.setText(R.id.zhiweiInfo, headData.getZihweiInfo());
        holder.setText(R.id.timeTv, headData.getTime());
        holder.setText(R.id.guanzhiNumTv, String.valueOf(headData.getFensiCount()));
        holder.setText(R.id.addressTv, String.valueOf(headData.getPosition()));
        holder.setText(R.id.commentTv, String.valueOf(headData.getPinglunCount()));
        holder.setText(R.id.likeTv, String.valueOf(headData.getDianzanCount()));
        holder.setText(R.id.shareTv, String.valueOf(headData.getSharecount()));
        holder.setText(R.id.contentTv, headData.getText());
        holder.setVisible(R.id.vipIv, headData.is_vip());
        holder.setText(R.id.guanzhuTv, headData.guanzhuText());
        holder.setBackgroundRes(R.id.guanzhuIv, headData.getGuanzhuRes());


        if (headData.isClickDianzhan()) {
            AnimUtils.scaleAnim(holder.getView(R.id.liketLL));
            headData.setClickDianzhan(false);
        }

        if (headData.isClickGuanzhu()) {
            AnimUtils.rotationAnim(holder.getView(R.id.guanzhuLL));
            headData.setClickGuanzhu(false);
        }
        if (headData.isClickPinglun()) {
            AnimUtils.scaleAnim(holder.getView(R.id.commentLL));
            headData.setClickPinglun(false);
        }
        holder.setOnClickListener(R.id.liketLL, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.isLogin(true)) {
                    activity.onDianzhanLick(headData, holder.getmPosition());
                }
            }
        });
        holder.setOnClickListener(R.id.commentLL, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.isLogin(true)) {
                    activity.onPingLunLick(headData, holder.getmPosition());
                }
            }
        });
        holder.setOnClickListener(R.id.guanzhuLL, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.isLogin(true)) {
                    activity.onGuanzhuLick(headData, holder.getmPosition(), GuanZhu.getState(headData.isguanzhu()));
                }
            }
        });


    }

    public void headChanged() {
        notifyDataSetChanged();
    }

}
