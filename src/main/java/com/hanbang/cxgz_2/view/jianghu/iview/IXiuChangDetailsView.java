package com.hanbang.cxgz_2.view.jianghu.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangDetailsHttpResponse;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IXiuChangDetailsView extends BaseListView{
    void upDataUi(XiuChangDetailsHttpResponse datas);
    void headChanged();
    void pingLinSuccess();
}
