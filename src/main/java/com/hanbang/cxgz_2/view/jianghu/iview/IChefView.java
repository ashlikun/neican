package com.hanbang.cxgz_2.view.jianghu.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IChefView extends BaseListView {
    void upDataUi(List<ChefData> datas);

    int getBiaoqian();


}
