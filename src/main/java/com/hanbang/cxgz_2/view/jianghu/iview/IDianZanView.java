package com.hanbang.cxgz_2.view.jianghu.iview;

import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/10　14:42
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：点赞的view接口
 */

public interface IDianZanView {

    public void onDianzhanLick(XiuChangItemData daChuHttpResponse, int position);
}
