package com.hanbang.cxgz_2.view.jianghu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseListFragment;
import com.hanbang.cxgz_2.mode.javabean.base.BannerAdData;
import com.hanbang.cxgz_2.mode.javabean.home.ChefData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ChuShiItemData;
import com.hanbang.cxgz_2.pressenter.jianghu.DaChuPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.about_me.activity.StudioActivity;
import com.hanbang.cxgz_2.view.jianghu.adapter.DaChuAdapter;
import com.hanbang.cxgz_2.view.jianghu.iview.IDaChuView;
import com.hanbang.cxgz_2.view.widget.SuperSwipeRefreshLayout;
import com.hanbang.cxgz_2.view.widget.banner.ConvenientBanner;
import com.hanbang.cxgz_2.view.widget.banner.NetworkImageHolderView;
import com.hanbang.cxgz_2.view.widget.banner.holder.CBViewHolderCreator;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.empty_layout.OnRetryClickListion;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import java.util.ArrayList;
import java.util.List;


/**
 * 大厨Fragment
 * Created by yang on 2016/8/8.
 */
public class DaChuFragment extends BaseListFragment<IDaChuView, DaChuPresenter> implements IDaChuView
        , OnRetryClickListion
        , CBViewHolderCreator
        , SuperGridLayout.OnItemClickListener<ChefData> {


    DaChuAdapter adapter = null;


    List<BannerAdData> bannerAdDatas = new ArrayList<>();
    List<ChuShiItemData> listDatas = new ArrayList<>();
    private ConvenientBanner convenientBanner;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getHttpData();
    }


    @Override
    public int getContentView() {
        return R.layout.jianghu_chushi;
    }

    @Override
    public void initView() {
        super.initView();
        listSwipeView.getRecyclerView().setLoadMoreEnabled(false);
        convenientBanner = new ConvenientBanner(activity);
        convenientBanner.setPageIndicatorId(new int[]{R.drawable.banner_circle_default, R.drawable.banner_circle_select});
        convenientBanner.setPages(this, bannerAdDatas);
        convenientBanner.setWHBili();
        ((SuperSwipeRefreshLayout) listSwipeView.getSwipeRefreshLayout()).setViewPager(convenientBanner.getViewPager());
        listSwipeView.getRecyclerView().addHeaderView(convenientBanner);
    }

    @Override
    public Object getAdapter() {
        return adapter = new DaChuAdapter(activity, this, listDatas);
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration.Builder(activity)
                .sizeResId(R.dimen.dp_10)
                .colorResId(R.color.main_black)
                .build();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData();
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData();
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData();
    }


    @Override
    public void upDataUI(List<ChuShiItemData> datas) {
        bannerAdDatas.add(new BannerAdData(1, "http://imgsrc.baidu.com/forum/pic/item/b1c343d9f2d3572ccf18c7308a13632763d0c373.jpg"));
        bannerAdDatas.add(new BannerAdData(2, "http://img0.imgtn.bdimg.com/it/u=891814847,3397298689&fm=21&gp=0.jpg"));
        bannerAdDatas.add(new BannerAdData(3, "http://img3.imgtn.bdimg.com/it/u=2286035086,4168118241&fm=21&gp=0.jpg"));
        bannerAdDatas.add(new BannerAdData(4, "http://img4.imgtn.bdimg.com/it/u=3806360125,3446946955&fm=21&gp=0.jpg"));


        convenientBanner.notifyDataSetChanged();
        listDatas.addAll(datas);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void clearData() {
        bannerAdDatas.clear();
        listDatas.clear();
    }

    @Override
    public Object createHolder() {
        return new NetworkImageHolderView() {

            @Override
            public void onItemClicklistener(View item, int position, BannerAdData data) {

            }
        };
    }

    @Override
    public DaChuPresenter initPressenter() {
        return new DaChuPresenter();
    }


//    @OnClick({R.id.hycxLL, R.id.cjtdLL, R.id.gfcx1LL, R.id.gfcx2LL, R.id.gfcx3LL})
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.hycxLL:
//                break;
//            case R.id.cjtdLL:
//                break;
//            case R.id.gfcx1LL:
//                break;
//            case R.id.gfcx2LL:
//                break;
//            case R.id.gfcx3LL:
//                break;
//        }
//    }

    @Override
    public void onItemClick(GridLayout parent, View view, int position, ChefData data) {

        StudioActivity.startUI(activity, data.getId(), StudioActivity.CHUSHI);
    }


    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.getSwipeRefreshLayout();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onLoadding() {

    }
}
