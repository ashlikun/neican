package com.hanbang.cxgz_2.view.jianghu.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseSwipeView;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ZhangGuiItemData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IZhangguiView extends BaseSwipeView {

    void upDataUI(List<ZhangGuiItemData> datas);
}
