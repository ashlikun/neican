package com.hanbang.cxgz_2.view.jianghu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseListFragment;
import com.hanbang.cxgz_2.mode.enumeration.GuanZhu;
import com.hanbang.cxgz_2.mode.httpresponse.jianghu.XiuChangHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.base.BannerAdData;
import com.hanbang.cxgz_2.mode.javabean.base.KeyAndValus;
import com.hanbang.cxgz_2.mode.javabean.jianghu.XiuChangItemData;
import com.hanbang.cxgz_2.pressenter.jianghu.XiuChangPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.jianghu.activity.XiuChangDetailsActivity;
import com.hanbang.cxgz_2.view.jianghu.adapter.XiuChangAdapter;
import com.hanbang.cxgz_2.view.jianghu.iview.IDianZanView;
import com.hanbang.cxgz_2.view.jianghu.iview.IGuanZhuView;
import com.hanbang.cxgz_2.view.jianghu.iview.IPingLunView;
import com.hanbang.cxgz_2.view.jianghu.iview.IXiuChangView;
import com.hanbang.cxgz_2.view.widget.TabSelect;
import com.hanbang.cxgz_2.view.widget.banner.ConvenientBanner;
import com.hanbang.cxgz_2.view.widget.banner.NetworkImageHolderView;
import com.hanbang.cxgz_2.view.widget.banner.holder.CBViewHolderCreator;
import com.hanbang.cxgz_2.view.widget.dialog.DialogComment;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import butterknife.BindView;


/**
 * 作者　　: 李坤
 * 创建时间: 11:48 Administrator
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：秀场主界面
 */


public class XiuChangFragment extends BaseListFragment<IXiuChangView, XiuChangPresenter> implements OnItemClickListener<XiuChangItemData>,
        IXiuChangView, CBViewHolderCreator, IDianZanView, IGuanZhuView, IPingLunView {
    @BindView(R.id.tabSelect)
    TabSelect tabSelect;


    @Override
    public int getContentView() {
        return R.layout.jianghu_xiuchang;
    }

    ConvenientBanner convenientBanner;
    XiuChangAdapter adapter = null;
    XiuChangHttpResponse data = XiuChangHttpResponse.init();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getHttpData(true);
    }

    @Override
    public void initView() {
        adapter = new XiuChangAdapter(activity, this, data.getDongtaiList());
        convenientBanner = new ConvenientBanner(activity);
        convenientBanner.setWHBili();
        convenientBanner.setPageIndicator(new int[]{R.drawable.banner_circle_default, R.drawable.banner_circle_select});
        convenientBanner.setPages(this, data.getBannerAdDatas());
        listSwipeView.getRecyclerView().addHeaderView(convenientBanner);
        super.initView();
        tabSelect.setContent(new KeyAndValus(4, "全部"),
                new KeyAndValus(1, "我关注的全部"),
                new KeyAndValus(2, "我关注的厨师"),
                new KeyAndValus(3, "我关注的经纪人"));

        tabSelect.setOnItemClickListener(new TabSelect.OnItemClickListener() {
            @Override
            public void onItemClick(int key, int position, String text) {
                presenter.getHttpData(true, key);
            }
        });
        adapter.setOnItemClickListener(this);
    }

    @Override
    public Object getAdapter() {
        return adapter;
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new HorizontalDividerItemDecoration
                .Builder(activity)
                .sizeResId(R.dimen.dp_10)
                .colorResId(R.color.main_black)
                .diaableFirstLastDivider()
                .build();
    }

    @Override
    public void clearData() {
        data.clearAll();
    }


    @Override
    public void onRefresh() {
        presenter.getHttpData(true);
    }

    @Override
    public XiuChangPresenter initPressenter() {
        return new XiuChangPresenter();
    }

    @Override
    public void upDataUI(XiuChangHttpResponse xiuChangHttpResponse) {
        data.addData(xiuChangHttpResponse);
        convenientBanner.notifyDataSetChanged();
        adapter.notifyDataSetChanged();
        listSwipeView.getRecyclerView().getItemAnimator().setChangeDuration(0);

    }


    @Override
    public void onLoadding() {
        presenter.getHttpData(false);
    }

    @Override
    public Object createHolder() {
        return new NetworkImageHolderView() {
            @Override
            public void onItemClicklistener(View item, int position, BannerAdData data) {

            }
        };
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true);
    }


    @Override
    public void onItemClick(ViewGroup parent, View view, XiuChangItemData data, int position) {
        XiuChangDetailsActivity.startUi(activity, data.getId());
    }


    @Override
    public void itemChanged(int position) {
        adapter.itemChanged(position);
    }


    public void onDianzhanLick(XiuChangItemData daChuHttpResponse, int position) {
        presenter.dianzhan(daChuHttpResponse, position);
    }


    public void onGuanzhuLick(XiuChangItemData daChuHttpResponse, int position, GuanZhu isGuanzhu) {
        presenter.guanzhu(daChuHttpResponse, position, isGuanzhu);
    }

    @Override
    public void onPingLunLick(final XiuChangItemData daChuHttpResponse, final int position) {
        DialogComment dialogComment = new DialogComment(activity);
        dialogComment.setSendCallback(new DialogComment.OnSendCallback() {
            @Override
            public void onSend(String content) {
                presenter.pinglun(daChuHttpResponse, position, content);
            }
        });
        dialogComment.show();
    }
}
