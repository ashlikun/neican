package com.hanbang.cxgz_2.view.other;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.view.widget.TouTingMusicPlayView;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yang on 2016/9/3.
 */

public class TestActivity extends Activity {


    @BindView(R.id.activity_homepage_GridView_hotFigure)
    SuperGridLayout gridView;

    public static void startUI(Activity activity) {
        Intent intent = new Intent(activity, TestActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aaa);
        ButterKnife.bind(this);

        ((TouTingMusicPlayView) findViewById(R.id.touting)).setBuy(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                UiUtils.showInput(edit_query);
//            }
//        }, 1000);  //在一秒后打开
//        Timer timer2 = new Timer();
//        timer2.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                UiUtils.exitInput(TestActivity.this);
//            }
//        }, 3000);  //在一秒后打开
    }

    @OnClick(R.id.sss)
    public void testListener(View v) {

        List<String> list = new ArrayList<>();
        CommonAdapter adapter = null;

        gridView.setAdapter(adapter = new CommonAdapter<String>(TestActivity.this, R.layout.item_homepage_answer, list) {
            @Override
            public void convert(ViewHolder holder, String o) {
                holder.setText(R.id.item_homepage_answer_question, o);
            }

        });

        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        adapter.notifyDataSetChanged();


    }


}
