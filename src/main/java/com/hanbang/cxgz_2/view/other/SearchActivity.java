package com.hanbang.cxgz_2.view.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.SearchEnum;
import com.hanbang.cxgz_2.mode.javabean.other.SearchData;
import com.hanbang.cxgz_2.pressenter.othre.SearchPresenter;
import com.hanbang.cxgz_2.view.other.iview.ISearchView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;

/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/19 13:23
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：搜索的页面
 */

public class SearchActivity extends BaseMvpActivity<ISearchView, SearchPresenter> implements ISearchView {

    @BindView(R.id.activity_search_searchClassify)
    GridView searchClassify;

    private ArrayList<SearchData> datas = new ArrayList<>();
    //记录传过来的搜索类型，默认为资讯
    private SearchEnum type;

    @Override
    public int getContentView() {
        return R.layout.activity_search;
    }

    public static void startUi(Context context,  SearchEnum type) {
        Intent intent = new Intent(context, SearchActivity.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("type",type);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }


    @Override
    public void parseIntent(Intent intent) {
        type = (SearchEnum) intent.getSerializableExtra("type");
    }


    @Override
    public void initView() {
        toolbar.addAction(1, "取消");

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                finish();
                return false;
            }
        });


        datas.addAll(presenter.getDatas(this));
        searchClassify.setAdapter(adapter);

        for (int i = 0; i < datas.size(); i++) {
            if (type.getValuse().equals(datas.get(i).getName())) {
                datas.get(i).setSelect(true);
            } else {
                datas.get(i).setSelect(false);
            }
        }


    }


    @Override
    public StatusChangListener getStatusChangListener() {
        return null;
    }

    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {
        return null;
    }

    @Override
    public void clearPagingData() {

    }

    @Override
    public int getPageindex() {
        return 0;
    }

    @Override
    public int getPageCount() {
        return 0;
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return null;
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void clearData() {

    }

    @Override
    public SearchPresenter initPressenter() {
        return new SearchPresenter();
    }


    CommonAdapter adapter = new CommonAdapter<SearchData>(this, R.layout.item_search, datas) {

        @Override
        public void convert(final ViewHolder holder, final SearchData item) {
            holder.setText(R.id.item_search_name, item.getName());
            if (item.isSelect()) {
                holder.setTextColor(R.id.item_search_name, SearchActivity.this.getResources().getColor(R.color.main_color));
                holder.setBackgroundColor(R.id.item_search_name, SearchActivity.this.getResources().getColor(R.color.black));
            } else {
                holder.setTextColor(R.id.item_search_name, SearchActivity.this.getResources().getColor(R.color.black));
                holder.setBackgroundColor(R.id.item_search_name, SearchActivity.this.getResources().getColor(R.color.gray_ee));
            }
            holder.getmPosition();

            holder.setOnClickListener(R.id.item_search_name, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < datas.size(); i++) {
                        if (i == holder.getmPosition()) {
                            datas.get(i).setSelect(true);
                        } else {
                            datas.get(i).setSelect(false);
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            });
        }

    };


    @Override
    public void upDataUi() {

    }
}
