package com.hanbang.cxgz_2.view.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.mode.enumeration.AgreementEnum;
import com.hanbang.cxgz_2.utils.ui.WebViewUtils;

import butterknife.BindView;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/22 11:35
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：各种协议的页面
 */

public class AgreementActivity extends BaseActivity {
    AgreementEnum type;
    @BindView(R.id.activity_web_view)
    WebView webView;

    public static void startUI(Context context, AgreementEnum type) {
        Intent intent = new Intent(context, AgreementActivity.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("type", type);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    public void initView() {
        toolbar.setTitle(type.getValuse());
        toolbar.setBack(this);
        WebViewUtils.configWebview(webView);
        webView.loadUrl(type.getUrl());
    }

    @Override
    public void parseIntent(Intent intent) {
        type = (AgreementEnum) getIntent().getSerializableExtra("type");
    }

    @Override
    public int getContentView() {
        return R.layout.activity_web_view;
    }


}
