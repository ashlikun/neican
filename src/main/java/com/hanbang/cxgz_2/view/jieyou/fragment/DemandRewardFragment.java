package com.hanbang.cxgz_2.view.jieyou.fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.GridLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseSwipeFragment;
import com.hanbang.cxgz_2.mode.enumeration.RewardTypeEnum;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeRewardHttpResponse;
import com.hanbang.cxgz_2.pressenter.jieyou.HomeDemandRewardPresenter;
import com.hanbang.cxgz_2.view.jieyou.activity.DemandRewardClassifyActivity;
import com.hanbang.cxgz_2.view.jieyou.activity.DemandRewardDetailsActivity;
import com.hanbang.cxgz_2.view.jieyou.adapter.DemandReswardAdapter;
import com.hanbang.cxgz_2.view.jieyou.iview.IHomeDemandRewardView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 解忧商铺-悬赏需求
 */
public class DemandRewardFragment extends BaseSwipeFragment<IHomeDemandRewardView, HomeDemandRewardPresenter> implements IHomeDemandRewardView {
    //分类
    @BindView(R.id.fragment_reward_homepagerr_classify)
    SuperGridLayout classify;
    //菜品研发
    @BindView(R.id.fragment_reward_homepagerr_gridViewCPYF)
    SuperGridLayout caiPinYanFa;
    //招聘
    @BindView(R.id.fragment_reward_homepagerr_gridViewZP)
    SuperGridLayout zaoPin;
    //求职
    @BindView(R.id.fragment_reward_homepagerr_gridViewQZ)
    SuperGridLayout quZhi;
    //店铺转让
    @BindView(R.id.fragment_reward_homepagerr_gridViewDPZR)
    SuperGridLayout dianPuZuanRang;
    //二手设备
    @BindView(R.id.fragment_reward_homepagerr_gridViewESSB)
    SuperGridLayout erShouSheBei;
    //加盟代理
    @BindView(R.id.fragment_reward_homepagerr_gridViewJMDJ)
    SuperGridLayout jiaMengDaiLi;

    private HomeRewardHttpResponse esotericaResponse = HomeRewardHttpResponse.init();
    private DemandReswardAdapter adapter = null;


    public static DemandRewardFragment newInstance() {
        DemandRewardFragment listFragment = new DemandRewardFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_reward_homepager;
    }

    @Override
    public void initView() {
        super.initView();
        if (esotericaResponse.getDishesdevelop().size() == 0) {
            presenter.getHttpData();
        }
        setAdapter();

    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    @Override
    public void onRefresh() {

        presenter.getHttpData();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData();
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData();
    }


    @Override
    public void upDataUi(HomeRewardHttpResponse datas) {
        esotericaResponse.clear();
        esotericaResponse.addData(datas);
        adapter.notifyDataSetChanged();
    }


    @Override
    public HomeDemandRewardPresenter initPressenter() {
        return new HomeDemandRewardPresenter();
    }


    @Override
    public void clearData() {

    }


    public void setAdapter() {
        adapter = new DemandReswardAdapter(activity, esotericaResponse);
        classify.setAdapter(adapter.classify);
        caiPinYanFa.setAdapter(adapter.caiPinYanFa);
        zaoPin.setAdapter(adapter.zaoPin);
        quZhi.setAdapter(adapter.quZhi);
        dianPuZuanRang.setAdapter(adapter.dianPuZuanRang);
        erShouSheBei.setAdapter(adapter.erShouSheBei);
        jiaMengDaiLi.setAdapter(adapter.jiaMengDaiLi);



        classify.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardClassifyActivity.startUi(getActivity(), adapter.classifyDatas.get(position).getTitle());
            }
        });


        caiPinYanFa.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardDetailsActivity.startUI(activity, esotericaResponse.getDishesdevelop().get(position).getID());
            }
        });



        zaoPin.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardDetailsActivity.startUI(activity, esotericaResponse.getAdvertises().get(position).getID());
            }
        });


        quZhi.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardDetailsActivity.startUI(activity, esotericaResponse.getCandidate().get(position).getID());
            }
        });

        dianPuZuanRang.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardDetailsActivity.startUI(activity, esotericaResponse.getShopassignment().get(position).getID());
            }
        });

        erShouSheBei.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardDetailsActivity.startUI(activity, esotericaResponse.getUsedplant().get(position).getID());
            }
        });


        jiaMengDaiLi.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                DemandRewardDetailsActivity.startUI(activity, esotericaResponse.getAgentproxy().get(position).getID());
            }
        });

    }


    @OnClick(value = {
            R.id.fragment_reward_homepagerr_CPYF, R.id.fragment_reward_homepagerr_ZP, R.id.fragment_reward_homepagerr_QZ,
            R.id.fragment_reward_homepagerr_DPZR, R.id.fragment_reward_homepager_ESSB, R.id.fragment_reward_homepager_JMDJ,

    })
    public void onClickLister(View view) {
        switch (view.getId()) {
            case R.id.fragment_reward_homepagerr_CPYF:
                DemandRewardClassifyActivity.startUi(getActivity(), RewardTypeEnum.CAI_PIN_YAN_FA);
                break;
            case R.id.fragment_reward_homepagerr_ZP:
                DemandRewardClassifyActivity.startUi(getActivity(), RewardTypeEnum.ZHAO_PIN);
                break;
            case R.id.fragment_reward_homepagerr_QZ:
                DemandRewardClassifyActivity.startUi(getActivity(), RewardTypeEnum.QIU_ZHI);
                break;
            case R.id.fragment_reward_homepagerr_DPZR:
                DemandRewardClassifyActivity.startUi(getActivity(), RewardTypeEnum.DIAN_PU_ZHUAN_RANG);
                break;
            case R.id.fragment_reward_homepager_ESSB:
                DemandRewardClassifyActivity.startUi(getActivity(), RewardTypeEnum.ER_SHOU_SHE_BEI);
                break;
            case R.id.fragment_reward_homepager_JMDJ:
                DemandRewardClassifyActivity.startUi(getActivity(), RewardTypeEnum.JIA_MENG_DAI_LI);
                break;
        }
    }


}
