package com.hanbang.cxgz_2.view.jieyou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseSwipeActivity;
import com.hanbang.cxgz_2.mode.javabean.jianghu.StudyClassroomDetailsData;
import com.hanbang.cxgz_2.pressenter.jieyou.StudyClassroomDetailsPresenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.ToastUtils;
import com.hanbang.cxgz_2.view.jieyou.iview.IStudyClassroomDetailsView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogComment;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;
import com.hanbang.videoplay.view.VideoPlayer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/12 11:40
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：学习课堂详情页
 */

public class StudyClassroomDetailsActivity extends BaseSwipeActivity<IStudyClassroomDetailsView, StudyClassroomDetailsPresenter> implements IStudyClassroomDetailsView {
    @BindView(R.id.activity_study_classroom_details_haiBao)
    ImageView haiBao;
    @BindView(R.id.activity_study_classroom_details_title)
    TextView title;
    @BindView(R.id.activity_study_classroom_details_time)
    TextView time;
    @BindView(R.id.activity_study_classroom_details_address)
    TextView address;
    @BindView(R.id.activity_study_classroom_details_price)
    TextView price;
    @BindView(R.id.activity_study_classroom_details_portrait)
    ImageView portrait;
    @BindView(R.id.activity_study_classroom_details_name)
    TextView name;
    @BindView(R.id.activity_study_classroom_details_job)
    TextView job;
    @BindView(R.id.activity_study_classroom_details_company)
    TextView company;
    @BindView(R.id.activity_study_classroom_details_zhaoSengRenShu)
    TextView zhaoSengRenShu;
    @BindView(R.id.activity_study_classroom_details_baoMingRenShu)
    TextView baoMingRenShu;
    @BindView(R.id.activity_study_classroom_details_remark)
    TextView remark;
    @BindView(R.id.activity_study_classroom_details_teacherBrief)
    TextView teacherBrief;
    @BindView(R.id.activity_study_classroom_details_classroomBrief)
    TextView classroomBrief;
    @BindView(R.id.activity_study_classroom_details_gouChengTuPian)
    SuperGridLayout gouChengTuPian;


    ArrayList<String> gouChengTuPianPath = new ArrayList<>();

    private int id = -1;

    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    public int getStatusBarResource() {
        return R.color.transparency;
    }

    @Override
    public int getContentView() {
        return R.layout.activity_study_classroom_details;
    }

    public static void startUI(Context context, int id) {
        Intent intent = new Intent(context, StudyClassroomDetailsActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void parseIntent(Intent intent) {
        id = intent.getIntExtra("id", -1);
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setBack(this);
        toolbar.setTitle("线下课堂");

        presenter.getHttpData(id);


    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    CommonAdapter adapter = new CommonAdapter<String>(this, R.layout.item_esoterca_liu_cheng_tu_pian, gouChengTuPianPath) {
        @Override
        public void convert(ViewHolder holder, String o) {
            holder.setImageBitmap(R.id.item_esoterca_liu_cheng_tu_pian_picture, o);
            holder.getView(R.id.item_esoterca_liu_cheng_tu_pian_title).setVisibility(View.INVISIBLE);

        }


    };

    @Override
    public void onRefresh() {
        presenter.getHttpData(id);
    }

    @Override
    public void clearData() {

    }

    @Override
    public StudyClassroomDetailsPresenter initPressenter() {
        return new StudyClassroomDetailsPresenter();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(id);
    }


    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onBackPressed() {
        if (VideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    /**
     * @param data
     */
    @Override
    public void upDataUi(StudyClassroomDetailsData data) {
        GlideUtils.show(haiBao, data.getImg_url());
        title.setText(data.getTitle());
        if (data.getBegin_time().equals("随到随学")) {
            time.setText("时间：" + data.getBegin_time());
        } else {
            time.setText("时间：" + data.getBegin_time() + "--\t" + data.getEnd_time());
        }
        address.setText(data.getAddress());
        price.setText("￥" + data.getPrice());
        GlideUtils.showCircle(portrait, data.getAvatar());
        name.setText(data.getUser_name());
        job.setText(data.getJob_CN());
        company.setText(data.getCompany());
        zhaoSengRenShu.setText("招生人数：" + StringUtils.isNullToConvert(data.getZhaosrenshu()));
        baoMingRenShu.setText(StringUtils.isNullToConvert(data.getBaomingCount()));
        remark.setText(data.getTimeRemark());
        teacherBrief.setText(data.getWorkjingli());
        classroomBrief.setText(data.getRemark());

        gouChengTuPianPath.clear();
        gouChengTuPianPath.addAll(presenter.transitionPicture(data.getContent_img()));
        gouChengTuPian.setAdapter(adapter);

    }

    @OnClick({R.id.activity_study_classroom_details_lookComment, R.id.activity_study_classroom_details_comment})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_study_classroom_details_lookComment:
                break;
            case R.id.activity_study_classroom_details_comment:
                DialogComment comment = new DialogComment(this);

                comment.setSendCallback(new DialogComment.OnSendCallback() {
                    @Override
                    public void onSend(String content) {
                        ToastUtils.getToastShort(content);
                    }
                });
                comment.show();
                break;
        }
    }
}
