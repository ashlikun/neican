package com.hanbang.cxgz_2.view.jieyou.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeEsotericHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.mode.javabean.home.JieYouEsotericaData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * Created by Administrator on 2016/8/17.
 */

public class EsotericaAdapter {
    private Context context;
    private HomeEsotericHttpResponse homeHttpRequest;

    //1 //爆款精品
    public CommonAdapter gridViewCLJX = null;
    //2:潮流精选
    public CommonAdapter gridViewBKJP = null;
    //2高毛利
    public CommonAdapter gridViewGML = null;
    //3大师经典
    public CommonAdapter gridViewDSJD = null;
    //4买断秘籍
    public CommonAdapter gridViewMDMJ = null;
    //5视频秘籍
    public CommonAdapter gridViewSPMJ = null;
    //6分类搜索
    public CommonAdapter gridViewFLSS = null;


    public EsotericaAdapter(Context context, HomeEsotericHttpResponse homeHttpRequest) {
        this.homeHttpRequest = homeHttpRequest;
        this.context = context;
        initAdapter();
    }


    public void notifyDataSetChanged() {
        gridViewCLJX.notifyDataSetChanged();
        gridViewBKJP.notifyDataSetChanged();
        gridViewGML.notifyDataSetChanged();
        gridViewDSJD.notifyDataSetChanged();
        gridViewMDMJ.notifyDataSetChanged();
        gridViewSPMJ.notifyDataSetChanged();
        gridViewFLSS.notifyDataSetChanged();
    }

    private void initAdapter() {
        //爆款精品
        gridViewBKJP = new CommonAdapter<JieYouEsotericaData>(context, R.layout.item_allay_shop_esoterca, homeHttpRequest.getBaokuan()) {
            @Override
            public void convert(ViewHolder helper, JieYouEsotericaData item) {
                try {
                    if (!StringUtils.isBlank(item.getImg_urlSmallurl())) {
                        helper.setImageBitmap(R.id.item_allay_shop_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_urlSmallurl())));
                    } else {
                        helper.setImageBitmap(R.id.item_allay_shop_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    }

                    if (!StringUtils.isBlank(item.getImg_urlSmallurl())) {
                        helper.setImageBitmapCircle(R.id.item_allay_shop_esoterca_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_urlSmallurl())));
                    } else {
                        helper.setImageBitmapCircle(R.id.item_allay_shop_esoterca_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    }
                    helper.setText(R.id.item_allay_shop_esoterca_name, StringUtils.isNullToConvert(item.getUser_name()));
                    helper.setText(R.id.item_allay_shop_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    helper.setText(R.id.item_allay_shop_esoterca_grade, "评分:" + StringUtils.isNullToConvert(item.getPingfen()) + "分");
                    helper.setText(R.id.item_allay_shop_esoterca_read, "销量:" + StringUtils.isNullToConvert(item.getBuycount()));
                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_allay_shop_esoterca_price, "免费");
                    } else {
                        helper.setText(R.id.item_allay_shop_esoterca_price, "￥" + StringUtils.isNullToConvert(item.getPrice(), 2));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        };


        //潮流精选
        gridViewCLJX = new CommonAdapter<EsotericaData>(context, R.layout.item_homepage_esoterca, homeHttpRequest.getFashionChoice()) {
            @Override
            public void convert(ViewHolder holder, EsotericaData item) {
                try {
                    holder.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    holder.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    holder.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
                    holder.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
                    if (item.getPrice() == 0) {
                        holder.setText(R.id.item_homepage_esoterca_price, "免费");
                    } else {
                        holder.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //高毛利
        gridViewGML = new CommonAdapter<EsotericaData>(context, R.layout.item_homepage_esoterca, homeHttpRequest.getHighMaoli()) {
            @Override
            public void convert(ViewHolder helper, EsotericaData item) {
                try {
                    helper.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    helper.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    helper.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
                    helper.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_homepage_esoterca_price, "免费");
                    } else {
                        helper.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //大师经典
        gridViewDSJD = new CommonAdapter<EsotericaData>(context, R.layout.item_homepage_esoterca, homeHttpRequest.getDashiclassic()) {
            @Override
            public void convert(ViewHolder helper, EsotericaData item) {
                try {
                    helper.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    helper.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    helper.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
                    helper.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_homepage_esoterca_price, "免费");
                    } else {
                        helper.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //买断秘籍
        gridViewMDMJ = new CommonAdapter<EsotericaData>(context, R.layout.item_homepage_esoterca, homeHttpRequest.getBuyduan()) {
            @Override
            public void convert(ViewHolder helper, EsotericaData item) {
                try {
                    helper.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    helper.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    helper.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
                    helper.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_homepage_esoterca_price, "免费");
                    } else {
                        helper.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //视频秘籍
        gridViewSPMJ = new CommonAdapter<EsotericaData>(context, R.layout.item_homepage_esoterca, homeHttpRequest.getVidoMiji()) {
            @Override
            public void convert(ViewHolder helper, EsotericaData item) {
                try {
                    helper.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    helper.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    helper.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
                    helper.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_homepage_esoterca_price, "免费");
                    } else {
                        helper.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //分类搜索
        gridViewFLSS = new CommonAdapter<EsotericaData>(context, R.layout.item_homepage_esoterca, homeHttpRequest.getMiJiSourceNewToTime()) {
            @Override
            public void convert(ViewHolder helper, EsotericaData item) {
                try {
                    helper.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    helper.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    helper.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
                    helper.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_homepage_esoterca_price, "免费");
                    } else {
                        helper.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

    }



}
