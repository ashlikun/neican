package com.hanbang.cxgz_2.view.jieyou.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.jianghu.DemandRewardDetailsData;
import com.hanbang.cxgz_2.utils.ui.DrawableUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.jieyou.activity.DemandRewardDetailsActivity;
import com.hanbang.cxgz_2.view.widget.autoloadding.RecyclerViewWithHeaderAndFooter;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import java.util.ArrayList;

/**
 * Created by yang on 2016/9/13.
 */

public class DemandRewardDetailsAdapter extends CommonAdapter<DemandRewardDetailsData.Participation> {
    DemandRewardDetailsActivity activity;
    DemandRewardDetailsData datas;

    public DemandRewardDetailsAdapter(final DemandRewardDetailsActivity activity, final RecyclerViewWithHeaderAndFooter recyclerView,
                                      ArrayList<DemandRewardDetailsData.Participation> gouChengTuPianPath) {

        super(activity, R.layout.item_demand_reward_participation, gouChengTuPianPath);
        this.activity = activity;

        final View view = UiUtils.getInflaterView(activity, R.layout.activity_demand_reward_details_head, recyclerView, false);
        recyclerView.addHeaderView(view);

        //注册数据源发生变化
        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if (datas != null) {
                    convertHeader(activity,new ViewHolder(mContext, view, null, 0), datas);
                }
            }
        });
    }

    /**
     * item 赋值
     *
     * @param holder
     * @param data
     */
    @Override
    public void convert(ViewHolder holder, DemandRewardDetailsData.Participation data) {
        holder.setImageBitmapCircle(R.id.item_demand_reward_participation_portrait, data.getAvatar());
        holder.setText(R.id.item_demand_reward_participation_title, data.getUser_name());
        holder.setText(R.id.item_demand_reward_participation_time, data.getCjsj());
        holder.setText(R.id.item_demand_reward_participation_participation, data.getCONTENT());
    }

    /**
     * 头部数据的赋值
     *
     * @param holder
     * @param data
     */
    public void convertHeader(DemandRewardDetailsActivity activity,final ViewHolder holder, DemandRewardDetailsData data) {
        holder.setImageBitmapCircle(R.id.activity_demand_reward_details_head_portrait, data.getAvatar());
        ((TextView) holder.getView(R.id.activity_demand_reward_details_head_status)).setBackgroundDrawable(
                DrawableUtils.getGradientDrawable(R.color.SwipeRefreshLayout_3, 0, 3, 5));
        holder.setText(R.id.activity_demand_reward_details_head_name, data.getUser_name());
        holder.setText(R.id.activity_demand_reward_details_head_jobAndCompany, data.getJobCN() + "|" + data.getCompany());
        holder.setText(R.id.activity_demand_reward_details_head_price, data.getXunShangPrice());
        holder.setText(R.id.activity_demand_reward_details_head_participation, data.getJoinCount());
        holder.setText(R.id.activity_demand_reward_details_head_status, data.getStatusCN());
        holder.setText(R.id.activity_demand_reward_details_head_date, data.getCjsj());
        holder.setText(R.id.activity_demand_reward_details_head_content, data.getXuQiuContent());

        ((SuperGridLayout)holder.getView(R.id.activity_demand_reward_details_head_picture)).setAdapter(new CommonAdapter<String>(activity,R.layout.item_esoterca_liu_cheng_tu_pian,data.getXuQiuImgList()) {
            @Override
            public void convert(ViewHolder holder, String o) {
                holder.setImageBitmap(R.id.item_esoterca_liu_cheng_tu_pian_picture, o);
                holder.getView(R.id.item_esoterca_liu_cheng_tu_pian_title).setVisibility(View.INVISIBLE);
            }


        });


    }

    public void notifyDataSetChangedAll(DemandRewardDetailsData datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }


}
