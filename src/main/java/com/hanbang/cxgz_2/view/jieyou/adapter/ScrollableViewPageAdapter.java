package com.hanbang.cxgz_2.view.jieyou.adapter;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.hanbang.cxgz_2.view.adapter.SectionsPagerAdapter;

import java.util.ArrayList;

/**
 * Created by yang on 2016/8/13.
 */
public class ScrollableViewPageAdapter {
    /**
     * 显示对应的fragment
     *
     * @param viewPager
     */
    public static void initFragmentPager(FragmentManager fm, ViewPager viewPager, final ArrayList<Fragment> fragmentList, TabLayout tabLayout, String[] title, int index) {
        //为viewPage设置adapter
        SectionsPagerAdapter mAdapter = new SectionsPagerAdapter(fm, fragmentList, title);
        viewPager.setAdapter(mAdapter);
        //tabLayout关联ViewPage
        tabLayout.setupWithViewPager(viewPager);
        //显示页面设置
        viewPager.setCurrentItem(index);
        //viewPage页面改的监听
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }


}
