package com.hanbang.cxgz_2.view.jieyou.fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.GridLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseSwipeFragment;
import com.hanbang.cxgz_2.mode.enumeration.ClassroomClassifyEnum;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeStudyHttpResponse;
import com.hanbang.cxgz_2.pressenter.jieyou.StudyClassroomFragmentPresenter;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;
import com.hanbang.cxgz_2.view.jieyou.activity.StudyClassroomClassifyActivity;
import com.hanbang.cxgz_2.view.jieyou.activity.StudyClassroomDetailsActivity;
import com.hanbang.cxgz_2.view.jieyou.adapter.StudyAdapter;
import com.hanbang.cxgz_2.view.jieyou.iview.IStudyClassFragmentView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import butterknife.BindView;
import butterknife.OnClick;

public class StudyClassroomFragment extends BaseSwipeFragment<IStudyClassFragmentView, StudyClassroomFragmentPresenter> implements IStudyClassFragmentView {
    @BindView(R.id.fragment_study_homepager_gridViewYYKT)
    SuperGridLayout gridViewYYKT;
    @BindView(R.id.fragment_study_homepager_gridViewXXKT)
    SuperGridLayout gridViewXXKT;

    private HomeStudyHttpResponse esotericaResponse = new HomeStudyHttpResponse();
    private StudyAdapter adapter = null;


    public static StudyClassroomFragment newInstance() {
        StudyClassroomFragment listFragment = new StudyClassroomFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_study_homepager;
    }

    @Override
    public void initView() {
        super.initView();
        if (esotericaResponse.getGroupKeTang().size() == 0) {
            presenter.getHttpData();
        }
        setAdapter();

    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    @Override
    public void onRefresh() {

        presenter.getHttpData();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData();
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData();
    }


    @Override
    public void upDataUi(HomeStudyHttpResponse datas) {
        esotericaResponse.clear();
        esotericaResponse.addData(datas);
        adapter.notifyDataSetChanged();

    }


    @Override
    public StudyClassroomFragmentPresenter initPressenter() {
        return new StudyClassroomFragmentPresenter();
    }


    @Override
    public void clearData() {

    }


    public void setAdapter() {
        adapter = new StudyAdapter(activity, esotericaResponse);
        gridViewYYKT.setAdapter(adapter.gridViewYYKT);
        gridViewXXKT.setAdapter(adapter.gridViewXXKT);


        gridViewYYKT.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                SnackbarUtil.showShort(getActivity(), "还没有添加", SnackbarUtil.Info).show();

            }
        });


        gridViewXXKT.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {

                StudyClassroomDetailsActivity.startUI(getContext(), esotericaResponse.getLearnClass().get(position).getId());

            }
        });
    }


    @OnClick(value = {R.id.fragment_study_homepager_YYKT, R.id.fragment_esoterica_homepage_XXKT})
    public void onClickLister(View view) {
        switch (view.getId()) {
            case R.id.fragment_study_homepager_YYKT:
                StudyClassroomClassifyActivity.startUi(getActivity(), ClassroomClassifyEnum.YU_YIN);
                break;
            case R.id.fragment_esoterica_homepage_XXKT:
                StudyClassroomClassifyActivity.startUi(getActivity(), ClassroomClassifyEnum.XIAN_XIA);
                break;
        }
    }


}
