package com.hanbang.cxgz_2.view.jieyou.fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.GridLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.fragment.BaseSwipeFragment;
import com.hanbang.cxgz_2.mode.enumeration.EsotericClassifyEnum;
import com.hanbang.cxgz_2.mode.enumeration.SearchEnum;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeEsotericHttpResponse;
import com.hanbang.cxgz_2.pressenter.jieyou.HomeEsotericPresenter;
import com.hanbang.cxgz_2.view.jieyou.activity.EsotericClassifyActivity;
import com.hanbang.cxgz_2.view.jieyou.activity.EsotericDetails_D_Activity;
import com.hanbang.cxgz_2.view.jieyou.activity.EsotericDetails_K_Activity;
import com.hanbang.cxgz_2.view.jieyou.adapter.EsotericaAdapter;
import com.hanbang.cxgz_2.view.jieyou.iview.IHomeEsotericView;
import com.hanbang.cxgz_2.view.other.SearchActivity;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.gridlayout.SuperGridLayout;

import butterknife.BindView;
import butterknife.OnClick;

public class EsotericaFragment extends BaseSwipeFragment<IHomeEsotericView, HomeEsotericPresenter> implements IHomeEsotericView {
    @BindView(R.id.fragment_esoterica_homepager_viewParent)
    View view;
    //爆款精品
    @BindView(R.id.fragment_esoterica_homepager_gridViewBKJP)
    SuperGridLayout gridViewBKJP;
    //潮流精选
    @BindView(R.id.fragment_esoterica_homepager_gridViewCLJX)
    SuperGridLayout gridViewCLJX;
    //高毛利
    @BindView(R.id.fragment_esoterica_homepager_gridViewGML)
    SuperGridLayout gridViewGML;
    //大师经典
    @BindView(R.id.fragment_esoterica_homepager_gridViewDSJD)
    SuperGridLayout gridViewDSJD;
    //买断秘籍
    @BindView(R.id.fragment_esoterica_homepager_gridViewMDMJ)
    SuperGridLayout gridViewMDMJ;
    //视频秘籍
    @BindView(R.id.fragment_esoterica_homepager_gridViewSPMJ)
    SuperGridLayout gridViewSPMJ;
    //分类搜索
    @BindView(R.id.fragment_esoterica_homepager_gridViewFLSS)
    SuperGridLayout gridViewFLSS;

    private HomeEsotericHttpResponse esotericaResponse = new HomeEsotericHttpResponse();
    private EsotericaAdapter adapter = null;


    public static EsotericaFragment newInstance() {
        EsotericaFragment listFragment = new EsotericaFragment();
        return listFragment;
    }


    @Override
    public int getContentView() {
        return R.layout.fragment_esoterica_homepager;
    }

    @Override
    public void initView() {
        super.initView();
        if (esotericaResponse.getBaokuan().size() == 0) {
            presenter.getHttpData();
        }
        setAdapter();
        setListener();
    }


    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData();
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData();
    }


    @Override
    public void upDataUi(HomeEsotericHttpResponse datas) {
        esotericaResponse.clare();
        esotericaResponse.addData(datas);
        adapter.notifyDataSetChanged();

    }


    @Override
    public HomeEsotericPresenter initPressenter() {
        return new HomeEsotericPresenter();
    }


    @Override
    public void clearData() {

    }


    public void setAdapter() {
        adapter = new EsotericaAdapter(activity, esotericaResponse);

        gridViewBKJP.setAdapter(adapter.gridViewBKJP);
        gridViewCLJX.setAdapter(adapter.gridViewCLJX);
        gridViewGML.setAdapter(adapter.gridViewGML);
        gridViewDSJD.setAdapter(adapter.gridViewDSJD);
        gridViewMDMJ.setAdapter(adapter.gridViewMDMJ);
        gridViewSPMJ.setAdapter(adapter.gridViewSPMJ);
        gridViewFLSS.setAdapter(adapter.gridViewFLSS);
    }


    @OnClick(value = {
            R.id.fragment_esoterica_homepager_bkjp, R.id.fragment_esoterica_homepager_cljx, R.id.fragment_esoterica_homepager_gml,
            R.id.fragment_esoterica_homepager_dsjd, R.id.fragment_esoterica_homepager_mdmj, R.id.fragment_esoterica_homepage_spmj,
            R.id.fragment_esoterica_homepager_flss
    })
    public void onClickLister(View view) {
        switch (view.getId()) {
            case R.id.fragment_esoterica_homepager_bkjp:
                EsotericClassifyActivity.startUi(getActivity(), EsotericClassifyEnum.BAO_KUAN);
                break;
            case R.id.fragment_esoterica_homepager_cljx:
                EsotericClassifyActivity.startUi(getActivity(), EsotericClassifyEnum.CHAO_LIU);
                break;
            case R.id.fragment_esoterica_homepager_gml:
                EsotericClassifyActivity.startUi(getActivity(), EsotericClassifyEnum.GAO_MAO_LI);
                break;
            case R.id.fragment_esoterica_homepager_dsjd:
                EsotericClassifyActivity.startUi(getActivity(), EsotericClassifyEnum.DA_SHI_JING_DIANG);
                break;
            case R.id.fragment_esoterica_homepager_mdmj:
                EsotericClassifyActivity.startUi(getActivity(), EsotericClassifyEnum.MAI_DUAN);
                break;
            case R.id.fragment_esoterica_homepage_spmj:
                EsotericClassifyActivity.startUi(getActivity(), EsotericClassifyEnum.SHI_PIN);
                break;
            case R.id.fragment_esoterica_homepager_flss:
                SearchActivity.startUi(getActivity(), SearchEnum.miJi);
                break;
        }
    }

    private void setListener() {

        //爆款精品
        gridViewBKJP.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_K_Activity.startUI(getActivity(), esotericaResponse.getBaokuan().get(position).getId());
            }
        });

        //潮流精选
        gridViewCLJX.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_K_Activity.startUI(getActivity(), esotericaResponse.getFashionChoice().get(position).getId());

            }
        });

        //高毛利
        gridViewGML.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_K_Activity.startUI(getActivity(), esotericaResponse.getHighMaoli().get(position).getId());

            }
        });
        //大师经典
        gridViewDSJD.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_K_Activity.startUI(getActivity(), esotericaResponse.getDashiclassic().get(position).getId());

            }
        });
        //买断秘籍
        gridViewMDMJ.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_D_Activity.startUI(getActivity(), esotericaResponse.getBuyduan().get(position).getId());

            }
        });
        //视频秘籍
        gridViewSPMJ.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_K_Activity.startUI(getActivity(), esotericaResponse.getVidoMiji().get(position).getId());

            }
        });
        //分类搜索
        gridViewFLSS.setOnItemClickListener(new SuperGridLayout.OnItemClickListener() {
            @Override
            public void onItemClick(GridLayout parent, View view, int position, Object data) {
                EsotericDetails_K_Activity.startUI(getActivity(), esotericaResponse.getMiJiSourceNewToTime().get(position).getId());

            }
        });

    }


}
