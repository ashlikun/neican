package com.hanbang.cxgz_2.view.jieyou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.RewardTypeEnum;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeRewardHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.pressenter.jieyou.DemandRewardClassifyPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.jieyou.adapter.DemandReswardAdapter;
import com.hanbang.cxgz_2.view.jieyou.iview.IDemanRewardClassifyView;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * 悬赏需求列表
 */
public class DemandRewardClassifyActivity extends BaseMvpActivity<IDemanRewardClassifyView, DemandRewardClassifyPresenter> implements IDemanRewardClassifyView, ListSwipeView.ListSwipeViewListener {
    @BindView(R.id.switchRoot)
    ListSwipeView switchRoot;
    private RewardTypeEnum classify;
    List<DemandRewardData> datas = new ArrayList<>();

    private DemandReswardAdapter adapter = null;
    HomeRewardHttpResponse homeHttpRequest = HomeRewardHttpResponse.init();

    /**
     * 启动信息详情
     *
     * @param context
     */
    public static void startUi(Context context, RewardTypeEnum classify) {
        Intent intent = new Intent(context, DemandRewardClassifyActivity.class);

        Bundle bundle = new Bundle();
        bundle.putSerializable("classify", classify);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }


    @Override
    public DemandRewardClassifyPresenter initPressenter() {
        return new DemandRewardClassifyPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        classify = (RewardTypeEnum) getIntent().getSerializableExtra("classify");
    }

    @Override
    public int getContentView() {
        return R.layout.activity_common_list;
    }

    @Override
    public void initView() {
        toolbar.setTitle(classify.getValuse());
        toolbar.setBack(this);


        switchRoot.getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        switchRoot.setOnRefreshListener(this);
        switchRoot.setOnLoaddingListener(this);
        switchRoot.getRecyclerView().addItemDecoration(new HorizontalDividerItemDecoration.Builder(this).build());

        adapter = new DemandReswardAdapter(this, homeHttpRequest, classify);

        CommonAdapter classifyAdapter = adapter.setAdapter(classify);
        switchRoot.getRecyclerView().setAdapter(classifyAdapter);
        presenter.getHttpData(true, classify.getKey());

        //设置item的监听
        classifyAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View view, Object data, int position) {
                DemandRewardDetailsActivity.startUI(DemandRewardClassifyActivity.this, datas.get(position).getID());
            }
        });

    }


    @Override
    public void upDataUi(List<DemandRewardData> datas) {
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
            return;
        }
        this.datas.addAll(datas);
        homeHttpRequest.setDatas(this.datas, classify);
        adapter.notifyDataSetChanged(classify);

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return switchRoot.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return switchRoot.getPagingHelp().getValidData(c);
    }

    @Override
    public void clearData() {
        this.datas.clear();
        homeHttpRequest.clear();
    }


    @Override
    public void clearPagingData() {
        switchRoot.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return switchRoot.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return switchRoot.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true, classify.getKey());
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false, classify.getKey());
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true, classify.getKey());
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true, classify.getKey());
    }


}
