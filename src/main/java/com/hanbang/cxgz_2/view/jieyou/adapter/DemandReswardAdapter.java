package com.hanbang.cxgz_2.view.jieyou.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.enumeration.RewardTypeEnum;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeRewardHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;
import com.hanbang.cxgz_2.mode.javabean.jianghu.ItmeRewardClassifyData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

import java.util.ArrayList;

/**
 * 悬赏需求-adapter
 * Created by Administrator on 2016/8/17.
 */

public class DemandReswardAdapter {
    //分类
    public CommonAdapter classify = null;
    //菜品研发
    public CommonAdapter caiPinYanFa = null;
    //招聘
    public CommonAdapter zaoPin = null;
    //求职
    public CommonAdapter quZhi = null;
    //商铺转让
    public CommonAdapter dianPuZuanRang = null;
    //二手设备
    public CommonAdapter erShouSheBei = null;
    //加盟代理
    public CommonAdapter jiaMengDaiLi = null;

    private Context context;
    private HomeRewardHttpResponse response;

    public ArrayList<ItmeRewardClassifyData> classifyDatas = new ArrayList<>();

    public DemandReswardAdapter(Context context, HomeRewardHttpResponse homeHttpRequest) {
        this.response = homeHttpRequest;
        this.context = context;
        initAdapter(true, null);
    }

    /**
     * 刷新所有的adapter
     */
    public void notifyDataSetChanged() {
        caiPinYanFa.notifyDataSetChanged();
        zaoPin.notifyDataSetChanged();
        quZhi.notifyDataSetChanged();
        dianPuZuanRang.notifyDataSetChanged();
        erShouSheBei.notifyDataSetChanged();
        jiaMengDaiLi.notifyDataSetChanged();
    }

    /**
     * 根据分类创建 Adapter
     *
     * @param context
     * @param homeHttpRequest
     * @param rewardClassifyEnum
     */
    public DemandReswardAdapter(Context context, HomeRewardHttpResponse homeHttpRequest, RewardTypeEnum rewardClassifyEnum) {
        this.response = homeHttpRequest;
        this.context = context;
        initAdapter(false, rewardClassifyEnum);

    }

    /**
     * set adapter
     * 根据分类设置 adapter
     *
     * @param rewardClassifyEnum
     * @return
     */
    public CommonAdapter setAdapter(RewardTypeEnum rewardClassifyEnum) {

        if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.CAI_PIN_YAN_FA.getValuse())) {
            return caiPinYanFa;

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.ZHAO_PIN.getValuse())) {
            return zaoPin;

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.QIU_ZHI.getValuse())) {
            return quZhi;

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.DIAN_PU_ZHUAN_RANG.getValuse())) {
            return dianPuZuanRang;

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.ER_SHOU_SHE_BEI.getValuse())) {
            return erShouSheBei;

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.JIA_MENG_DAI_LI.getValuse())) {
            return jiaMengDaiLi;
        }

        return null;
    }

    /**
     * Refresh adapter
     * 按分类刷新 adapter
     *
     * @param rewardClassifyEnum
     */
    public void notifyDataSetChanged(RewardTypeEnum rewardClassifyEnum) {

        if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.CAI_PIN_YAN_FA.getValuse())) {
            caiPinYanFa.notifyDataSetChanged();

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.ZHAO_PIN.getValuse())) {
            zaoPin.notifyDataSetChanged();

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.QIU_ZHI.getValuse())) {
            quZhi.notifyDataSetChanged();

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.DIAN_PU_ZHUAN_RANG.getValuse())) {
            dianPuZuanRang.notifyDataSetChanged();

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.ER_SHOU_SHE_BEI.getValuse())) {
            erShouSheBei.notifyDataSetChanged();

        } else if (rewardClassifyEnum.getValuse().equals(RewardTypeEnum.JIA_MENG_DAI_LI.getValuse())) {
            jiaMengDaiLi.notifyDataSetChanged();

        }

    }


    private void initAdapter(boolean all, RewardTypeEnum type) {
        if (all) {
            if (classifyDatas.size() == 0) {
                getGridData();
            }

            //分类
            classify = new CommonAdapter<ItmeRewardClassifyData>(context, R.layout.item_demand_reward_classify, classifyDatas) {
                @Override
                public void convert(ViewHolder holder, ItmeRewardClassifyData item) {
                    holder.setImageResource(R.id.item_demand_reward_classify_image, item.getPictureId());
                    holder.setText(R.id.item_demand_reward_classify_text, item.getTitle().getValuse());

                }
            };
        }


        //菜品研发
        if (all || type.getValuse().equals(RewardTypeEnum.CAI_PIN_YAN_FA.getValuse()))
            caiPinYanFa = new CommonAdapter<DemandRewardData>(context, R.layout.item_homepage_reward, response.getDishesdevelop()) {
                @Override
                public void convert(ViewHolder helper, DemandRewardData item) {
                    try {
                        helper.setImageBitmapCircle(R.id.item_homepage_reward_portrait, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getAvatar())));
                        helper.setText(R.id.item_homepage_reward_name, StringUtils.isNullToConvert(item.getUser_name()));
                        helper.setText(R.id.item_homepage_reward_jobCompany, StringUtils.isNullToConvert(item.getJobCN() + " | " + item.getCompany()));
                        helper.setText(R.id.content, StringUtils.isNullToConvert(item.getXuQiuContent()));
                        helper.setText(R.id.item_homepage_reward_participation, "参与:" + StringUtils.isNullToConvert(item.getJoinCount()));
                        if (item.getStatus() == 2) {
                            helper.getView(R.id.item_homepage_reward_status).setSelected(true);
                        } else {
                            helper.getView(R.id.item_homepage_reward_status).setSelected(false);
                        }
                        helper.setText(R.id.time, item.getCjsj());
                        helper.setText(R.id.price, StringUtils.isNullToConvert(item.getXunShangPrice() + "元"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            };

        //招聘
        if (all || type.getValuse().equals(RewardTypeEnum.ZHAO_PIN.getValuse()))
            zaoPin = new CommonAdapter<DemandRewardData>(context, R.layout.item_homepage_reward_zao_pin, response.getAdvertises()) {
                @Override
                public void convert(ViewHolder helper, DemandRewardData item) {
                    try {
                        helper.setText(R.id.content, StringUtils.isNullToConvert(item.getXuQiuContent()));
                        helper.setText(R.id.price, StringUtils.isNullToConvert(item.getXunShangPrice() + "元"));
                        helper.setText(R.id.address, StringUtils.isNullToConvert(item.getJobCN() + " | " + item.getCompany()));
                        helper.setText(R.id.time, item.getCjsj());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

        //求职
        if (all || type.getValuse().equals(RewardTypeEnum.QIU_ZHI.getValuse()))
            quZhi = new CommonAdapter<DemandRewardData>(context, R.layout.item_homepage_reward_zao_pin, response.getCandidate()) {
                @Override
                public void convert(ViewHolder helper, DemandRewardData item) {
                    try {
                        helper.setText(R.id.content, StringUtils.isNullToConvert(item.getXuQiuContent()));
                        helper.setText(R.id.price, StringUtils.isNullToConvert(item.getXunShangPrice() + "元"));
                        helper.setText(R.id.address, StringUtils.isNullToConvert(item.getJobCN() + " | " + item.getCompany()));
                        helper.setText(R.id.time, item.getCjsj());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

        //商铺转让
        if (all || type.getValuse().equals(RewardTypeEnum.DIAN_PU_ZHUAN_RANG.getValuse()))
            dianPuZuanRang = new CommonAdapter<DemandRewardData>(context, R.layout.item_demand_reward_h, response.getShopassignment()) {
                @Override
                public void convert(ViewHolder helper, DemandRewardData item) {
                    try {
                        helper.setImageBitmap(R.id.image, HttpLocalUtils.getHttpFileUrl(item.getXuQiuImg()));


                        helper.setText(R.id.content, StringUtils.isNullToConvert(item.getXuQiuContent()));
                        helper.setText(R.id.address, StringUtils.isNullToConvert(item.getJobCN() + " | " + item.getCompany()));
                        helper.setText(R.id.price, StringUtils.isNullToConvert(item.getXunShangPrice() + "元"));
                        helper.setText(R.id.time, item.getCjsj());


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

        //二手设备
        if (all || type.getValuse().equals(RewardTypeEnum.ER_SHOU_SHE_BEI.getValuse()))
            erShouSheBei = new CommonAdapter<DemandRewardData>(context, R.layout.item_demand_reward_h, response.getUsedplant()) {
                @Override
                public void convert(ViewHolder helper, DemandRewardData item) {
                    try {
                        helper.setImageBitmap(R.id.image, HttpLocalUtils.getHttpFileUrl(item.getXuQiuImg()));
                        helper.setText(R.id.content, StringUtils.isNullToConvert(item.getXuQiuContent()));
                        helper.setText(R.id.address, StringUtils.isNullToConvert(item.getJobCN() + " | " + item.getCompany()));
                        helper.setText(R.id.price, StringUtils.isNullToConvert(item.getXunShangPrice() + "元"));
                        helper.setText(R.id.time, item.getCjsj());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

        //加盟代理
        if (all || type.getValuse().equals(RewardTypeEnum.JIA_MENG_DAI_LI.getValuse()))
            jiaMengDaiLi = new CommonAdapter<DemandRewardData>(context, R.layout.item_demand_reward_h, response.getAgentproxy()) {
                @Override
                public void convert(ViewHolder helper, DemandRewardData item) {
                    try {
                        helper.setImageBitmap(R.id.image, HttpLocalUtils.getHttpFileUrl(item.getXuQiuImg()));
                        helper.setText(R.id.content, StringUtils.isNullToConvert(item.getXuQiuContent()));
                        helper.setText(R.id.address, StringUtils.isNullToConvert(item.getJobCN() + " | " + item.getCompany()));
                        helper.setText(R.id.price, StringUtils.isNullToConvert(item.getXunShangPrice() + "元"));
                        helper.setText(R.id.time, item.getCjsj());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

    }


    public ArrayList<ItmeRewardClassifyData> getGridData() {

        classifyDatas.add(new ItmeRewardClassifyData(R.mipmap.demand_channel_icon_research, RewardTypeEnum.CAI_PIN_YAN_FA));
        classifyDatas.add(new ItmeRewardClassifyData(R.mipmap.demand_channel_icon_recruit, RewardTypeEnum.ZHAO_PIN));
        classifyDatas.add(new ItmeRewardClassifyData(R.mipmap.demand_channel_icon_job, RewardTypeEnum.QIU_ZHI));
        classifyDatas.add(new ItmeRewardClassifyData(R.mipmap.demand_channel_icon_sell, RewardTypeEnum.DIAN_PU_ZHUAN_RANG));
        classifyDatas.add(new ItmeRewardClassifyData(R.mipmap.demand_channel_icon_equipment, RewardTypeEnum.ER_SHOU_SHE_BEI));
        classifyDatas.add(new ItmeRewardClassifyData(R.mipmap.demand_channel_icon_join, RewardTypeEnum.JIA_MENG_DAI_LI));

        return classifyDatas;
    }


}
