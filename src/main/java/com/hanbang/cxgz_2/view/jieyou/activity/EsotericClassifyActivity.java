package com.hanbang.cxgz_2.view.jieyou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.click.OnItemClickListener;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMvpActivity;
import com.hanbang.cxgz_2.mode.enumeration.EsotericClassifyEnum;
import com.hanbang.cxgz_2.mode.javabean.home.EsotericaData;
import com.hanbang.cxgz_2.pressenter.jieyou.EsotericClassifyPresenter;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.utils.ui.divider.VerticalDividerItemDecoration;
import com.hanbang.cxgz_2.view.jieyou.iview.IEsotericClassifyView;
import com.hanbang.cxgz_2.view.widget.ListSwipeView;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * 秘籍列表
 */
public class EsotericClassifyActivity extends BaseMvpActivity<IEsotericClassifyView, EsotericClassifyPresenter> implements IEsotericClassifyView, ListSwipeView.ListSwipeViewListener {
    @BindView(R.id.switchRoot)
    ListSwipeView switchRoot;
    CommonAdapter adapter;
    private ArrayList<EsotericaData> datas = new ArrayList<>();
    private EsotericClassifyEnum classify;


    /**
     * 启动信息详情
     *
     * @param context
     */
    public static void startUi(Context context, EsotericClassifyEnum classify) {
        Intent intent = new Intent(context, EsotericClassifyActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("classify", classify);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }


    @Override
    public EsotericClassifyPresenter initPressenter() {
        return new EsotericClassifyPresenter();
    }

    @Override
    public void parseIntent(Intent intent) {
        classify = (EsotericClassifyEnum) getIntent().getSerializableExtra("classify");

    }

    @Override
    public int getContentView() {
        return R.layout.activity_common_list;
    }

    @Override
    public void initView() {
        toolbar.setTitle(classify.getValuse());
        toolbar.setBack(this);


        switchRoot.getRecyclerView().setLayoutManager(new GridLayoutManager(this, 2));
        switchRoot.setOnRefreshListener(this);
        switchRoot.setOnLoaddingListener(this);
        presenter.getHttpData(true, classify.getKey());

        switchRoot.getRecyclerView().addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .sizeResId(R.dimen.dp_5)
                .colorResId(R.color.gray_ee)
                .build());

        switchRoot.getRecyclerView().addItemDecoration(new VerticalDividerItemDecoration.Builder(this)
                .sizeResId(R.dimen.dp_5)
                .colorResId(R.color.gray_ee)
                .build());






        switchRoot.setAdapter(adapter = new CommonAdapter<EsotericaData>(this, R.layout.item_homepage_esoterca, datas) {
            @Override
            public void convert(ViewHolder holder, EsotericaData item) {
                try {
                    holder.setImageBitmap(R.id.item_homepage_esoterca_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    holder.setText(R.id.item_homepage_esoterca_title, StringUtils.isNullToConvert(item.getTitle()));
                    holder.setText(R.id.item_homepage_esoterca_grade, "评分:" + item.getPingfen());
                    holder.setText(R.id.item_homepage_esoterca_read, "阅读:" + item.getClick());
                    if (item.getPrice() == 0) {
                        holder.setText(R.id.item_homepage_esoterca_price, "免费");
                    } else {
                        holder.setText(R.id.item_homepage_esoterca_price, "￥" + StringUtils.roundDouble(item.getPrice()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        //跳转到详情
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View view, Object data, int position) {
                if (classify.getValuse().equals(EsotericClassifyEnum.MAI_DUAN.getValuse())) {

                    EsotericDetails_D_Activity.startUI(EsotericClassifyActivity.this, datas.get(position).getId());
                } else {
                    EsotericDetails_K_Activity.startUI(EsotericClassifyActivity.this, datas.get(position).getId());
                }
            }
        });


    }


    @Override
    public void upDataUi(List<EsotericaData> d) {
        datas.addAll(d);
        if (datas.size() == 0) {
            loadingAndRetryManager.showEmpty(new ContextData());
        }
        adapter.notifyDataSetChanged();

    }


    //获取下拉刷新
    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return switchRoot.getSwipeRefreshLayout();
    }

    //获取改变加载状态
    @Override
    public StatusChangListener getStatusChangListener() {
        return switchRoot.getStatusChangListener();
    }

    //获取分页的有效数据
    @Override
    public <T> Collection<T> getValidData(Collection<T> c) {

        return switchRoot.getPagingHelp().getValidData(c);
    }

    @Override
    public void clearData() {
        datas.clear();
    }


    @Override
    public void clearPagingData() {
        switchRoot.getPagingHelp().clear();
    }

    @Override
    public int getPageindex() {
        return switchRoot.getPagingHelp().getPageindex();
    }

    @Override
    public int getPageCount() {
        return switchRoot.getPagingHelp().getPageCount();
    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(true, classify.getKey());
    }

    @Override
    public void onLoadding() {
        presenter.getHttpData(false, classify.getKey());
    }

    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(true, classify.getKey());
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(true, classify.getKey());
    }


}
