package com.hanbang.cxgz_2.view.jieyou.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.home.DemandRewardData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IDemanRewardClassifyView extends BaseListView{
    void upDataUi(List<DemandRewardData> datas);
}
