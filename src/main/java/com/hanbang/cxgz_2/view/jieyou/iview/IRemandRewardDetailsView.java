package com.hanbang.cxgz_2.view.jieyou.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseSwipeView;
import com.hanbang.cxgz_2.mode.javabean.jianghu.DemandRewardDetailsData;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/12 14:45
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：学习课堂详情
 */

public interface IRemandRewardDetailsView extends BaseSwipeView {
    void upDataUi(DemandRewardDetailsData datas);
}
