package com.hanbang.cxgz_2.view.jieyou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseMainActivity;
import com.hanbang.cxgz_2.mode.javabean.base.BannerAdData;
import com.hanbang.cxgz_2.pressenter.jieyou.JieyouPresenter;
import com.hanbang.cxgz_2.view.adapter.SectionsPagerAdapter;
import com.hanbang.cxgz_2.view.jieyou.fragment.DemandRewardFragment;
import com.hanbang.cxgz_2.view.jieyou.fragment.EsotericaFragment;
import com.hanbang.cxgz_2.view.jieyou.fragment.StudyClassroomFragment;
import com.hanbang.cxgz_2.view.jieyou.iview.IJieyouView;
import com.hanbang.cxgz_2.view.widget.banner.ConvenientBanner;
import com.hanbang.cxgz_2.view.widget.banner.NetworkImageHolderView;
import com.hanbang.cxgz_2.view.widget.banner.holder.CBViewHolderCreator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Administrator on 2016/8/15.
 */

public class JieyouActivity extends BaseMainActivity<IJieyouView, JieyouPresenter> implements IJieyouView, CBViewHolderCreator {

    @BindView(R.id.advertisement)
    ConvenientBanner advertisement;
    private List<BannerAdData> advertisementDatas;

    private String[] title = new String[]{"菜品秘籍", "学习课堂", "悬赏需求"};
    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private int index;


    public static void startUI(Context context, int index) {
        Intent intent = new Intent(context, JieyouActivity.class);
        intent.putExtra("index", index);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }


    @Override
    public void initView() {
        super.initView();
        advertisementDatas = new ArrayList<>();
        advertisementDatas.add(new BannerAdData(1, "http://img0.imgtn.bdimg.com/it/u=1458042595,2336590661&fm=206&gp=0.jpg"));
        advertisementDatas.add(new BannerAdData(2, "http://img4.imgtn.bdimg.com/it/u=3646491133,3402401671&fm=21&gp=0.jpg"));
        advertisement.setPages(this, advertisementDatas);
        advertisement.setWHBili();
        advertisement.startTurning(2000);


        fragmentList.add(EsotericaFragment.newInstance());
        fragmentList.add(StudyClassroomFragment.newInstance());
        fragmentList.add(DemandRewardFragment.newInstance());
        //为viewPage设置adapter
        SectionsPagerAdapter mAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragmentList, title);
        viewPager.setAdapter(mAdapter);
        //tabLayout关联ViewPage
        tabLayout.setupWithViewPager(viewPager);
        //显示页面设置
        viewPager.setCurrentItem(index);
    }

    @Override
    public void parseIntent(Intent intent) {
        index = intent.getIntExtra("index", 0);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        index = intent.getIntExtra("index", 0);
        viewPager.setCurrentItem(index);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int currentItem() {
        return 2;
    }

    @Override
    public JieyouPresenter initPressenter() {
        return new JieyouPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main_jieyou;
    }

    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    public int getStatusBarResource() {
        return R.color.translucent;
    }

    @Override
    public void clearData() {

    }


    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.fragment_allay_shop_header_viewpager)
    ViewPager viewPager;

    @Override
    public Object createHolder() {
        return new NetworkImageHolderView() {

            @Override
            public void onItemClicklistener(View item, int position, BannerAdData data) {

            }
        };
    }
}
