package com.hanbang.cxgz_2.view.jieyou.adapter;

import android.content.Context;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeStudyHttpResponse;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.DateUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;

/**
 * 学习课堂，adapter
 * Created by Administrator on 2016/8/17.
 */

public class StudyAdapter {
    //语音课堂
    public CommonAdapter gridViewYYKT = null;

    //线下课堂
    public CommonAdapter gridViewXXKT = null;

    private Context context;
    private HomeStudyHttpResponse homeHttpRequest;

    public StudyAdapter(Context context, HomeStudyHttpResponse homeHttpRequest) {
        this.homeHttpRequest = homeHttpRequest;
        this.context = context;
        initAdapter();
    }


    public void notifyDataSetChanged() {
        gridViewYYKT.notifyDataSetChanged();
        gridViewXXKT.notifyDataSetChanged();

    }

    private void initAdapter() {


        gridViewYYKT = new CommonAdapter<StudyClassroomData>(context, R.layout.item_study_classroom, homeHttpRequest.getGroupKeTang()) {
            @Override
            public void convert(ViewHolder helper, StudyClassroomData item) {
                try {
                    if (!StringUtils.isBlank(item.getImg_urlSmall())) {
                        helper.setImageBitmap(R.id.item_study_classroom_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_urlSmall())));
                    } else {
                        helper.setImageBitmap(R.id.item_study_classroom_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    }
                    helper.setText(R.id.item_study_classroom_title, StringUtils.isNullToConvert(item.getTitle()));
                    helper.setText(R.id.item_study_classroom_grade, StringUtils.isNullToConvert(item.getPingfen()) + "分");
                    helper.setText(R.id.item_study_classroom_read, "阅读:" + StringUtils.isNullToConvert(item.getClick()));
                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_study_classroom_price, "免费");
                    } else {
                        helper.setText(R.id.item_study_classroom_price, "￥" + StringUtils.isNullToConvert(item.getPrice()));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };


        gridViewXXKT = new CommonAdapter<StudyClassroomData>(context, R.layout.item_study_classroom_h, homeHttpRequest.getLearnClass()) {
            @Override
            public void convert(ViewHolder helper, StudyClassroomData item) {
                try {
                    if (!StringUtils.isBlank(item.getImg_urlSmall())) {
                        helper.setImageBitmap(R.id.item_study_classroom_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_urlSmall())));
                    } else {
                        helper.setImageBitmap(R.id.item_study_classroom_image, HttpLocalUtils.getHttpFileUrl(StringUtils.isNullToConvert(item.getImg_url())));
                    }
                    helper.setText(R.id.item_study_classroom_title, StringUtils.isNullToConvert(item.getTitle()));

                    helper.setText(R.id.item_study_classroom_grade, "开课时间:" + StringUtils.isNullToConvert(DateUtils.getTime(item.getAdd_time())));
                    helper.setText(R.id.item_study_classroom_read, "报名人数:" + StringUtils.isNullToConvert(item.getIsbaomingcount()));

                    if (item.getPrice() == 0) {
                        helper.setText(R.id.item_study_classroom_price, "免费");
                    } else {
                        helper.setText(R.id.item_study_classroom_price, "￥" + StringUtils.isNullToConvert(item.getPrice()));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };


    }


}
