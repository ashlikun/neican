package com.hanbang.cxgz_2.view.jieyou.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseSwipeView;
import com.hanbang.cxgz_2.mode.httpresponse.jieyou.HomeStudyHttpResponse;

/**
 * Created by yang on 2016/8/17.
 */
public interface IStudyClassFragmentView extends BaseSwipeView {
    void upDataUi(HomeStudyHttpResponse datas);
}
