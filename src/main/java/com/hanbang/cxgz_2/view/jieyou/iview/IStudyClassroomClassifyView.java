package com.hanbang.cxgz_2.view.jieyou.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;

import java.util.List;



/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/16 13:51
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：
 */

public interface IStudyClassroomClassifyView extends BaseListView{
    void upDataUi(List<StudyClassroomData> datas);
}
