package com.hanbang.cxgz_2.view.jieyou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseSwipeActivity;
import com.hanbang.cxgz_2.mode.javabean.jie_you.EsotericDetailsData;
import com.hanbang.cxgz_2.pressenter.jieyou.EsotericDetails_K_Presenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.jieyou.iview.IEsotericDetails_k_View;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;


/**
 * 开放式秘籍详情
 * Created by yang on 2016/8/30.
 */

public class EsotericDetails_D_Activity extends BaseSwipeActivity<IEsotericDetails_k_View, EsotericDetails_K_Presenter> implements IEsotericDetails_k_View {
    private int id = -1;
    private EsotericDetailsData data;

    @Override
    public int getContentView() {
        return R.layout.activity_esoteric_details_d;
    }

    public static void startUI(Context context, int id) {
        Intent intent = new Intent(context, EsotericDetails_D_Activity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }

    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }

    @Override
    public int getStatusBarResource() {
        return R.color.transparency;
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void parseIntent(Intent intent) {
        id = intent.getIntExtra("id", -1);
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setBack(this);
        toolbar.addAction(1, "", R.drawable.material_ic_share);
        presenter.getHttpData(id);

    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    @Override
    public void upDataUi(EsotericDetailsData datas) {
        data = datas;
        addData();
    }

    private void addData() {
        GlideUtils.show(findViewById(R.id.activity_esoteric_details_d_haiBao), StringUtils.isNullToConvert(data.getImg_url()));
        GlideUtils.showCircle(findViewById(R.id.activity_esoteric_details_d_portrait), StringUtils.isNullToConvert(data.getAvatar()));

        if (!data.is_vip()) {
            findViewById(R.id.activity_esoteric_details_d_vip).setVisibility(View.GONE);
        }

        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_name), StringUtils.isNullToConvert(data.getUser_name()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_jobAndCompany), StringUtils.isNullToConvert(data.getJob_CN()) + " | " + StringUtils.isNullToConvert(data.getCompany()));
        if (data.isShouCang()) {
            UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_collect), "已收藏");
            ((ImageView) findViewById(R.id.activity_esoteric_details_d_collect_icon)).setImageResource(R.mipmap.already_collection_icon);
        } else {
            UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_collect), "收藏");
            ((ImageView) findViewById(R.id.activity_esoteric_details_d_collect_icon)).setImageResource(R.mipmap.collection_icon);
        }

        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_workOff), "售出:" + StringUtils.isNullToConvert(data.getBuycount()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_read), "阅读:" + StringUtils.isNullToConvert(data.getClick()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_grade), "评分:" + StringUtils.isNullToConvert(data.getPingfen()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_price), "￥" + StringUtils.isNullToConvert(data.getPrice()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_feature), StringUtils.isNullToConvert(data.getTedian()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_title), StringUtils.isNullToConvert(data.getTitle()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_caiXi), StringUtils.isNullToConvert(data.getCuisines_idCN()));

        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_evaluate), StringUtils.isNullToConvert(data.getPingjia()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_LiuCheng), StringUtils.isNullToConvert(data.getMaiduanliucheng()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_statement), StringUtils.isNullToConvert(data.getMaiduanshengming()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_chefBrief), StringUtils.isNullToConvert(data.getWorkjingli()));//大厨简介就是工作经历
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_d_welcome), StringUtils.isNullToConvert(data.getHuanyingzongchoumaiduan()));


    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(id);
    }

    @Override
    public void clearData() {

    }

    @Override
    public EsotericDetails_K_Presenter initPressenter() {
        return new EsotericDetails_K_Presenter();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(id);
    }


}
