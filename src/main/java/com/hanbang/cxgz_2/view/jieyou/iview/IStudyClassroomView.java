package com.hanbang.cxgz_2.view.jieyou.iview;

import com.hanbang.cxgz_2.appliction.base.view.BaseListView;
import com.hanbang.cxgz_2.mode.javabean.home.StudyClassroomData;

import java.util.List;

/**
 * Created by Administrator on 2016/8/15.
 */

public interface IStudyClassroomView extends BaseListView{
    void upDataUi(List<StudyClassroomData> datas);
}
