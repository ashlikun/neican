package com.hanbang.cxgz_2.view.jieyou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseListActivity;
import com.hanbang.cxgz_2.mode.javabean.jianghu.DemandRewardDetailsData;
import com.hanbang.cxgz_2.pressenter.jieyou.DemandRewardDetailsPresenter;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.view.jieyou.adapter.DemandRewardDetailsAdapter;
import com.hanbang.cxgz_2.view.jieyou.iview.IRemandRewardDetailsView;
import com.hanbang.cxgz_2.view.widget.dialog.DialogComment;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.videoplay.view.VideoPlayer;

import java.util.ArrayList;

import butterknife.OnClick;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/13 14:15
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：悬赏需求详情页
 */

public class DemandRewardDetailsActivity extends BaseListActivity<IRemandRewardDetailsView, DemandRewardDetailsPresenter> implements IRemandRewardDetailsView {

    private String id;
    public ArrayList<DemandRewardDetailsData.Participation> gouChengTuPianPath = new ArrayList<>();
    private DemandRewardDetailsAdapter adapter;

    @Override
    public int getContentView() {
        return R.layout.activity_demand_reward_details;
    }

    public static void startUI(Context context, String id) {
        Intent intent = new Intent(context, DemandRewardDetailsActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void parseIntent(Intent intent) {
        id = intent.getStringExtra("id");
    }

    @Override
    public void initView() {
        super.initView();

        toolbar.setBack(this);
        toolbar.setTitle("悬赏需求详情");
        presenter.getHttpData(id);
        listSwipeView.getRecyclerView().setLoadMoreEnabled(false);
    }


    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {


        return new HorizontalDividerItemDecoration.Builder(this).build();
    }

    @Override
    public Object getAdapter() {
        return adapter = new DemandRewardDetailsAdapter(this, listSwipeView.getRecyclerView(), gouChengTuPianPath);
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return listSwipeView.getSwipeRefreshLayout();
    }


    @Override
    public void onRefresh() {
        presenter.getHttpData(id);
    }

    @Override
    public void clearData() {

    }

    @Override
    public DemandRewardDetailsPresenter initPressenter() {
        return new DemandRewardDetailsPresenter();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(id);
    }


    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onBackPressed() {
        if (VideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    /**
     * @param data
     */
    @Override
    public void upDataUi(DemandRewardDetailsData data) {
        gouChengTuPianPath.addAll(data.getCanyu());
        adapter.notifyDataSetChangedAll(data);

    }


    @Override
    public void onLoadding() {

        presenter.getHttpData(id);
    }

    @OnClick({R.id.bar_bottom_reward_details_transmit, R.id.bar_bottom_reward_details_collect, R.id.bar_bottom_reward_details_participation})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bar_bottom_reward_details_transmit:

                break;
            case R.id.bar_bottom_reward_details_collect:

                break;
            case R.id.bar_bottom_reward_details_participation:
                DialogComment dialog = new DialogComment(this);
                dialog.show();

                break;
        }
    }
}
