package com.hanbang.cxgz_2.view.jieyou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseSwipeActivity;
import com.hanbang.cxgz_2.mode.javabean.jianghu.EsotericZhuFuLiaoData;
import com.hanbang.cxgz_2.mode.javabean.jie_you.EsotericDetailsData;
import com.hanbang.cxgz_2.pressenter.jieyou.EsotericDetails_K_Presenter;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.LogUtils;
import com.hanbang.cxgz_2.utils.other.StringUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.jieyou.iview.IEsotericDetails_k_View;
import com.hanbang.cxgz_2.view.widget.ListViewForScrollView;
import com.hanbang.cxgz_2.view.widget.empty_layout.ContextData;
import com.hanbang.cxgz_2.view.widget.media.VideoPlayView;
import com.hanbang.cxgz_2.view.widget.media.MusicPlayView;
import com.hanbang.videoplay.view.VideoPlayer;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import butterknife.BindView;


/**
 * 开放式秘籍详情
 * Created by yang on 2016/8/30.
 */

public class EsotericDetails_K_Activity extends BaseSwipeActivity<IEsotericDetails_k_View, EsotericDetails_K_Presenter> implements IEsotericDetails_k_View {
    @BindView(R.id.SimpleRatingBar)
    SimpleRatingBar ratingBar;
    @BindView(R.id.activity_esoteric_details_k_zhuLiao)
    ListViewForScrollView zhuLiao;
    @BindView(R.id.activity_esoteric_details_k_fuLiao)
    ListViewForScrollView fuLiao;
    @BindView(R.id.activity_esoteric_details_k_liuChengTuPian)
    ListViewForScrollView liuChengTuPian;
    @BindView(R.id.music_play)
    MusicPlayView musicPlayView;


    private int id = -1;
    private EsotericDetailsData data;
    @Override
    public int getStatusBarPaddingTop() {
        return 0;
    }
    @Override
    public int getStatusBarResource() {
        return R.color.transparency;
    }

    @Override
    public int getContentView() {
        return R.layout.activity_esoteric_details_k;
    }

    public static void startUI(Context context, int id) {
        Intent intent = new Intent(context, EsotericDetails_K_Activity.class);
        //355有视频
        intent.putExtra("id", id);
        context.startActivity(intent);
    }


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void parseIntent(Intent intent) {
        id = intent.getIntExtra("id", -1);
    }

    @Override
    public void initView() {
        super.initView();
        toolbar.setBack(this);
        toolbar.addAction(1, "分享", R.drawable.material_ic_share);
        presenter.getHttpData(id);

        ratingBar.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                LogUtils.e(rating);
            }
        });
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipe;
    }

    @Override
    public void upDataUi(EsotericDetailsData datas) {
        data = datas;
        addData();
    }

    private void addData() {
        GlideUtils.show(findViewById(R.id.activity_esoteric_details_k_haiBao), StringUtils.isNullToConvert(data.getImg_url()));
        GlideUtils.showCircle(findViewById(R.id.activity_esoteric_details_k_portrait), StringUtils.isNullToConvert(data.getAvatar()));

        if (!data.is_vip()) {
            findViewById(R.id.activity_esoteric_details_k_vip).setVisibility(View.GONE);
        }

        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_name), StringUtils.isNullToConvert(data.getUser_name()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_jobAndCompany), StringUtils.isNullToConvert(data.getJob_CN()) + " | " + StringUtils.isNullToConvert(data.getCompany()));
        if (data.isShouCang()) {
            UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_collect), "已收藏");
            ((ImageView) findViewById(R.id.activity_esoteric_details_k_collect_icon)).setImageResource(R.mipmap.already_collection_icon);
        } else {
            UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_collect), "收藏");
            ((ImageView) findViewById(R.id.activity_esoteric_details_k_collect_icon)).setImageResource(R.mipmap.collection_icon);
        }

        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_workOff), "售出:" + StringUtils.isNullToConvert(data.getBuycount()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_read), "阅读:" + StringUtils.isNullToConvert(data.getClick()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_grade), "评分:" + StringUtils.isNullToConvert(data.getPingfen()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_price), "￥" + StringUtils.isNullToConvert(data.getPrice()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_feature), StringUtils.isNullToConvert(data.getTedian()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_title), StringUtils.isNullToConvert(data.getTitle()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_caiXi), StringUtils.isNullToConvert(data.getCuisines_idCN()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_LiuCheng), StringUtils.isNullToConvert(data.getGongyiliucheng()));
        UiUtils.setTextViewContent(findViewById(R.id.activity_esoteric_details_k_zhuYiShiXiang), StringUtils.isNullToConvert(data.getZhuyishixiang()));


        zhuLiao.setAdapter(new CommonAdapter<EsotericZhuFuLiaoData>(this, R.layout.item_esoterca_zhu_fu_liao, presenter.transitionMaterial(data.getZhuliao())) {
            @Override
            public void convert(ViewHolder holder, EsotericZhuFuLiaoData item) {
                holder.setText(R.id.item_esoterca_zhu_fu_liao_name, item.getName());
                holder.setText(R.id.item_esoterca_zhu_fu_liao_weight, item.getWeight());
            }
        });


        fuLiao.setAdapter(new CommonAdapter<EsotericZhuFuLiaoData>(this, R.layout.item_esoterca_zhu_fu_liao, presenter.transitionMaterial(data.getFuliao())) {
            @Override
            public void convert(ViewHolder holder, EsotericZhuFuLiaoData item) {
                holder.setText(R.id.item_esoterca_zhu_fu_liao_name, item.getName());
                holder.setText(R.id.item_esoterca_zhu_fu_liao_weight, item.getWeight());
            }
        });

        liuChengTuPian.setAdapter(new CommonAdapter<EsotericZhuFuLiaoData>(this, R.layout.item_esoterca_liu_cheng_tu_pian, presenter.transitionPicture(data.getGuochengtupian())) {
            @Override
            public void convert(ViewHolder holder, EsotericZhuFuLiaoData item) {
                holder.setText(R.id.item_esoterca_liu_cheng_tu_pian_step, item.getName());
                holder.setImageBitmap(R.id.item_esoterca_liu_cheng_tu_pian_picture, item.getWeight());
            }
        });

        //音频的播放
        if (!TextUtils.isEmpty(data.getAudioUrl())) {
            musicPlayView.setVisibility(View.VISIBLE);
            musicPlayView.setFilePath(HttpLocalUtils.getHttpFileUrl(data.getAudioUrl()));
        } else {
//            musicPlayView.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(data.getLiuchengshipin())) {
            videoPlay.setVisibility(View.VISIBLE);
            videoPlay.setUpDefault(data.getLiuchengshipin(), data.getTitle());
        }

        GlideUtils.show(videoPlay.thumbImageView, data.getImg_url());


    }

    @Override
    public void onRefresh() {
        presenter.getHttpData(id);
    }

    @Override
    public void clearData() {

    }

    @Override
    public EsotericDetails_K_Presenter initPressenter() {
        return new EsotericDetails_K_Presenter();
    }


    @Override
    public void onRetryClick(ContextData data) {
        presenter.getHttpData(id);
    }

    @Override
    public void onEmptyClick(ContextData data) {
        presenter.getHttpData(id);
    }


    @Override
    protected void onPause() {
        super.onPause();
        videoPlay.pause();
        if (musicPlayView != null) {
            musicPlayView.pause();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (musicPlayView != null) {
            musicPlayView.release();
        }
        videoPlay.release();

    }

    @Override
    public void onBackPressed() {
        if (VideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    @BindView(R.id.videoPlay)
    VideoPlayView videoPlay;

}
