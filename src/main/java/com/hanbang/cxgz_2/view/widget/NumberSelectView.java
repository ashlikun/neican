package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;


public class NumberSelectView extends LinearLayout {
    private OnItemClickListener onItemClickListener = null;
    private int textSize = 14;
    private int dimenSize = 30;
    private int centreTextColor = 0xFF000000;
    private TextView numTextView = null;
    private ImageView addImageView = null;
    private ImageView subImageView = null;
    private int number = 1;
    private final int maxNumber = 100;
    private final int minNumber = 0;


    public NumberSelectView(Context context) {
        super(context, null);
    }

    public NumberSelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.NumberSelectView);
        textSize = a.getDimensionPixelSize(
                R.styleable.NumberSelectView_textSize, ObjectUtils.sp2px(context, textSize));
        dimenSize = a.getDimensionPixelSize(
                R.styleable.NumberSelectView_dimenSize, dimenSize);
        centreTextColor = a.getColor(
                R.styleable.NumberSelectView_centreTextColor, centreTextColor);

        a.recycle();

        initView();
        setNumber(number);
    }

    private void initView() {
        LayoutParams addSubparams = new LayoutParams(dimenSize, dimenSize);
        addSubparams.gravity = Gravity.CENTER;

        LayoutParams textParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        textParams.weight = 1;
        textParams.gravity = Gravity.CENTER;


        subImageView = new ImageView(getContext());
        subImageView.setOnClickListener(onClickListener);
        subImageView.setImageResource(R.mipmap.ic_launcher);
        subImageView.setPadding(dimenSize / 4, dimenSize / 4, dimenSize / 4,
                dimenSize / 4);
        UiUtils.setImageViewTint(subImageView, R.color.blue);
        addView(subImageView, addSubparams);


        numTextView = new TextView(getContext());
        numTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        numTextView.setTextColor(centreTextColor);
        numTextView.setText(String.valueOf(number));
        numTextView.setPadding(dimenSize / 6, 0, dimenSize / 6, 0);
        numTextView.setGravity(Gravity.CENTER);
        addView(numTextView, textParams);


        addImageView = new ImageView(getContext());
        addImageView.setOnClickListener(onClickListener);
        addImageView.setImageResource(R.mipmap.ic_launcher);
        addImageView.setPadding(dimenSize / 4, dimenSize / 4, dimenSize / 4,
                dimenSize / 4);
        addView(addImageView, addSubparams);
    }

    private OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v == addImageView) {
                if (onItemClickListener != null) {
                    if (onItemClickListener.onAddClick(number,
                            NumberSelectView.this)) {
                        setNumber(number + 1);
                    }
                } else {
                    setNumber(number + 1);
                }
            } else if (v == subImageView) {
                if (onItemClickListener != null) {
                    if (onItemClickListener.onSubClick(number,
                            NumberSelectView.this)) {
                        setNumber(number - 1);
                    }
                } else {
                    setNumber(number - 1);
                }
            }
        }
    };

    public void setNumber(int num) {
        if (numTextView != null && num >= minNumber && num <= maxNumber) {
            numTextView.setText(String.valueOf(num));
            number = num;
            if (number == minNumber) {
                addImageView.setEnabled(true);
                subImageView.setEnabled(false);
                addImageView.clearColorFilter();
                UiUtils.setImageViewTint(subImageView, R.color.gray_e8);

            } else if (number == maxNumber) {
                addImageView.setEnabled(false);
                subImageView.setEnabled(true);
                addImageView.clearColorFilter();
                UiUtils.setImageViewTint(addImageView, R.color.gray_e8);
            } else {
                addImageView.setEnabled(true);
                subImageView.setEnabled(true);
                addImageView.clearColorFilter();
                subImageView.clearColorFilter();
            }
        }
    }

    public int getNumber() {
        return number;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
        invalidate();
    }

    public int getDimenSize() {
        return dimenSize;
    }

    public void setDimenSize(int dimenSize) {
        this.dimenSize = dimenSize;
        invalidate();
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /*
     * 点击的回调接口
     */
    public interface OnItemClickListener {
        boolean onAddClick(int num, NumberSelectView view);

        boolean onSubClick(int num, NumberSelectView view);
    }
}
