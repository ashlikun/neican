package com.hanbang.cxgz_2.view.widget.autoloadding;

import android.support.v4.widget.SwipeRefreshLayout;

import com.hanbang.cxgz_2.utils.http.PagingHelp;

/**
 * Created by Administrator on 2016/8/8.
 */

public interface BaseSwipeInterface {
    void setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout);

    void setOnLoaddingListener(OnLoaddingListener swipeRefreshLayout);

    PagingHelp getPagingHelp();

    StatusChangListener getStatusChangListener();
}
