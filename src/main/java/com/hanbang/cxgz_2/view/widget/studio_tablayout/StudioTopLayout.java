package com.hanbang.cxgz_2.view.widget.studio_tablayout;

import android.content.Context;
import android.support.v7.widget.ButtonBarLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.javabean.about_me.StudioData;
import com.hanbang.cxgz_2.utils.animator.AnimUtils;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.widget.BorderImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/9　17:47
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioTopLayout extends RelativeLayout implements View.OnClickListener {

    public int FENSHI = 1;
    public int GUANZHU = 2;
    public int LIAOTIAN = 3;
    public int SQBS = 4;
    @BindView(R.id.guanzhuIv)
    View guanzhuIv;

    private OnClickListener onClickListener;

    public StudioTopLayout(Context context) {
        this(context, null);
    }

    public StudioTopLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StudioTopLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        UiUtils.getInflaterView(getContext(), R.layout.studio_toplayout, this);
        ButterKnife.bind(this);
        fenshiBL.setOnClickListener(this);
        guanzhuBL.setOnClickListener(this);
        liaotianBl.setOnClickListener(this);
        sqbsBL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (onClickListener != null) {
            switch (v.getId()) {
                case R.id.fenshiBL:
                    onClickListener.onClick(v, FENSHI);
                    break;
                case R.id.guanzhuBL:
                    onClickListener.onClick(v, GUANZHU);
                    break;
                case R.id.liaotianBl:
                    onClickListener.onClick(v, LIAOTIAN);
                    break;
                case R.id.sqbsBL:
                    onClickListener.onClick(v, SQBS);
                    break;
            }
        }
    }

    public void upDataUI(StudioData data) {
        GlideUtils.showCircle(touxiang, data.getAvatarimgMidurl());
        isVip.setVisibility(data.is_vip() ? VISIBLE : GONE);
        name.setText(data.getUser_name());
        zhiwei.setText(data.getJobCN());
        danwei.setText(data.getCompany());
        fenshiTv.setText(data.getMembershipcount());
        guanzhuTv.setText(data.guanzhuText());
        guanzhuIv.setBackgroundResource(data.getGuanzhuRes());
    }

    public void guanzhuChanged(StudioData data) {
        AnimUtils.rotationAnim(guanzhuBL);
        guanzhuTv.setText(data.guanzhuText());
        guanzhuIv.setBackgroundResource(data.getGuanzhuRes());
    }

    public interface OnClickListener {
        void onClick(View v, int item);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @BindView(R.id.isVip)
    View isVip;
    @BindView(R.id.touxiang)
    BorderImageView touxiang;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.zhiwei)
    TextView zhiwei;
    @BindView(R.id.danwei)
    TextView danwei;
    @BindView(R.id.fenshiBL)
    ButtonBarLayout fenshiBL;
    @BindView(R.id.guanzhuBL)
    ButtonBarLayout guanzhuBL;
    @BindView(R.id.liaotianBl)
    ButtonBarLayout liaotianBl;
    @BindView(R.id.sqbsBL)
    ButtonBarLayout sqbsBL;
    @BindView(R.id.fenshiTv)
    TextView fenshiTv;
    @BindView(R.id.guanzhuTv)
    TextView guanzhuTv;


}
