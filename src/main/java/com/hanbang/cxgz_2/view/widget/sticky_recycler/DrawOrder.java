package com.hanbang.cxgz_2.view.widget.sticky_recycler;

/**
 * Created by aurel on 10/11/14.
 */
public enum DrawOrder {
    OverItems,
    UnderItems
}
