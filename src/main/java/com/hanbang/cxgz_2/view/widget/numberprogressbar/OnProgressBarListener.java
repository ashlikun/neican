package com.hanbang.cxgz_2.view.widget.numberprogressbar;

public interface OnProgressBarListener {

    void onProgressChange(int current, int max);
}
