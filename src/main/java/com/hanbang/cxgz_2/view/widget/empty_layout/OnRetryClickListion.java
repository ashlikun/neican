package com.hanbang.cxgz_2.view.widget.empty_layout;

/**
 * Created by Administrator on 2016/7/28.
 */

public interface OnRetryClickListion {
    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     *
     * 方法功能：数据加载失败点击重新加载
     */

    void onRetryClick(ContextData data);

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     *
     * 方法功能：数据为空点击重新加载
     */

    void onEmptyClick(ContextData data);
}
