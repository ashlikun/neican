package com.hanbang.cxgz_2.view.widget.dialog;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;
import com.hanbang.cxgz_2.utils.ui.ScreenInfoUtils;
import com.hanbang.cxgz_2.utils.ui.ToastUtils;
import com.hanbang.cxgz_2.utils.ui.divider.HorizontalDividerItemDecoration;
import com.hanbang.cxgz_2.utils.ui.divider.VerticalDividerItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/23 13:36
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：选择菜系
 */

public class DialogSelectCaiXi extends Dialog {
    @BindView(R.id.dialog_select_emoji_recycleView)
    RecyclerView recyclerView;
    @BindView(R.id.dialog_select_emoji_back)
    ImageView back;
    ArrayList<CaiXiData> datas;


    private OnClickCallback clickCallback;


    public DialogSelectCaiXi(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    public DialogSelectCaiXi(Context context, ArrayList<CaiXiData> datas) {
        this(context, R.style.Dialog_emoji);
        this.datas = datas;

    }

    private void init() {
        setContentView(R.layout.dialog_select_cai_xi);
        ButterKnife.bind(this, getWindow().getDecorView().findViewById(R.id.dialog_select_root));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        ScreenInfoUtils screen = new ScreenInfoUtils();
        lp.width = (screen.getWidth()); //设置宽度
        lp.height = (ActionBar.LayoutParams.WRAP_CONTENT); //设置宽度
        getWindow().setAttributes(lp);
        getWindow().getAttributes().gravity = Gravity.BOTTOM;
        setAdapter();
    }

    int index = 0;

    private void setAdapter() {
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));


        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).sizeResId(R.dimen.dp_15).colorResId(R.color.white).build());
        recyclerView.addItemDecoration(new VerticalDividerItemDecoration.Builder(getContext()).sizeResId(R.dimen.dp_5).colorResId(R.color.white).build());
        recyclerView.setAdapter(new CommonAdapter<CaiXiData>(getContext(), R.layout.item_search, datas) {

            @Override
            public void convert(final ViewHolder holder, final CaiXiData item) {

                holder.setText(R.id.item_search_name, item.getTitle());

                if (item.isSelect()) {
                    holder.setTextColor(R.id.item_search_name, getContext().getResources().getColor(R.color.main_color));
                    holder.setBackgroundColor(R.id.item_search_name, getContext().getResources().getColor(R.color.black));

                } else {
                    holder.setTextColor(R.id.item_search_name, getContext().getResources().getColor(R.color.black));
                    holder.setBackgroundColor(R.id.item_search_name, getContext().getResources().getColor(R.color.gray_ee));
                }

                //选择
                holder.setOnClickListener(R.id.item_search_name, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (datas.get(holder.getmPosition()).isSelect()) {
                            datas.get(holder.getmPosition()).setIsSelect(false);
                            index--;

                        } else {
                            if (index == 5) {
                                ToastUtils.getToastShort("最多选择5种菜系");
                                return;
                            }

                            datas.get(holder.getmPosition()).setIsSelect(true);
                            index++;
                        }

                        notifyDataSetChanged();
                    }
                });
            }

        });


        /**
         * 代表删除表情
         */
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer names = new StringBuffer();
                StringBuffer ids = new StringBuffer();
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).isSelect()) {
                        names.append(datas.get(i).getTitle() + ",");

                        ids.append(datas.get(i).getId() + ",");
                    }
                }

                if (ids.length() != 0 && names.length() != 0) {

                    clickCallback.onClick(names.toString().substring(0,names.toString().lastIndexOf(",")),
                            ids.toString().substring(0,ids.toString().lastIndexOf(",")));
                    dismiss();

                } else {

                    ToastUtils.getToastShort("菜系不可为空");

                }

            }
        });
    }


    public void setClickCallback(OnClickCallback clickCallback) {

        this.clickCallback = clickCallback;
    }


    public interface OnClickCallback {
        /**
         * 表情的名字，如果返回的是-1则为删除
         */

        void onClick(String name, String id);
    }


}
