package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.utils.http.PagingHelp;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.widget.autoloadding.BaseSwipeInterface;
import com.hanbang.cxgz_2.view.widget.autoloadding.ConfigChang;
import com.hanbang.cxgz_2.view.widget.autoloadding.GridViewForAutoLoadding;
import com.hanbang.cxgz_2.view.widget.autoloadding.ListViewForAutoLoad;
import com.hanbang.cxgz_2.view.widget.autoloadding.OnLoaddingListener;
import com.hanbang.cxgz_2.view.widget.autoloadding.RecyclerViewAutoLoadding;
import com.hanbang.cxgz_2.view.widget.autoloadding.StatusChangListener;
import com.hanbang.cxgz_2.view.widget.empty_layout.OnRetryClickListion;
import com.hanbang.cxgz_2.view.widget.swipemenulistview.SwipeMenuListView;

import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/28.
 */
public class ListSwipeView extends RelativeLayout {
    /**
     * 当前的容器模式 0:listview  1：gridVIew, 2:recycleView
     */
    private int mode;
    @BindView(R.id.swipe)
    public SwipeRefreshLayout swipeView;
    @BindView(R.id.list_swipe_target)
    BaseSwipeInterface viewGroup;

    public ListSwipeView(Context context) {
        this(context, null);
    }

    public ListSwipeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ListSwipeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ListSwipeView, 0, 0);
        mode = a.getInt(R.styleable.ListSwipeView_mode, 2);
        a.recycle();
        if (!isInEditMode()) {
            initView();
        }

    }


    public boolean isRecycleView() {
        return mode == 2;
    }

    public boolean isListleView() {
        return mode == 2;
    }


    private void initView() {
        if (mode == 0) {
            UiUtils.getInflaterView(getContext(), R.layout.swipe_listview, this, true);
        } else if (mode == 1) {
            UiUtils.getInflaterView(getContext(), R.layout.swipe_gridview, this, true);
        } else if (mode == 2) {
            UiUtils.getInflaterView(getContext(), R.layout.swipe_recycle, this, true);
        } else if (mode == 3) {
            UiUtils.getInflaterView(getContext(), R.layout.swipe_swipelist, this, true);
        }
        ButterKnife.bind(this, this);
        /**
         * 设置集合view的刷新view
         */
        viewGroup.setSwipeRefreshLayout(swipeView);
        UiUtils.setColorSchemeResources(swipeView);

    }

    /**
     * 当前的容器模式 0:listview  1：gridVIew, 2:recycleView, 3swipeList
     */
    public void switchView(int mode) {
        this.mode = mode;
        initView();
    }

    /**
     * 设置item点击事件 listView he GridViewForAutoLoadding
     *
     * @param onItemClickListen
     */
    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListen) {
        if (viewGroup instanceof ListViewForAutoLoad) {
            ((ListViewForAutoLoad) viewGroup).setOnItemClickListener(onItemClickListen);
        } else if (viewGroup instanceof GridViewForAutoLoadding) {
            ((GridViewForAutoLoadding) viewGroup).setOnItemClickListener(onItemClickListen);
        } else if (viewGroup instanceof RecyclerViewAutoLoadding) {

        }
    }


    /**
     * 设置加载更多的回调
     *
     * @param onLoaddingListener
     */
    public void setOnLoaddingListener(OnLoaddingListener onLoaddingListener) {
        viewGroup.setOnLoaddingListener(onLoaddingListener);

    }

    /**
     * 设置下拉刷新的回调
     *
     * @param listener
     */
    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
        if (swipeView != null) {
            swipeView.setOnRefreshListener(listener);
        }
    }

    /**
     * 设置适配器
     */

    public void setAdapter(BaseAdapter adapter) {
        if (viewGroup instanceof ListViewForAutoLoad) {
            ((ListViewForAutoLoad) viewGroup).setAdapter(adapter);
        } else if (viewGroup instanceof GridViewForAutoLoadding) {
            ((GridViewForAutoLoadding) viewGroup).setAdapter(adapter);
        }
    }

    public void setDividerHeight(float dp) {
        if (viewGroup instanceof ListViewForAutoLoad) {
            ((ListViewForAutoLoad) viewGroup).setDividerHeight(ObjectUtils.dip2px(getContext(), dp));
        } else if (viewGroup instanceof GridViewForAutoLoadding) {
            ((GridViewForAutoLoadding) viewGroup).setHorizontalSpacing(ObjectUtils.dip2px(getContext(), dp));
            ((GridViewForAutoLoadding) viewGroup).setVerticalSpacing(ObjectUtils.dip2px(getContext(), dp));
        }
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        ((RecyclerViewAutoLoadding) viewGroup).setAdapter(adapter);
    }

    /**
     * 获取pagingHelp
     */
    public PagingHelp getPagingHelp() {
        return viewGroup.getPagingHelp();
    }

    /**
     * 获取分页的有效数据
     */
    public <T> Collection<T> getValidData(Collection<T> c) {
        return getPagingHelp().getValidData(c);
    }

    /**
     * 获取容器view
     *
     * @return
     */
    public ListViewForAutoLoad getListView() {
        return (ListViewForAutoLoad) viewGroup;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeView;
    }

    public SwipeMenuListView getSwipeList() {
        return (SwipeMenuListView) viewGroup;
    }

    public ConfigChang getConfigChang() {
        if (viewGroup instanceof ConfigChang) {
            return (ConfigChang) viewGroup;
        } else {
            return null;
        }
    }

    /**
     * 刷新Swp
     *
     * @return
     */
    public void setRefreshing(boolean refreshing) {
        swipeView.setRefreshing(refreshing);
    }

    public GridViewForAutoLoadding getGridView() {
        return (GridViewForAutoLoadding) viewGroup;
    }

    public RecyclerViewAutoLoadding getRecyclerView() {
        return (RecyclerViewAutoLoadding) viewGroup;
    }


    public StatusChangListener getStatusChangListener() {
        return viewGroup.getStatusChangListener();
    }

    public interface ListSwipeViewListener extends SwipeRefreshLayout.OnRefreshListener, OnLoaddingListener, OnRetryClickListion {

    }

    public interface ListViewListener extends OnLoaddingListener, OnRetryClickListion {

    }


}
