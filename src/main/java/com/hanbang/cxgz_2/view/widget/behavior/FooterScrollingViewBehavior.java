package com.hanbang.cxgz_2.view.widget.behavior;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;

import com.hanbang.cxgz_2.utils.other.LogUtils;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/22　17:29
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class FooterScrollingViewBehavior extends CoordinatorLayout.Behavior<View> {

    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();


    private int sinceDirectionChange;


    public FooterScrollingViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //1.判断滑动的方向 我们需要垂直滑动
    @Override
    public boolean onStartNestedScroll(CoordinatorLayout parent, View child, View directTargetChild, View target, int nestedScrollAxes) {
        final boolean started = (nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0
                && parent.getHeight() - directTargetChild.getHeight() <= child.getHeight();
        return started;
    }

    //2.根据滑动的距离显示和隐藏footer view
    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, View child, View target, int dx, int dy, int[] consumed) {


        LogUtils.e(dx + "   " + dy);

        if (dy < 0) {//向下滚动   呈现
            if (child.getTranslationY() >= 0) {
                float trY = child.getTranslationY() + dy <= 0 ?
                        0 : child.getTranslationY() + dy;

                consumed[1] = (int) (child.getTranslationY() - trY);
                child.setTranslationY(trY);
            }

        } else {//向上滚动   消失
            if (child.getTranslationY() <= child.getHeight()) {
                float trY = child.getTranslationY() -
                        dy >= child.getHeight() ? child.getHeight() : child.getTranslationY() + dy;
                consumed[1] = (int) (child.getTranslationY() - trY);
                child.setTranslationY(trY);
            }
        }
    }

//    @Override
//    public boolean onDependentViewChanged(CoordinatorLayout parent, AppBarLayout child, View dependency) {
//        // return super.onDependentViewChanged(parent, child, dependency);
//        child.setTranslationY(Math.abs(dependency.getTranslationY()));
//        return true;
//    }

}