package com.hanbang.cxgz_2.view.widget.media;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.utils.http.GlideUtils;
import com.hanbang.cxgz_2.utils.http.HttpLocalUtils;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;
import com.hanbang.videoplay.view.VideoPlayer;
import com.hanbang.videoplay.view.VideoPlayerStandard;

import java.util.Map;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by Administrator on 2016/9/1.
 * <p>
 * thumbImageView
 */

public class VideoPlayView extends VideoPlayerStandard {
    private MusicPlayView musicPlayView;
    VideoPlayer.JCAutoFullscreenListener sensorEventListener;
    SensorManager sensorManager;

    public VideoPlayView(Context context) {
        super(context);
    }

    public VideoPlayView(Context context, AttributeSet attrs) {

        super(context, attrs);


    }

    @Override
    public void init(Context context) {
        if (isInEditMode()) {
            return;
        }
        super.init(context);

        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        sensorEventListener = new VideoPlayer.JCAutoFullscreenListener();
    }

    /**
     * 设置资源背景
     *
     * @param res
     */
    public void setThumbRes(@DrawableRes int res) {
        thumbImageView.setImageResource(res);
    }

    /**
     * 设置网络图片背景
     *
     * @param res
     */
    public void setThumbUrl(@NonNull String res) {

        GlideUtils.show(thumbImageView, res);
    }

    @Override
    public boolean setUp(String url, int screen, Map<String, String> mapHeadData, Object... objects) {
        url = HttpLocalUtils.getHttpFileUrl(url);
        boolean res = super.setUp(url, screen, mapHeadData, objects);
        setRootViewPadding(currentScreen);
        Sensor accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(sensorEventListener, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        return res;
    }

    @Override
    public boolean setUp(String url, int screen, Object... objects) {
        url = HttpLocalUtils.getHttpFileUrl(url);
        boolean res = super.setUp(url, screen, objects);
        setRootViewPadding(currentScreen);
        Sensor accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(sensorEventListener, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        return res;
    }

    public boolean setUpDefault(String url, Object... objects) {
        return setUp(url, VideoPlayerStandard.SCREEN_LAYOUT_LIST, objects);
    }

    public void setRootViewPadding(int screen) {
        Activity activity = ObjectUtils.getActivity(getContext());
        if (activity instanceof BaseActivity) {
            if (screen == SCREEN_WINDOW_FULLSCREEN) {//
                ((BaseActivity) activity).setRootViewPadding(0);
                ((BaseActivity) activity).mTintManager.setStatusBarTintEnabled(false);
                ((BaseActivity) activity).mTintManager.setNavigationBarTintEnabled(false);
            } else {
                ((BaseActivity) activity).setRootViewPadding(((BaseActivity) activity).getStatusBarPaddingTop());
                ((BaseActivity) activity).mTintManager.setStatusBarTintEnabled(true);
                ((BaseActivity) activity).mTintManager.setNavigationBarTintEnabled(true);
            }
        }
    }


    @Override
    public boolean goToOtherListener() {
        setRootViewPadding(SCREEN_LAYOUT_LIST);
        Sensor accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(sensorEventListener, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        return super.goToOtherListener();

    }

    @Override
    public void prepareVideo() {
        super.prepareVideo();
        if (musicPlayView != null) {
            musicPlayView.pause();
        }


    }

    public void setMusicPlayView(MusicPlayView musicPlayView) {

        this.musicPlayView = musicPlayView;
    }

}
