package com.hanbang.cxgz_2.view.widget.dialog;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.recyclerview.CommonAdapter;
import com.hanbang.cxgz_2.utils.ui.ScreenInfoUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;
import com.hanbang.cxgz_2.view.widget.emoj.EmojiconHandler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 选择类型的dialog
 * Created by yang on 2016/8/24.
 */

public class DialogSelectEmoji extends Dialog {
    @BindView(R.id.dialog_select_emoji_recycleView)
    RecyclerView recyclerView;
    @BindView(R.id.dialog_select_emoji_back)
    ImageView back;

    private OnClickCallback clickCallback;


    public DialogSelectEmoji(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    public DialogSelectEmoji(Context context) {
        this(context, R.style.Dialog_emoji);

    }

    private void init() {
        setContentView(R.layout.dialog_select_emoji);
        ButterKnife.bind(this, getWindow().getDecorView().findViewById(R.id.dialog_select_root));
        back.setImageResource(R.drawable.ic_delete);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        ScreenInfoUtils screen = new ScreenInfoUtils();
        lp.width = (screen.getWidth()); //设置宽度
        lp.height = (ActionBar.LayoutParams.WRAP_CONTENT); //设置宽度
        getWindow().setAttributes(lp);
        getWindow().getAttributes().gravity = Gravity.BOTTOM;
        setAdapter();
    }


    private void setAdapter() {
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.HORIZONTAL));
        ArrayList<EmojiData> datas = new ArrayList<>();

        for (int i = 0; i < EmojiconHandler.emojiMap.size(); i++) {
            datas.add(new EmojiData(i + 1, EmojiconHandler.emojiMap.valueAt(i)));
        }


        recyclerView.setAdapter(new CommonAdapter<EmojiData>(getContext(), R.layout.item_select_emoji, datas) {

            @Override
            public void convert(ViewHolder holder, final EmojiData item) {
                holder.setImageResource(R.id.item_photo_selectImage, item.pictureId);

                holder.setOnClickListener(R.id.item_photo_selectImage, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        clickCallback.onClick(item.getName());
                    }
                });
            }
        });
        /**
         * 代表删除表情
         */
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickCallback.onClick("-1");
            }
        });
    }

    /**
     * 监听表情窗口的销毁与开启
     *
     * @param textView
     */
    public void setListener(final TextView textView) {
        //销毁的监听,获取焦点，弹起键盘
        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                textView.setFocusable(true);
                UiUtils.showInput(textView);
            }
        });

        //显示的监听,获取焦点，隐藏键盘
        setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                textView.setFocusable(true);
                UiUtils.exitInput(textView);
            }
        });
    }


    public void setClickCallback(OnClickCallback clickCallback) {

        this.clickCallback = clickCallback;
    }


    public interface OnClickCallback {
        /**
         * 表情的名字，如果返回的是-1则为删除
         */

        void onClick(String name);
    }


    /**
     * 表情的实体类
     */
    public class EmojiData {
        int name;
        int pictureId;

        public EmojiData(int name, int pictureId) {
            this.name = name;
            this.pictureId = pictureId;
        }

        public String getName() {
            if (name < 10) {
                return "[f0" + name + "]";
            } else {
                return "[f" + name + "]";
            }

        }

        public void setName(int name) {
            this.name = name;
        }

        public int getPictureId() {
            return pictureId;
        }

        public void setPictureId(int pictureId) {
            this.pictureId = pictureId;
        }
    }

}
