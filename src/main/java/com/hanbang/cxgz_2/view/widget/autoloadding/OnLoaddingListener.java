package com.hanbang.cxgz_2.view.widget.autoloadding;

/**
 * Created by Administrator on 2016/3/14.
 */
public interface OnLoaddingListener {

    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/22 11:06
     *
     * 方法功能：分页加载回调
     */

    void onLoadding();
}
