package com.hanbang.cxgz_2.view.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 日期时间选择控件
 */
public class CalendarPickerUtil {
    /***
     * 日期控件
     */
    private DatePicker datePicker;
    private TimePicker timePicker;
    /**
     * 对话框
     */
    private AlertDialog alertDialog;
    private Context context;
    private TextView title;
    private TextView cancel;
    private TextView confirm;

    private int myYear;
    private int myMonth;
    private int myDay;
    private int myHour;
    private int myMinute;

    Calendar calendar = Calendar.getInstance();

    /**
     * 当前时间
     */
//    private long crenterTime;
    /**
     * 最大日期值
     */
    private int maxTime;

    private OnClickCallback onClickCallback = null;

    /**
     * 弹出选择框构，并传入一个最大日期值
     */
    public CalendarPickerUtil(Context context) {
        this.context = context;
        this.maxTime = maxTime;

        LinearLayout dateTimeLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.dialog_calender, null);
        title = (TextView) dateTimeLayout.findViewById(R.id.shoping_cart_dialog_title);
        cancel = (TextView) dateTimeLayout.findViewById(R.id.shoping_cart_dialog_cancel);
        confirm = (TextView) dateTimeLayout.findViewById(R.id.shoping_cart_dialog_confirm);
        datePicker = (DatePicker) dateTimeLayout.findViewById(R.id.datepicker);
        timePicker = (TimePicker) dateTimeLayout.findViewById(R.id.timePicker);


        setPikcerSize(datePicker);
        setPikcerSize(timePicker);

        //时间设置为24小时
        timePicker.setIs24HourView(true);
        // dislog 的初始化
        alertDialog = new AlertDialog.Builder(context).setIcon(null).setTitle(null).setView(dateTimeLayout).show();
        dateTimePicKDialog();


    }

    /**
     * 弹出日期时间选择框方法
     */
    public void dateTimePicKDialog() {

        myYear = calendar.get(Calendar.YEAR);
        myMonth = calendar.get(Calendar.MONTH);
        myDay = calendar.get(Calendar.DAY_OF_MONTH);
        myHour = calendar.get(Calendar.HOUR);
        myMinute = calendar.get(Calendar.MINUTE);
        title.setText(myYear + "年" + myMonth + "月" + myDay + "日 " + myHour + "时" + myMinute + "分");

        /**
         * 取消键的监听
         */
        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

        /**
         * 确认键的监听
         */
        confirm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (onClickCallback != null) {
//                    onClickCallback.onclick(myYear + "年" + (myMonth) + "月" + myDay + "日");
                    onClickCallback.onclick(myYear, myMonth, myDay, myHour, myMinute);
                }
                alertDialog.dismiss();
            }
        });
        /**
         * 日期监听
         */
        datePicker.init(myYear, myMonth - 1, myDay, new OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myYear = year;
                myMonth = monthOfYear + 1;
                myDay = dayOfMonth;
                title.setText(myYear + "年" + (myMonth) + "月" + myDay + "日 " + myHour + "时" + myMinute + "分");

            }
        });

        /**
         * 时间的监听
         */
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                myHour = hourOfDay;
                myMinute = minute;
                title.setText(myYear + "年" + (myMonth) + "月" + myDay + "日 " + myHour + "时" + myMinute + "分");
            }
        });
        alertDialog.show();
    }

    /**
     * 设置最大日期
     *
     * @param maxDate 整形的天数
     */
    public void setMaxDate(int maxDate) {
        long date = calendar.getTime().getTime();

        date = date + ((long) maxDate * 24 * 60 * 60 * 1000) - 1000;

        datePicker.setMaxDate(date);
    }

    /**
     * 设置最小日期
     *
     * @param minDate 整形的天数
     */
    public void setMinDate(int minDate) {
        long date1 = calendar.getTime().getTime();
//        long date = new Date().getTime();
        //减去1000毫秒是因为最小日期不能等于当前时间
        date1 = date1 + ((long) minDate * 24 * 60 * 60 * 1000) - 1000;
        datePicker.setMinDate(date1);
    }


    public interface OnClickCallback {
        void onclick(int year, int month, int day, int hour, int minute);
    }

    public void setOnClickCallback(OnClickCallback callback) {
        onClickCallback = callback;

    }


    /**
     * DatePicker 或者 TimePicker 调整大小
     *
     * @param tp
     */
    public static void setPikcerSize(FrameLayout tp) {
        List<NumberPicker> npList = findNumberPicker(tp);
        for (int i = 0; i < npList.size(); i++) {
            setNumberPickerSize(npList.get(i), tp instanceof DatePicker ? i == 0 : false);
        }
    }

    /**
     * 调整numberpicker大小
     */
    private static void setNumberPickerSize(NumberPicker np, boolean isOne) {
        UiUtils.setViewMeasure(np);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Math.round(np.getMeasuredWidth() / (isOne ? 1.2f : 2.2f)), ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(ObjectUtils.dip2px(np.getContext(), 4), 0, ObjectUtils.dip2px(np.getContext(), 4), 0);
        np.setLayoutParams(params);

    }


    /**
     * 得到viewGroup里面的numberpicker组件
     *
     * @param viewGroup
     * @return
     */
    private static List<NumberPicker> findNumberPicker(ViewGroup viewGroup) {
        List<NumberPicker> npList = new ArrayList<NumberPicker>();
        View child = null;
        if (null != viewGroup) {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                child = viewGroup.getChildAt(i);
                if (child instanceof NumberPicker) {
                    npList.add((NumberPicker) child);
                } else if (child instanceof LinearLayout) {
                    List<NumberPicker> result = findNumberPicker((ViewGroup) child);
                    if (result.size() > 0) {
                        return result;
                    }
                }
            }
        }
        return npList;
    }




}