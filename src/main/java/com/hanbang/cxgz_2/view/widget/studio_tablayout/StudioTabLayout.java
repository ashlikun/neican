package com.hanbang.cxgz_2.view.widget.studio_tablayout;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.utils.ui.DrawableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/9　9:47
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioTabLayout extends LinearLayout {
    private List<StudioTabItem> menus = new ArrayList<>();
    int ovalTop = 0;
    private int textSizeSp = 12;
    OnItemClickListener onItemClickListener;

    public StudioTabLayout(Context context) {
        this(context, null);
    }

    public StudioTabLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StudioTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.BOTTOM);
    }


    public void addSubMenus(StudioTabItem... sub) {
        clear();
        for (StudioTabItem s : sub) {
            menus.add(s);
        }
        addSubView();
    }

    public void addSubMenus(List<StudioTabItem> menus) {
        clear();
        this.menus.addAll(menus);
    }

    public void clear() {
        menus.clear();
        removeAllViews();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (menus != null && menus.size() > 0 && getChildCount() <= 0) {
            addSubView();
        }


        super.onLayout(changed, l, t, r, b);
    }

    private void addSubView() {

        for (int i = 0; i < menus.size(); i++) {

            addView(getItemView(menus.get(i)));
            if (i != menus.size() - 1) {
                addView(getLinView());
            }
        }
    }


    private LinearLayout getItemView(final StudioTabItem s) {
        TopOvalView lin = new TopOvalView(getContext());

        lin.setOrientation(VERTICAL);
        lin.setGravity(Gravity.CENTER);
        lin.setPadding(0, getResources().getDimensionPixelSize(R.dimen.dp_8), 0,
                getResources().getDimensionPixelSize(R.dimen.dp_6));
        lin.setHeave(s.isHeave());

        LayoutParams imbParams = new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ImageView imb = new ImageView(getContext());
        if (s.getDrawableRes() > 0) {
            imb.setImageResource(s.getDrawableRes());
        } else if (s.getDrawable() != null) {
            imb.setImageDrawable(s.getDrawable());
        }
        imb.setLayoutParams(imbParams);
        lin.addView(imb, imbParams);
        LayoutParams titleParams = new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        titleParams.topMargin = getResources().getDimensionPixelSize(R.dimen.dp_8);
        TextView title = new TextView(getContext());
        title.setTextSize(textSizeSp);
        title.setText(s.getTitle());
        if (s.isHeave()) {
            title.setTextColor(getResources().getColor(R.color.main_yellow));
        } else
            title.setTextColor(DrawableUtils.getColorSelect(getResources().getColor(R.color.white), getResources().getColor(R.color.coffee)));
        imb.setLayoutParams(titleParams);
        lin.addView(title, titleParams);
        LinearLayout.LayoutParams params = new LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        lin.setLayoutParams(params);
        lin.setTag(s.group);

        lin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    if (onItemClickListener.onItemClick(v, s.group, s)) {
                        selectItem(s.group);
                    }
                }
            }
        });

        ovalTop = lin.getOvalTop();
        if (s.isHeave()) {
            setManageTop();
        }


        return lin;
    }

    public void selectItem(int group) {
        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            if (v.getTag() != null) {
                try {
                    int g = (int) v.getTag();
                    v.setSelected(g == group);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }
    }


    private View getLinView() {
        View v = new View(getContext());
        v.setBackgroundResource(R.color.main_yellow);
        LinearLayout.LayoutParams params = new LayoutParams(getResources().getDimensionPixelSize(R.dimen.dp_0_5), ViewGroup.LayoutParams.MATCH_PARENT);
        params.topMargin = ovalTop;
        v.setLayoutParams(params);
        return v;
    }

    public void setManageTop() {
        ViewGroup.LayoutParams params = getLayoutParams();
        if (params instanceof AppBarLayout.LayoutParams) {
            ((AppBarLayout.LayoutParams) params).topMargin = -ovalTop;
        }
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {

        boolean onItemClick(View view, int position, StudioTabItem data);
    }
}
