package com.hanbang.cxgz_2.view.widget.studio_tablayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.hanbang.cxgz_2.R;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/9　13:59
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class TopOvalView extends LinearLayout {
    Paint paint = new Paint();
    private boolean isPaddingOk = false;
    private RectF rect = new RectF(); //椭圆区域
    private RectF rectJu = new RectF(); //矩形区域
    private float ovalWidth = 0;
    private float ovalHeight = 0;
    private boolean isHeave = false;
    private float factor = 4.0f;//凸起系数

    public TopOvalView(Context context) {
        this(context, null);

    }

    public TopOvalView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TopOvalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }


    private void initView(Context context, AttributeSet attrs) {
        ovalWidth = getResources().getDimensionPixelSize(R.dimen.dp_50);
        ovalHeight = getResources().getDimensionPixelSize(R.dimen.dp_40);

        paint.setColor(getResources().getColor(R.color.studio_tablayout_bg));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        setHeave(isHeave);
    }

    public void setHeave(boolean heave) {
        isHeave = heave;

        if (isHeave) {
            setBackgroundResource(R.color.translucent);

            if (!isPaddingOk) {
                setPadding(getPaddingLeft(), (int) (getPaddingTop() + ovalHeight / factor), getPaddingRight(), getPaddingBottom());
            }
            isPaddingOk = true;

        } else {
            setBackgroundResource(R.color.studio_tablayout_bg);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (width == 0 || height == 0) {
            width = getWidth();
            height = getHeight();
        }
        if (width == 0 || height == 0) {
            return;
        }
        if (width != 0 && width < ovalWidth) {
            ovalWidth = (float) (width / 1.2);
        }
        rect.top = 0;
        rect.left = width / 2 - ovalWidth / 2;
        rect.right = rect.left + ovalWidth;
        rect.bottom = ovalHeight;

        rectJu.top = ovalHeight / factor;
        rectJu.left = 0;
        rectJu.right = width;
        rectJu.bottom = height;
    }
    public int getOvalTop() {
        return (int) (ovalHeight / factor);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isHeave) {
            canvas.drawOval(rect, paint);
            canvas.drawRect(rectJu, paint);
        }

    }
}
