package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hanbang.cxgz_2.utils.ui.DrawableUtils;
import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.utils.ui.UiUtils;


import butterknife.BindView;

/**
 * Created by Administrator on 2016/4/26.
 */
public class SpinnerButton extends LinearLayout {

    public SpinnerButton(Context context) {
        this(context, null);
    }

    public SpinnerButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpinnerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        initView(attrs);
    }

    private void initView(AttributeSet attrs) {
        ViewGroup root = (ViewGroup) UiUtils.getInflaterView(getContext(), R.layout.spinner_button_view);
        textview = (TextView) root.findViewById(R.id.textview);
        iconV = root.findViewById(R.id.tubiao);
        root.removeAllViews();

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SpinnerButton, 0, 0);

        String title = a.getString(R.styleable.SpinnerButton_sbTitleText);
        textview.setText(title);
        String tint = a.getString(R.styleable.SpinnerButton_sbHintText);
        textview.setHint(tint);
        int titlecolor = a.getColor(R.styleable.SpinnerButton_sbTitleColor, -1);
        if (titlecolor != -1) {
            textview.setTextColor(titlecolor);
        }
        textview.setLineSpacing(a.getDimensionPixelSize(R.styleable.SpinnerButton_sbLineSpacingExtra, 0), 1);

        //是否显示下拉图标
        int iconVisable = a.getInt(R.styleable.SpinnerButton_iconVisable, VISIBLE);
        iconV.setVisibility(iconVisable);
        setMaxHeight(a.getDimensionPixelSize(R.styleable.SpinnerButton_sbMaxHeight, 0));
        setMaxLines(a.getInt(R.styleable.SpinnerButton_sbMaxLines, 0));
        setMaxWidth(a.getDimensionPixelSize(R.styleable.SpinnerButton_sbMaxWidth, 0));

        //是否显示边框
        boolean frameVisable = a.getBoolean(R.styleable.SpinnerButton_frameVisable, true);
        if (!frameVisable) {
            setBackgroundResource(R.color.translucent);
        } else {
            DrawableUtils.setBackgroundCompat(this, DrawableUtils.getStateListDrawable(R.color.translucent, R.color.gray, (int) getResources().getDimension(R.dimen.dp_4), (int) getResources().getDimension(R.dimen.dp_0_5), R.color.gray));
        }
        addView(textview);
        addView(iconV);
        a.recycle();


    }

    public void setText(String string) {
        if (textview != null && string != null)
            textview.setText(string);
    }

    /**
     * 设置输入框的InputType
     * 官方文档http://developer.android.com/intl/zh-cn/reference/android/widget/TextView.html#attr_android:inputType
     * 列 ： text : TYPE_CLASS_TEXT | TYPE_TEXT_VARIATION_NORMAL
     * textPassword : TYPE_CLASS_TEXT | TYPE_TEXT_VARIATION_PASSWORD
     * number:TYPE_CLASS_NUMBER | TYPE_NUMBER_VARIATION_NORMAL
     * numberDecimal:TYPE_CLASS_NUMBER | TYPE_NUMBER_FLAG_DECIMAL
     * numberPassword:TYPE_CLASS_NUMBER | TYPE_NUMBER_VARIATION_PASSWORD
     * phone:TYPE_CLASS_PHONE
     * datetime:TYPE_CLASS_DATETIME | TYPE_DATETIME_VARIATION_NORMAL
     * date:TYPE_CLASS_DATETIME | TYPE_DATETIME_VARIATION_DATE
     * time:TYPE_CLASS_DATETIME | TYPE_DATETIME_VARIATION_TIME
     */
    public void setInputType(int inputType) {
        textview.setInputType(inputType);
    }

    public void setMaxLength(int maxLength) {
        if (maxLength > 0) {
            textview.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        }
    }

    public void setMaxLines(int maxLines) {
        if (maxLines > 0) {
            textview.setMaxLines(maxLines);
        }
    }

    public void setMaxHeight(int maxHeight) {
        if (maxHeight > 0) {
            textview.setMaxHeight(maxHeight);
        }
    }

    public void setMaxWidth(int maxWidth) {
        if (maxWidth > 0) {
            textview.setMaxWidth(maxWidth);
        }
    }


    public void setMinLines(int minLines) {
        if (minLines > 0) {
            textview.setMinLines(minLines);
        }
    }

    public void setMinHeight(int minHeight) {
        if (minHeight > 0) {
            textview.setMinHeight(minHeight);
        }
    }

    public void setMinWidth(int minWidth) {
        if (minWidth > 0) {
            textview.setMinWidth(minWidth);
        }
    }

    @BindView(R.id.textview)
     TextView textview;
    @BindView(R.id.tubiao)
     View iconV;
}
