//XTextView类 
package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class XTextView extends TextView {
	public XTextView(Context context) {
		super(context);
	}

	public XTextView(Context context, AttributeSet set) {
		super(context, set);
	}

	public XTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

	// 半角转化为全角的方法
	public String ToSBC(String input) {
		// 半角转全角：
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 32) {
				c[i] = (char) 12288;
				continue;
			}
			if (c[i] < 127 && c[i] > 32)
				c[i] = (char) (c[i] + 65248);
		}
		return new String(c);
	}

	// 全角转化为半角的方法
	public String ToDBC(String input) {
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 12288) {
				c[i] = (char) 32;
				continue;
			}
			if (c[i] > 65280 && c[i] < 65375)
				c[i] = (char) (c[i] - 65248);
		}
		return new String(c);
	}

	// 在标点符号后面加空格
	public String ToBiao(String input) {
		input = input.replaceAll("，", "， ");
		input = input.replaceAll(",", ", ");

		input = input.replaceAll("。", "。 ");
		input = input.replaceAll("：", "： ");
		input = input.replaceAll(":", ": ");
		input = input.replaceAll("！", "！ ");
		input = input.replaceAll("!", "! ");

		input = input.replaceAll("？", "？ ");

		input = input.replaceAll("“", "“ ");

		input = input.replaceAll("”", "” ");

		input = input.replaceAll("【", "【 ");
		input = input.replaceAll("】", "】 ");

		input = input.replaceAll("（", "（ ");

		input = input.replaceAll("）", "） ");
		input = input.replaceAll("-", "- ");
		input = input.replaceAll("、", "、 ");

		return input;
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		String aaa = ToBiao(text.toString());
		super.setText(ToDBC(aaa), type);
	}
}