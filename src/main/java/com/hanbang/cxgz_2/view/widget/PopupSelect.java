package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.javabean.other.CaiXiData;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;
import com.hanbang.cxgz_2.utils.ui.UiUtils;

import java.util.ArrayList;

/**
 *
 * @author yang
 */
public class PopupSelect extends PopupWindow implements OnClickListener {
    private View conentView;

    /**
     * 存放里的item
     */
    public ArrayList<TextView> parentView;

    /**
     * 对话框点击事件
     */
    private OnClickCallback onClickCallback = null;

    /**
     * 存放条目的父容器（server item parent vessel）
     */
    private LinearLayout parent;

    /**
     * 控制滑动的显示范围（contrle slide scope）
     */
    private ScrollView sv;

    /**
     * 最多显示的item 的数量
     */
    private int itemNum;

    public View view;

    public int type;

    



    /**
     * @param context 传入一个调入者对象（上下文菜单）
     * @param content 这个数组是item 的显示内容
     * @param itemNum 最多显示的数量
     */
    public PopupSelect(final Context context, ArrayList<CaiXiData> content, int itemNum, int type) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        /** 得到视图 */
        conentView = inflater.inflate(R.layout.item_popupwindow_public, null);

        this.type = type;
        parent = (LinearLayout) conentView.findViewById(R.id.parent);
        sv = (ScrollView) conentView.findViewById(R.id.scrollView);
        this.itemNum = itemNum;
        addItem(context, content);

        /**
         * popup 的一些设置
         */
        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);

        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);

        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(new BitmapDrawable());
        // 刷新状态
        this.update();
        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimationPreview);
        this.setOutsideTouchable(true);

    }

    /**
     * popup 的初始化与数据的加载，和添加监听
     *
     * @param context 上下文
     * @param content 显示的内容
     */

    private void addItem(Context context, ArrayList<CaiXiData> content) {
        if (content == null) return;
        parentView = new ArrayList<TextView>();

        for (int i = 0; i < content.size(); i++) {

            TextView view = new TextView(context);
            view.setGravity(Gravity.CENTER);
            view.setText(content.get(i).getTitle());
            view.setTextColor(0xff000000);
            view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 80, 0));
            view.setId(content.get(i).getId());
            view.setTextSize(15);
            view.setPadding(ObjectUtils.dip2px(context, 10), 0, ObjectUtils.dip2px(context, 10), 0);
            parent.addView(view);
            parentView.add(view);

            // 单个item 的高度
            int itemHeight = view.getLayoutParams().height;
            /**
             * time 显示的最大高度
             */
            if (i == itemNum) {
                LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) sv.getLayoutParams(); // 取控件sv当前的布局参数
                linearParams.height = itemHeight * itemNum;// 当控件的高强制设成50象素
                sv.setLayoutParams(linearParams); // 使设置好的布局参数应用到控件sv
            }

        }

        // 遍历添加监听
        for (int i = 0; i < parentView.size(); i++) {
            parentView.get(i).setOnClickListener(this);
        }

    }


    /**
     * 对话框的显示位置
     *
     * @param parent 父控件
     */
    public void showPopupWindow(View parent) {
        UiUtils.setViewMeasure(conentView);
        if (!this.isShowing()) {

            int w1 = conentView.getMeasuredWidth();
            int w2 = parent.getWidth();
            float res = w1 > w2 ? -w1 / 2.0f + w2 / 2.0f : w2 / 2.0f - w1 / 2.0f;
            setWidth(w1);

            this.showAsDropDown(parent, Math.round(res), 0);


        } else {

            this.dismiss();
        }

    }

    /**
     * 监听的处理
     */
    @Override
    public void onClick(View v) {

        if (onClickCallback != null) {
            /**
             * 实现接口并赋值
             */
            onClickCallback.onClick( v.getId(), ((TextView) v).getText().toString(), type);
            dismiss();

        } else {
            dismiss();
        }

    }

    /**
     * 接口
     */
    public interface OnClickCallback {

         void onClick(int id, String content, int type);
    }

    /**
     * 当设置监听时必须实现接口,通过接口参数把一些信息传递给调用者
     *
     * @param callback
     */
    public void setOnClickCallback(OnClickCallback callback) {
        onClickCallback = callback;
    }

}
