package com.hanbang.cxgz_2.view.widget.studio_tablayout;

import android.graphics.drawable.Drawable;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/9　10:04
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class StudioTabItem {
    int drawableRes;
    Drawable drawable;
    int group;
    String title;
    boolean isHeave = false;//是否是凸起的item
    boolean isClickUp = false;//是否是点击跳转

    public StudioTabItem(String title, int group, Drawable drawable) {
        this.title = title;
        this.group = group;
        this.drawable = drawable;
    }

    public StudioTabItem(String title, int group, int drawableRes) {
        this.title = title;
        this.group = group;
        this.drawableRes = drawableRes;
    }

    public StudioTabItem(String title, int group, int drawableRes, boolean isHeave) {
        this.drawableRes = drawableRes;
        this.group = group;
        this.title = title;
        this.isHeave = isHeave;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public int getDrawableRes() {
        return drawableRes;
    }

    public void setDrawableRes(int drawableRes) {
        this.drawableRes = drawableRes;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isHeave() {
        return isHeave;
    }

    public void setHeave(boolean heave) {
        isHeave = heave;
    }

    public boolean isClickUp() {
        return isClickUp;
    }

    public StudioTabItem setClickUp(boolean clickUp) {
        isClickUp = clickUp;
        return this;
    }
}
