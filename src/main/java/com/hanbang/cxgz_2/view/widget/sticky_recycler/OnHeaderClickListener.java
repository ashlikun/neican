package com.hanbang.cxgz_2.view.widget.sticky_recycler;

import android.view.View;

/**
 * Created by aurel on 08/11/14.
 */
public interface OnHeaderClickListener {
    void onHeaderClick(View header, long headerId);
}
