/*
 * Copyright 2014 Hieu Rocker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hanbang.cxgz_2.view.widget.emoj;

import android.content.Context;
import android.text.Spannable;
import android.util.SparseIntArray;

import com.hanbang.cxgz_2.R;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Hieu Rocker (rockerhieu@gmail.com)
 */
public final class EmojiconHandler {
    private EmojiconHandler() {
    }

    public static final SparseIntArray emojiMap = new SparseIntArray(1029);
    private static final SparseIntArray sSoftbanksMap = new SparseIntArray(471);
    private static final SparseIntArray sEmojiModifiersMap = new SparseIntArray(5);
    private static Map<String, Integer> sEmojisModifiedMap = new HashMap<>();

    static {
        sEmojiModifiersMap.put(0x1f3fb, 1);
        sEmojiModifiersMap.put(0x1f3fc, 1);
        sEmojiModifiersMap.put(0x1f3fd, 1);
        sEmojiModifiersMap.put(0x1f3fe, 1);
        sEmojiModifiersMap.put(0x1f3ff, 1);
    }

    static {

        emojiMap.put("[f01]".hashCode(), R.drawable.f01);
        emojiMap.put("[f02]".hashCode(), R.drawable.f02);
        emojiMap.put("[f03]".hashCode(), R.drawable.f03);
        emojiMap.put("[f04]".hashCode(), R.drawable.f04);
        emojiMap.put("[f05]".hashCode(), R.drawable.f05);
        emojiMap.put("[f06]".hashCode(), R.drawable.f06);
        emojiMap.put("[f07]".hashCode(), R.drawable.f07);
        emojiMap.put("[f08]".hashCode(), R.drawable.f08);
        emojiMap.put("[f09]".hashCode(), R.drawable.f09);
        emojiMap.put("[f10]".hashCode(), R.drawable.f10);

        emojiMap.put("[f11]".hashCode(), R.drawable.f11);
        emojiMap.put("[f12]".hashCode(), R.drawable.f12);
        emojiMap.put("[f13]".hashCode(), R.drawable.f13);
        emojiMap.put("[f14]".hashCode(), R.drawable.f14);
        emojiMap.put("[f15]".hashCode(), R.drawable.f15);
        emojiMap.put("[f16]".hashCode(), R.drawable.f16);
        emojiMap.put("[f17]".hashCode(), R.drawable.f17);
        emojiMap.put("[f18]".hashCode(), R.drawable.f18);
        emojiMap.put("[f19]".hashCode(), R.drawable.f19);
        emojiMap.put("[f20]".hashCode(), R.drawable.f20);

        emojiMap.put("[f21]".hashCode(), R.drawable.f21);
        emojiMap.put("[f22]".hashCode(), R.drawable.f22);
        emojiMap.put("[f23]".hashCode(), R.drawable.f23);
        emojiMap.put("[f24]".hashCode(), R.drawable.f24);
        emojiMap.put("[f25]".hashCode(), R.drawable.f25);
        emojiMap.put("[f26]".hashCode(), R.drawable.f26);
        emojiMap.put("[f27]".hashCode(), R.drawable.f27);
        emojiMap.put("[f28]".hashCode(), R.drawable.f28);
        emojiMap.put("[f29]".hashCode(), R.drawable.f29);
        emojiMap.put("[f30]".hashCode(), R.drawable.f30);

        emojiMap.put("[f31]".hashCode(), R.drawable.f31);
        emojiMap.put("[f32]".hashCode(), R.drawable.f32);
        emojiMap.put("[f33]".hashCode(), R.drawable.f33);
        emojiMap.put("[f34]".hashCode(), R.drawable.f34);
        emojiMap.put("[f35]".hashCode(), R.drawable.f35);
        emojiMap.put("[f36]".hashCode(), R.drawable.f36);
        emojiMap.put("[f37]".hashCode(), R.drawable.f37);
        emojiMap.put("[f38]".hashCode(), R.drawable.f38);
        emojiMap.put("[f39]".hashCode(), R.drawable.f39);
        emojiMap.put("[f40]".hashCode(), R.drawable.f40);

        emojiMap.put("[f41]".hashCode(), R.drawable.f41);
        emojiMap.put("[f42]".hashCode(), R.drawable.f42);
        emojiMap.put("[f43]".hashCode(), R.drawable.f43);
        emojiMap.put("[f44]".hashCode(), R.drawable.f44);
        emojiMap.put("[f45]".hashCode(), R.drawable.f45);
        emojiMap.put("[f46]".hashCode(), R.drawable.f46);
        emojiMap.put("[f47]".hashCode(), R.drawable.f47);
        emojiMap.put("[f48]".hashCode(), R.drawable.f48);
        emojiMap.put("[f49]".hashCode(), R.drawable.f49);
        emojiMap.put("[f50]".hashCode(), R.drawable.f50);

        emojiMap.put("[f51]".hashCode(), R.drawable.f51);
        emojiMap.put("[f52]".hashCode(), R.drawable.f52);
        emojiMap.put("[f53]".hashCode(), R.drawable.f53);
        emojiMap.put("[f54]".hashCode(), R.drawable.f54);
        emojiMap.put("[f55]".hashCode(), R.drawable.f55);
        emojiMap.put("[f56]".hashCode(), R.drawable.f56);
        emojiMap.put("[f57]".hashCode(), R.drawable.f57);
        emojiMap.put("[f58]".hashCode(), R.drawable.f58);
        emojiMap.put("[f59]".hashCode(), R.drawable.f59);
        emojiMap.put("[f60]".hashCode(), R.drawable.f60);

        emojiMap.put("[f61]".hashCode(), R.drawable.f61);
        emojiMap.put("[f62]".hashCode(), R.drawable.f62);
        emojiMap.put("[f63]".hashCode(), R.drawable.f63);
        emojiMap.put("[f64]".hashCode(), R.drawable.f64);
        emojiMap.put("[f65]".hashCode(), R.drawable.f65);
        emojiMap.put("[f66]".hashCode(), R.drawable.f66);
        emojiMap.put("[f67]".hashCode(), R.drawable.f67);
        emojiMap.put("[f68]".hashCode(), R.drawable.f68);
        emojiMap.put("[f69]".hashCode(), R.drawable.f69);
        emojiMap.put("[f70]".hashCode(), R.drawable.f70);

        emojiMap.put("[f71]".hashCode(), R.drawable.f71);
        emojiMap.put("[f72]".hashCode(), R.drawable.f72);
        emojiMap.put("[f73]".hashCode(), R.drawable.f73);
        emojiMap.put("[f74]".hashCode(), R.drawable.f74);
        emojiMap.put("[f75]".hashCode(), R.drawable.f75);
        emojiMap.put("[f76]".hashCode(), R.drawable.f76);
        emojiMap.put("[f77]".hashCode(), R.drawable.f77);
        emojiMap.put("[f78]".hashCode(), R.drawable.f78);
        emojiMap.put("[f79]".hashCode(), R.drawable.f79);
        emojiMap.put("[f80]".hashCode(), R.drawable.f80);


        emojiMap.put("[f81]".hashCode(), R.drawable.f81);
        emojiMap.put("[f82]".hashCode(), R.drawable.f82);
        emojiMap.put("[f83]".hashCode(), R.drawable.f83);
        emojiMap.put("[f84]".hashCode(), R.drawable.f84);
        emojiMap.put("[f85]".hashCode(), R.drawable.f85);
        emojiMap.put("[f86]".hashCode(), R.drawable.f86);
        emojiMap.put("[f87]".hashCode(), R.drawable.f87);
        emojiMap.put("[f88]".hashCode(), R.drawable.f88);
        emojiMap.put("[f89]".hashCode(), R.drawable.f89);
        emojiMap.put("[f90]".hashCode(), R.drawable.f90);

        emojiMap.put("[f91]".hashCode(), R.drawable.f91);
        emojiMap.put("[f92]".hashCode(), R.drawable.f92);
        emojiMap.put("[f93]".hashCode(), R.drawable.f93);
        emojiMap.put("[f94]".hashCode(), R.drawable.f94);
        emojiMap.put("[f95]".hashCode(), R.drawable.f95);
        emojiMap.put("[f96]".hashCode(), R.drawable.f96);
        emojiMap.put("[f97]".hashCode(), R.drawable.f97);
        emojiMap.put("[f98]".hashCode(), R.drawable.f98);
        emojiMap.put("[f99]".hashCode(), R.drawable.f99);



    }

    private static boolean isSoftBankEmoji(char c) {
        return c == '[';
    }

    private static int getEmojiResource(Context context, int codePoint) {
        return emojiMap.get(codePoint);
    }

    private static int getSoftbankEmojiResource(char c) {
        return sSoftbanksMap.get(c);
    }

    /**
     * Convert emoji characters of the given Spannable to the according emojicon.
     *
     * @param context
     * @param text
     * @param emojiSize
     * @param emojiAlignment
     * @param textSize
     */
    public static void addEmojis(Context context, Spannable text, int emojiSize, int emojiAlignment, int textSize) {
        addEmojis(context, text, emojiSize, emojiAlignment, textSize, 0, -1, false);
    }

    /**
     * Convert emoji characters of the given Spannable to the according emojicon.
     *
     * @param context
     * @param text
     * @param emojiSize
     * @param emojiAlignment
     * @param textSize
     * @param index
     * @param length
     */
    public static void addEmojis(Context context, Spannable text, int emojiSize, int emojiAlignment, int textSize, int index, int length) {
        addEmojis(context, text, emojiSize, emojiAlignment, textSize, index, length, false);
    }

    /**
     * Convert emoji characters of the given Spannable to the according emojicon.
     *
     * @param context
     * @param text
     * @param emojiSize
     * @param emojiAlignment
     * @param textSize
     * @param useSystemDefault
     */
    public static void addEmojis(Context context, Spannable text, int emojiSize, int emojiAlignment, int textSize, boolean useSystemDefault) {
        addEmojis(context, text, emojiSize, emojiAlignment, textSize, 0, -1, useSystemDefault);
    }

    /**
     * Convert emoji characters of the given Spannable to the according emojicon.
     *
     * @param context
     * @param text
     * @param emojiSize
     * @param emojiAlignment
     * @param textSize
     * @param index
     * @param length
     * @param useSystemDefault
     */
    public static void addEmojis(Context context, Spannable text, int emojiSize, int emojiAlignment, int textSize, int index, int length, boolean useSystemDefault) {
        if (useSystemDefault) {
            return;
        }

        int textLength = text.length();

        // remove spans throughout all text
        EmojiconSpan[] oldSpans = text.getSpans(0, textLength, EmojiconSpan.class);
        for (int i = 0; i < oldSpans.length; i++) {
            text.removeSpan(oldSpans[i]);
        }

        String regexEmotion = "\\[([\u4e00-\u9fa5\\w])+\\]";

        Pattern patternEmotion = Pattern.compile(regexEmotion);

        Matcher matcherEmotion = patternEmotion.matcher(text);
        while (matcherEmotion.find()) {
            // 获取匹配到的具体字符
            String key = matcherEmotion.group();
            // 匹配字符串的开始位置
            int start = matcherEmotion.start();
            Integer imgRes = emojiMap.get(key.hashCode());
            if (imgRes != 0) {
                text.setSpan(new EmojiconSpan(context, imgRes, emojiSize, emojiAlignment, textSize), start, start + key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }


    }





}
