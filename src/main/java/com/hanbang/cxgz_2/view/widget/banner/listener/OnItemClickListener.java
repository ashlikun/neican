package com.hanbang.cxgz_2.view.widget.banner.listener;

/**
 * Created by Sai on 15/11/13.
 */
public interface OnItemClickListener {
    public void onItemClick(int position);
}
