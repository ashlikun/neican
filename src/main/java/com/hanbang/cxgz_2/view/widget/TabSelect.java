package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.javabean.base.KeyAndValus;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;
import com.hanbang.cxgz_2.utils.ui.DrawableUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Administrator on 2016/8/24.
 */

public class TabSelect extends LinearLayout {
    ArrayList<KeyAndValus> content = new ArrayList<KeyAndValus>();
    OnItemClickListener onItemClickListener;

    public TabSelect(Context context) {
        this(context, null);
    }

    public TabSelect(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TabSelect(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setOrientation(HORIZONTAL);
    }


    public void setContent(KeyAndValus... c) {
        content.clear();
        content.addAll(Arrays.asList(c));
        addTextView();
    }

    private void addTextView() {
        removeAllViews();
        for (int i = 0; i < content.size(); i++) {
            KeyAndValus c = content.get(i);
            LayoutParams la = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            la.weight = c.getValus().length();
            Button textView = new Button(getContext());
            textView.setTextColor(DrawableUtils.getColorSelect(getResources().getColor(R.color.main_black), getResources().getColor(R.color.main_color)));
            textView.setMaxLines(1);
            textView.setTextSize(ObjectUtils.px2sp(getContext(), getResources().getDimension(R.dimen.text_xxm)));
            textView.setText(c.getValus());
            textView.setGravity(Gravity.CENTER);
            textView.setPadding(0, getResources().getDimensionPixelSize(R.dimen.dp_8), 0,
                    getResources().getDimensionPixelOffset(R.dimen.dp_8));
            addView(textView, la);
            textView.setTag(i);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (Integer) view.getTag();
                    selectItem(position);
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(content.get(position).getKey(), position, content.get(position).getValus());

                    }
                }
            });
        }
        selectItem(0);
    }

    public void selectItem(int poistion) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            view.setEnabled(i == poistion ? false : true);
            view.setSelected(i == poistion ? true : false);
        }
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    public interface OnItemClickListener {
        void onItemClick(int key, int postion, String text);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
