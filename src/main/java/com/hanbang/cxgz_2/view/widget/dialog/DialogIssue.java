package com.hanbang.cxgz_2.view.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.baseadapter.ViewHolder;
import com.hanbang.cxgz_2.appliction.base.baseadapter.abslistview.CommonAdapter;
import com.hanbang.cxgz_2.mode.javabean.about_me.ItmeMeGridViewData;
import com.hanbang.cxgz_2.utils.ui.ScreenInfoUtils;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerClassroomActivity;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerEsoteric_D_Activity;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerEsoteric_K_Activity;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerXiuChangActivity;
import com.hanbang.cxgz_2.view.about_me.activity.IssuerRewardFragmentActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 发布的Dialog
 * Created by yang on 2016/8/24.
 */

public class DialogIssue extends Dialog {
    @BindView(R.id.dialog_issue_gridView)
    GridView gridView;
    @BindView(R.id.dialog_issue_item_text)
    TextView textView;

    private OnClickCallback clickCallback;
    private Context context;


    public DialogIssue(Context context) {
        this(context, R.style.Dialog_issue);
    }

    public DialogIssue(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        init();
    }

    private void init() {
        setContentView(R.layout.dialog_issue);
        ButterKnife.bind(this, getWindow().getDecorView().findViewById(R.id.dialog_issue_parent));


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        ScreenInfoUtils screen = new ScreenInfoUtils();
        lp.width = (screen.getWidth()); //设置宽度
        lp.height = (screen.getHeight()); //设置宽度
        getWindow().setAttributes(lp);
        getWindow().getAttributes().gravity = Gravity.CENTER;
        setAdater();
    }


    private void setAdater() {
        final ArrayList<ItmeMeGridViewData> datas = new ArrayList<>();
        datas.add(new ItmeMeGridViewData(R.mipmap.my_release_cheats, Global.ISSUE_ESOTERIC_K, IssuerEsoteric_K_Activity.class.getName()));
        datas.add(new ItmeMeGridViewData(R.mipmap.my_release_cheats, Global.ISSUE_ESOTERIC_D, IssuerEsoteric_D_Activity.class.getName()));
        datas.add(new ItmeMeGridViewData(R.mipmap.my_release_class, Global.ISSUE_STUDY_CLASSROOM, IssuerClassroomActivity.class.getName()));
        datas.add(new ItmeMeGridViewData(R.mipmap.my_release_needs, Global.ISSUE_DEMAND_REWARD, IssuerRewardFragmentActivity.class.getName()));
        datas.add(new ItmeMeGridViewData(R.mipmap.my_release_show, Global.ISSUE_SHOW_FIELD, IssuerXiuChangActivity.class.getName()));


        gridView.setAdapter(new CommonAdapter<ItmeMeGridViewData>(context, R.layout.dialog_issue_item, datas) {
            @Override
            public void convert(ViewHolder holder, ItmeMeGridViewData itmeMeGridViewData) {
                holder.setImageResource(R.id.dialog_issue_item_picture, itmeMeGridViewData.getPictureId());

                holder.setText(R.id.dialog_issue_item_text, itmeMeGridViewData.getTitle());
            }
        });


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                clickCallback.onClick(datas.get(position).getTitle(), datas.get(position).getActivityName());
                dismiss();
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


    public void setClickCallback(OnClickCallback clickCallback) {

        this.clickCallback = clickCallback;
    }

    public interface OnClickCallback {
        /**
         * @param title     标题
         * @param className 类名
         */
        void onClick(String title, String className);
    }

}
