package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by yang on 2016/9/18.
 */

public class WebViewForScroll extends WebView {


//    private int mMaxHeight = -1;
//
//    public WebViewForScroll(Context context) {
//        super(context);
//    }
//
//    public WebViewForScroll(Context context, AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    public WebViewForScroll(Context context, AttributeSet attrs, int defStyle) {
//        super(context, attrs, defStyle);
//    }
//
//    public WebViewForScroll(Context context, AttributeSet attrs, int defStyle,
//                            boolean privateBrowsing) {
//        super(context, attrs, defStyle, privateBrowsing);
//    }
//
//    public void setMaxHeight(int height) {
//        mMaxHeight = height;
//    }
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        if (mMaxHeight > -1 && getMeasuredHeight() > mMaxHeight) {
//            setMeasuredDimension(getMeasuredWidth(), mMaxHeight);
//        }
//    }




    private int expandSpec = MeasureSpec.makeMeasureSpec(
            Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

    public WebViewForScroll(Context context, AttributeSet attrs,
                                 int defStyle) {
        super(context, attrs, defStyle);
    }

    public WebViewForScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WebViewForScroll(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, expandSpec);
    }






}