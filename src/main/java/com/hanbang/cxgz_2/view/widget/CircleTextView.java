package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import com.hanbang.cxgz_2.utils.animator.AnimUtils;
import com.hanbang.cxgz_2.utils.http.BitmapUtil;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;


/**
 * Created by Administrator on 2015/12/23.
 */
public class CircleTextView extends TextView {

    private int circleColor = Color.RED;
    private Bitmap bitmap = null;
    private Paint paint = new Paint();
    private int number = 0;
    private int maxNumber = 99;

    public CircleTextView(Context context) {
        this(context, null);
    }

    public CircleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint.setColor(circleColor);
        paint.setAntiAlias(true);
        setGravity(Gravity.CENTER);
    }

    public CircleTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs);

    }

    public void setNUmber(int number) {
        setNUmber(number, number != this.number);
    }

    public void setNUmber(int number, boolean isAnimtion) {
        this.number = number;
        if (number < 0) {
            this.setVisibility(GONE);
        } else {
            this.setVisibility(VISIBLE);
        }
        if (number > maxNumber) {
            setText("99+");
        } else {
            setText(String.valueOf(number));
        }
        if (isAnimtion) {
            AnimUtils.scaleAnim(this, 1.4f, 0.5f, 400);
        }
    }

    public int getNumber() {
        return number;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        if (width <= height) {
            canvas.drawCircle(width / 2f, height / 2f, width / 2f, paint);
        } else {
            canvas.drawRoundRect(new RectF(0, 0, width, height), ObjectUtils.dip2px(getContext(), 5), ObjectUtils.dip2px(getContext(), 5), paint);
        }
        if (bitmap != null) {
            int tidu = width / 5;
            canvas.drawBitmap(bitmap, null, new Rect(tidu, tidu, (width) - tidu, (height) - tidu), null);
        }
        super.onDraw(canvas);
    }

    public int getCircleColor() {
        return circleColor;
    }

    public void setCircleColor(int circleColor) {
        this.circleColor = circleColor;
        paint.setColor(circleColor);
        paint.setAntiAlias(true);
        invalidate();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        invalidate();
    }

    public void setBitmapRes(int bitmap) {
        this.bitmap = BitmapUtil.decodeResource(getContext(), bitmap);
        invalidate();
    }


}
