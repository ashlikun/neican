package com.hanbang.cxgz_2.view.widget;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hanbang.cxgz_2.R;
import com.hanbang.cxgz_2.mode.javabean.jianghu.EsotericZhuFuLiaoData;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;
import com.hanbang.cxgz_2.utils.ui.ToastUtils;

import java.util.ArrayList;


/**
 * 作者　　: 杨阳
 * 创建时间: 2016/9/2 23:09
 * 邮箱　　：360621904@qq.com
 * <p>
 * 功能介绍：添加主料辅料的view
 */

public class IssuerEsotericAddMaterial extends LinearLayout implements View.OnClickListener {
    //要返回的数据
    private ArrayList<EsotericZhuFuLiaoData> datas = new ArrayList<>();
    private Context context;
    private OnClickCallback onClickCallback;
    //记录插入的行数
    private int index = -1;

    //添加的名称
    private String addName;
    //列名1
    private String lineName1 = "名称";
    //列名2
    private String LineName2 = "重量/数量";
    //默认显示行的数量
    private int defaultItemCount = 1;

    public IssuerEsotericAddMaterial(Context context) {
        this(context, null);


    }

    public IssuerEsotericAddMaterial(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.addListItem);

        if (!TextUtils.isEmpty(typedArray.getString(R.styleable.addListItem_addName))) {

            addName = typedArray.getString(R.styleable.addListItem_addName);
        }

        if (!TextUtils.isEmpty(typedArray.getString(R.styleable.addListItem_lineName1))) {

            lineName1 = typedArray.getString(R.styleable.addListItem_lineName1);
        }

        if (!TextUtils.isEmpty(typedArray.getString(R.styleable.addListItem_lineName2))) {

            LineName2 = typedArray.getString(R.styleable.addListItem_lineName2);
        }

        defaultItemCount = typedArray.getInteger(R.styleable.addListItem_defaultItemCount, 1);


        typedArray.recycle();
        init();
        addDefaultItemCount(defaultItemCount);
    }


    private void init() {
        //在布局中显示

        setOrientation(VERTICAL);
        setBackgroundColor(context.getResources().getColor(R.color.white));


        addItem(new EsotericZhuFuLiaoData(lineName1, LineName2), true);
        addListener(addName);
    }


    /**
     * 添加item
     *
     * @param data
     * @param first 是不是创建第一个item
     */
    private void addItem(EsotericZhuFuLiaoData data, boolean first) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        LinearLayout ll = new LinearLayout(context);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.setPadding(0, 0, 0, 0);
        ll.setBackgroundColor(context.getResources().getColor(R.color.gray_ee));
        ll.setGravity(Gravity.CENTER);
        ll.setLayoutParams(params);

        //主料
        LinearLayout.LayoutParams ll_1 = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
        final EditText material = new EditText(context);
        material.setText(data.getName());
        material.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        material.setTextColor(context.getResources().getColor(R.color.gray_99));
        material.setGravity(Gravity.CENTER);
        material.setBackgroundColor(0x00);
        material.setHint("输入名称");
        material.setLayoutParams(ll_1);

        ll.addView(material, ll_1);


        //分界线
        LinearLayout.LayoutParams ll_2 = new LinearLayout.LayoutParams(1, LayoutParams.MATCH_PARENT);
        View boundary = new TextView(context);
        boundary.setLayoutParams(ll_2);
        boundary.setBackgroundColor(context.getResources().getColor(R.color.white));

        ll.addView(boundary, ll_2);

        //重量
        LinearLayout.LayoutParams ll_3 = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
        final EditText weight = new EditText(context);
        weight.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        weight.setTextColor(ObjectUtils.sp2px(context, context.getResources().getColor(R.color.gray_99)));
        weight.setGravity(Gravity.CENTER);
        weight.setText(data.getWeight());
        weight.setBackgroundColor(0x00);
        weight.setHint("输入重量");
        weight.setLayoutParams(ll_3);

        ll.addView(weight, ll_3);


        //分界线
        LinearLayout.LayoutParams ll_4 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 1);
        View boundary2 = new View(context);
        boundary2.setLayoutParams(ll_4);
        boundary2.setBackgroundColor(context.getResources().getColor(R.color.white));

        addView(ll, getChildCount() - 1, params);
        addView(boundary2, getChildCount() - 1, ll_4);

        //第一创建为标题没有点击事件
        if (first) {
            material.setFocusable(false);
            material.setTextColor(context.getResources().getColor(R.color.black));
            weight.setFocusable(false);
            weight.setTextColor(context.getResources().getColor(R.color.black));
        }

        //设置标签
        if (index != -1) {
            material.setTag(index);
            weight.setTag(index);
        }

        //监听名称的改变
        material.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                datas.get((int) material.getTag()).setName(material.getText().toString().trim());
                setReturnDatas();
            }

        });

        //监听重量的改变
        weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                datas.get((int) weight.getTag()).setWeight(weight.getText().toString().trim());
                setReturnDatas();

            }
        });

        //为新出的item增加动画
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(ll, "translationY", -getHeight(), 0f);
        objectAnimator.setDuration(500);
        objectAnimator.setInterpolator(new OvershootInterpolator());
        objectAnimator.start();

    }

    /**
     * 增加继续添加按钮
     */
    private void addListener(String addName) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        LinearLayout ll = new LinearLayout(context);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.setBackgroundColor(context.getResources().getColor(R.color.main_color));
        ll.setPadding(0, 15, 0, 15);
        ll.setGravity(Gravity.CENTER);
        ll.setLayoutParams(params);

        //加号
        LinearLayout.LayoutParams ll_1 = new LinearLayout.LayoutParams(ObjectUtils.dip2px(context, context.getResources().getDimension(R.dimen.dp_6)), ObjectUtils.dip2px(context, context.getResources().getDimension(R.dimen.dp_6)));
        ImageView im = new ImageView(context);
        im.setImageResource(R.drawable.material_ic_add);
        im.setLayoutParams(ll_1);

        ll.addView(im, ll_1);

        //提示添加文本
        LinearLayout.LayoutParams ll_2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        TextView hint = new TextView(context);
        hint.setText(addName);
        hint.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        hint.setTextColor(context.getResources().getColor(R.color.white));
        hint.setLayoutParams(ll_2);
        hint.setPadding(10, 0, 0, 0);

        ll.addView(hint, ll_2);
        addView(ll, params);

        //分界线
        LinearLayout.LayoutParams ll_4 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 1);
        View boundary2 = new View(context);
        boundary2.setLayoutParams(ll_4);
        boundary2.setBackgroundColor(context.getResources().getColor(R.color.white));
        addView(boundary2, getChildCount() - 1, ll_4);

        ll.setOnClickListener(this);

    }

    /**
     * 显示默认显示Item数量
     */
    private void addDefaultItemCount(int count) {

        for (int i = 0; i < count; i++) {
            index++;
            datas.add(new EsotericZhuFuLiaoData("", ""));
            addItem(datas.get(index), false);
        }
    }

    @Override
    public void onClick(View v) {
        //先判断内容有没有填写
        for (int i = 0; i < datas.size(); i++) {

            if (datas.size() != 0) {
                if (TextUtils.isEmpty(datas.get(i).getName())) {
                    ToastUtils.getToastShort("名称没有填写");
                    return;
                }

                if (TextUtils.isEmpty(datas.get(i).getWeight())) {
                    ToastUtils.getToastShort("重量没有填写");
                    return;
                }
            }
        }

        index++;
        datas.add(new EsotericZhuFuLiaoData("", ""));
        addItem(datas.get(index), false);
    }

    /**
     * 回调接口
     */
    public interface OnClickCallback {

        void onClick(ArrayList<EsotericZhuFuLiaoData> datas);
    }

    /**
     * 设置回调
     *
     * @param callback
     */
    public void setOnClickCallback(OnClickCallback callback) {
        onClickCallback = callback;
    }

    /**
     * 设置返回数据
     */
    private void setReturnDatas() {
        if (onClickCallback != null && datas.size() > 0) {

            onClickCallback.onClick(datas);
        }
    }

}
