package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;

import com.hanbang.cxgz_2.R;

import java.util.Timer;
import java.util.TimerTask;


public class CountdownTextView extends XTextView {
	private long time = 0;
	private boolean isRun = false;
	private long day = 0;
	private long hours = 0;
	private long minutes = 0;
	private long second = 0;
	private Timer timer = new Timer();
	private String endString = "已结束";
	private MyTimerTask myTimerTask = null;

	private class MyTimerTask extends TimerTask {

		@Override
		public void run() {
			if (isRun) {
				if (time >= 0) {
					time--;
					handler.sendEmptyMessage(158);
				} else {
					handler.sendEmptyMessage(159);
					isRun = false;
					myTimerTask.cancel();
				}
			}
		}
	};

	private Handler handler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			if (msg.what == 158) {
				setText(getTimeToString());
			} else if (msg.what == 159) {
				setText(getTimeToString());
			}

		};

	};

	public CountdownTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CountdownTextView(Context context) {
		this(context, null);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		isRun = false;
		if (myTimerTask != null) {
			myTimerTask.cancel();
		}
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		if (myTimerTask != null) {
			myTimerTask.cancel();
		}
		myTimerTask = new MyTimerTask();
		timer.schedule(myTimerTask, 1000, 1000);
	}

	public long getTime() {
		return time;
	}

	public void setTime(long t) {
		if (t < 0) {
			t = 0;
		}
		this.time = t;
		isRun = true;
	}

	public void setTime(long t, String endString) {
		this.endString = endString;
		if (t < 0) {
			t = 0;
		}
		this.time = t;
		isRun = true;
	}

	public void setOnTimeEndListen(OnTimeEndListen onTimeEndListen) {
		this.onTimeEndListen = onTimeEndListen;
	}

	private OnTimeEndListen onTimeEndListen = null;

	public interface OnTimeEndListen {
		void onEndListen();
	}

	public boolean isRun() {
		return isRun;
	}

	public void setRun(boolean isRun) {
		this.isRun = isRun;
	}

	public void setEndString(String endS) {
		this.endString = endS;
	}

	private String getTimeToString() {
		StringBuilder stringBuilder = new StringBuilder();
		if (time >= 0) {
			day = time / (24 * 60 * 60);
			hours = (time % (24 * 60 * 60)) / (60 * 60);
			minutes = ((time % (24 * 60 * 60)) % (60 * 60)) / 60;
			second = ((time % (24 * 60 * 60)) % (60 * 60)) % 60;
			stringBuilder.append(day);
			stringBuilder.append("天  ");
			stringBuilder.append(hours);
			stringBuilder.append("时");
			stringBuilder.append(minutes);
			stringBuilder.append("分");
			stringBuilder.append(second);
			stringBuilder.append("秒");
			setTextColor(getResources().getColor(R.color.main_color));
		} else {
			stringBuilder.append(endString);
			setTextColor(getResources().getColor(R.color.gray_text));
			if (onTimeEndListen != null) {
				onTimeEndListen.onEndListen();
			}
		}
		return stringBuilder.toString();

	}

}
