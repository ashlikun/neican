package com.hanbang.cxgz_2.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.hanbang.cxgz_2.R;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/9　17:30
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class BorderImageView extends ImageView {
    private int borderWidth = 0;
    private int borderColor = 0;
    private Paint paint = new Paint();

    public BorderImageView(Context context) {
        this(context, null);
    }

    public BorderImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BorderImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BorderImageView);
        this.borderWidth = (int) typedArray.getDimension(R.styleable.BorderImageView_BIborderWidth, getResources().getDimensionPixelSize(R.dimen.dp_3));
        this.borderColor = typedArray.getColor(R.styleable.BorderImageView_BIborderColor, getResources().getColor(R.color.main_yellow));
        typedArray.recycle();
        paint.setStrokeWidth(borderWidth);
        paint.setColor(borderColor);
        paint.setStyle(Paint.Style.STROKE); //绘制空心圆
        paint.setAntiAlias(true);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, (getWidth() - borderWidth) / 2.0f, paint);
    }
}
