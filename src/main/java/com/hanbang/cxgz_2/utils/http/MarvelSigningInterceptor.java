package com.hanbang.cxgz_2.utils.http;

import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.mode.javabean.base.UserData;
import com.hanbang.cxgz_2.utils.other.AESEncrypt;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by likun on 2016/8/3.
 * 添加一些公共的参数
 */

public class MarvelSigningInterceptor implements Interceptor {

    public String[] noAction = new String[]{"LoginCode", "RegisterCode", "Register", "Login"};

    public MarvelSigningInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        //获取请求
        Request oldRequest = chain.request();
        // 添加新的参数

        //添加真实帐号与密码

        String Telphone = "";
        String SuccessID = "";
        String telphoneSystem = "0";
        if (UserData.getUserData() != null && UserData.getUserData().isLogin()) {
            try {
                Telphone = AESEncrypt.getInstance().encrypt(UserData.userData.getTelphone());
                SuccessID = AESEncrypt.getInstance().encrypt(UserData.userData.getSuccessID());
            } catch (Exception e) {
                e.printStackTrace();
            }

            //添加默认帐号与密码
        } else {
            try {
                Telphone = AESEncrypt.getInstance().encrypt(Global.DEFAULT_PHONE);
                SuccessID = AESEncrypt.getInstance().encrypt(Global.DEFAULT_PASSWORD);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                .newBuilder()
                .scheme(oldRequest.url().scheme())
                .host(oldRequest.url().host());
        String ac = oldRequest.url().encodedQuery();
        boolean isAddPublic = true;
        for (String action : noAction) {
            if (ac.contains(action)) {
                isAddPublic = false;
            }
        }
        RequestBody requestBody = oldRequest.body();
        if (isAddPublic) {

            if (oldRequest.body() instanceof FormBody) {
                FormBody.Builder newFormBody = new FormBody.Builder();
                FormBody oidFormBody = (FormBody) oldRequest.body();
                for (int i = 0; i < oidFormBody.size(); i++) {
                    newFormBody.addEncoded(oidFormBody.encodedName(i), oidFormBody.encodedValue(i));
                }
                newFormBody.addEncoded("Telphone", Telphone);
                newFormBody.addEncoded("SuccessID", SuccessID);
                newFormBody.addEncoded("telphoneSystem", telphoneSystem);
                requestBody = newFormBody.build();
            } else if (oldRequest.body() instanceof MultipartBody) {

                MultipartBody multipartBody = (MultipartBody) oldRequest.body();

                MultipartBody.Builder newMultipartBody = new MultipartBody.Builder(multipartBody.boundary());
                for (int i = 0; i < multipartBody.size(); i++) {
                    newMultipartBody.addPart(multipartBody.part(i));
                }
                newMultipartBody.addFormDataPart("Telphone", Telphone);
                newMultipartBody.addFormDataPart("SuccessID", SuccessID);
                newMultipartBody.addFormDataPart("telphoneSystem", telphoneSystem);
                newMultipartBody.setType(multipartBody.type());
                requestBody = newMultipartBody.build();
            } else if (oldRequest.body() instanceof RequestBody) {
                requestBody = new FormBody.Builder()
                        .add("Telphone", Telphone)
                        .add("SuccessID", SuccessID)
                        .add("telphoneSystem", telphoneSystem)
                        .build();
            }
        }


        // 新的请求
        Request newRequest = oldRequest.newBuilder()
                .method(oldRequest.method(), requestBody)
                .url(authorizedUrlBuilder.build())
                .build();
        return chain.proceed(newRequest);
    }
}
