package com.hanbang.cxgz_2.utils.http;

import com.hanbang.cxgz_2.appliction.base.presenter.BasePresenter;
import com.hanbang.cxgz_2.appliction.iview.common_view.IProgressView;
import com.hanbang.cxgz_2.utils.other.LogUtils;

/**
 * Created by Administrator on 2016/8/9.
 */

public abstract class HttpProgressCallBack<ResultType> extends HttpCallBack<ResultType> implements ProgressCallBack {
    //下载或者上传 回调的频率  ms
    public long rate = 500;

    protected boolean isShowProgress = true;

    public HttpProgressCallBack(Buider buider) {
        super(buider);
        isShowProgress = buider.isShowProgress;
    }

    public void onStart(boolean isCompress) {
        LogUtils.e("onStart", "onStart2");
        super.onStart();

        onLoading(0, 100, false, true, isCompress);
    }

    @Override
    public void onStart() {
        onStart(false);
        LogUtils.e("onStart", "onStart");
    }

    @Override
    public long getRate() {
        return rate;
    }

    @Override
    public void setRate(long rate) {
        this.rate = rate;
    }


    @Override
    protected void dismissUi() {
        super.dismissUi();
        if (isShowProgress && basePresenter != null && basePresenter.mvpView != null
                && basePresenter.mvpView instanceof IProgressView) {
            ((IProgressView) basePresenter.mvpView).dismissProgressDialog();
        }
        LogUtils.e("dismissUi", "dismissUi");
    }

    @Override
    public void onLoading(long progress, long total, boolean done, boolean isUpdate) {
        LogUtils.e("onLoading", "onLoading1");

        onLoading(progress, total, done, isUpdate, false);
    }


    public void onLoading(long progress, long total, boolean done, boolean isUpdate, boolean isCompress) {
        if (done) {
            dismissUi();
            return;
        }
        LogUtils.e("onLoading", "onLoading2");
        if (basePresenter != null && basePresenter.mvpView != null
                && basePresenter.mvpView instanceof IProgressView) {
            int percentage = (int) (progress * 100.0 / total);
            ((IProgressView) basePresenter.mvpView).upLoading(percentage, done, isUpdate, isCompress);
        }
    }

    public static class Buider extends HttpCallBack.Buider {
        protected boolean isShowProgress = true;

        public Buider(BasePresenter basePresenter) {
            super(basePresenter);
        }

        public void setShowProgress(boolean showProgress) {
            isShowProgress = showProgress;
        }


    }
}
