package com.hanbang.cxgz_2.utils.animator;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.view.View;

import com.hanbang.cxgz_2.utils.http.PagingHelp;
import com.hanbang.cxgz_2.utils.other.ObjectUtils;

import java.util.List;

/**
 * 作者　　: 李坤
 * 创建时间:2016/9/2　13:38
 * 邮箱　　：496546144@qq.com
 * <p>
 * 功能介绍：
 */

public class AnimUtils {
    /*
     * listview item 3d delete
	 */

    public static void deleteItemAnimationRotationX(final View item
            , final int position,
                                                    final PagingHelp pagingHelp, int duration, final OnItemDeleteListener onItemDeleteListener) {
        final int originHeight = item.getMeasuredHeight();
        final int originWidth = item.getMeasuredWidth();
        ObjectAnimator alpha = ObjectAnimator.ofFloat(item, "alpha", 0.5f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(item, "rotationX", -80);
        item.setPivotX(originWidth / 2);
        scaleY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator value) {
                float cVal = (float) value.getCurrentPlayTime()
                        / value.getDuration();
                item.getLayoutParams().height = (int) (originHeight - originHeight
                        * cVal);
                item.requestLayout();

            }
        });
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(alpha).with(scaleY);
        animatorSet.setDuration(duration);
        animatorSet.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator arg0) {

                item.setRotationX(0);
                item.setAlpha(1);
                item.setPivotX(0);
                if (pagingHelp != null) {
                    pagingHelp.deleteOneItem();
                }
                onItemDeleteListener.onDelete();

            }

        });
        animatorSet.start();
    }

    public static void deleteItemAnimationRotationX(final View item, final int position, int duration, final OnItemDeleteListener onItemDeleteListener) {
        deleteItemAnimationRotationX(item, position, null, duration, onItemDeleteListener);
    }

    public static void deleteItemAnimationRotationX(final View item,
                                                    final List<?> listDatas, final int position, final OnItemDeleteListener onItemDeleteListener) {
        deleteItemAnimationRotationX(item, position, null, 400, onItemDeleteListener);
    }

    /*
     * 上下抖动动画，用于提醒用户去点击 rotation:转动角度,
     */
    public static void shakeUp(View view, float scaleMax, float rotation) {
        getShakeUp(view, scaleMax, rotation).start();

    }

    public static ObjectAnimator getShakeUp(View view, float scaleMax, float rotation) {
        // Keyframe是一个时间/值对，用于定义在某个时刻动画的状态
        // 在不同时间段的X轴0.8-1.1的缩放
        PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofKeyframe(
                "scaleX", Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(0.1f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.2f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.3f, scaleMax),
                Keyframe.ofFloat(0.4f, scaleMax),
                Keyframe.ofFloat(0.5f, scaleMax),
                Keyframe.ofFloat(0.6f, scaleMax),
                Keyframe.ofFloat(0.7f, scaleMax),
                Keyframe.ofFloat(0.8f, scaleMax),
                Keyframe.ofFloat(0.9f, scaleMax), Keyframe.ofFloat(1f, 1f));
        // 在不同时间段的Y轴0.8-1.1的缩放
        PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofKeyframe(
                "scaleY", Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(0.1f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.2f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.3f, scaleMax),
                Keyframe.ofFloat(0.4f, scaleMax),
                Keyframe.ofFloat(0.5f, scaleMax),
                Keyframe.ofFloat(0.6f, scaleMax),
                Keyframe.ofFloat(0.7f, scaleMax),
                Keyframe.ofFloat(0.8f, scaleMax),
                Keyframe.ofFloat(0.9f, scaleMax), Keyframe.ofFloat(1f, 1f));

        // 在不同时间段的旋转 旋转角度 = 0.3*抖动系数
        PropertyValuesHolder pvhRotation = PropertyValuesHolder.ofKeyframe(
                "rotation", Keyframe.ofFloat(0f, 0f),
                Keyframe.ofFloat(0.1f, rotation),
                Keyframe.ofFloat(0.2f, -rotation),
                Keyframe.ofFloat(0.3f, rotation),
                Keyframe.ofFloat(0.4f, -rotation),
                Keyframe.ofFloat(0.5f, rotation),
                Keyframe.ofFloat(0.6f, -rotation),
                Keyframe.ofFloat(0.7f, rotation),
                Keyframe.ofFloat(0.8f, -rotation),
                Keyframe.ofFloat(0.9f, rotation), Keyframe.ofFloat(1f, 0f));
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(
                view, pvhScaleX, pvhScaleY, pvhRotation).setDuration(1000);
        return objectAnimator;
    }

    /*
     * 左右抖动动画，用于表单验证失败 translation:平移距离, 0.85, 6
     */
    public static void shakeLeft(View view, float scaleMax, float translation) {
        translation = ObjectUtils.dip2px(view.getContext(), translation);
        // Keyframe是一个时间/值对，用于定义在某个时刻动画的状态
        // 在不同时间段的X轴0.8-1.1的缩放
        PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofKeyframe(
                "scaleX", Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(0.1f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.2f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.3f, scaleMax),
                Keyframe.ofFloat(0.4f, scaleMax),
                Keyframe.ofFloat(0.5f, scaleMax),
                Keyframe.ofFloat(0.6f, scaleMax),
                Keyframe.ofFloat(0.7f, scaleMax),
                Keyframe.ofFloat(0.8f, scaleMax),
                Keyframe.ofFloat(0.9f, scaleMax), Keyframe.ofFloat(1f, 1f));
        // 在不同时间段的Y轴0.8-1.1的缩放
        PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofKeyframe(
                "scaleY", Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(0.1f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.2f, scaleMax - 0.3f),
                Keyframe.ofFloat(0.3f, scaleMax),
                Keyframe.ofFloat(0.4f, scaleMax),
                Keyframe.ofFloat(0.5f, scaleMax),
                Keyframe.ofFloat(0.6f, scaleMax),
                Keyframe.ofFloat(0.7f, scaleMax),
                Keyframe.ofFloat(0.8f, scaleMax),
                Keyframe.ofFloat(0.9f, scaleMax), Keyframe.ofFloat(1f, 1f));

        // 在不同时间
        PropertyValuesHolder pvhRotation = PropertyValuesHolder.ofKeyframe(
                "translationX", Keyframe.ofFloat(0f, 0f),
                Keyframe.ofFloat(0.1f, translation),
                Keyframe.ofFloat(0.2f, -translation),
                Keyframe.ofFloat(0.3f, translation),
                Keyframe.ofFloat(0.4f, -translation),
                Keyframe.ofFloat(0.5f, translation),
                Keyframe.ofFloat(0.6f, -translation),
                Keyframe.ofFloat(0.7f, translation),
                Keyframe.ofFloat(0.8f, -translation),
                Keyframe.ofFloat(0.9f, translation), Keyframe.ofFloat(1f, 0f));
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(
                view, pvhScaleX, pvhScaleY, pvhRotation).setDuration(1000);
        objectAnimator.start();

    }

    /**
     * 缩放动画，先放大，后缩小，   用于提醒用户状态的变化
     */
    public static void scaleAnim(View view, float scaleMax, float scaleMin, int duration) {

        PropertyValuesHolder scx = PropertyValuesHolder.ofFloat("scaleX", 1, scaleMax, 1, scaleMin, 1);
        PropertyValuesHolder scY = PropertyValuesHolder.ofFloat("scaleY", 1, scaleMax, 1, scaleMin, 1);

        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(
                view, scx, scY).setDuration(duration);
        objectAnimator.start();


    }

    public static void scaleAnim(View view) {
        scaleAnim(view, 1.3f, 0.5f, 800);
    }


    /**
     * 作者　　: 李坤
     * 创建时间: 2016/9/2 16:33
     * <p>
     * 方法功能：X轴翻转
     */

    public static void rotationAnim(View view, float start, float end, int duration) {

        PropertyValuesHolder scx = PropertyValuesHolder.ofFloat("rotationY", start, end);
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(
                view, scx).setDuration(duration);
        objectAnimator.start();


    }

    public static void rotationAnim(View view) {
        rotationAnim(view, 180, 360, 800);
    }
}
