/*
 * @(#)DateUtils.java    Created on 2004-10-20
 * Copyright (c) 2005 ZDSoft Networks, Inc. All rights reserved.
 * $Id: DateUtils.java 34780 2013-02-17 10:43:59Z xuan $
 */
package com.hanbang.cxgz_2.utils.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 处理日期时间的工具类
 *
 * @author xuan
 * @version $Revision: 1.0 $, $Date: 2013-3-25 上午9:22:45 $
 */
@SuppressLint("SimpleDateFormat")
public abstract class DateUtils {

    public final static String TIME_00_00_00 = "00:00:00";

    private static final int[] DAY_OF_MONTH = new int[]{31, 28, 31, 30, 31,
            30, 31, 31, 30, 31, 30, 31};

    @SuppressLint("SimpleDateFormat")
    public static String dateToWeek(int position) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        Date currentDate = new Date();
        Date fdate;
        List<String> list = new ArrayList<String>();
        Long fTime = currentDate.getTime();
        for (int a = 0; a < 7; a++) {
            fdate = new Date();
            fdate.setTime(fTime + (a * 24 * 3600000));
            list.add(sdf.format(fdate));
        }
        return list.get(position);
    }

    /**
     * @return
     */
    public static String getCurrentTime() {
        return getFormatDateTime(new Date(), "yyyy-MM-dd");
    }

    public static String getFormatDateTime(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        return sdf.format(date);
    }

    public static String getLocalTime(Context context, String time) {
        // 取出年月日来，比较字符串即可
        String str_curTime = DateFormat.format("yyyy-MM-dd", new Date())
                .toString();
        int result = str_curTime
                .compareTo(time.substring(0, time.indexOf(" ")));
        if (result > 0) {
            return "昨天"
                    + time.substring(time.indexOf(" "), time.lastIndexOf(":"));
        } else if (result == 0) {
            return "今天"
                    + time.substring(time.indexOf(" "), time.lastIndexOf(":"));
        } else {
            return time;
        }
    }

    /**
     * 取得指定天数后的时间
     *
     * @param date      基准时间
     * @param dayAmount 指定天数，允许为负数
     * @return 指定天数后的时间
     */
    public static Date addDay(Date date, int dayAmount) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, dayAmount);
        return calendar.getTime();
    }

    /**
     * 取得指定小时数后的时间
     *
     * @param date       基准时间
     * @param hourAmount 指定小时数，允许为负数
     * @return 指定小时数后的时间
     */
    public static Date addHour(Date date, int hourAmount) {
        if (date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hourAmount);
        return calendar.getTime();
    }

    /**
     * 取得指定分钟数后的时间
     *
     * @param date         基准时间
     * @param minuteAmount 指定分钟数，允许为负数
     * @return 指定分钟数后的时间
     */
    public static Date addMinute(Date date, int minuteAmount) {
        if (date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minuteAmount);
        return calendar.getTime();
    }

    /**
     * 比较两日期对象中的小时和分钟部分的大小.
     *
     * @param date        日期对象1, 如果为 <code>null</code> 会以当前时间的日期对象代替
     * @param anotherDate 日期对象2, 如果为 <code>null</code> 会以当前时间的日期对象代替
     * @return 如果日期对象1大于日期对象2, 则返回大于0的数; 反之返回小于0的数; 如果两日期对象相等, 则返回0.
     */
    public static int compareHourAndMinute(Date date, Date anotherDate) {
        if (date == null) {
            date = new Date();
        }

        if (anotherDate == null) {
            anotherDate = new Date();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hourOfDay1 = cal.get(Calendar.HOUR_OF_DAY);
        int minute1 = cal.get(Calendar.MINUTE);

        cal.setTime(anotherDate);
        int hourOfDay2 = cal.get(Calendar.HOUR_OF_DAY);
        int minute2 = cal.get(Calendar.MINUTE);

        if (hourOfDay1 > hourOfDay2) {
            return 1;
        } else if (hourOfDay1 == hourOfDay2) {
            // 小时相等就比较分钟
            return minute1 > minute2 ? 1 : (minute1 == minute2 ? 0 : -1);
        } else {
            return -1;
        }
    }

    /**
     * 比较两日期对象的大小, 忽略秒, 只精确到分钟.
     *
     * @param date        日期对象1, 如果为 <code>null</code> 会以当前时间的日期对象代替
     * @param anotherDate 日期对象2, 如果为 <code>null</code> 会以当前时间的日期对象代替
     * @return 如果日期对象1大于日期对象2, 则返回大于0的数; 反之返回小于0的数; 如果两日期对象相等, 则返回0.
     */
    public static int compareIgnoreSecond(Date date, Date anotherDate) {
        if (date == null) {
            date = new Date();
        }

        if (anotherDate == null) {
            anotherDate = new Date();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        date = cal.getTime();

        cal.setTime(anotherDate);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        anotherDate = cal.getTime();

        return date.compareTo(anotherDate);
    }

    /**
     * 取得当前时间的字符串表示，格式为2006-01-10 20:56:30.756
     *
     * @return 当前时间的字符串表示
     */
    public static String currentDate2String() {
        return date2String(new Date());
    }

    /**
     * 取得当前时间的字符串表示，格式为2006-01-10
     *
     * @return 当前时间的字符串表示
     */
    public static String currentDate2StringByDay() {
        return date2StringByDay(new Date());
    }

    /**
     * 取得今天的最后一个时刻
     *
     * @return 今天的最后一个时刻
     */
    public static Date currentEndDate() {
        return getEndDate(new Date());
    }

    /**
     * 取得今天的第一个时刻
     *
     * @return 今天的第一个时刻
     */
    public static Date currentStartDate() {
        return getStartDate(new Date());
    }

    /**
     * 把时间转换成字符串，格式为2006-01-10 20:56:30.756
     *
     * @param date 时间
     * @return 时间字符串
     */
    public static String date2String(Date date) {
        return date2String(date, "yyyy-MM-dd HH:mm:ss.SSS");
    }

    /**
     * 按照指定格式把时间转换成字符串，格式的写法类似yyyy-MM-dd HH:mm:ss.SSS
     *
     * @param date    时间
     * @param pattern 格式
     * @return 时间字符串
     */
    @SuppressLint("SimpleDateFormat")
    public static String date2String(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        return (new SimpleDateFormat(pattern)).format(date);
    }

    /**
     * 把时间转换成字符串，格式为2006-01-10
     *
     * @param date 时间
     * @return 时间字符串
     */
    public static String date2StringByDay(Date date) {
        return date2String(date, "yyyy-MM-dd");
    }

    /**
     * 把时间转换成字符串，格式为2006-01-10 20:56
     *
     * @param date 时间
     * @return 时间字符串
     */
    public static String date2StringByMinute(Date date) {
        return date2String(date, "yyyy-MM-dd HH:mm");
    }

    /**
     * 把时间转换成字符串，格式为2006-01-10 20:56:30
     *
     * @param date 时间
     * @return 时间字符串
     */
    public static String date2StringBySecond(Date date) {
        return date2String(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 根据某星期几的英文名称来获取该星期几的中文数. <br>
     * e.g. <li>monday -> 一</li> <li>sunday -> 日</li>
     *
     * @param englishWeekName 星期的英文名称
     * @return 星期的中文数
     */
    public static String getChineseWeekNumber(String englishWeekName) {
        if ("monday".equalsIgnoreCase(englishWeekName)) {
            return "一";
        }

        if ("tuesday".equalsIgnoreCase(englishWeekName)) {
            return "二";
        }

        if ("wednesday".equalsIgnoreCase(englishWeekName)) {
            return "三";
        }

        if ("thursday".equalsIgnoreCase(englishWeekName)) {
            return "四";
        }

        if ("friday".equalsIgnoreCase(englishWeekName)) {
            return "五";
        }

        if ("saturday".equalsIgnoreCase(englishWeekName)) {
            return "六";
        }

        if ("sunday".equalsIgnoreCase(englishWeekName)) {
            return "日";
        }

        return null;
    }

    /**
     * 根据指定的年, 月, 日等参数获取日期对象.
     *
     * @param year  年
     * @param month 月
     * @param date  日
     * @return 对应的日期对象
     */
    public static Date getDate(int year, int month, int date) {
        return getDate(year, month, date, 0, 0);
    }

    /**
     * 根据指定的年, 月, 日, 时, 分等参数获取日期对象.
     *
     * @param year      年
     * @param month     月
     * @param date      日
     * @param hourOfDay 时(24小时制)
     * @param minute    分
     * @return 对应的日期对象
     */
    public static Date getDate(int year, int month, int date, int hourOfDay,
                               int minute) {
        return getDate(year, month, date, hourOfDay, minute, 0);
    }

    /**
     * 根据指定的年, 月, 日, 时, 分, 秒等参数获取日期对象.
     *
     * @param year      年
     * @param month     月
     * @param date      日
     * @param hourOfDay 时(24小时制)
     * @param minute    分
     * @param second    秒
     * @return 对应的日期对象
     */
    public static Date getDate(int year, int month, int date, int hourOfDay,
                               int minute, int second) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, date, hourOfDay, minute, second);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    /**
     * 取得某个日期是星期几，星期日是1，依此类推
     *
     * @param date 日期
     * @return 星期几
     */
    public static int getDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 获取某天的结束时间, e.g. 2005-10-01 23:59:59.999
     *
     * @param date 日期对象
     * @return 该天的结束时间
     */
    public static Date getEndDate(Date date) {

        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);

        return cal.getTime();
    }

    /**
     * 取得一个月最多的天数
     *
     * @param year  年份
     * @param month 月份，0表示1月，依此类推
     * @return 最多的天数
     */
    public static int getMaxDayOfMonth(int year, int month) {
        if (month == 1 && isLeapYear(year)) {
            return 29;
        }
        return DAY_OF_MONTH[month];
    }

    /**
     * 得到指定日期的下一天
     *
     * @param date 日期对象
     * @return 同一时间的下一天的日期对象
     */
    public static Date getNextDay(Date date) {
        return addDay(date, 1);
    }

    /**
     * 获取某天的起始时间, e.g. 2005-10-01 00:00:00.000
     *
     * @param date 日期对象
     * @return 该天的起始时间
     */
    public static Date getStartDate(Date date) {
        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    /**
     * 根据日期对象来获取日期中的时间(HH:mm:ss).
     *
     * @param date 日期对象
     * @return 时间字符串, 格式为: HH:mm:ss
     */
    @SuppressLint("SimpleDateFormat")
    public static String getTime(Date date) {
        if (date == null) {
            return null;
        }

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        return format.format(date);
    }

    /**
     * 根据日期对象来获取日期中的时间 指定格式的字符串
     *
     * @param date 日期对象
     * @return 时间字符串
     */
    @SuppressLint("SimpleDateFormat")
    public static String getTime(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    /**
     * 根据日期对象来获取日期中的时间(HH:mm).
     *
     * @param date 日期对象
     * @return 时间字符串, 格式为: HH:mm
     */
    @SuppressLint("SimpleDateFormat")
    public static String getTimeIgnoreSecond(Date date) {
        if (date == null) {
            return null;
        }

        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(date);
    }

    /**
     * 判断是否是闰年
     *
     * @param year 年份
     * @return 是true，否则false
     */
    public static boolean isLeapYear(int year) {
        Calendar calendar = Calendar.getInstance();
        return ((GregorianCalendar) calendar).isLeapYear(year);
    }

    /**
     * 把字符串转换成日期，格式为2006-01-10 20:56:30
     *
     * @param str 字符串
     * @return 日期
     */
    public static Date string2Date(String str) {
        return string2Date(str, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 按照指定的格式把字符串转换成时间，格式的写法类似yyyy-MM-dd HH:mm:ss.SSS
     *
     * @param str     字符串
     * @param pattern 格式
     * @return 时间
     */
    public static Date string2Date(String str, String pattern) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = dateFormat.parse(str);
        } catch (ParseException e) {
            // ignore
        }
        return date;
    }

    public static Calendar string2Calender(String str) {
        return string2Calender(str, "yyyy-MM-dd HH:mm:ss");
    }

    public static Calendar string2Calender(String str, String pattern) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = dateFormat.parse(str);
        } catch (ParseException e) {
            // ignore
        }
        if (date == null) {
            return null;
        }
        calendar.setTime(date);
        return calendar;
    }

    /**
     * 取得一年中的第几周。
     *
     * @param date
     * @return
     */
    public static int getWeekOfYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * 获取上周的指定星期的日期。
     *
     * @param dayOfWeek 星期几，取值范围是 {@link Calendar#MONDAY} - {@link Calendar#SUNDAY}
     */
    public static Date getDateOfPreviousWeek(int dayOfWeek) {
        if (dayOfWeek > 7 || dayOfWeek < 1) {
            throw new IllegalArgumentException("参数必须是1-7之间的数字");
        }

        return getDateOfRange(dayOfWeek, -7);
    }

    /**
     * 获取本周的指定星期的日期。
     *
     * @param dayOfWeek 星期几，取值范围是 {@link Calendar#MONDAY} - {@link Calendar#SUNDAY}
     */
    public static Date getDateOfCurrentWeek(int dayOfWeek) {
        if (dayOfWeek > 7 || dayOfWeek < 1) {
            throw new IllegalArgumentException("参数必须是1-7之间的数字");
        }

        return getDateOfRange(dayOfWeek, 0);
    }

    /**
     * 获取下周的指定星期的日期。
     *
     * @param dayOfWeek 星期几，取值范围是 {@link Calendar#MONDAY} - {@link Calendar#SUNDAY}
     */
    public static Date getDateOfNextWeek(int dayOfWeek) {
        if (dayOfWeek > 7 || dayOfWeek < 1) {
            throw new IllegalArgumentException("参数必须是1-7之间的数字");
        }

        return getDateOfRange(dayOfWeek, 7);
    }

    private static Date getDateOfRange(int dayOfWeek, int dayOfRange) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        cal.set(Calendar.DATE, cal.get(Calendar.DATE) + dayOfRange);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 获取星期
     *
     * @param date
     * @return
     */
    public static String getWeekOfDate(Date date) {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int w = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        return weekDays[w];
    }


    /**
     * 把字符串转换成日期，格式为2006-01-10   年月日
     *
     * @param str 字符串
     * @return 返回日期String
     */
    public static String string2String(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return getTime(string2Date(str, "yyyy-MM-dd"), "yyyy-MM-dd");
    }

    /**
     * 把字符串转换成日期,
     *
     * @param str 字符串, 必须是yyyy-MM-dd HH:mm:ss
     * @return 返回日期String
     */
    public static String string2String(String str, String pattern) {
        return getTime(string2Date(str), pattern);
    }

    /**
     * 把字符串转换成日期
     *
     * @param str 字符串  没有限制   和pattern一致就可以
     * @return 返回日期String
     */
    public static String string2String2(String str, String pattern) {
        return getTime(string2Date(str, pattern), pattern);
    }


    /**
     * \/Date(1461686400000)\/ 转换成指定格式的String "(?<=p[(])\d{4}(?=,\d,\d[)];)" "p(1003,7,9);p(1048,1,2);p(1009,2,2);"
     */
    public static String dateToString(String date, String pattern) {
        if (StringUtils.isEmpty(date)) return "";
        Pattern p = Pattern.compile("\\d{10,}");
        Matcher matcher = p.matcher(date);
        long time = 0;
        while (matcher.find()) { //注意这里，是while不是if
            time = Long.valueOf(matcher.group());
        }
        if (time == 0) {
            return "";
        }
        return getTime(new Date(time), pattern);
    }

    /**
     * \/Date(1461686400000)\/ 转换成指定格式的String "(?<=p[(])\d{4}(?=,\d,\d[)];)" "p(1003,7,9);p(1048,1,2);p(1009,2,2);"
     */
    public static Long dateToLong(String date) {
        if (StringUtils.isEmpty(date)) return 0l;
        Pattern p = Pattern.compile("\\d{10,}");
        Matcher matcher = p.matcher(date);
        long time = 0;
        while (matcher.find()) { //注意这里，是while不是if
            time = Long.valueOf(matcher.group());
        }
        if (time == 0) {
            return 0l;
        }
        return time;
    }

    /**
     * \/Date(1461686400000)\/ 转换成指定格式的 "(?<=p[(])\d{4}(?=,\d,\d[)];)" "p(1003,7,9);p(1048,1,2);p(1009,2,2);"
     */
    public static String dateToString(String date) {
        return dateToString(date, "yyyy-MM-dd");
    }


    public static int getDay(String date, String pattern) {
        Calendar calendar = Calendar.getInstance();
        Date d = string2Date(date, pattern);
        if (d == null) return 0;
        calendar.setTime(d);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static int getDay(String date) {
        return getDay(date, "yyyy-MM-dd");
    }

    public static int getMonth(String date, String pattern) {
        Calendar calendar = Calendar.getInstance();
        Date d = string2Date(date, pattern);
        if (d == null) return 0;
        calendar.setTime(d);
        return calendar.get(Calendar.MONTH) + 1;
    }

    public static int getMonth(String date) {
        return getMonth(date, "yyyy-MM-dd");
    }


    /**
     * 显示偷听的时间
     *
     * @param time
     * @return
     */
    public static String showEavesdropTime(int time) {
        try {
            if (time < 1) {
                return "";
            }
            return time / 60 + "'" + time % 60;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }


    /**
     * 传过来一个还long的字符串 根据时间的的长短返回对应的时间 1，不到1分钟返回刚刚 2，超过1分钟返回**分钟 3，不到1天返回**小时
     * 4，超过1天返回long 的日期
     *
     * @param text
     * @return
     */
    public static String getTime(String text) {
        try {


            // 1.把传过来的Date 中的数字取出来。2.然后设置时间格式。3.把一个long 数值转换成日字符串
            long time_long = dateToLong(text);

            Calendar currData = Calendar.getInstance();
            Calendar mubiaoData = Calendar.getInstance();
            mubiaoData.setTimeInMillis(time_long);

            if (currData.get(Calendar.DAY_OF_YEAR) > mubiaoData.get(Calendar.DAY_OF_YEAR) || currData.get(Calendar.YEAR) > mubiaoData.get(Calendar.YEAR)) {
                //今天之前
                return getTime(mubiaoData.getTime(), "yyyy-MM-dd");
            } else {
                //今天
                int caizhi = (int) ((currData.getTimeInMillis() - mubiaoData.getTimeInMillis()) / 1000.0);//秒
                if (caizhi <= 60) { //一分钟之内
                    return "刚刚";
                } else if (caizhi <= 60 * 60) { //一小时之内
                    return (int) (caizhi / 60.0) + "分钟前";
                } else {
                    return (int) (caizhi / 60.0 / 60.0) + "小时前";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;

    }
}
