package com.hanbang.cxgz_2.utils.other;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.SparseArray;

import com.hanbang.cxgz_2.appliction.MyApplication;
import com.hanbang.cxgz_2.R;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * 得到资源中的随机图片
 * Created by yang on 2016/7/1.
 */
public class RandomResourcesImageB {
    private SparseArray<Integer> mImages;
    private int index = 0;
    private Context context;

    public RandomResourcesImageB(Context context) {
        mImages = new SparseArray<>();
        this.context = context;
        addim();
    }

    /**
     * 单个添加资源图片
     *
     * @param imageId
     */
    public void setImage(int imageId) {
        mImages.put(index, imageId);
        index++;
    }

    /**
     * 多个添加资源图片
     *
     * @param imageId
     */
    public void setImage(int... imageId) {

        for (int temp : imageId) {
            mImages.put(index, temp);
            index++;
        }

    }


    /**
     * 获取随机资源图片
     * 返回一个.jpg文件
     *
     * @return
     */
    public File getRandomImageFile() {
        if (mImages == null || mImages.size() == 0) {
            LogUtils.e("没添加资源图片");
            return null;
        }
        try {
            int random = getRandom(mImages.size(), 0);
            //把资源图片转bipmap对象
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), mImages.get(random));

            //如果有储存卡，存在在卡上
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                return getFileFromBytes(getBitmapByte(bitmap), MyApplication.appSDCachePath);
                //没有储存卡，存在手机内部储存空间
            } else {
                return getFileFromBytes(getBitmapByte(bitmap), MyApplication.appFilePath);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 将图片内容解析成字节数组
     *
     * @throws Exception
     */
    public byte[] getBitmapByte(Bitmap bitmap) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //参数1转换类型，参数2压缩质量，参数3字节流资源
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        try {
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }


    /**
     * 把字节数组保存为一个文件
     */
    public static File getFileFromBytes(byte[] b, String outputFile) {
        BufferedOutputStream stream = null;
        File file = null;
        File file1 = null;
        try {
            file = new File(outputFile);
            if (file.exists() || file.mkdirs()) {
                file1 = new File(file, "123456.jpg");
                file1.createNewFile();
                FileOutputStream fstream = new FileOutputStream(file1);
                stream = new BufferedOutputStream(fstream);
            }
            stream.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return file1;
    }

    /**
     * 添加默认资源图片
     */
    public void addim() {
        mImages.put(0, R.mipmap.potrait_1);
        mImages.put(1, R.mipmap.potrait_2);
        mImages.put(2, R.mipmap.potrait_3);
        mImages.put(3, R.mipmap.potrait_4);
        mImages.put(4, R.mipmap.potrait_5);
        mImages.put(5, R.mipmap.potrait_6);
        mImages.put(6, R.mipmap.potrait_7);
        index = 6;
    }


    /**
     * 集合中的整数，注意不包括最大值本身，所以要加1
     *
     * @param max 随机数的最大值
     * @param min 随机数的最小值
     * @return
     */
    public static int getRandom(int max, int min) {
        if (max <= min) {
            return 0;
        }
        Random random = new Random();
        int result = random.nextInt(max - min + 1);
        return result + min;
    }

}
