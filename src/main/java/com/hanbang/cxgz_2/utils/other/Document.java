package com.hanbang.cxgz_2.utils.other;


/**
 * 结构与帮助文档
 * Created by yang on 2016/7/5.
 * ￥
 * 1，不能以数字开头
 * 2，不能包含中文
 * 3，不能包含‘-’
 * 4，可以用'_'
 * 5，其它符号都不能用
 * 6,不能有大写字母
 */
public class Document {


    //==============================================================================appliction   (应用包)
    private void appliction() {
        /**
         *  {@link MyApplication}                           (应用的配置)
         */
    }

    //==============================================================================mode        (业务包)
    private void mode() {
        /**
         *  {@link ***}                                    (***)
         */

        String httpresponse = "请求响应的实体数据";

    }

    //==============================================================================pressenter  (任命者包)
    private void pressenter() {
        /**
         *  {@link ***}                                     (***)
         */
    }

    //==============================================================================utils       (工具包)
    private void utils() {
        /**
         *  {@link ***}                                     (***)
         */

        String http = "请求工具与请求业务";
        /**
         *  {@link ServiceApi}                              (请求接口的定义)
         *  {@link HttpRequest}                             (请求方法)
         *  {@link HttpManager}                             (http请求管理类)
         */

    }

    //==============================================================================view        (view显示包)
    private void view() {
        /**
         *  {@link ***}                                     (***)
         */
    }


    /**
     *   MultiImageSelector.create()
     .showCamera(true) // show camera or not. true by default
     .count(9) // max select image size, 9 by default. used width #.multi()
     .multi() // multi mode, default mode;
     .origin(picturePath) // original select data set, used width #.multi()
     .start(this, activity.REQUEST_CODE);
     */

}
