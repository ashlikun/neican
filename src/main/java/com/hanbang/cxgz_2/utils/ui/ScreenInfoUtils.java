package com.hanbang.cxgz_2.utils.ui;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import static com.hanbang.cxgz_2.appliction.MyApplication.myApp;

/**
 * �õ���Ļ����ܶȵ�
 *
 * @author
 * @email chenshi011@163.com
 */
public class ScreenInfoUtils {
    /**
     */
    private int width;
    /**
     */
    private int height;
    /**
     */
    private float density;
    /**
     */
    private int densityDpi;


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public int getDensityDpi() {
        return densityDpi;
    }

    public void setDensityDpi(int densityDpi) {
        this.densityDpi = densityDpi;
    }

    public ScreenInfoUtils() {
        ini();
    }

    private void ini() {
        DisplayMetrics metric = new DisplayMetrics();
        WindowManager wm = (WindowManager) myApp.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metric);
        width = metric.widthPixels;
        height = metric.heightPixels;
        density = metric.density;
        densityDpi = metric.densityDpi;
    }
}
