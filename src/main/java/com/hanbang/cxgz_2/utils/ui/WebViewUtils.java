package com.hanbang.cxgz_2.utils.ui;

import android.annotation.SuppressLint;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.hanbang.cxgz_2.utils.http.HttpManager;
import com.hanbang.cxgz_2.utils.other.StringUtils;


/**
 * Created by Administrator on 2016/1/20.
 */
public class WebViewUtils {

    /*
    * 获取到服务器数据后开始跟新UI
    */
    public static void loadDataWithBaseURL(WebView webView, String content) {
        if (content == null) {
            content = "";
        }
        content = StringUtils.decodeUnicode(content);
        // 配置webview数据源
        StringBuilder data = new StringBuilder();
        data.append("<style type=\"text/css\">img{WIDTH:100% !important;HEIGHT:auto !important;}");
        data.append(getFont());
        data.append("</style><base href=\"");
        data.append(HttpManager.BASE_URL);
        data.append("\"/><body class=\"Likun\" style=\"padding-left:0%;padding-right:0%;\">");
        data.append(content);
        data.append("</body>");
        // 设置webview 加载数据
        webView.loadDataWithBaseURL(null, data.toString(), "text/html",
                "utf-8", null);
    }

    public static String getFont() {
        StringBuilder builder = new StringBuilder();
        // <body class="MsoNormal"> </body>
        builder.append(".Likun { font-family: fzlt;}");
        builder.append("@font-face {font-family:fzlt;");
        builder.append("src:url(file:///android_asset/fzlt.ttf);}");
        return builder.toString();
    }

    @SuppressLint("SetJavaScriptEnabled")
    public static void configWebview(WebView webView) {
        // 设置文本编码
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        // 可以缩放
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        // 开启 Application Caches 功能
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setLoadWithOverviewMode(true);
        // webView.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);// 默认缩放模式
        webView.getSettings().setUseWideViewPort(false);
    }
}
