package com.hanbang.cxgz_2.utils.shared;

import android.content.Intent;

import com.hanbang.cxgz_2.appliction.Global;
import com.hanbang.cxgz_2.appliction.base.view.activity.BaseActivity;
import com.hanbang.cxgz_2.mode.javabean.other.SharedData;
import com.hanbang.cxgz_2.utils.other.LogUtils;
import com.hanbang.cxgz_2.utils.ui.SnackbarUtil;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.ShareContentCustomizeCallback;

/**
 * 分享的方法
 * Created by yang on 2016/2/16.
 */
public class SharedUtil {


    public abstract class OnSharedComplete {
        abstract void onComplete(SharedData sharedData);

        public void onError(SharedData sharedData) {

        }

        public void onCancel(SharedData sharedData) {

        }
    }

    private static void shered(final BaseActivity context, final SharedData sharedData) {
        shered(context, sharedData, null);
    }

    /**
     * @param context
     * @param sharedData
     */
    private static void shered(final BaseActivity context, final SharedData sharedData, final OnSharedComplete onSharedComplete) {
        OnekeyShare oks = new OnekeyShare();
        // 关闭sso授权
        oks.disableSSOWhenAuthorize();
        // 参考代码配置章节，设置分享参数
        // 通过OneKeyShareCallback来修改不同平台分享的内容
        oks.setShareContentCustomizeCallback(new ShareContentCustomizeCallback() {

            @Override
            public void onShare(Platform platform, Platform.ShareParams paramsToShare) {
                switch (platform.getName()) {

                    case "QQ":
                    case "QZone":
                        paramsToShare.setTitle(sharedData.getTitle());
                        paramsToShare.setText(sharedData.getContent());
                        paramsToShare.setTitleUrl(sharedData.getClickUrl());
                        paramsToShare.setImageUrl(sharedData.getImageUrl());
                        paramsToShare.setShareType(Platform.SHARE_WEBPAGE);
                        break;
                    case "Wechat":
                    case "WechatFavorite":
                    case "WechatMoments":
                        paramsToShare.setTitle(sharedData.getTitle());
                        paramsToShare.setText(sharedData.getContent());
                        paramsToShare.setImageUrl(sharedData.getImageUrl());
                        paramsToShare.setUrl(sharedData.getClickUrl());
                        paramsToShare.setShareType(Platform.SHARE_WEBPAGE);
                        break;
                    default:
                        break;

                    case "SinaWeibo":
                        paramsToShare.setText(sharedData.getTitle() + sharedData.getClickUrl());
                        paramsToShare.setImageUrl(sharedData.getImageUrl());
                        paramsToShare.setShareType(Platform.SHARE_WEBPAGE);
                        break;
                }

            }
        });

        //分享成功的回调
        oks.setCallback(new PlatformActionListener() {

            @Override
            public void onError(Platform arg0, int arg1, Throwable arg2) {
                LogUtils.e("PlatformActionListener", "onError   " + arg1);
                if (onSharedComplete != null) {
                    onSharedComplete.onError(sharedData);
                }
            }

            @Override
            public void onComplete(Platform arg0, int arg1, HashMap<String, Object> arg2) {
                context.showSnackbar("分享成功", SnackbarUtil.Info, false);
                //发送成功广播
                Intent intent = new Intent();
                intent.setAction(Global.SHARED);
                intent.putExtra(Global.SHARED, sharedData);
                intent.putExtra("id", sharedData.getId());
                context.sendBroadcast(intent);
                if (onSharedComplete != null) {
                    onSharedComplete.onComplete(sharedData);
                }

            }

            @Override
            public void onCancel(Platform arg0, int arg1) {
                LogUtils.e("PlatformActionListener", "onCancel   " + arg1);
                if (onSharedComplete != null) {
                    onSharedComplete.onCancel(sharedData);
                }
            }
        });
        oks.show(context);
    }


}
